#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)

class CCloset final : public CGameObject
{
public:
	enum STATE { DoorFull, Hatchet_Reload, DoorUnhideFast, DoorUnhide, DoorSlasherPickCamper, DoorOpenEmpty, DoorIdleClosed, DoorHideFast, DoorHide};
private:
	explicit CCloset(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCloset(const CCloset& rhs);
	virtual ~CCloset() = default;
public:   
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
	void SetState(STATE eState) { m_CurState = eState; m_iCurAnimation = eState; State_Check(); m_IsReciveServer = false; }
private:
	void State_Check();
	void Update_Sound();
	void ComunicateWithServer();
	void SetUp_Sound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundValue = 1.f, const _int& OtherOption = 0);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom =  nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	wstring m_Key;
	_int	m_iOtherOption = 0;
	STATE	m_OldState = DoorIdleClosed;
	STATE	m_CurState = DoorIdleClosed;
	_int	m_SoundCheck = 0;
	_float	m_fDeltaTime = 0.f;
	_bool	m_IsReciveServer = true;
	list<SoundData>	m_SoundList;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CCloset* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END