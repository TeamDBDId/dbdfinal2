#pragma once
#include "GameObject.h"

_BEGIN(Client)
class CCamper;
class CSlasher;
class CUI_Texture;
class CUI_Perk : public CGameObject
{
public:
	typedef struct tagPerk
	{
		_uint TagNum;
		wstring TagTexture;
	}PERKINFO;
private:
	explicit CUI_Perk(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Perk(const CUI_Perk& rhs);
	virtual ~CUI_Perk() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	void	Set_Num(const _int& CurNum);
	void	Is_AllTextureRender(_bool IsRendering);
private:
	void	SetUp_PerkBase();
	void	SetUp_CurPerk(const _int& Num);
	void	SetUp_PerkInfo();
	void	Update_PerkCoolTime();
	void	LateInit();
	void	Player_Check();
private:
	CUI_Texture* Find_UITexture(wstring UIName);
	void Delete_UI_Texture(wstring UIName, _bool IsDead = false);
	void Delete_AllPerk();
private:
	map<wstring, CUI_Texture*>	m_UITexture;
	CCamper*					m_pCamper = nullptr;
	CSlasher*					m_pSlasher = nullptr;
	PERKINFO					m_CurPerk[4];
	_bool						m_bLateInit = false;
	_int						m_PlayerNum = 0;
	_int						m_iPerk = 0;
	_int						m_CurNum = 0;
	vector<PERKINFO>			m_vecCamperPerk;
	vector<PERKINFO>			m_vecSlasherPerk;
public:
	static CUI_Perk* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END