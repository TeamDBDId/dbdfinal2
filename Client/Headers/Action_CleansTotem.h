#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CTotem;
class CAction_CleansTotem final : public CAction
{
public:
	explicit CAction_CleansTotem();
	virtual ~CAction_CleansTotem() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetTotem(CGameObject* pTotem) { m_pTotem = (CTotem*)pTotem; }
private:
	_float m_fIndex = 0.f;
	_int m_iState = 0;
	CTotem* m_pTotem = nullptr;
protected:
	virtual void Free();
};

_END