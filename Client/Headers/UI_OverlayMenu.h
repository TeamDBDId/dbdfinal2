#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_OverlayMenu : public CGameObject
{
public:
	typedef struct tagPerk
	{
		_uint	TagNum;
		wstring TagTexture;
		wstring MainText;
		wstring SubText;
	}PERKINFO;
	enum ButtonState { Touch, UnTouch, Click };
	enum Lobby2Button { NONE, BACK, READY, CHANGE_JOB, CHANGE_CHARICTER, CHANGE_PERK, MAINPERK, INVENPERK, CHARICTOR};
	enum LobbyState{ None, Go_Back, Ready, Change_To_Camper, Change_To_Slasher};
private:
	explicit CUI_OverlayMenu(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_OverlayMenu(const CUI_OverlayMenu& rhs);
	virtual ~CUI_OverlayMenu() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);

	_uint	Get_State() { _uint State = m_iClickButton; m_iClickButton = None;  return State; }
	void	Set_Job(_bool Job) { m_JobState = Job; m_OldJobState = !m_JobState; }
	_bool	Get_Job() { return m_JobState; }
private:
	void Set_Edge();
	void SetUp_Inventory();
	void SetUp_PerkInfo();
	void SetUp_Change_Charicter();
	void SetUp_Perk();
	void Ready_Check();
	void Mouse_Check();
	void Lobby_Check();
	void Server_Check();

	void Set_SideButton();
	void Set_StartIcon();
	void Set_OtherButton();
	void Set_Background();
	void Set_CurPerk();

	void	Update_Job();
	_bool	Set_Camper();
	_bool	Set_Slasher();

	void Button_L2_Change_Job(ButtonState State);
	void Button_L2_Charictor(ButtonState State);
	void Button_L2_Change_Charicter(ButtonState State);
	void Button_L2_Change_Perk(ButtonState State);
	void Button_L2_Main_Perk(ButtonState State);
	void Button_L2_Inven_Perk(ButtonState State);
	void Button_L2_Go_Back(ButtonState State);
	void Button_L2_Ready(ButtonState State);

	void Add_Explain(wstring CharictorName, wstring CharictorInfo, _vec2 vPosition);

	CUI_Texture* Find_UITexture(wstring UIName);
	void Delete_CharictorPage();
	void Delete_Inventory();
	void Delete_Perk();
	void Delete_UI_Texture(wstring UIName, _bool IsDead = false);
	void Delete_All();
	void Ready_Ready();
	void Compute_CurPerk();
	void Delete_Explain();
	vector<PERKINFO> m_vecCamperPerk;
	vector<PERKINFO> m_vecSlasherPerk;
private:
	_bool			m_IsClone = false;
	map<wstring, CUI_Texture*> m_UITexture;

	_uint			m_iClickButton = NONE;
	_bool			m_JobState = false;
	_bool			m_OldJobState = false;
	
	_uint			m_CurButton = NONE;
	_uint			m_OldButton = NONE;

	_uint			m_iMaxPlayer = 1;
	_uint			m_iCurReadyPlayer = 0;

	_bool			m_IsReady = false;
	_int			m_WaitingTime = 6;
	
	_uint			m_CurUI = NONE;
	_uint			m_OldButtonIndex = 0;
	_uint			m_CurButtonIndex = 0;
	_uint			m_CurPerk = 0;

	vector<_int>*	m_vecEquippedPerk;
	_int*			m_CurCamper = nullptr;
	_int*			m_CurSlasher = nullptr;
public:
	static CUI_OverlayMenu* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END