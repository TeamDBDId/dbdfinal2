#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CAction_CrawlToHit final : public CAction
{
public:
	explicit CAction_CrawlToHit();
	virtual ~CAction_CrawlToHit() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
private:
	_bool m_bIsInit = false;
	_vec3 m_vOriginPos;
	vector<_vec3> m_vecPosBK;
	vector<_vec3> m_vecPosFT;
	_float m_fIndex = 0.f;
	_int m_iState = 0;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END