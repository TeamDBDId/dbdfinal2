#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)

class CExitDoorLight;
class CExitDoor final : public CGameObject
{
public:
	enum STATE { SwitchActivation, Opening, Open, Closed };
private:
	explicit CExitDoor(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CExitDoor(const CExitDoor& rhs);
	virtual ~CExitDoor() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
	virtual void Render_Stencil();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
public:
	void SetState(STATE eState) { m_CurState = eState; m_iCurAnimation = m_CurState; }
private:
	void SetUp_Sound();
	void Update_Sound();
	void Update_ProgressTimeSound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);
private:
	void SetUp_ExitDoorLight();
	void State_Check();
	void Check_LightRender();
	void ComunicateWithServer();
private:
	_float				m_fAnimationTime = 0.f;
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;

	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
	CExitDoorLight*		m_pDoorLight = nullptr;
private:
	list<SoundData>	m_SoundList;
	wstring m_Key;
	_int	m_iOtherOption = 0;
	STATE	m_OldState = Closed;
	STATE	m_CurState = Closed;
	_float	m_fRatio = 0.f;
	_int	m_iSoundCheck = 0;
	_int	m_iProgressSoundCheck = 0;
	_float	m_fDeltaTime = 0.f;
	_bool	bOut = false;
	_vec3	m_vLightPos;
	_bool	m_bHeal = false;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CExitDoor* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END