#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CAction_HealingSelf final : public CAction
{
public:
	explicit CAction_HealingSelf();
	virtual ~CAction_HealingSelf() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
private:
	_float m_fHealTime = 0;
	_int m_iState = 0;
	_bool m_bIsFinished = false;
protected:
	virtual void Free();
};

_END