#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CMeatHook;
class CAction_BeingKilledBySpider final : public CAction
{
public:
	explicit CAction_BeingKilledBySpider();
	virtual ~CAction_BeingKilledBySpider() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetHook(CGameObject* pHook) { m_pHook = (CMeatHook*)pHook; }
private:
	_int			m_iState = 0;
	CMeatHook*		m_pHook = nullptr;
protected:
	virtual void Free();
};

_END