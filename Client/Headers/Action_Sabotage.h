#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)
class CMeatHook;
class CAction_Sabotage final : public CAction
{
#define SABOTAGETIME 30.0f
public:
	explicit CAction_Sabotage();
	virtual ~CAction_Sabotage() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetHook(CGameObject* pHook) { m_pHook = (CMeatHook*)pHook; }
private:
	_float m_fDesTime = 0.f;
	_float m_fFailCoolTime = 0.f;
	_int m_iState = 0;
	_float m_fSabotageTime = 0.f;
	CMeatHook* m_pHook = nullptr;
protected:
	virtual void Free();
};

_END