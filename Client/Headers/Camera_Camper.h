#pragma once

#include "Defines.h"
#include "Camera.h"

_BEGIN(Engine)
class CFrustum;
class CCollider;
_END

_BEGIN(Client)
class CCamera_Camper final : public CCamera
{
private:
	explicit CCamera_Camper(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamera_Camper(const CCamera_Camper& rhs);
	virtual ~CCamera_Camper() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	void	Set_OrbitMatrix();
	void	Set_FlashLight(_bool bFlash) { m_bFlash = bFlash; }
	void	Set_IsLock(_bool bLock) { m_bLock = bLock; }
	void	Set_LockMatrix(_matrix* pLockMat) { m_pLockMatrix = pLockMat; }
public:
	virtual _int Do_Coll(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	virtual _int Do_Dist(const _float& _fDist);
	void Set_FlashCamera(_bool bFlash) { m_bFlash = bFlash; }
	void Set_IsLockCamera(_bool bLock) { m_IsLock = bLock; }
	_bool Get_FlashCamera() { return m_bFlash; }
	_bool Get_IsLockCamera() { return m_IsLock; }
	void Set_MoriCamera(_bool bMori) { m_bMoriCamera = bMori; }
public:
	_bool	m_bIsControl = false;
private:
	CFrustum*		m_pFrustumCom = nullptr;
	CCollider*		m_pColliderCom = nullptr;
private:
	const _matrix*	m_MoriCamera = nullptr;
	const _matrix*	m_PlayerRoot = nullptr;
	const _matrix*	m_PlayerMat = nullptr;
	const _matrix*	m_PlayerRHandMat = nullptr;
	_matrix*		m_pLockMatrix = nullptr;
	_float			m_fDistance = 0.f;
	_float			m_fCollDist = 0.f;
	_float			m_fSpeed = 0.f;
	POINT			m_ptMouse;
	_vec3			m_vCamPos = _vec3();
	_float			m_fOriginY = 140.f;
	_float			m_MoveX = 0;
	_float			m_MoveY = 0;
	_bool			m_IsLock = false;
	_bool			m_bStart = false;
	_bool			m_bFlash = false;
	_bool			m_bLock = false;
	_bool			m_bMoriCamera = false;
private:
	void	CommunicateWithServer();
	HRESULT Ready_Component();
	void	Start_Camera(const _float& fTimeDelta);
	_bool	CheckCollision(_vec3* pOut);
public:
	static CCamera_Camper* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
protected:
	virtual void Free();
};

_END