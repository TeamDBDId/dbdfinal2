#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_Ending : public CGameObject
{
public:
	enum ButtonState	{ Touch, UnTouch, Click };
	enum Button			{ None, Spectate, Exit};
	enum EndingState	{ Ending, Ending_Fade, Ending_Start, Ending_Background, Ending_Panel, Ending_Select, Observing};
private:
	explicit CUI_Ending(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Ending(const CUI_Ending& rhs);
	virtual ~CUI_Ending() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
private:
	CUI_Texture* Find_UITexture(wstring UIName);
	void Delete_UI_Texture(wstring UIName);
	void Set_FadeIn();
	void Set_Ending_Start();
	void Set_Ending_Point_Background();
	void Set_Ending_Panel();
	void Set_Buttons();
	void Delete_All();
	void Delete_NoneGray();

	_bool IsWin();
	void Update_Score();
	void Update_State(const _float& fTimeDelta);
	void Update_OtherState();
	void Mouse_Check();
	void Button_Check();
	void Update_Observe();
	void Update_AllScore();
	void Update_Name(const _float& fTImeDelta);
	void Button_Spectate(ButtonState State);
	void Button_Exit(ButtonState State);
	_int Find_ObserveNum(_bool IsNext);
	void Game_End();
private:
	_uint		m_iPlayerNum = 0;
	map<wstring, CUI_Texture*> m_UITexture;
	EndingState	m_State = Ending;
	_uint		m_iCondition[5];
	_float		m_fDeltaTime = 0.f;
	_int		m_iScore[4];
	_float		m_fTimer = 0.f;
	_int		m_fTimerCheck = 0;
	_int		m_OldButton = None;
	_int		m_CurButton = None;
public:
	static CUI_Ending* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END