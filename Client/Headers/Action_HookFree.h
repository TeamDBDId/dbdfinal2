#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CMeatHook;
class CAction_HookFree final : public CAction
{
public:
	explicit CAction_HookFree();
	virtual ~CAction_HookFree() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetHook(CGameObject* pHook) { m_pHook = (CMeatHook*)pHook; }
private:
	_bool m_bIsInit = false;
	_float	m_fIndex = 0.f;
	_vec3 m_vOriginPos;
	_int m_iState = 0;
	vector<_vec3>	m_vecPos;
	CMeatHook*		m_pHook = nullptr;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END