#pragma once

#include "Defines.h"
#include "BaseEffect.h"

_BEGIN(Engine)
class CTransform;
class CTexture;
class CBuffer_RcTex;
class CMesh_Static;
_END

_BEGIN(Client)
class CCamper;
class CEffect_BG final : public CBaseEffect
{
public:
	enum eKind { BG_GENON,BG_HOOK,BG_END };
private:
	explicit CEffect_BG(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_BG(const CEffect_BG& _rhs);
	virtual ~CEffect_BG() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	virtual void Set_Param(const _vec3& _vPos, const void* _pVoid = nullptr);
public:
	void Set_Kind(const eKind& _eKind, void* _pCamper = nullptr);

private:
	CTransform*			m_pTransformCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;

	CTexture*			m_pTextureCom = nullptr;
	_float				m_fBGSize = 0.f;


	CCamper*			m_pCamper = nullptr;
private:
	_matrix				m_pGenWorld = _matrix();
	eKind				m_eKind = BG_END;
private:
	virtual HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
	virtual HRESULT SetUp_ContantTable(LPD3DXEFFECT _pEffect);
	virtual HRESULT SetUp_ConstantTable(LPD3DXEFFECT _pEffect, const _uint& iAttributeID);
public:
	static CEffect_BG* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END