#pragma once
#include "Base.h"
#include <ft2build.h>
#include <freetype\freetype.h>
_BEGIN(Client)

class CFontManager : public CBase
{
	_DECLARE_SINGLETON(CFontManager)
	typedef struct
	{
		wstring FontName;
		_vec2 FontSize;
	}FONTINFO;

private:
	explicit CFontManager();
	virtual ~CFontManager() = default;

	
public:
	void	Set_GraphicDevice(LPDIRECT3DDEVICE9 pGraphic_Device) { m_pGraphic_Device = pGraphic_Device; m_pGraphic_Device->AddRef(); }
	HRESULT	Ready_FontManager();
	LPDIRECT3DTEXTURE9	FindFont(wstring FontName, _vec2 FontSize, D3DXCOLOR Color, _uint FontNum = 0);

	/*void	DeleteString(wstring strString);*/
	void	ReleaseAllTextures();
private:
	FT_Library							m_Library = nullptr;
	FT_Face								m_Face[3] = { nullptr, nullptr, nullptr };
	_int								m_nCache;
	map<wstring, LPDIRECT3DTEXTURE9>	m_FontCache;
	LPDIRECT3DDEVICE9 m_pGraphic_Device = nullptr;


private:
	virtual void Free() override;

};
_END