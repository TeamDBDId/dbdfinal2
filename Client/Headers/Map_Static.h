#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
class CFrustum;
_END


_BEGIN(Client)

class CMap_Static :	public CGameObject
{
private:
	explicit CMap_Static(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMap_Static(const CMap_Static& rhs);
	virtual ~CMap_Static() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix* VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_CubeMap(_matrix* View, _matrix* Proj);
	virtual void Render_Stemp();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int Get_OtherOption();
private:
	void Rendering_Check();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	_matrix m_matLocalInv;
	_vec3	m_vLocalPos;
	_float	m_Max = 0.f;
	wstring m_Key;
	_int	m_iOtherOption = 0;
	_bool	m_isRendering = false;
	_bool	m_isInstancing = false;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
public:
	static CMap_Static* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
