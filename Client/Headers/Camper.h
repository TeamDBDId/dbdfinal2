#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"
#include "AnimationKey.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
_END

_BEGIN(Client)
class CCustomLight;
class CItem;
class CCamper final : public CGameObject
{
private:
#define PREMONITION_RANGE		3600.f
#define BOND_RANGE			3600.f
#define SYMPATHY_RANGE		12800.f
#define NURSECALLING_RANGE	2800.f
#define DEER_RANGE			4600.f
#define HOMOGENEITY_RANGE	1600.f
public:
	enum CHARACTER { CH_DWIGHT, CH_BILL, CH_NEA, CH_MEG, CH_END};
	enum CONDITION { HEALTHY, INJURED, DYING, SPECIAL, OBSTACLE, HOOKED, HOOKDEAD, BLOODDEAD, ESCAPE, END};
	enum HOOKSTAGE { EscapeAttempts, Struggle, Sacrifice, HookStage_END};
	enum BLOODSP { BACK, FRONT, LEFT, RIGHT, BOTTOM, TOP };
private:
	explicit CCamper(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamper(const CCamper& rhs);
	virtual ~CCamper() = default;
public:
	const _matrix* Get_LHandMatrix() { return m_pLHandMatrix; }
	const _matrix* Get_RHandMatrix() { return m_pRHandMatrix; }
	_matrix* Get_CalRHandMatrix() { return &m_matRHand; }
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_Stencil();
	virtual void Render_ShadowMap(_matrix* VP, LPD3DXEFFECT pEffect, _vec4 vLightPos); 
	virtual void Render_Stemp();
	virtual void Render_CubeMap(_matrix* View, _matrix* Proj);
	void Render_UIImage();
	void SetBright(const _float& fTimeDelta);
public:
	virtual _int Do_Coll(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	virtual _int Do_Coll_Player(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	void PickUpItem();
public:
	void Set_State(AC::ANIMATION_CAMPER eState) { m_eCurState = eState; State_Check(); }
	HRESULT Set_Index(_uint index);
	_uint Get_Index() { return m_iIndex; }
	_vec3 Get_Pos() { return *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION); }
	void IsLockKey(_bool IsLock) { m_bIsLockKey = IsLock; }
	_bool IsColl() { return m_isColl; }
	void SetColl(_bool IsColl) { m_isColl = IsColl; }
	void SetCurCondition(CONDITION eCondition) { m_eCurCondition = eCondition; }
	void SetOldCondition(CONDITION eCondition) { m_eOldCondition = eCondition; }
	void SetHookStage(HOOKSTAGE eHookStage) { m_eHookStage = eHookStage; }
	void SetHeal(_float fHeal) { m_fHeal = fHeal; }
	void SetDyingEnergy() { m_fDyingEnergy = m_fEnergy; }
	void SetHookedEnergy() { m_fHookEnergy = m_fEnergy; }
	void SetEnergy(_float fEnergy) { m_fEnergy = fEnergy; }
	void SetCarry(_bool bCarry) { m_bIsCarried = bCarry; bCalZZi = bCarry; }
	void SetMaxEnergyAndMaxHeal();
	void SetColledItem(CItem* pItem) { m_pColledItem = pItem; }
	void SetItem(CItem* pItem) { m_pItem = pItem; }
	void Change_Mesh(_uint iNum);
	void Set_Penetraition(_float fTime) { m_fPenetrationTime = fTime; }
	void Set_PColor(_vec4 Color) { m_Color = (D3DXCOLOR)Color; }
	void Set_PerkTime(wstring tag, _float fTime) { m_mapCoolTime[tag] = fTime; }
	void Set_HealUp(_bool bUp) { m_bHealUp = bUp; }
	void Set_SlasherColl(_bool IsColl);
	void BloodSpread(_int index);
	_bool IsLook() { return m_isLook; }
public:
	AC::ANIMATION_CAMPER GetState() { return m_eCurState; }
public:
	HOOKSTAGE GetHookStage() { return m_eHookStage; }
	CONDITION GetCurCondition() { return m_eCurCondition; }
	CONDITION GetOldCondition() { return m_eOldCondition; }
	_float GetHeal() { return m_fHeal; }
	_float GetMaxHeal() { return m_fMaxHeal; }
	_float GetEnergy() { return m_fEnergy; }
	_float GetMaxEnergy() { return m_fMaxEnergy; }
	_float GetDyingEnergy() { return m_fDyingEnergy; }
	_float GetHookedEnergy() { return m_fHookEnergy; }
	_bool IsCarry() { return m_bIsCarried; }
	CGameObject* GetTargetOfInteraction() { return m_pTargetOfInteraction; }
	CItem* GetColledItem() { return m_pColledItem; }
	CItem* GetCurItem() { return m_pItem; }
	_float Get_PerkTime(wstring tag) { return m_mapCoolTime[tag]; }
	_bool Get_CalZZi() { return m_bUseCalZZi; }
	CHARACTER Get_Character() { return m_Character; }
	CMesh_Dynamic* Get_Mesh() { return m_pMeshCom; }
private:
	void Update_StateSound(const _float& fTimeDelta);
	void Check_InteractionOfObject(CGameObject* pGameObject);
	void Init_Camera();
	void CommunicationWithServer(const _float& fTimeDelta);
	void Compute_Map_Index();
	void Collision_Check();
	void Check_Perk(const _float& fTimeDelta);
	void Check_FootStep();
	void Check_RGB();
	void Set_StartPosition();
private:
	void SetUp_Sound();
	void Update_Sound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);
	void Add_SoundListFemale(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);
private:
	_uint					m_iSoundCheck = 0;
	list<SoundData>			m_MSoundList;
	list<SoundData>			m_FSoundList;
	_float					m_fRatio = 0.f;
private:
	CTransform*				m_pTransformCom = nullptr;
	CRenderer*				m_pRendererCom = nullptr;
	CShader*				m_pShaderCom = nullptr;
	CMesh_Dynamic*			m_pMeshCom = nullptr;
	CCollider*				m_pColliderCom = nullptr;
	CMesh_Dynamic*			m_pOriginMeshCom = nullptr;
private:
	_bool					m_bFootColl[2] = { false, false };
	CCollider*				m_pFootCollider[2] = { nullptr, nullptr };
	const _matrix*			m_pFootMatrix[2] = { nullptr, nullptr };
	//vector<_matrix*>		m_vecStencil;
private:
	CHARACTER				m_Character = CH_DWIGHT;
	_bool					m_bUseFlashLight = false;
	_float					m_fFailDelay = 0.0f;
	_float					m_fBloodEffectTime = 0.f;
	_float					m_fFootPrintTime = 1.5f;
	_float					m_fSoundTime = 0.0f;
	_bool					m_bWoman = false;
	_bool					m_bUseCalZZi = false;
	_bool					m_bCalZZi = false;
	_bool					m_bCanOut = false;
	CItem*					m_pItem = nullptr;
	CItem*					m_pColledItem = nullptr;
	D3DXCOLOR				m_Color = D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f);
	_bool					m_bPlayAnimation = true;
	_bool					LateInit = false;
	_bool					m_bIsLockKey = false;
	_bool					m_bIsInCloset = false;
	_bool					m_bIsCarried = false;
	AC::ANIMATION_CAMPER	m_eCurState;
	AC::ANIMATION_CAMPER	m_eOldState;
	CONDITION				m_eCurCondition = HEALTHY;
	CONDITION				m_eOldCondition = HEALTHY;
	HOOKSTAGE				m_eHookStage = EscapeAttempts;
	CGameObject*			m_pTargetOfInteraction = nullptr;
	CGameObject*			m_pCamperOfInteraction = nullptr;
	list<CGameObject*>		m_CollObjList;
	_vec3					m_vTargetPos;
	_vec3					m_vTargetCamperPos;
	_float					m_fPenetrationTime = 0.f;
	_float					m_fFacedSlasherHeadTime = 0.f;
	_float					m_fTimeDelta = 0.f;
	_float					m_fHeal = 0.f;
	_float					m_fMaxHeal = 0.f;
	_float					m_fEnergy = 0.f;
	_float					m_fMaxEnergy = 0.f;
	_float					m_fDyingEnergy = 0.f;
	_float					m_fHookEnergy = 0.f;
	_float					m_fMoveSpeed = 1.f;
	_float					m_fAttackedTime = 0.f;
	_float					m_fDissolveTime = 0.f;
	_uint					m_iIndex = 0;
	_bool					m_bSpiritDisapear = false;
	_bool					m_bInitEffect = false;
	const _matrix*			m_pMatCamera = nullptr;
	const _matrix*			m_pRHandMatrix = nullptr;
	const _matrix*			m_pLHandMatrix = nullptr;
	const _matrix*			m_pTorsoMatrix = nullptr;
	_matrix					m_matRHand = _matrix();
	_bool					m_IsReciveServer = true;
	_bool					m_bHealUp = false;
	_bool					m_bAdrenaline= false;
	map<wstring, _float>	m_mapCoolTime;
	_float					m_BreathTime = 0.f;
	_bool					m_IsScream;
	_bool					m_IsHookScream;
	_bool					m_IsInjured;
private:
	HRESULT Ready_Component();
	HRESULT Ready_Actions();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED * pMeshContainer, const _uint & iAttributeID);
private:
	void Key_Input(const _float& fTimeDelta);
	void State_Check();
	void Check_Attacked(const _float& fTimeDelta);
	void MoveSpeed_Check();
	void IsCarried(const _float& fTimeDelta);
	void Processing_Others(const _float& fTimeDelta);
	void Use_FlashLight(const _float& fTimeDelta);
public:
	static CCamper* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
private:
	CCustomLight*		m_pCustomLight = nullptr;
};

_END