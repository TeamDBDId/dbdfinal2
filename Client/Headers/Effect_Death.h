#pragma once

#include "Defines.h"
#include "BaseEffect.h"


_BEGIN(Client)
class CEffect_Death final : public CGameObject
{
private:
	explicit CEffect_Death(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_Death(const CEffect_Death& _rhs);
	virtual ~CEffect_Death() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	void Set_Param(const _vec3& _vPos);

private:
	void Make_Smoke(const _float& _fTick);
	void Make_Smoke2();
public:
	static CEffect_Death* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
private:
	const _uint		SMOKE_CNT = 18;

private:
	_vec3			m_vPos = _vec3(0.f, 0.f, 0.f);
	_vec3			m_vSmokeDir = _vec3(0.f, 0.f, 0.f);


	_float			m_fTick = 0.f;

	_float			m_fDelay = 0.f;
};

_END