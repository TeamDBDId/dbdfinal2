#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamera_Camper;
class CSlasher;
class CCamper;
class CAction_Mori : public CAction
{
public:
	explicit CAction_Mori();
	virtual ~CAction_Mori() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void Send_ServerData() override;
	void SetCamper(CGameObject* pCamper) { m_pCamper = (CCamper*)pCamper; }
private:
	_float			m_fTime = 0.f;
	_int			m_iState = 0;
	_float			m_fDelay = 0.f;
	_vec3			m_vOriginPos = _vec3();
	CCamper*		m_pCamper = nullptr;
	CSlasher*		m_pSlasher = nullptr;
	CCamera_Camper* m_pCamperCamera = nullptr;
private:
	_bool			m_bIsInit = false;
protected:
	virtual void Free();
};

_END

