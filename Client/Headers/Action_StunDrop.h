#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamper;
class CAction_StunDrop final : public CAction
{
public:
	explicit CAction_StunDrop();
	virtual ~CAction_StunDrop() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
private:
	_bool m_bIsInit = false;
	_float m_fIndex = 0.f;
	_vec3 m_vOriginPos;
	_float m_fFaintTime = 0.f;
	_int m_iState = 0;
	vector<_vec3>	m_vecCamperPos;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END