#pragma once

// Server
extern bool bStartGameServer;
extern bool bUseServer;
extern bool bCanGoOut;
extern bool bCalZZi;
extern bool bObserver;

extern int	exCountPlayerNum;
extern int	exPlayerNumber;
extern int	exCharacterNum;
extern int  exiObservPlayer;
extern char MyIPAdress[16];

extern Server_Data server_data;
extern LServer_Data lserver_data;

extern SOCKET Lobbysock;
extern SOCKET Gamesock;

extern CamperData camper_data;
extern SlasherData slasher_data;
extern Client_Data client_data;
extern LobbyData lobby_data;

extern int sendPacket;
extern LARGE_INTEGER   tickPerSecond;
extern LARGE_INTEGER   startTick, endTick;

extern LARGE_INTEGER   GtickPerSecond;
extern LARGE_INTEGER   GstartTick, GendTick;

extern bool ActiveOption;

extern double dTime;
extern double dGTime;

extern bool	bLobbyServer;
extern bool	bGameServer;

extern float fWorldTimer;
extern bool	bCameraReady;
// ����
inline _vec3 operator^(_vec3 u, _vec3 v)
{
	return _vec3(u.y*v.z - u.z*v.y, -u.x*v.z + u.z*v.x, u.x*v.y - u.y*v.x);
}
// ����
inline float operator*(_vec3 u, _vec3 v)
{
	return (u.x*v.x + u.y*v.y + u.z*v.z);
}

inline _vec3 slerp(_vec3 vA, _vec3 vB, _float fTime)
{
	_vec3 a, b;

	D3DXVec3Normalize(&a, &vA);
	D3DXVec3Normalize(&b, &vB);

	_float fAngle = acosf(min(max(D3DXVec3Dot(&a, &b), -1.f), 1.f));
	_float fSinTotal = sinf(fAngle);

	_float fRatioA = sinf((1.f - fTime) * fAngle) / fSinTotal;
	_float fRatioB = sinf(fTime * fAngle) / fSinTotal;
	
	_vec3 vOut;

	vOut.x = fRatioA * vA.x + fRatioB * vB.x;
	vOut.y = fRatioA * vA.y + fRatioB * vB.y;
	vOut.z = fRatioA * vA.z + fRatioB * vB.z;

	return vOut;
}

const unsigned int g_iBackCX = 1280;
const unsigned int g_iBackCY = 720;

const unsigned int g_iFullCX = 1920;//1280;
const unsigned int g_iFullCY = 1080;//720;

enum OBJECT_ID { CAMPER = 0x100, SLASHER = 0x200, CLOSET = 0x400, HATCH = 0x800, HOOK = 0x1000, PLANK = 0x2000, WINDOW = 0x4000, CHEST = 0x8000, GENERATOR = 0x10000, EXITDOOR = 0x20000, TOTEM = 0x40000, ITEM = 0x80000, OBJECT_ID_END = 0x200000 };
enum SCENEID {SCENE_STATIC, SCENE_LOGO, SCENE_STAGE, SCENE_LOBBY, SCENE_END};

enum EFFECTID {_E_DEATH
	, _E_EXIT1 , _E_EXIT2
	, _E_FAIL1 , _E_FAIL2
	, _E_FIRE1 , _E_FIRE2
	, _E_GENE1 , _E_GENE2
	, _E_JUMP
	, _E_LEAF
	, _E_MIST
	, _E_CHANGE
	,_E_PARTICLE
	,_E_GENON
};

enum DIR {FRONT,BACK,LEFT,RIGHT};
	
enum WINDOW_MODE { FULL_MODE= 0, WINDOW_MODE = 1};
extern HINSTANCE g_hInst;
extern HWND g_hWnd;
extern unsigned int eWINMODE;

#define KEYMGR	CKeyManager::GetInstance()
#define EFFMGR	CEffectManager::GetInstance()

#define LSLASHER	1
#define CLICKBACK	6

#define _MAP_INTERVAL 800

#define NO_EVENT				0
#define EVENT					1
#define FAILED_SKILL_CHECK		10
#define SUCCESS_SKILL_CHECK		11
#define SUCCESS_CHECK			12

#define WINDOWMODE				WINDOW_MODE
#define FULLMODE				FULL_MODE

// SecAnim Slasher
//#define	PLAYERATTACK			1
//#define PICK_UP					2
//#define HOOK_IN					3
//#define DROP					4
//#define GRAB_LOCKER				5
//#define GRAB_OBSTACLES			6
//#define GRAB_GENERIC_FAST		7
//
//// SecAnim Camper
//#define HOOK_BEING				1
//#define BEING_HEAL				2
//#define HEALED					3
//#define END_HEAL				5555
//#define ClOSET_OCCUPIED			50
//#define WIGGLE_RIGHT			60
//#define WIGGLE_LEFT				61
//#define FLASHBANG				70

// Packet
#define PLAYERATTACK			0x00000001
#define PICK_UP					0x00000002
#define HOOK_IN					0x00000004
#define DROP					0x00000008
#define GRAB_LOCKER				0x00000010
#define GRAB_OBSTACLES			0x00000020
#define GRAB_GENERIC_FAST		0x00000040
#define MORICAMPER				0x00020000
#define CALZZI					0x00040000

#define HOOK_BEING				0x00000080
#define BEING_HEAL				0x00000100
#define HEALED					0x00000200
#define END_HEAL				0x00000400
#define ClOSET_OCCUPIED			0x00000800
#define WIGGLE_RIGHT			0x00001000
#define WIGGLE_LEFT				0x00002000
#define FLASHBANG				0x00004000
#define HOOK_BEINGBORROWED		0x00008000
#define XXPALOMA				0x00010000
#define STUNDROP				0X00020000


#define CLEAR_PACKET			0X00200000

// Perk_Camper

#define DEADHARD				0x00000001
#define PREMONITION				0x00000002
#define DARKSENSE				0x00000004
#define CRUCIALBLOW				0x00000008
#define SELFHEAL				0x00000010
#define PALOMA					0x00000020
#define BORROWEDTIME			0x00000040
#define ADRENALINE				0x00000080
#define BREAKING				0x00000100
#define SYMPATHY				0x00000200
#define BOND					0x00000400
#define HOMOGENEITY				0x00000800
#define CALMSOUL				0X00001000
								
// Perk_Slasher					
								
#define SWALLOWEDHOPE			0x00000001
#define RUIN					0x00000002
#define RESENTMENT				0x00000004
#define BBQANDCHILLY			0x00000008
#define DISSONANCE				0x00000010
#define MURMUR					0x00000020
#define DEERSTALKER				0x00000040
#define INCANTATION				0x00000080
#define NURSECALLING			0x00000100

// CURSE
#define CSWALLOWEDHOPE			0x00000001
#define CRUIN					0x00000002
#define CINCANTATION			0x00000004