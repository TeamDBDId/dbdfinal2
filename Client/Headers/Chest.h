#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)

class CChest final : public CGameObject
{
public:
	enum STATE { Open, Close, IdleOpen, Idle, Closing };
private:
	explicit CChest(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CChest(const CChest& rhs);
	virtual ~CChest() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
public:
	void SetState(STATE eState) { m_CurState = eState; m_iCurAnimation = eState; }
private:
	void State_Check();
	void Set_Cloth();
	void ComunicateWithServer();

private:
	void SetUp_Sound();
	void Update_Sound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	_uint					m_iSoundCheck = 0;
	list<SoundData>			m_SoundList;
	_float					m_fRatio = 0.f;

	wstring m_Key;
	_int	m_iOtherOption = 0;
	STATE	m_OldState = Idle;
	STATE	m_CurState = Idle;
	_float	m_fDeltaTime = 0.f;
	_bool	m_bInitItem = false;
	_int	m_SoundCheck = 0;
	_float  m_fSoundTimer = 0.f;

private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CChest* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END