#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_Loading : public CGameObject
{
private:
	explicit CUI_Loading(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Loading(const CUI_Loading& rhs);
	virtual ~CUI_Loading() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	void	Set_Job(_bool IsSlaher);
private:
	void AddTipText(wstring Text);
	void Set_Background();
	void Set_Logo();
	void Update_LoadingBar();
	void Update_Logo();
	void Update_TipText();
	CUI_Texture* Find_UITexture(wstring UIName);
	void Delete_UI_Texture(wstring UIName);
	void Set_TipText();
	void Delete_All();
private:
	map<wstring, CUI_Texture*> m_UITexture;
	vector<wstring>	m_vecTipText;
	_int m_PlayerNum = 0;
	_bool	m_IsSlasher = false;
	_bool	m_IsEnd = false;
	_float	m_fDeltaTime = 0.f;
	
public:
	static CUI_Loading* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END