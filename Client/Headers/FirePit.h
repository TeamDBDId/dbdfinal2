#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
class CFrustum;
_END


_BEGIN(Client)

class CCustomLight;
class CFirePit : public CGameObject
{
private:
	explicit CFirePit(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CFirePit(const CFirePit& rhs);
	virtual ~CFirePit() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
private:
	void Init_Light();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	_bool				m_bReadyCom = false;
	_float				m_fBrTime = 0.f;
	_float				m_fBright = 0.f;
	_bool				m_LateInit = false;
	CCustomLight*		m_pCustomLight = nullptr;
	CCollider*			m_pLightCollider = nullptr;
private:
	_float	m_Max = 0.f;
	wstring m_Key = L"";
	_int	m_iOtherOption = 0;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
public:
	static CFirePit* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
