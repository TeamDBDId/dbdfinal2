#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CPicking;
class CFrustum;
_END

_BEGIN(Client)
class CCamper;
class CSpider;
class CMeatHook final : public CGameObject
{
public:
	enum STATE {
		Stand, SpiderStruggle_MIN, SpiderStruggle_MAX, SpiderStruggle2Sacrifice, SpiderStabOut, SpiderStabLoop, SpiderStabIN, SpiderReaction_OUT,
		SpiderReaction_Loop, SpiderReaction_IN,	Spider_Struggle, Idle, HookedFree, Broken_Idle, BreakEnd, Struggle, GetHookedOut, GetHookedIn };
private:
	explicit CMeatHook(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMeatHook(const CMeatHook& rhs);
	virtual ~CMeatHook() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_Stencil();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
	STATE Get_Animation() { return m_CurState; }
public:
	void SetState(STATE eState);
	void Set_PenetrationTime(_float fTime) { m_fPenetrationTime = fTime; }
	CSpider* Get_Spider() { return m_pSpider; }
	const _matrix* Get_CamperAttachMatrix() { return m_pMatCamperAttach; }
private:
	void State_Check();
	void ComunicateWithServer();
	void Attach_Spider();
private:
	void Penetration_Check();
private:
	void SetUp_Sound();
	void Update_Sound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);
private:
	_uint					m_iSoundCheck = 0;
	_float					m_fRatio = 0.f;
	list<SoundData>			m_SoundList;
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	wstring m_Key;
	_int m_iOtherOption = 0;
	STATE m_OldState = Idle;
	STATE m_CurState = Idle;
	_float m_fDeltaTime = 0.f;
	_float m_fBrokenTime = 0.f;
	const _matrix*			m_pMatCamperAttach = nullptr;
	_float					m_fPenetrationTime = 0.f;
	CSpider*			m_pSpider = nullptr;
	CCamper*			m_pCamper = nullptr;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CMeatHook* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END