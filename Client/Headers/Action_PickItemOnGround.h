#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CAction_PickItemOnGround final : public CAction
{
public:
	explicit CAction_PickItemOnGround();
	virtual ~CAction_PickItemOnGround() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
private:
	_int m_iState = 0;
	_bool  m_bPickItem = false;
	_float m_fIndex = 0.f;
protected:
	virtual void Free();
};

_END