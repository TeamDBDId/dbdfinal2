#pragma once

#include "Defines.h"
#include "BaseEffect.h"


_BEGIN(Client)
class CEffect_PlayerChange final : public CGameObject
{
private:
	explicit CEffect_PlayerChange(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_PlayerChange(const CEffect_PlayerChange& _rhs);
	virtual ~CEffect_PlayerChange() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	void Set_Param(const _vec3& _vPos);

private:
	void Make_Smoke1();
	void Make_Smoke2();
public:
	static CEffect_PlayerChange* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
private:
	const _uint		SMOKE_CNT = 18;

private:
	_vec3			m_vPos = _vec3(0.f, 0.f, 0.f);
	_vec3			m_vPlayerPos = _vec3(0.f, 0.f, 0.f);


	_float			m_fTick = 0.f;
};

_END