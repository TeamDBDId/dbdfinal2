#pragma once

#include "Defines.h"
#include "BaseEffect.h"

_BEGIN(Engine)
class CTransform;
class CTexture;
class CBuffer_RcTex;
_END

_BEGIN(Client)
class CEffect_BloodAnim final : public CBaseEffect
{
public:
	enum BloodType { B_Drop, B_DropBig,B_Anim, B_DropAnim,B_End};

private:
	explicit CEffect_BloodAnim(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_BloodAnim(const CEffect_BloodAnim& _rhs);
	virtual ~CEffect_BloodAnim() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	virtual void Set_Param(const _vec3& _vPos, const void* _pVoid);
	void Set_Param(const BloodType& _eType);

private:
	void Make_Drop();
	void Make_DropBig();
	void Make_DropAnim();
	void Make_Anim();
	
	
	
private:
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTransform*			m_pTransformCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;

private:
	UISIZE				m_uiSize = {};

	BloodType			m_eType = B_End;
	_uint				m_iPass = 0;

	_uint				m_iNum = 0;
private:
	virtual HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
	virtual HRESULT SetUp_ContantTable(LPD3DXEFFECT _pEffect);
public:
	static CEffect_BloodAnim* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END