#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CMeatHook;
class CAction_SpiderReaction final : public CAction
{
public:
	explicit CAction_SpiderReaction();
	virtual ~CAction_SpiderReaction() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetHook(CGameObject* pHook) { m_pHook = (CMeatHook*)pHook; }
	void SetFailedHookBeing(_bool IsFailed) { m_bFailedHookBeing = IsFailed; }
private:
	_float			m_fStrugleTime = 0.f;
	_int			m_iState = 0;
	_bool			m_bIsInit = false;
	_bool			m_bIsIn = false;
	_bool			m_bIsLoop = false;
	_bool			m_bIsOut = false;
	_bool			m_bIsSpaceKey = false;
	_bool			m_bFailedHookBeing = false;
	_float			m_fSpaceAcc = 0.f;
	_float			m_fDelay = 0.f;
	CMeatHook*		m_pHook = nullptr;
protected:
	virtual void Free();
};

_END