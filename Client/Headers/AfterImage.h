#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
_END

_BEGIN(Client)

class CAfterImage final : public CGameObject
{
private:
	explicit CAfterImage(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CAfterImage(const CAfterImage& rhs);
	virtual ~CAfterImage() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	void SetInfo(const _vec3 vPos1, const _vec3 vPos2);
	void ClearBuffer();
	void Set_Render(_bool bRender) { m_bRender = bRender; }
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
private:
	_bool				m_bRender = false;
	_float				m_fTime = 0.f;
	_float				m_fScale = 0.f;
	VTXTEX*				m_pVB = nullptr;
	_int				m_iIndex = 0;
	_uint				m_iCount = 0;
	IDirect3DVertexDeclaration9* m_pB = nullptr;
private:
	HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect);
public:
	static CAfterImage* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
};

_END