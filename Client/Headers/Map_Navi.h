#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
//class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
_END


_BEGIN(Client)

class CMap_Navi : public CGameObject
{
private:
	explicit CMap_Navi(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMap_Navi(const CMap_Navi& rhs);
	virtual ~CMap_Navi() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();


public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();

private:
	CTransform*			m_pTransformCom = nullptr;
//	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;

private:
	wstring m_Key;
	_int m_iOtherOption = 0;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
public:
	static CMap_Navi* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
