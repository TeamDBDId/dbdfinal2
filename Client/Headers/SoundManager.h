#pragma once
#include "Base.h"
#include <fmod.hpp>
#include <fmod.h>
_BEGIN(Client)
class CSoundManager : public CBase
{
	_DECLARE_SINGLETON(CSoundManager)
public:
	enum CHANNEL_ID { CH_CAMPER1, CH_CAMPER2, CH_CAMPER3, CH_CAMPER4, CH_SLASHER, CH_UI, CH_BGM, CH_END };

private:
	explicit CSoundManager();
	virtual ~CSoundManager() = default;

public:
	void Ready_Sound();
	void Update_Sound();
	void LoadSound(const string& FileName, _bool Is3D = true, const _bool IsLoop = false);
	void ChannelVolume(const _int& ChannelNum, const _float& fVolume);
	void ChannelVolume(const string& ChannelNum, const _float& fVolume);
	void ChannelStop(const _int& ChannelNum);
	void ChannelStop(const string& ChannelName);
	_int PlaySound(const string& FileName, const _vec3& vPosition = _vec3(0.f,0.f,0.f), const _float& fMaxDistance = 1700.f, const _float& fVolume = 1.f);
	_int PlaySound(const string& ChannelName, const string& FileName, const _vec3& vPosition = _vec3(0.f, 0.f, 0.f), const _float& fMaxDistance = 1700.f, const _float& fVolume = 1.f);

	void Load_LobbySound();
	void Load_StageSound();

	void Delete_AllSound();

	void Set_PlayerNum(_int PlayerNum) { m_iPlayerNum = PlayerNum; }
private:
	_float dbToVolume(_float db) { return powf(10.f, 0.05f*db); }
private:
	map<_int, FMOD_CHANNEL*>	m_MapChannel;
	map<string, FMOD_CHANNEL*>	m_MapNameChannel;
	map<string, FMOD_SOUND*>	m_MapSound;
	FMOD_SYSTEM*				m_pSystem;
	_int						m_iChannelNum = 0;
	_int						m_iPlayerNum = 0;
private:
	virtual void Free() override;

};
_END