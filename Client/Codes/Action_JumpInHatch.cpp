#include "stdafx.h"
#include "..\Headers\Action_JumpInHatch.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Camera_Camper.h"

_USING(Client)

CAction_JumpInHatch::CAction_JumpInHatch()
{
}

HRESULT CAction_JumpInHatch::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(true);
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	m_bIsPlaying = true;

	pCamper->Set_State(AC::JumpInHatch);

	m_iState = AC::JumpInHatch;

	return NOERROR;
}

_int CAction_JumpInHatch::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_iState == AC::JumpInHatch && pMeshCom->IsOverTime(0.25f))
	{
		GET_INSTANCE_MANAGEMENTR(END_ACTION);

		CCamera_Camper* pCamperCam = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
		if (nullptr == pCamperCam)
		{
			Safe_Release(pManagement);
			return END_ACTION;
		}	

		if (exPlayerNumber == ((CCamper*)m_pGameObject)->Get_Index())
			pCamperCam->Set_IsLockCamera(true);
		((CCamper*)m_pGameObject)->SetColl(false);
		((CCamper*)m_pGameObject)->SetOldCondition(CCamper::ESCAPE);
		((CCamper*)m_pGameObject)->SetCurCondition(CCamper::ESCAPE);
		CTransform* pTrans = (CTransform*)(pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").front())->Get_ComponentPointer(L"Com_Transform");
		if(nullptr == pTrans)
		{
			Safe_Release(pManagement);
			return END_ACTION;
		}
		pTrans->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(300000.f, 0.f, 300000.f));
		m_pGameObject->SetDead();
		
		Safe_Release(pManagement);

		return END_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_JumpInHatch::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(CCamper::ESCAPE);
	pCamper->IsLockKey(false);
}

void CAction_JumpInHatch::Send_ServerData()
{
}

void CAction_JumpInHatch::Free()
{
	CAction::Free();
}
