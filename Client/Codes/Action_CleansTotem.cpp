#include "stdafx.h"
#include "..\Headers\Action_CleansTotem.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Totem.h"

_USING(Client)

CAction_CleansTotem::CAction_CleansTotem()
{
}

HRESULT CAction_CleansTotem::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Totem);
	pCamper->Set_State(AC::CleansTotem_Loop);
	m_iState = AC::CleansTotem_Loop;

	return NOERROR;
}

_int CAction_CleansTotem::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (nullptr == pCamper)
		return END_ACTION;
	if (pCamper->GetCurCondition() == CCamper::DYING || pCamper->GetOldCondition() == CCamper::DYING)
		return END_ACTION;

	if (m_pTotem->Get_ProgressTime() >= m_pTotem->Get_MaxProgressTime())
	{
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Boldness, wstring(L"��ȭ��"), 600);
		return END_ACTION;
	}
	if (!KEYMGR->MousePressing(0))
		return END_ACTION;

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_CleansTotem::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (pCamper->GetCurCondition() != CCamper::DYING)
		pCamper->SetCurCondition(pCamper->GetOldCondition());
	else
		pCamper->SetCurCondition(CCamper::DYING);
	pCamper->IsLockKey(false);
	pCamper->Set_State(AC::Idle);
	
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	m_pTotem = nullptr;
	Send_ServerData();
}

void CAction_CleansTotem::Send_ServerData()
{
	if (m_pTotem != nullptr)
	{
		camper_data.InterationObject = m_pTotem->GetID();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_CleansTotem::Free()
{
	CAction::Free();
}
