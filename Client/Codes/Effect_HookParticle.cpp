#include "stdafx.h"
#include "Effect_HookParticle.h"
#include "Management.h"
#include "MeshTexture.h"

#include "Target_Manager.h"
#include "Math_Manager.h"
_USING(Client)

CEffect_HookParticle::CEffect_HookParticle(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_HookParticle::CEffect_HookParticle(const CEffect_HookParticle & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_HookParticle::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_HookParticle::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;


	m_vSize.x = Math_Manager::CalRandFloatFromTo(1.f,7.f);
	m_vSize.y = Math_Manager::CalRandFloatFromTo(1.f, 7.f);
	m_vSize.z = Math_Manager::CalRandFloatFromTo(1.f, 7.f);





	m_fRad = Math_Manager::CalRandFloatFromTo(0.f,6.28f);

	return NOERROR;
}

_int CEffect_HookParticle::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (7.f < m_fTime)
		m_isDead = true;


	m_pTransformCom->Move_V3(m_vDir*_fTick);


	return _int();
}

_int CEffect_HookParticle::LastUpdate_GameObject(const _float & _fTick)
{

	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;


	vRight = *(_vec3*)&matView.m[0][0] * m_vSize.x;
	vUp = *(_vec3*)&matView.m[1][0] * m_vSize.y;
	vLook = *(_vec3*)&matView.m[2][0] * m_vSize.z;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);

	m_pTransformCom->Rotation_Axis_Angle(m_fRad, &vLook);

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_HookParticle::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(1);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CEffect_HookParticle::Set_Param(const _vec3 & _vPos, const void * _pVoid)
{
	m_eType = P_NORMAL;

	_vec3 vPos = _vPos;

	vPos.x += Math_Manager::CalRandIntFromTo(-100, 100);
	vPos.y += Math_Manager::CalRandIntFromTo(50, 200);
	vPos.z += Math_Manager::CalRandIntFromTo(-100, 100);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	m_vDir.y = Math_Manager::CalRandIntFromTo(20, 80);

}

void CEffect_HookParticle::Set_Param2(const _vec3 & _vPos)
{
	m_eType = P_EXPLOSION;

	_vec3 vPos = _vPos;
	vPos.x += Math_Manager::CalRandIntFromTo(-100, 100);
	vPos.z += Math_Manager::CalRandIntFromTo(-100, 100);
	vPos.y += Math_Manager::CalRandIntFromTo(0, 100);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	m_vDir = vPos - _vPos;
	m_vDir.y = Math_Manager::CalRandIntFromTo(10, 20);
	m_vDir *= 8.f;
}





HRESULT CEffect_HookParticle::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_HookEff");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Hook_Spark");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_HookParticle::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{

	CBaseEffect::SetUp_ContantTable(_pEffect);



	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");


	m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");
	_pEffect->SetTexture("g_Tex1", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"FootGradient.tga")));

	_pEffect->SetFloat("g_fTime", m_fTime);


	_float fAlpha = 1.f;


	if (P_NORMAL == m_eType)
	{
		if (m_fTime < 1.f)
			fAlpha = m_fTime;
		else if (6.f<m_fTime)
			fAlpha = (7.f - m_fTime);
	}
	

	_pEffect->SetFloat("g_fAlpha", fAlpha);


	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_HookParticle * CEffect_HookParticle::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_HookParticle*	pInst = new CEffect_HookParticle(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_HookParticle::Clone_GameObject()
{
	CEffect_HookParticle*	pInst = new CEffect_HookParticle(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_HookParticle Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_HookParticle::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	//for (size_t i = 0; i<2; ++i)
		Safe_Release(m_pTextureCom);

	//지울것들



	CBaseEffect::Free();
}