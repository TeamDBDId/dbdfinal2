#include "stdafx.h"
#include "Management.h"
#include "UI_Texture.h"
#include "UI_Loading.h"
#include "LoadManager.h"
#include "Math_Manager.h"
#include "FontManager.h"
_USING(Client);


CUI_Loading::CUI_Loading(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Loading::CUI_Loading(const CUI_Loading & rhs)
	: CGameObject(rhs)
{
}


HRESULT CUI_Loading::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_Loading::Ready_GameObject()
{
	GET_INSTANCE(CLoadManager)->Set_MouseVisible(false);
	return NOERROR;
}

_int CUI_Loading::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	m_fDeltaTime += fTimeDelta;
	if (m_fDeltaTime > 7.f)
		m_fDeltaTime = 0.f;

	Update_LoadingBar();
	Update_Logo();
	Update_TipText();
	return _int();
}

_int CUI_Loading::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_Loading::Set_Job(_bool IsSlaher)
{
	m_IsSlasher = IsSlaher;
	Set_TipText();
	Set_Background();
	Set_Logo();
}

void CUI_Loading::AddTipText(wstring Text)
{
	GET_INSTANCE(CFontManager)->FindFont(Text, _vec2(11.f, 16.3f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	m_vecTipText.push_back(Text);
}

void CUI_Loading::Set_Background()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;
	wstring Player;
	if (m_IsSlasher)
		Player = L"Killer";
	else
		Player = L"Camper";

	_vec2 vPos = { g_iBackCX*0.5f, g_iBackCY*0.5f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Black.tga"));
	pTexture->Set_Scale(_vec2((_float)g_iBackCX, (_float)g_iBackCY),false);
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Loading_Back", pTexture });

	vPos = { 0.25f * g_iBackCX,0.38421f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(Player+wstring(L"Tip_01.tga")));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"Loading_Tip1", pTexture });

	vPos = { 0.5f * g_iBackCX,0.38421f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(Player + wstring(L"Tip_02.tga")));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"Loading_Tip2", pTexture });

	vPos = { 0.75f * g_iBackCX,0.38421f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(Player + wstring(L"Tip_03.tga")));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"Loading_Tip3", pTexture });

	vPos = { 0.5f * g_iBackCX, 0.911579f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"LoadingBar_Base.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Loading_Base", pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"LoadingBar_Bar.tga"));
	pTexture->Set_Pos(vPos);
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_UV(_vec2(0.f, 1.f));
	m_UITexture.insert({ L"Loading_Bar", pTexture });

	Safe_Release(pManagement);
}

void CUI_Loading::Set_Logo()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;
	_vec2 vPos = { 0.9288f * g_iBackCX,0.83895f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Loading_Logo.tga"));
	pTexture->Set_Pos(vPos);
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Loading_LogoBase", pTexture });

	vPos = { 0.905144f, 0.845474f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Loading_Logo_1.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	pTexture->Set_Alpha(0.f);
	m_UITexture.insert({ L"S_1", pTexture });

	vPos = { 0.919898f, 0.840822f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Loading_Logo_2.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	pTexture->Set_Alpha(0.f);
	m_UITexture.insert({ L"S_2", pTexture });

	vPos = { 0.93651f, 0.834749f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Loading_Logo_3.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	pTexture->Set_Alpha(0.f);
	m_UITexture.insert({ L"S_3", pTexture });

	vPos = { 0.949651f, 0.845878f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Loading_Logo_4.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	pTexture->Set_Alpha(0.f);
	m_UITexture.insert({ L"S_4", pTexture });

	vPos = { 0.92908f, 0.842779f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Lobby2", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Loading_Logo_5.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	pTexture->Set_Alpha(0.f);
	m_UITexture.insert({ L"S_5", pTexture });

	vPos = { 0.9288f * g_iBackCX,0.91579f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"불러오는 중"), _vec2(14.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	pTexture->Set_Pos(vPos);
	pTexture->Set_Color(D3DXCOLOR(0.5f,0.5f,0.5f,1.f));
	pTexture->IsColor(true);
	pTexture->Set_FadeIn(0.3f);
	m_UITexture.insert({ L"Loading_LogoText", pTexture });

	vPos = { 0.5f * g_iBackCX,0.88f*g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOBBY, L"Layer_Loading", (CGameObject**)&pTexture);
	_int iIndex = Math_Manager::CalRandIntFromTo(0, m_vecTipText.size() - 1);
	pTexture->Set_Font(m_vecTipText[iIndex], _vec2(11.f, 16.3f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	m_vecTipText.erase(m_vecTipText.begin() + iIndex);	pTexture->Set_Pos(vPos);
	pTexture->Set_Alpha(1.f);
	m_UITexture.insert({ L"Loading_TextTip", pTexture });

	Safe_Release(pManagement);
}

void CUI_Loading::Update_LoadingBar()
{
	if (m_IsEnd)
		return;
	_float fProgress = GET_INSTANCE(CLoadManager)->Get_Progress();
	if (fProgress > 1.f)
		fProgress = 1.f;
	if (fProgress >= 1.f && lobby_data.Ready == true)
	{
		_int MyNumber = lserver_data.Number[client_data.client_imei];

		if (MyNumber > 0 && MyNumber <= 5)
		{
			lobby_data.Ready = false;
			m_IsEnd = true;
		}
	}
	CUI_Texture* pTexture = nullptr;
	
	pTexture = Find_UITexture(wstring(L"Loading_Bar"));
	if (pTexture == nullptr)
		return;
	pTexture->Set_UV(_vec2(fProgress * 1.f, 1.f));
}

void CUI_Loading::Update_Logo()
{
	_float fRatio = 2.f;
	CUI_Texture* pTexture = nullptr;
	if (m_fDeltaTime >= 5.f)
	{
		pTexture = Find_UITexture(wstring(L"S_1"));
		_float fAlpha = (7.f - m_fDeltaTime)*0.5f;
		pTexture->Set_Alpha(fAlpha);
		pTexture = Find_UITexture(wstring(L"S_2"));
		pTexture->Set_Alpha(fAlpha);
		pTexture = Find_UITexture(wstring(L"S_3"));
		pTexture->Set_Alpha(fAlpha);
		pTexture = Find_UITexture(wstring(L"S_4"));
		pTexture->Set_Alpha(fAlpha);
		pTexture = Find_UITexture(wstring(L"S_5"));
		pTexture->Set_Alpha(fAlpha);
	}
	else if(m_fDeltaTime >= 4.f)
	{ 
		pTexture = Find_UITexture(wstring(L"S_5"));
		pTexture->Set_Alpha(1.f);
		pTexture->Set_UV(_vec2(fRatio*(m_fDeltaTime - 4.f), 1.f));
	}
	else if (m_fDeltaTime >= 3.f)
	{
		pTexture = Find_UITexture(wstring(L"S_4"));
		pTexture->Set_Alpha(1.f);
		pTexture->Set_UV(_vec2(1.f, fRatio*(m_fDeltaTime - 3.f)));
	}
	else if (m_fDeltaTime >= 2.f)
	{
		pTexture = Find_UITexture(wstring(L"S_3"));
		pTexture->Set_Alpha(1.f);
		pTexture->Set_UV(_vec2(1.f, fRatio*(m_fDeltaTime - 2.f)));
	}
	else if (m_fDeltaTime >= 1.f)
	{
		pTexture = Find_UITexture(wstring(L"S_2"));
		pTexture->Set_Alpha(1.f);
		pTexture->Set_UV(_vec2(1.f, fRatio*(m_fDeltaTime-1.f)));
	}
	else
	{
		pTexture = Find_UITexture(wstring(L"S_1"));
		pTexture->Set_Alpha(1.f);
		pTexture->Set_UV(_vec2(1.f, fRatio*m_fDeltaTime));
	}
}

void CUI_Loading::Update_TipText()
{
	CUI_Texture* pTexture = Find_UITexture(wstring(L"Loading_TextTip"));
	if(m_fDeltaTime>=0.f && m_fDeltaTime <= 1.f)
		pTexture->Set_Alpha(m_fDeltaTime);
	else if(m_fDeltaTime >= 6.f && m_fDeltaTime <= 7.f)
		pTexture->Set_Alpha(7.f- m_fDeltaTime);
	if (pTexture->Get_Alpha() == 0.f)
	{
		if (m_vecTipText.size() == 0)
			return;
		_int iIndex = Math_Manager::CalRandIntFromTo(0, m_vecTipText.size()-1);
		pTexture->Set_Font(m_vecTipText[iIndex], _vec2(11.f, 16.3f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f),1);
		m_vecTipText.erase(m_vecTipText.begin() + iIndex);
	}
}

CUI_Loading * CUI_Loading::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Loading*	pInstance = new CUI_Loading(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Loading Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Loading::Clone_GameObject()
{
	CUI_Loading*	pInstance = new CUI_Loading(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Loading Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


CUI_Texture * CUI_Loading::Find_UITexture(wstring UIName)
{
	auto& iter = m_UITexture.find(UIName);
	if (iter == m_UITexture.end())
		return nullptr;
	return iter->second;
}

void CUI_Loading::Delete_UI_Texture(wstring UIName)
{
	auto& iter = m_UITexture.find(wstring(UIName));
	m_UITexture.erase(iter);
}

void CUI_Loading::Set_TipText()
{
	wstring Font;
	if (m_IsSlasher)
	{
		AddTipText(wstring(L"살인마들은 각각 자신만의 특수 능력을 가지고 있습니다."));
		AddTipText(wstring(L"살인마들은 각각 자신만의 특수 능력을 가지고 있습니다."));
		AddTipText(wstring(L"발전기 근처를 순찰하는 것은 생존자들을 찾기 아주 좋은 방법입니다."));
		AddTipText(wstring(L"공격 버튼을 길게 누르면 돌진공격을 할 수 있습니다."));
		AddTipText(wstring(L"잠식된 이후에 갈고리에 걸린 생존자는 엔티티에게 희생당합니다."));
		AddTipText(wstring(L"생존자들을 갈고리에 거는 것은 살인마들의 최종 목표이다."));
		AddTipText(wstring(L"생존자를 추격하는 동안 주변의 지형지물과 생존자의 움직임을 살펴 그들의 이동경로를 예측해야 합니다."));
		AddTipText(wstring(L"달리는 생존자들은 빨간색 발자국을 남깁니다. 이를 이용해 생존자들을 추격하십시오"));
		AddTipText(wstring(L"만약 시야에서 생존자를 놓쳤다면, 주위에 귀를 기울여 보세요."));
		
	}
	else
	{
		AddTipText(wstring(L"생존자들은 자신의 행동에 대해 잘 이해하고 있어야 합니다."));
		AddTipText(wstring(L"달리기는 살인마들이 보고 추격할 수 있는 발자국을 남깁니다."));
		AddTipText(wstring(L"생존자는 손전등을 살인마에게 조준해서 살인마를 잠깐 동안 실명시킬 수 있습니다."));
		AddTipText(wstring(L"판자를 내리는 것은 큰 소음을 냅니다."));
		AddTipText(wstring(L"큰 소음을 내는 행동들, 예를들면 스킬체크를 실패한 생존자는 살인마에게 위치를 알려줍니다."));
		AddTipText(wstring(L"생존자들은 상자나 바닥에 있는 아이템을 가지고있던 아이템과 교환할 수 있습니다."));
		AddTipText(wstring(L"모든 발전기의 수리가 완료된 후, 생존자들은 출구를 통해 탈출할 수 있습니다."));
		AddTipText(wstring(L"발전기 아이콘 옆의 숫자는 출구에 전원을 공급하기 위해 앞으로 수리해야 할 발전기 개수 입니다."));
		AddTipText(wstring(L"갈고리에 걸린 생존자는 다른 생존자에게 구출받거나, 4%의 확률로 탈출할 수 있습니다."));
		AddTipText(wstring(L"판자를 살인마에게 직격시키면, 살인마는 잠시 기절합니다."));
	}
}

void CUI_Loading::Delete_All()
{
	for (auto& iter : m_UITexture)
	{
		iter.second->SetDead();
	}
	m_UITexture.clear();
}

void CUI_Loading::Free()
{
	CGameObject::Free();
}
