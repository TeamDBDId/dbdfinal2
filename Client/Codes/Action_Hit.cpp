#include "stdafx.h"
#include "..\Headers\Action_Hit.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Machete.h"
#include "Camper.h"

_USING(Client)

CAction_Hit::CAction_Hit()
{
}

HRESULT CAction_Hit::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	m_fIndex = 0.f;
	m_fAccTimeDelta = 1.f;
	m_fAcc_Attack_Intime = 0.f;
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	m_bIsPlaying = true;
	pSlasher->IsLockKey(true);
	pSlasher->Get_CarriedCamper() == nullptr ? pSlasher->Set_UpperState(AS::Attack_In) : pSlasher->Set_UpperState(AS::Carry_Attack_In);
	pSlasher->Set_WeaponColl(false);
	m_iState = AS::Attack_In;
	m_bIsInit = false;
	CManagement* pManagement = CManagement::GetInstance();
	m_pMachete = (CMachete*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Machete", 0);
	m_pMachete->Init_BeHitObject();

	if (server_data.Slasher.iCharacter == CSlasher::C_SPIRIT)
	{
		m_pMachete->Set_SpiritAttack(false);
		pSlasher->Init_SkiilCoolTime();
	}
	return NOERROR;
}

_int CAction_Hit::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;
	((CSlasher*)m_pGameObject)->RotateY(fTimeDelta);

	_int iIndex = (_int)m_fIndex;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_fAcc_Attack_Intime += fTimeDelta;

	if (server_data.Slasher.iCharacter == 0)
	{
		if (m_iState == AS::Attack_In)
		{
			m_fAccTimeDelta = 1.5f;

			if (pSlasher->Get_CarriedCamper() != nullptr || (m_fAcc_Attack_Intime > 0.5f) || !KEYMGR->MousePressing(0))
			{
				m_iState = AS::Attack_Swing;
				pSlasher->Get_CarriedCamper() == nullptr ? pSlasher->Set_State(AS::Attack_Swing) : pSlasher->Set_State(AS::CarryAttack_Swing);
			}
		}
		else if (m_iState == AS::Attack_Swing)
		{
			m_fAccTimeDelta = 1.5f;
			if (!m_bIsInit)
			{
				pSlasher->Set_WeaponColl(true);
				m_bIsInit = true;
			}
			CGameObject* pBeHitTarget = m_pMachete->Get_BeHitObject();
			if (pBeHitTarget != nullptr)
			{
				if (pBeHitTarget->GetID() & CAMPER)
				{
					GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Brutality, wstring(L"Ÿ��"), 456);
					pSlasher->Set_WeaponColl(false);
					if (pSlasher->Get_CarriedCamper() == nullptr)
					{
						pSlasher->Set_State(AS::Attack_Wipe);
						m_iState = AS::Attack_Wipe;
					}
					else
					{
						pSlasher->Set_State(AS::Carry_Attack_MissOut);
						m_iState = AS::Attack_Miss_Out;
					}
				}
				else
				{
					pSlasher->Set_WeaponColl(false);
					m_iState = AS::Attack_Bow;
					pSlasher->Get_CarriedCamper() == nullptr ? pSlasher->Set_State(AS::Attack_Bow) : pSlasher->Set_State(AS::Carry_Attack_Bow);
				}
			}
			else
			{
				if (pMeshCom->IsOverTime(0.3f))
				{
					pSlasher->Set_WeaponColl(false);
					m_iState = AS::Attack_Miss_Out;
					pSlasher->Get_CarriedCamper() == nullptr ? pSlasher->Set_State(AS::Attack_Miss_Out) : pSlasher->Set_State(AS::Carry_Attack_MissOut);
				}
			}
			
		}
		else if (m_iState == AS::Attack_Miss_Out)
		{
			m_fAccTimeDelta = 0.1f;
			if (pMeshCom->IsOverTime(0.2f))
					return END_ACTION;
		}
		else if (m_iState == AS::Attack_Wipe)
		{
			m_fAccTimeDelta = 0.1f;
			if (pMeshCom->IsOverTime(0.2f))
				return END_ACTION;
		}
		else if (m_iState == AS::Attack_Bow)
		{
			m_fAccTimeDelta = 0.1f;
			if (pMeshCom->IsOverTime(0.2f))
				return END_ACTION;
		}
	}
	else
	{
		if (m_iState == AS::Attack_In)
		{
			m_fAccTimeDelta = 1.5f;

			if (pSlasher->Get_CarriedCamper() != nullptr || (m_fAcc_Attack_Intime > 0.5f) || !KEYMGR->MousePressing(0))
			{
				if (!m_bIsInit)
				{
					pSlasher->Set_WeaponColl(true);
					m_bIsInit = true;
				}
				m_fIndex = 0.f;
				m_iState = AS::Attack_Swing;
				pSlasher->Get_CarriedCamper() == nullptr ? pSlasher->Set_State(AS::Attack_Swing) : pSlasher->Set_State(AS::CarryAttack_Swing);
			}
		}
		else if (m_iState == AS::Attack_Swing)
		{
			m_fAccTimeDelta = 1.5f;

			CGameObject* pBeHitTarget = m_pMachete->Get_BeHitObject();
			if (pBeHitTarget != nullptr)
			{
				if (pBeHitTarget->GetID() & CAMPER)
				{
					//pSlasher->Set_WeaponColl(false);
					GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Brutality, wstring(L"Ÿ��"), 100);
					if (pSlasher->Get_CarriedCamper() == nullptr)
					{
						pSlasher->Set_State(AS::Attack_Wipe);
						m_iState = AS::Attack_Wipe;
					}
					else
					{
						pSlasher->Set_State(AS::Carry_Attack_MissOut);
						m_iState = AS::Attack_Miss_Out;
					}
				}
				else
				{
					pSlasher->Set_WeaponColl(false);
					m_iState = AS::Attack_Bow;
					pSlasher->Get_CarriedCamper() == nullptr ? pSlasher->Set_State(AS::Attack_Bow) : pSlasher->Set_State(AS::Carry_Attack_Bow);
				}
			}
			else
			{
				if (iIndex > 5)
				{
					pSlasher->Set_WeaponColl(false);
					m_iState = AS::Attack_Miss_Out;
					pSlasher->Get_CarriedCamper() == nullptr ? pSlasher->Set_State(AS::Attack_Miss_Out) : pSlasher->Set_State(AS::Carry_Attack_MissOut);
				}
			}
		}
		else if (m_iState == AS::Attack_Miss_Out)
		{
			m_fAccTimeDelta = 0.1f;
			m_pMachete->Set_SpiritAttack(true);
			if (pMeshCom->IsOverTime(0.3f))
				return END_ACTION;
		}
		else if (m_iState == AS::Attack_Wipe)
		{
			m_fAccTimeDelta = 0.1f;
			m_pMachete->Set_SpiritAttack(true);
			if (pMeshCom->IsOverTime(0.3f))
				return END_ACTION;
		}
		else if (m_iState == AS::Attack_Bow)
		{
			m_fAccTimeDelta = 0.1f;
			m_pMachete->Set_SpiritAttack(true);
			if (pMeshCom->IsOverTime(0.3f))
				return END_ACTION;
		}
	}

	if (KEYMGR->KeyPressing(DIK_W))
	{
		pTransform->Go_Straight(fTimeDelta * m_fAccTimeDelta);
	}
	if (KEYMGR->KeyPressing(DIK_S))
	{
		pTransform->BackWard(fTimeDelta * m_fAccTimeDelta);
	}
	if (KEYMGR->KeyPressing(DIK_A))
	{
		pTransform->Go_Left(fTimeDelta * m_fAccTimeDelta);
	}
	if (KEYMGR->KeyPressing(DIK_D))
	{
		pTransform->Go_Right(fTimeDelta * m_fAccTimeDelta);
	}

	return UPDATE_ACTION;
}

void CAction_Hit::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	pSlasher->Set_WeaponColl(false);
	pSlasher->IsLockKey(false);
}

void CAction_Hit::Send_ServerData()
{
}

void CAction_Hit::Free()
{
	CAction::Free();
}
