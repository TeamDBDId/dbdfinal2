#include "stdafx.h"
#include "..\Headers\Camera_Debug.h"
#include "Management.h"

_USING(Client)

CCamera_Debug::CCamera_Debug(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CCamera(pGraphic_Device)
{
}

CCamera_Debug::CCamera_Debug(const CCamera_Debug & rhs)
	: CCamera(rhs)
{

}

HRESULT CCamera_Debug::Ready_Prototype()
{
	if (FAILED(CCamera::Ready_Prototype()))
		return E_FAIL;

	


	return NOERROR;
}

HRESULT CCamera_Debug::Ready_GameObject()
{
	// 트랜스폼 컴포넌트 생성해 놓는다.
	if (FAILED(CCamera::Ready_GameObject()))
		return E_FAIL;

	if (FAILED(CCamera_Debug::Ready_Component()))
		return E_FAIL;



	m_pTransform->SetUp_Speed(450.f, D3DXToRadian(360.f));

	D3DVIEWPORT9			ViewPort;
	m_pGraphic_Device->GetViewport(&ViewPort);


	m_ptMouse.x = ViewPort.Width >> 1;
	m_ptMouse.y = ViewPort.Height >> 1;

	ClientToScreen(g_hWnd, &m_ptMouse);

	
	
	return NOERROR;
}

_int CCamera_Debug::Update_GameObject(const _float & fTimeDelta)
{
	//SetCursorPos(m_ptMouse.x, m_ptMouse.y);

	if (nullptr == m_pInput_Device)
		return -1;

	if (false == m_bIsControl)
		return 1;

	if (KEYMGR->KeyPressing(DIK_RIGHT))
	{
		m_pTransform->Go_Right(fTimeDelta);
	}

	if (KEYMGR->KeyPressing(DIK_LEFT))
	{
		m_pTransform->Go_Left(fTimeDelta);
	}

	if (KEYMGR->KeyPressing(DIK_UP))
	{
		m_pTransform->Go_Straight(fTimeDelta);
	}

	if (KEYMGR->KeyPressing(DIK_DOWN))
	{
		m_pTransform->BackWard(fTimeDelta);
	}

	_long	MouseMove = 0;

	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_X))
	{
		m_pTransform->Rotation_Y(D3DXToRadian(MouseMove) * fTimeDelta);		
	}

	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_Y))
	{


		m_pTransform->Rotation_Axis(D3DXToRadian(MouseMove) * fTimeDelta, m_pTransform->Get_StateInfo(CTransform::STATE_RIGHT));
	}

	Invalidate_ViewProjMatrix();

	m_pFrustumCom->Transform_ToWorld();

	return _int();
}

_int CCamera_Debug::LastUpdate_GameObject(const _float & fTimeDelta)
{


	return _int();
}

void CCamera_Debug::Render_GameObject()
{
}



HRESULT CCamera_Debug::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

CCamera_Debug * CCamera_Debug::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera_Debug*	pInstance = new CCamera_Debug(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCamera_Debug Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


CGameObject * CCamera_Debug::Clone_GameObject()
{
	CCamera_Debug*	pInstance = new CCamera_Debug(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCamera_Debug Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamera_Debug::Free()
{
	Safe_Release(m_pFrustumCom);
	CCamera::Free();
}
