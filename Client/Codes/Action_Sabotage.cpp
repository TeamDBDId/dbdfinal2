#include "stdafx.h"
#include "..\Headers\Action_Sabotage.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "MeatHook.h"
#include "Item.h"

_USING(Client)

CAction_Sabotage::CAction_Sabotage()
{
}

HRESULT CAction_Sabotage::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fFailCoolTime > 0.1f)
		return NOERROR;

	if (nullptr != m_pHook)
	{
		if (m_pHook->GetID() & HOOK)
		{
			if (server_data.Game_Data.Hook[m_pHook->GetID() - HOOK] == 13
				|| server_data.Game_Data.Hook[m_pHook->GetID() - HOOK] == 14)
				return NOERROR;
		}

		CTransform* pTransform = (CTransform*)m_pHook->Get_ComponentPointer(L"Com_Transform");
		if (nullptr == pTransform)
			return NOERROR;

		for (int i = 0; i < 4; ++i)
		{
			if (i == (exPlayerNumber - 1))
				continue;
			if (!server_data.Campers[i].bConnect)
				continue;
			if (!server_data.Campers[i].bLive)
				continue;
			if (server_data.Campers[i].iState == CCamper::HOOKED)
			{
				_float fDist = D3DXVec3Length(&(server_data.Campers[i].vPos - *pTransform->Get_StateInfo(CTransform::STATE_POSITION)));
				if (fDist <= 220.f)
					return NOERROR;
			}
		}
	}

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->Set_State(AC::Sabotage_In);
	m_iState = AC::Sabotage_In;
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Sabotage);
	GET_INSTANCE(CUIManager)->Set_SkillCheckState(0);

	return NOERROR;
}

_int CAction_Sabotage::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (nullptr == pCamper)
		return END_ACTION;

	if (pCamper->GetCurCondition() == CCamper::DYING || pCamper->GetOldCondition() == CCamper::DYING)
		return END_ACTION;

	if (nullptr != pCamper->GetCurItem() && pCamper->GetCurItem()->Get_ItemType() == CItem::TOOLBOX)
	{
		if (KEYMGR->MousePressing(1))
			pCamper->GetCurItem()->Use_Item(fTimeDelta, nullptr, nullptr);
	}

	if (nullptr != m_pHook)
	{
		if (m_pHook->GetID() & HOOK)
		{
			if (server_data.Game_Data.Hook[m_pHook->GetID() - HOOK] == 13
				|| server_data.Game_Data.Hook[m_pHook->GetID() - HOOK] == 14)
				return END_ACTION;
		}
	}

	_float fProgress = server_data.Game_Data.HookBroken[m_pHook->GetID() - HOOK];

	if (fProgress > SABOTAGETIME)
	{
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Boldness, L"갈고리 파괴공작", _int(m_fDesTime * 20.f));
		return END_ACTION;
	}
	GET_INSTANCE(CUIManager)->Set_ProgressPoint(fProgress, SABOTAGETIME);

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	_int SkillCheck = GET_INSTANCE(CUIManager)->Get_SkillCheckState();

	if (SkillCheck == FAILED_SKILL_CHECK)
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->Set_State(AC::GeneratorFail);
		return END_ACTION;
	}
	else if(SkillCheck == SUCCESS_SKILL_CHECK)
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Boldness, L"스킬 체크(훌륭함)", 150);
	else if(SkillCheck == 12)
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Boldness, L"스킬 체크(좋음)", 50);

	if (m_iState == AC::Sabotage_In && pMeshCom->IsOverTime(0.3f))
	{
		((CCamper*)m_pGameObject)->Set_State(AC::Sabotage_Loop);

		m_iState = AC::Sabotage_Loop;
	}

	m_fDesTime += fTimeDelta;

	Send_ServerData();

	if (!KEYMGR->MousePressing(1))
		return END_ACTION;

	return UPDATE_ACTION;
}

void CAction_Sabotage::End_Action()
{
	camper_data.InterationObject = 0;
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	m_fDesTime = 0.f;
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->IsLockKey(false);
	m_pHook = nullptr;
	Send_ServerData();
}

void CAction_Sabotage::Send_ServerData()
{
	if (nullptr != m_pHook)
	{
		camper_data.InterationObject = m_pHook->GetID();
		camper_data.InterationObjAnimation = 100;
	}
	else
	{
		camper_data.InterationObject = 0;
	}
}

void CAction_Sabotage::Free()
{
	CAction::Free();
}
