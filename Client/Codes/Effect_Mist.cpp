#include "stdafx.h"
#include "Effect_Mist.h"
#include "Management.h"


#include "Target_Manager.h"
#include "Math_Manager.h"
_USING(Client)

CEffect_Mist::CEffect_Mist(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_Mist::CEffect_Mist(const CEffect_Mist & _rhs)
	: CBaseEffect(_rhs)
	
{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Mist::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Mist::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->Scaling(800.f, 800.f, 0.f);

	m_fRad = Math_Manager::CalRandFloatFromTo(0.f, 6.28f);

	return NOERROR;
}

_int CEffect_Mist::Update_GameObject(const _float & _fTick)
{



	m_fTime += _fTick;
	if (7.f < m_fTime)
		m_isDead = true;

	if (m_isDead)
		return 1;

	return _int();
}

_int CEffect_Mist::LastUpdate_GameObject(const _float & _fTick)
{
	//안개 거리컬링
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (2800.f < m_fCameraDistance)
	{
		m_isDead = true;
		return NOERROR;
	}
		
	

	if (2000.f<m_fCameraDistance)
		return NOERROR;

	if (m_fCameraDistance<400.f)
		return NOERROR;


	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;

	
	vRight = *(_vec3*)&matView.m[0][0] * 800.f;
	vUp = *(_vec3*)&matView.m[1][0] * 800.f;
	vLook = *(_vec3*)&matView.m[2][0] * 800.f;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);

	m_pTransformCom->Rotation_Axis_Angle(m_fRad, &vLook);


	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_Mist::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(_E_MIST);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
	
}

void CEffect_Mist::Set_Param(const _vec3 & _vPos, const void * _pVoid)
{
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vPos);
}





HRESULT CEffect_Mist::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Effect");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Mist_SubUV");
	if (FAILED(Add_Component(L"Com_Texture0", m_pTextureCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_Mist::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);

	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");


	m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");

	_pEffect->SetInt("g_iColumn", _uint(m_fTime*9.14f) / 8);
	_pEffect->SetInt("g_iRow", _uint(m_fTime*9.14f) % 8);
	
	_float fAlpha = 0.f;

	if (1700.f < m_fCameraDistance)
		fAlpha = powf((2000.f-m_fCameraDistance) / 300.f, 3.f)* (-cosf(m_fTime*0.897f) + 1.f)*0.35f;
	else if(m_fCameraDistance<600.f)
		fAlpha = powf((m_fCameraDistance-300.f)/300.f,3.f)* (-cosf(m_fTime*0.897f)+1.f)*0.35f;
	else
		fAlpha = (-cosf(m_fTime*0.897f) + 1.f)*0.35f;

	if (fAlpha < 0.f)
		fAlpha = 0.f;

	_pEffect->SetFloat("g_fAlpha", fAlpha);

	_pEffect->SetFloat("g_fDot", 0.125f);
	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Mist * CEffect_Mist::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Mist*	pInst = new CEffect_Mist(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Mist::Clone_GameObject()
{
	CEffect_Mist*	pInst = new CEffect_Mist(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("Mist Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Mist::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	//for(size_t i =0;i<3;++i)
	Safe_Release(m_pTextureCom);

	//지울것들



	CBaseEffect::Free();
}
