#include "stdafx.h"
#include "Management.h"
#include "UI_Perk.h"
#include <string>
#include "UI_Texture.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client);


CUI_Perk::CUI_Perk(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Perk::CUI_Perk(const CUI_Perk & rhs)
	: CGameObject(rhs)
	,m_vecCamperPerk(rhs.m_vecCamperPerk)
	,m_vecSlasherPerk(rhs.m_vecSlasherPerk)
{
}


HRESULT CUI_Perk::Ready_Prototype()
{
	SetUp_PerkInfo();
	return NOERROR;
}

HRESULT CUI_Perk::Ready_GameObject()
{
	SetUp_PerkBase();
	m_PlayerNum = exPlayerNumber;
	Set_Num(m_PlayerNum - 1);

	return NOERROR;
}

_int CUI_Perk::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	Player_Check();
	LateInit();
	Update_PerkCoolTime();

	return _int();
}

_int CUI_Perk::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_Perk::Set_Num(const _int & CurNum)
{
	SetUp_CurPerk(CurNum);
}

void CUI_Perk::SetUp_PerkInfo()
{
	m_vecCamperPerk.push_back({ DEADHARD, wstring(L"Perk_DeadHard.png") });
	m_vecCamperPerk.push_back({ PREMONITION, wstring(L"Perk_Premonition.png") });
	m_vecCamperPerk.push_back({ DARKSENSE, wstring(L"Perk_Darksense.png") });
	m_vecCamperPerk.push_back({ CRUCIALBLOW, wstring(L"Perk_Crucialblow.png") });
	m_vecCamperPerk.push_back({ SELFHEAL, wstring(L"Perk_SelfHeal.png") });
	m_vecCamperPerk.push_back({ PALOMA, wstring(L"Perk_Paloma.png") });
	m_vecCamperPerk.push_back({ BORROWEDTIME, wstring(L"Perk_Borrowedtime.png") });
	m_vecCamperPerk.push_back({ ADRENALINE, wstring(L"Perk_Adrenaline.png") });
	m_vecCamperPerk.push_back({ BREAKING, wstring(L"Perk_Breaking.png") });
	m_vecCamperPerk.push_back({ SYMPATHY, wstring(L"Perk_Sympathy.png") });
	m_vecCamperPerk.push_back({ BOND, wstring(L"Perk_Bond.png") });
	m_vecCamperPerk.push_back({ HOMOGENEITY, wstring(L"Perk_Homogeneity.png") });
	m_vecCamperPerk.push_back({ CALMSOUL, wstring(L"Perk_Calmsoul.png") });

	m_vecSlasherPerk.push_back({ SWALLOWEDHOPE, wstring(L"Perk_Swallowedhope.png") });
	m_vecSlasherPerk.push_back({ RUIN, wstring(L"Perk_Ruin.png") });
	m_vecSlasherPerk.push_back({ RESENTMENT, wstring(L"Perk_Resentment.png") });
	m_vecSlasherPerk.push_back({ BBQANDCHILLY, wstring(L"Perk_Bbqandchilly.png") });
	m_vecSlasherPerk.push_back({ DISSONANCE, wstring(L"Perk_Dissonance.png") });
	m_vecSlasherPerk.push_back({ MURMUR, wstring(L"Perk_Murmur.png") });
	m_vecSlasherPerk.push_back({ DEERSTALKER, wstring(L"Perk_Deerstalker.png") });
	m_vecSlasherPerk.push_back({ INCANTATION, wstring(L"Perk_Incantation.png") });
	m_vecSlasherPerk.push_back({ NURSECALLING, wstring(L"Perk_Nursecalling.png") });
}

void CUI_Perk::Update_PerkCoolTime()
{
	if (m_PlayerNum <= 0)
		return;
	if (!server_data.Campers[exPlayerNumber-1].bLive)
		return;
	if (nullptr == m_pCamper && nullptr == m_pSlasher)
		return;

	_float fCoolTime = 0.0f;

	if (m_PlayerNum < 5)
	{
		for (int i = 0; i < 4; ++i)
		{
			if (m_CurPerk[i].TagNum & DEADHARD)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				fCoolTime = m_pCamper->Get_PerkTime(L"DEADHARD");
				
				_float fCurTime = (fCoolTime / 40.f) * 360.f;

				if (fCoolTime <= 0.01f)
				{
					if (m_pCamper->GetCurCondition() != CCamper::INJURED)
						fCoolTime = 0.0f;
					else
						fCoolTime = 360.f;
				}			

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;
				
				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & PREMONITION)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				fCoolTime = m_pCamper->Get_PerkTime(L"PREMONITION");	
				_float fCurTime = (fCoolTime / 30.f) * 360.f;
				if (fCoolTime <= 0.01f)
					fCoolTime = 360.f;
				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & CRUCIALBLOW)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				_float fCurTime = 0.0f;
				if (m_pCamper->Get_CalZZi())
					fCurTime = 360.f;
				else
					fCoolTime = 0.0f;
					
				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & PALOMA)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				fCoolTime = m_pCamper->Get_PerkTime(L"PALOMA");
				
				_float fCurTime = (fCoolTime / 40.f) * 360.f;
				if (fCoolTime <= 0.01f)
					fCoolTime = 360.f;

				_float fPalomaTime = m_pCamper->Get_PerkTime(L"INCLOSET");
				if (fPalomaTime > 0.01f)
					fCoolTime = 0.0f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & PALOMA)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				fCoolTime = m_pCamper->Get_PerkTime(L"PALOMA");
				_float fCurTime = (fCoolTime / 40.f) * 360.f;
				if (fCoolTime <= 0.01f)
					fCoolTime = 360.f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & BORROWEDTIME)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				fCoolTime = m_pCamper->Get_PerkTime(L"HOOK_BEINGBORROWED");
				_float fCurTime = (fCoolTime / 40.f) * 360.f;
				if (fCoolTime <= 0.01f)
					fCoolTime = 360.f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & ADRENALINE)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				fCoolTime = m_pCamper->Get_PerkTime(L"ADRENALINE");
				_float fCurTime = (fCoolTime / 5.f) * 360.f;
				if (fCoolTime <= 0.01f)
					fCoolTime = 360.f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & BREAKING)
			{
				CUI_Texture* pTexture = Find_UITexture(wstring(L"Perk_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				fCoolTime = m_pCamper->Get_PerkTime(L"BREAKING");
				_float fCurTime = (fCoolTime / 40.f) * 360.f;
				if (fCoolTime <= 0.01f)
					fCoolTime = 360.f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else
			{
				CUI_Texture* pTexture = Find_UITexture(m_CurPerk[i].TagTexture);
				if (nullptr == pTexture)
					return;

				_float fCurTime = 360.f;
				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
		}		
	}
	else
	{
		for (int i = 0; i < 4; ++i)
		{
			if (m_CurPerk[i].TagNum & SWALLOWEDHOPE)
			{
				CUI_Texture* pTexture = Find_UITexture(m_CurPerk[i].TagTexture);
				if (nullptr == pTexture)
					return;

				_float fCurTime = 0.f;
				if(server_data.Game_Data.Curse & CSWALLOWEDHOPE)
					fCurTime = 360.f;
				else
					fCurTime = 0.f;
				
				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & RUIN)
			{
				CUI_Texture* pTexture = Find_UITexture(m_CurPerk[i].TagTexture);
				if (nullptr == pTexture)
					return;

				_float fCurTime = 0.f;
				if (server_data.Game_Data.Curse & CRUIN)
					fCurTime = 360.f;
				else
					fCurTime = 0.f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else if (m_CurPerk[i].TagNum & INCANTATION)
			{
				CUI_Texture* pTexture = Find_UITexture(m_CurPerk[i].TagTexture);
				if (nullptr == pTexture)
					return;

				_float fCurTime = 0.f;
				if (server_data.Game_Data.Curse & CINCANTATION)
					fCurTime = 360.f;
				else
					fCurTime = 0.f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
			else
			{
				CUI_Texture* pTexture = Find_UITexture(m_CurPerk[i].TagTexture);
				if (nullptr == pTexture)
					return;

				_float fCurTime = 360.f;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));

				pTexture = Find_UITexture(wstring(L"Perk_Base2_") + to_wstring(i));
				if (nullptr == pTexture)
					return;

				pTexture->Set_Angle(_vec2(0.0f, D3DXToRadian(fCurTime)));
			}
		}
	}
}

void CUI_Perk::LateInit()
{
	if (m_bLateInit)
		return;
	
	if (!bStartGameServer)
		return;

	GET_INSTANCE_MANAGEMENT;
	
	if (m_PlayerNum < 5)
	{
		for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
		{
			if ((pCamper->GetID() - CAMPER) + 1 == m_PlayerNum)
			{
				m_pCamper = (CCamper*)pCamper;
				m_bLateInit = true;
				Safe_Release(pManagement);
				return;
			}
		}
	}
	else
	{
		m_pSlasher = (CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front();
		m_bLateInit = true;
	}

	Safe_Release(pManagement);
}

void CUI_Perk::Player_Check()
{
	if (!bObserver)
		return;
	if (m_PlayerNum != exiObservPlayer)
	{
		m_PlayerNum = exiObservPlayer;
		m_bLateInit = false;
		Delete_AllPerk();
		Set_Num(m_PlayerNum - 1);
		Is_AllTextureRender(true);
	}
}

void CUI_Perk::SetUp_PerkBase()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture = nullptr;
	_vec2 vPos = { 0.914893f * g_iBackCX, 0.793624f * g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Perk", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Perk_Base.tga"));
	pTexture->Set_Scale(_vec2(0.3f, 0.3f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Base_0", pTexture });

	vPos = { 0.884751f * g_iBackCX, 0.855704f * g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Perk", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Perk_Base.tga"));
	pTexture->Set_Scale(_vec2(0.3f, 0.3f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Base_1", pTexture });

	vPos = { 0.946808f * g_iBackCX, 0.855704f * g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Perk", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Perk_Base.tga"));
	pTexture->Set_Scale(_vec2(0.3f, 0.3f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Base_2", pTexture });

	vPos = { 0.914893f * g_iBackCX, 0.917784f * g_iBackCY };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Perk", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Perk_Base.tga"));
	pTexture->Set_Scale(_vec2(0.3f, 0.3f));
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Perk_Base_3", pTexture });

	Safe_Release(pManagement);
}

void CUI_Perk::SetUp_CurPerk(const _int& Num)
{
	vector<PERKINFO> m_vecPerk;
	D3DXCOLOR BaseColor;
	GET_INSTANCE_MANAGEMENT;
	if (Num == 4)
	{
		m_iPerk = server_data.Slasher.Perk;
		m_vecPerk = m_vecSlasherPerk;
		BaseColor = { 1.f,0.2f,0.2f,1.f };
	}
	else
	{
		m_iPerk = server_data.Campers[Num].Perk;
		m_vecPerk = m_vecCamperPerk;
		BaseColor = { 0.0588235f, 0.3137254f, 0.0862745f,1.f };
	}
	
	CUI_Texture* pTexture = nullptr;
	_vec2 vPos[4];
	vPos[0] = { 0.914893f * g_iBackCX, 0.793624f * g_iBackCY };
	vPos[1] = { 0.884751f * g_iBackCX, 0.855704f * g_iBackCY };
	vPos[2] = { 0.946808f * g_iBackCX, 0.855704f * g_iBackCY };
	vPos[3] = { 0.914893f * g_iBackCX, 0.917784f * g_iBackCY };
	_int Index = 0;
	for (_uint i = 0; i < m_vecPerk.size(); i++)
	{
		if (m_vecPerk[i].TagNum &m_iPerk)
		{
 			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Perk", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"Perk_Base2.tga"));
			pTexture->Set_Scale(_vec2(0.35f*0.75f, 0.35f*0.75f));
			pTexture->Set_Pos(vPos[Index]);
			pTexture->Set_Color(BaseColor);
			//pTexture->IsColor(true);
			pTexture->IsColorCoolTime(true);

			m_UITexture.insert({ wstring(L"Perk_Base2_") + to_wstring(Index), pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Perk", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(m_vecPerk[i].TagTexture));
			pTexture->Set_Scale(_vec2(0.3f, 0.3f));
			pTexture->Set_Pos(vPos[Index]);
			pTexture->IsCoolTime(true);

			m_UITexture.insert({ wstring(L"Perk_") + to_wstring(Index), pTexture });
			m_CurPerk[Index] = m_vecPerk[i];
			Index++;
		}
		if (Index == 4)
			break;
	}
	
	Safe_Release(pManagement);
}

void CUI_Perk::Is_AllTextureRender(_bool IsRendering)
{
	for (auto&iter : m_UITexture)
		iter.second->IsRendering(IsRendering);
}

CUI_Texture * CUI_Perk::Find_UITexture(wstring UIName)
{
	auto& iter = m_UITexture.find(UIName);
	if (iter == m_UITexture.end())
		return nullptr;
	return iter->second;
}

void CUI_Perk::Delete_UI_Texture(wstring UIName, _bool IsDead)
{
	auto& iter = m_UITexture.find(wstring(UIName));
	if (iter != m_UITexture.end())
	{
		if (IsDead)
			iter->second->SetDead();
		m_UITexture.erase(iter);
	}
}

void CUI_Perk::Delete_AllPerk()
{
	for (int i = 0; i < 4; i++)
	{
		Delete_UI_Texture(wstring(L"Perk_") + to_wstring(i), true);
		Delete_UI_Texture(wstring(L"Perk_Base2_") + to_wstring(i), true);
	}
}

CUI_Perk * CUI_Perk::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Perk*	pInstance = new CUI_Perk(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Perk Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Perk::Clone_GameObject()
{
	CUI_Perk*	pInstance = new CUI_Perk(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Perk Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_Perk::Free()
{
	CGameObject::Free();
}
