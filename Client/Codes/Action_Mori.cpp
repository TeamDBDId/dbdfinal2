#include "stdafx.h"
#include "..\Headers\Action_Mori.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Camper.h"
#include "Camera_Camper.h"

_USING(Client)

CAction_Mori::CAction_Mori()
{
}

HRESULT CAction_Mori::Ready_Action()
{
	m_fTime = 0.0f;
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	m_bIsPlaying = true;
	m_bIsInit = false;
	if (m_pGameObject->GetID() & SLASHER)
	{
		m_pSlasher = (CSlasher*)m_pGameObject;
		if (nullptr == m_pSlasher)
			return FAILED_ACTION;

		if(nullptr == m_pCamper)
			return FAILED_ACTION;

		m_pSlasher->Set_MoriCamper(m_pCamper);

		//CTransform* pTransform = (CTransform*)m_pSlasher->Get_ComponentPointer(L"Com_Transform");
		//if (nullptr == pTransform)
		//	return FAILED_ACTION;
		//
		//_vec3 vCamperPos = server_data.Campers[m_pCamper->GetID() - CAMPER].vPos;
		//_vec3 vCamperLook = server_data.Campers[m_pCamper->GetID() - CAMPER].vLook;

		//_vec3 vMoriPos = vCamperPos - (vCamperLook * 15.f);
		//_vec3 vSlasherLook = vCamperPos - vMoriPos;
		//vSlasherLook.y = 0.0f;
		//_vec3 vUp = { 0.0f,1.0f,0.0f };

		//D3DXVec3Normalize(&vSlasherLook, &vSlasherLook);

		//_vec3 vRight = vUp ^ vSlasherLook;

		//pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
		//pTransform->Set_StateInfo(CTransform::STATE_UP, &vUp);
		//pTransform->Set_StateInfo(CTransform::STATE_LOOK, &vSlasherLook);
		//pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vMoriPos);
	}	
	else
	{
		m_pCamper = (CCamper*)m_pGameObject;
		if(nullptr == m_pCamper)
			return FAILED_ACTION;

		m_pSlasher = nullptr;

		GET_INSTANCE_MANAGEMENTR(NOERROR);

		m_pCamperCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
		if (nullptr == m_pCamperCamera)
		{
			Safe_Release(pManagement);
			return FAILED_ACTION;
		}
	
		if (server_data.Slasher.iCharacter == CSlasher::C_SPIRIT)
		{
			CManagement* pManagement = CManagement::GetInstance();
			CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
			CTransform* pSlasherTransform = (CTransform*)pSlasher->Get_ComponentPointer(L"Com_Transform");
			CTransform* pCamperTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
			pCamperTransform->Set_Matrix(pSlasherTransform->Get_Matrix());
		}

		Safe_Release(pManagement);
	}

	if(nullptr == m_pSlasher)
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		if (nullptr == pCamper)
			return FAILED_ACTION;

		pCamper->SetColl(false);
		pCamper->IsLockKey(true);
		m_pCamperCamera->Set_IsLock(true);
		m_pCamperCamera->Set_MoriCamera(true);

		if (server_data.Slasher.iCharacter == CSlasher::C_WRAITH)
		{
			pCamper->Set_State(AC::TW_Mori);
			m_iState = AC::TW_Mori;
		}
		else
		{
			pCamper->Set_State(AC::HK_Mori);
			m_iState = AC::HK_Mori;
		}
	}
	else
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		if (nullptr == pSlasher)
			return FAILED_ACTION;

		pSlasher->IsLockKey(true);		
		pSlasher->Set_State(AS::Mori);
		m_iState = AS::Mori;
	}

	return NOERROR;
}

_int CAction_Mori::Update_Action(const _float & fTimeDelta)
{
	m_fTime += fTimeDelta;
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_pSlasher == nullptr)
	{
		CManagement* pManagement = CManagement::GetInstance();
		CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
		if (server_data.Slasher.iCharacter == CSlasher::C_WRAITH && !m_bIsInit && pSlasher->Get_State() == AS::Mori)
		{
			CTransform* pSlasherTransform = (CTransform*)pSlasher->Get_ComponentPointer(L"Com_Transform");
			CTransform* pCamperTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
			if (nullptr == pSlasherTransform)
				return FAILED_ACTION;

			_matrix matCamperAttach;
			D3DXMatrixIdentity(&matCamperAttach);
			memcpy(&matCamperAttach.m[0], &-*(_vec4*)&pSlasher->Get_CamperAttachMatrix()->m[0], sizeof(_vec4));
			memcpy(&matCamperAttach.m[1], &*(_vec4*)&pSlasher->Get_CamperAttachMatrix()->m[2], sizeof(_vec4));
			memcpy(&matCamperAttach.m[2], &*(_vec4*)&pSlasher->Get_CamperAttachMatrix()->m[1], sizeof(_vec4));
			memcpy(&matCamperAttach.m[3], &*(_vec4*)&pSlasher->Get_CamperAttachMatrix()->m[3], sizeof(_vec4));
			matCamperAttach.m[3][0] = -62.496f;
			matCamperAttach.m[3][1] = 0.f;
			matCamperAttach.m[3][2] = 226.485f;
			m_vOriginPos = *(_vec3*)matCamperAttach.m[3];
			_matrix matSlasher = pSlasherTransform->Get_Matrix();

			matSlasher = matCamperAttach * matSlasher;
			m_bIsInit = true;
			pCamperTransform->Set_Matrix(matSlasher);
		}

		if (pMeshCom->IsOverTime(0.28f))
		{
			m_pCamper->Set_State(AC::DeathGround);
			if (server_data.Slasher.iCharacter == CSlasher::C_WRAITH)
			{
				CTransform* pTransform = (CTransform*)m_pCamper->Get_ComponentPointer(L"Com_Transform");
				pTransform->Set_StateInfo(CTransform::STATE_POSITION, &m_vOriginPos);
			}
			return END_ACTION;
		}
	}
	else
	{
		if (pMeshCom->IsOverTime(0.28f))
		{
			if (CSlasher::C_WRAITH == server_data.Slasher.iCharacter)
				m_pSlasher->Set_State(AS::Idle);

			GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Sacrifice, wstring(L"����"), 1000);
			return END_ACTION;
		}
	}

	return UPDATE_ACTION;
}

void CAction_Mori::End_Action()
{
	//cout << m_fTime << endl;
	m_bIsPlaying = false;

	if (m_pSlasher == nullptr)
	{
		//m_pCamper->SetDead();
		m_pCamperCamera->Set_IsLock(false);
		camper_data.bLive = false;
		m_pCamperCamera->Set_MoriCamera(false);
		((CCamper*)m_pGameObject)->SetCurCondition(CCamper::CONDITION::BLOODDEAD);
		m_pGameObject->SetDead();
	}
	else
	{
		m_pSlasher->IsLockKey(false);
		m_pSlasher->Set_MoriCamper(nullptr);
	}

	m_pCamper = nullptr;
	m_pSlasher = nullptr;
	m_pCamperCamera = nullptr;
}

void CAction_Mori::Send_ServerData()
{
}

void CAction_Mori::Free()
{
	CAction::Free();
}

