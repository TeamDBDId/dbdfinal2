#include "stdafx.h"
#include "..\Headers\Action_Stun_Pallet.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Plank.h"
#include "Camper.h"

_USING(Client)

CAction_Stun_Pallet::CAction_Stun_Pallet()
{
}

HRESULT CAction_Stun_Pallet::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	m_fIndex = 0.f;
	m_fFaintTime = 0.f;
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	m_bIsPlaying = true;
	pSlasher->IsLockKey(true);

	pSlasher->Set_State(AS::Stun_Pallet);
	m_iState = AS::Stun_Pallet;

	return NOERROR;
}

_int CAction_Stun_Pallet::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;
	m_fFaintTime += fTimeDelta;

	_int iIndex = (int)m_fIndex;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;

	if (m_fFaintTime >= 1.5f)
		return END_ACTION;
	
	if (m_iState == AS::Stun_Pallet && pMeshCom->IsOverTime(0.25f))
		pSlasher->Set_State(AS::Idle);
	

	return UPDATE_ACTION;
}

void CAction_Stun_Pallet::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;
	CSlasher* pSlahser = (CSlasher*)m_pGameObject;
	pSlahser->IsLockKey(false);
	pSlahser->Set_Carry(false);
	CCamper* pCarriedCamper = (CCamper*)pSlahser->Get_CarriedCamper();
	if (nullptr != pCarriedCamper)
	{
		pCarriedCamper->SetColl(true);
		pSlahser->Set_CarriedCamper(nullptr);
	}

}

void CAction_Stun_Pallet::Send_ServerData()
{
}

void CAction_Stun_Pallet::Free()
{
	CAction::Free();
}
