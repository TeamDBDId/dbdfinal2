#include "stdafx.h"
#include "..\Headers\Action_SpiderReaction.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Input_Device.h"
#include "MeatHook.h"
#include "Action_HookFree.h"
#include "Action_BeingKilledBySpider.h"
#include "Action_HookBeing.h"
#include "Camera_Camper.h"

_USING(Client)

CAction_SpiderReaction::CAction_SpiderReaction()
{
}

HRESULT CAction_SpiderReaction::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (server_data.Campers[exPlayerNumber - 1].iState == AC::HookBeingCamperEnd
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::HookBeingCamperIn)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;

	pCamper->SetCurCondition(CCamper::HOOKED);
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	m_fSpaceAcc = 0.f;
	m_fDelay = -1.f;
	CManagement* pManagement = CManagement::GetInstance();
	list<CGameObject*>* pObjList = pManagement->Get_ObjList(SCENE_STAGE, L"Layer_GameObject");
	m_bFailedHookBeing = false;
	CTransform* pCamperTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
	_vec3 vCamperPos = *pCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);
	for (auto pHook : *pObjList)
	{
		if (pHook->GetID() & HOOK)
		{
			CTransform* pHookTransform = (CTransform*)pHook->Get_ComponentPointer(L"Com_Transform");
			_vec3 vHookPos = *pHookTransform->Get_StateInfo(CTransform::STATE_POSITION);

			_float fDist = D3DXVec3Length(&(vCamperPos - vHookPos));
			if (fDist <= 300.f)
			{
				m_pHook = (CMeatHook*)pHook;
				m_pHook->Get_CamperAttachMatrix();

				_matrix matCamperAttach;

				memcpy(&matCamperAttach.m[0], &-*(_vec4*)&m_pHook->Get_CamperAttachMatrix()->m[0], sizeof(_vec4));
				memcpy(&matCamperAttach.m[1], &*(_vec4*)&m_pHook->Get_CamperAttachMatrix()->m[2], sizeof(_vec4));
				memcpy(&matCamperAttach.m[2], &*(_vec4*)&m_pHook->Get_CamperAttachMatrix()->m[1], sizeof(_vec4));
				memcpy(&matCamperAttach.m[3], &m_pHook->Get_CamperAttachMatrix()->m[3], sizeof(_vec4));

				_matrix matHook = pHook->Get_Matrix();
				matHook = matCamperAttach * matHook;

				pCamperTransform->Set_Matrix(matHook);

				break;
			}
		}
	}

	CCamper::HOOKSTAGE HookStageOfCamper = pCamper->GetHookStage();


	if (HookStageOfCamper == CCamper::EscapeAttempts)
	{
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_HookStruggle);
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(0.f, 1.5f);
		pCamper->SetEnergy(pCamper->GetHookedEnergy());
		pCamper->SetMaxEnergyAndMaxHeal();
		pCamper->Set_State(AC::TT_Hook_Out);
		m_iState = AC::TT_Hook_Out;
		if(nullptr != m_pHook)
			m_pHook->SetState(CMeatHook::GetHookedOut);
	}
	else
	{
		GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_IdleResist);
		pCamper->Set_State(AC::Spider_Reaction_In);
		m_iState = AC::Spider_Reaction_In;
		if (nullptr != m_pHook)
			m_pHook->SetState(CMeatHook::SpiderReaction_IN);
		pCamper->SetEnergy(pCamper->GetHookedEnergy());
		pCamper->SetMaxEnergyAndMaxHeal();
	}

	m_fStrugleTime = 0.f;
	Send_ServerData();
	return NOERROR;
}

_int CAction_SpiderReaction::Update_Action(const _float & fTimeDelta)
{
	if (server_data.Campers[exPlayerNumber - 1].iState == AC::HookBeingCamperEnd
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::HookBeingCamperIn)
	{

		m_bIsPlaying = false;
		return FAILED_ACTION;
	}	

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;

	pCamper->SetEnergy(pCamper->GetEnergy() - (fTimeDelta));

	if (KEYMGR->KeyDown(DIK_NUMPAD9))
	{
		CAction_HookFree* pAction = (CAction_HookFree*)m_pGameObject->Find_Action(L"Action_HookFree");
		pAction->SetHook(m_pHook);
		m_pGameObject->Set_Action(L"Action_HookFree", 5.f);
		return END_ACTION;
	}

	if (nullptr != pCamper && pCamper->GetCurCondition() == CCamper::INJURED)
		return END_ACTION;

	CCamper::HOOKSTAGE HookStageOfCamper = pCamper->GetHookStage();
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	if (HookStageOfCamper == CCamper::EscapeAttempts)
	{
		if (pCamper->GetEnergy() <= 60.f)
		{
			pCamper->SetHookStage(CCamper::Struggle);
			pCamper->Set_State(AC::Spider_Reaction_In);
			m_iState = AC::Spider_Reaction_In;
			m_pHook->SetState(CMeatHook::SpiderReaction_IN);
		}

		if (m_iState == AC::TT_Hook_Out && pMeshCom->IsOverTime(0.25f))
		{
			pCamper->Set_State(AC::HookedIdle);
			m_iState = AC::HookedIdle;
			if(nullptr != m_pHook)
				m_pHook->SetState(CMeatHook::Idle);;
		}
		else if (m_iState == AC::TT_Hook_Out && KEYMGR->MousePressing(0))
			m_iState = AC::HookedIdle;
		
		if (m_iState == AC::HookedIdle)
		{
			if (m_fDelay >= 0.f)
				m_fDelay -= fTimeDelta;
			else
			{
				if (KEYMGR->MousePressing(CInput_Device::DIM_LBUTTON))
				{
					pCamper->Set_State(AC::HookedStruggle);
					if(m_pHook != nullptr)
						m_pHook->SetState(CMeatHook::Struggle);
					m_fSpaceAcc += fTimeDelta;
				}
				else
				{
					pCamper->Set_State(AC::HookedIdle);
					if (m_pHook != nullptr)
						m_pHook->SetState(CMeatHook::Idle);
					m_fSpaceAcc = 0.f;
				}
				GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fSpaceAcc, 1.5f);
			}
		}
		
		if (m_fSpaceAcc >= 1.5f)
		{
			int iRand = rand() % 100;
			if (iRand > 95)
			{
				m_pHook->SetState(CMeatHook::HookedFree);
				pCamper->SetCurCondition(CCamper::INJURED);
				pCamper->SetHookStage(CCamper::Struggle);
				CAction_HookFree* pAction = (CAction_HookFree*)m_pGameObject->Find_Action(L"Action_HookFree");
				pAction->SetHook(m_pHook);
				m_pGameObject->Set_Action(L"Action_HookFree", 5.f);
				return END_ACTION;
			}
			else
			{
				if (pCamper->GetEnergy() <= 79.9f)
					pCamper->SetEnergy(60.1f);
				else
					pCamper->SetEnergy(pCamper->GetEnergy() - 20.f);
				pCamper->Set_State(AC::HookedIdle);
				if (m_pHook != nullptr)
					m_pHook->SetState(CMeatHook::Idle);
				m_fSpaceAcc = 0.f;
				m_fDelay = 1.f;
			}
		}

	}
	else if (HookStageOfCamper == CCamper::Struggle)
	{
		CManagement* pManagement = CManagement::GetInstance();
		if (m_iState == AC::Spider_Reaction_Out && pMeshCom->IsOverTime(0.25f))
		{
			GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_IdleResist);
			pCamper->Set_State(AC::Spider_Struggle);
			m_iState = AC::Spider_Struggle;
			m_pHook->SetState(CMeatHook::Spider_Struggle);
		}
		else if (m_iState == AC::Spider_Reaction_Loop && pMeshCom->IsOverTime(0.25f))
		{
			GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_IdleResist);
			pCamper->Set_State(AC::Spider_Reaction_Out);
			m_iState = AC::Spider_Reaction_Out;
			m_pHook->SetState(CMeatHook::SpiderReaction_OUT);
		}
		else if (m_iState == AC::Spider_Reaction_In && pMeshCom->IsOverTime(0.25f))
		{
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
			CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0);
			pCamera->Set_MoriCamera(true);
			GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_IdleResist);
			pCamper->Set_State(AC::Spider_Reaction_Loop);
			m_iState = AC::Spider_Reaction_Loop;
			m_pHook->SetState(CMeatHook::SpiderReaction_Loop);
		}

		if (m_iState == AC::Spider_Struggle)
		{
			CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0);
			pCamera->Set_MoriCamera(false);
			GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_ClickResist);
			m_fSpaceAcc += fTimeDelta;
			m_fStrugleTime += fTimeDelta;
			if (pCamper->GetEnergy() <= 0.f)
				pCamper->SetHookStage(CCamper::Sacrifice);

			if (KEYMGR->KeyDown(DIK_SPACE))
				m_fSpaceAcc = 0.f;

			if (m_fSpaceAcc > 1.f)
			{
				pCamper->Set_State(AC::Spider_StruggleToSacrifice);
				pCamper->SetHookStage(CCamper::Sacrifice);
				pCamper->SetEnergy(0.f);
				m_pHook->SetState(CMeatHook::SpiderStruggle2Sacrifice);
			}
		}
	}
	else
	{
		GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_IdleResist);
		CAction_BeingKilledBySpider* pAction = (CAction_BeingKilledBySpider*)m_pGameObject->Find_Action(L"Action_BeingKilledBySpider");
		pAction->SetHook(m_pHook);
		m_pGameObject->Set_Action(L"Action_BeingKilledBySpider", 400.f);
		return END_ACTION;
	}

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_SpiderReaction::End_Action()
{
	if (!m_bFailedHookBeing)
	{
		CManagement* pManagement = CManagement::GetInstance();
		CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0);
		pCamera->Set_MoriCamera(false);
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Survival, wstring(L"����"), _int(m_fStrugleTime * 7.5f));
	}
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetHookedEnergy();
	GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_ResistEnd);
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	m_bIsPlaying = false;
	m_pHook = nullptr;
}

void CAction_SpiderReaction::Send_ServerData()
{
	if (m_pHook != nullptr)
	{
		camper_data.InterationObject = m_pHook->GetID();
		camper_data.InterationObjAnimation = m_pHook->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_SpiderReaction::Free()
{
	CAction::Free();
}
