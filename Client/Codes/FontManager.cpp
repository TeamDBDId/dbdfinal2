#include "stdafx.h"
#include "..\Headers\FontManager.h"

_USING(Client)
_IMPLEMENT_SINGLETON(CFontManager)


CFontManager::CFontManager()
{
	Ready_FontManager();
}

HRESULT CFontManager::Ready_FontManager()
{
	if (FT_Init_FreeType(&m_Library))
		return E_FAIL;

	if (FT_New_Face(m_Library, "../Bin/Resources/Font/NanumGothic.ttf", 0, &m_Face[0]) == FT_Err_Unknown_File_Format)
		return E_FAIL;
	if (FT_New_Face(m_Library, "../Bin/Resources/Font/NanumGothicBold.ttf", 0, &m_Face[1]) == FT_Err_Unknown_File_Format)
		return E_FAIL;
	if (FT_New_Face(m_Library, "../Bin/Resources/Font/NanumGothicExtraBold.ttf", 0, &m_Face[2]) == FT_Err_Unknown_File_Format)
		return E_FAIL;

	return NOERROR;
}

LPDIRECT3DTEXTURE9 CFontManager::FindFont(wstring FontName, _vec2 FontSize, D3DXCOLOR Color, _uint FontNum)
{
	auto& iter = m_FontCache.find(FontName);
	if (iter != m_FontCache.end())
		return iter->second;
	else
	{
		_int					nLength = 0;
		_int					nWidth = 0, nHeight = 0;
		_int					nTWidth = 0, nTHeight = 0;
		_byte					*pDestData = 0;
		_ulong					*TexData = 0;
		FT_GlyphSlot			slot = m_Face[FontNum]->glyph;
		FT_Glyph_Metrics		*pMetrics = 0;
		FT_Bitmap				*pBitmap = &(slot->bitmap);
		FT_Pos					By = 0, Bx = 0, Py = 0, Px = 0;
		LPDIRECT3DSURFACE9		pSurface;
		LPDIRECT3DTEXTURE9		Texture;
		D3DLOCKED_RECT			FontTextureRect;

		FT_Set_Pixel_Sizes(m_Face[FontNum], (_uint)FontSize.x*10, (_uint)FontSize.y*10);

		nLength = (_int)FontName.length();

		for (_int i = 0; i < nLength; i++)
		{
			_int Height;

			FT_Load_Char(m_Face[FontNum], FontName[i], FT_LOAD_RENDER);
			pBitmap = &(slot->bitmap);
			pMetrics = &(slot->metrics);

			// 얻어온 크기를 /12 하여 대입한다.
			Height = pMetrics->height >> 6;

			// 문자의 상단으로 부터의 높이를 구한다.
			By = ((pMetrics->vertAdvance - pMetrics->horiBearingY) - (pMetrics->vertAdvance / 6)) >> 6;

			// 지금까지 지정한 문자보다 더 높이가 크면
			if (nHeight < By + Height)
			{
				// 대입시킨다.
				nHeight = By + Height;
			}

			// 문자열 가로크기 증가시킨다.
			nWidth += pMetrics->horiAdvance >> 6;
		}
		// 얻어온 문자열의 가로,세로 크기로 텍스쳐를 생성한다.
		m_pGraphic_Device->CreateTexture(nWidth, nHeight, 1, 0,
			D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &Texture, 0);

		if (Texture == nullptr)
			return nullptr;
		// 폰트를 그릴 위치를 텍스쳐로 잡는다.
		Texture->GetSurfaceLevel(0, &pSurface);

		// 서페이스를 저급으로 접근하기 위해.
		pSurface->LockRect(&FontTextureRect, NULL, D3DLOCK_DISCARD);

		for (int a = 0; a < nLength; a++)
		{
			FT_Load_Char(m_Face[FontNum], FontName[a], FT_LOAD_RENDER);

			pBitmap = &(slot->bitmap);
			pMetrics = &(slot->metrics);

			pDestData = (_byte*)FontTextureRect.pBits;

			nTWidth = pMetrics->width >> 6;
			nTHeight = pMetrics->height >> 6;

			Bx = pMetrics->horiBearingX >> 6;
			By = ((pMetrics->vertAdvance - pMetrics->horiBearingY) - (pMetrics->vertAdvance / 6)) >> 6;


			for (int i = 0; i < nTHeight; i++)
			{
				TexData = (_ulong*)(&pDestData[(Py + By + i) * FontTextureRect.Pitch]);

				for (int j = 0; j < nTWidth; j++)
				{
					if (pBitmap->buffer[i * pBitmap->width + j])
					{
						TexData[Px + Bx + j] = ((pBitmap->buffer[i * pBitmap->width + j]) << 24) + (_ulong)Color;
					}
				}
			}
			Px += pMetrics->horiAdvance >> 6;
		}
		pSurface->UnlockRect();
		Texture->Release();

		m_nCache++;

		m_FontCache.insert({ FontName, Texture });
		
		return Texture;
	}
}

void CFontManager::ReleaseAllTextures()
{
	for (auto& iter : m_FontCache)
	{
		Safe_Release(iter.second);
	}
	m_FontCache.erase(m_FontCache.begin(), m_FontCache.end());

	m_FontCache.clear();
	m_nCache = 0;
}

void CFontManager::Free()
{
	if (m_Library != nullptr)
	{
		FT_Done_Face(m_Face[0]);
		FT_Done_Face(m_Face[1]);
		FT_Done_Face(m_Face[2]);
		FT_Done_FreeType(m_Library);
	}
	ReleaseAllTextures();
	Safe_Release(m_pGraphic_Device);
}
