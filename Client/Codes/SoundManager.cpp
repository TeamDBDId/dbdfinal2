#include "stdafx.h"
#include "SoundManager.h"
#include <string>

_USING(Client)
_IMPLEMENT_SINGLETON(CSoundManager)

CSoundManager::CSoundManager()
{
	Ready_Sound();
}
void CSoundManager::Ready_Sound()
{
	FMOD_System_Create(&m_pSystem);
	FMOD_System_Init(m_pSystem, 1000, FMOD_INIT_NORMAL, nullptr);
}

void CSoundManager::Update_Sound()
{
	FMOD_System_Update(m_pSystem);
	if (m_iPlayerNum >= 1 && m_iPlayerNum <= 5)
	{
		FMOD_VECTOR Pos, Look;
		FMOD_VECTOR UP = { 0.f, 1.f, 0.f };
		if (m_iPlayerNum == 5)
		{
			Pos = { server_data.Slasher.vPos.x,server_data.Slasher.vPos.y,server_data.Slasher.vPos.z };
			Look = { server_data.Slasher.vLook.x,server_data.Slasher.vLook.y,server_data.Slasher.vLook.z };
		}
		else
		{
			Pos = { server_data.Campers[m_iPlayerNum - 1].vCamPos.x,server_data.Campers[m_iPlayerNum - 1].vCamPos.y,server_data.Campers[m_iPlayerNum - 1].vCamPos.z };
			Look = { server_data.Campers[m_iPlayerNum - 1].vCamLook.x,server_data.Campers[m_iPlayerNum - 1].vCamLook.y,server_data.Campers[m_iPlayerNum - 1].vCamLook.z };
		}
		FMOD_System_Set3DListenerAttributes(m_pSystem, 0, &Pos, nullptr, &Look, &UP);
		
	}
}

void CSoundManager::LoadSound(const string & FileName, _bool Is3D, _bool IsLoop)
{
	auto& iter = m_MapSound.find(FileName);
	if (iter != m_MapSound.end())
		return;
	FMOD_MODE eMode = FMOD_DEFAULT;
	eMode |= Is3D ? FMOD_3D| FMOD_3D_LINEARROLLOFF : FMOD_2D;
	eMode |= IsLoop ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;

	string FilePath = Is3D ? "../Bin/Resources/Sound/3D/" : "../Bin/Resources/Sound/2D/";
	FilePath += FileName;

	FMOD_SOUND* pSound = nullptr;
	FMOD_System_CreateSound(m_pSystem, FilePath.c_str(), eMode, nullptr, &pSound);
	
	m_MapSound.insert({ FileName, pSound });
}

void CSoundManager::ChannelVolume(const _int & ChannelNum, const _float & fVolume)
{
	auto iter = m_MapChannel.find(ChannelNum);
	FMOD_CHANNEL* pChannel= iter->second;

	FMOD_Channel_SetPaused(pChannel, true);
	FMOD_Channel_SetVolume(pChannel, fVolume);
	FMOD_Channel_SetPaused(pChannel, false);
	Update_Sound();
}

void CSoundManager::ChannelVolume(const string & ChannelName, const _float & fVolume)
{
	auto iter = m_MapNameChannel.find(ChannelName);
	FMOD_CHANNEL* pChannel = iter->second;

	FMOD_Channel_SetPaused(pChannel, true);
	FMOD_Channel_SetVolume(pChannel, fVolume);
	FMOD_Channel_SetPaused(pChannel, false);
	Update_Sound();
}

void CSoundManager::ChannelStop(const _int & ChannelNum)
{
	auto iter = m_MapChannel.find(ChannelNum);
	if (iter == m_MapChannel.end())
		return;
	FMOD_CHANNEL* pChannel = iter->second;
	FMOD_Channel_SetVolume(pChannel, 0.f);
	FMOD_Channel_SetPaused(pChannel, true);
	Update_Sound();
}

void CSoundManager::ChannelStop(const string& ChannelName)
{
	auto iter = m_MapNameChannel.find(ChannelName);
	if (iter == m_MapNameChannel.end())
		return;
	FMOD_CHANNEL* pChannel = iter->second;
	FMOD_Channel_SetVolume(pChannel, 0.f);
	FMOD_Channel_SetPaused(pChannel, true);
	Update_Sound();
}

_int CSoundManager::PlaySound(const string & FileName, const _vec3& vPosition, const _float& fMaxDistance, const _float& fVolume)
{
	auto iter = m_MapSound.find(FileName);
	if (m_MapSound.end() == iter)
		return -1;
	_float Volume = fVolume;
	FMOD_CHANNEL* pChannel = nullptr;
	FMOD_System_PlaySound(m_pSystem, iter->second, nullptr,  TRUE,	&pChannel);
	if (pChannel != nullptr)
	{
		FMOD_MODE CurMode;
		FMOD_Sound_GetMode(iter->second, &CurMode);
		if (CurMode& FMOD_3D)
		{
			Volume *= 1.5f;
			FMOD_VECTOR Pos = { vPosition.x,vPosition.y,vPosition.z };
			FMOD_Channel_Set3DMinMaxDistance(pChannel, 0.f, fMaxDistance);
			FMOD_Channel_Set3DAttributes(pChannel, &Pos, nullptr);
		}
		FMOD_Channel_SetVolume(pChannel, Volume);
		FMOD_Channel_SetPaused(pChannel,false);
		m_MapChannel.insert({ m_iChannelNum ,pChannel });
	}
	return m_iChannelNum++;
}

_int CSoundManager::PlaySound(const string & ChannelName, const string & FileName, const _vec3 & vPosition, const _float & fMaxDistance, const _float & fVolume)
{
	auto iter = m_MapSound.find(FileName);
	if (m_MapSound.end() == iter)
		return -1;
	_float Volume = fVolume;
	FMOD_CHANNEL* pChannel = nullptr;
	FMOD_System_PlaySound(m_pSystem, iter->second, nullptr, TRUE, &pChannel);
	if (pChannel != nullptr)
	{
		FMOD_MODE CurMode;
		FMOD_Sound_GetMode(iter->second, &CurMode);
		if (CurMode& FMOD_3D)
		{
			Volume *= 1.5f;
			FMOD_VECTOR Pos = { vPosition.x,vPosition.y,vPosition.z };
			FMOD_Channel_Set3DMinMaxDistance(pChannel, 0.f, fMaxDistance);
			FMOD_Channel_Set3DAttributes(pChannel, &Pos, nullptr);
		}
		FMOD_Channel_SetVolume(pChannel, Volume);
		FMOD_Channel_SetPaused(pChannel, false);

		auto& iter = m_MapNameChannel.find(ChannelName);
		if (iter != m_MapNameChannel.end())
		{
			FMOD_Channel_SetPaused(iter->second, true);
			m_MapNameChannel.erase(ChannelName);
		}
		m_MapNameChannel.insert({ ChannelName, pChannel });
	}
	return 0;
}

void CSoundManager::Load_LobbySound()
{
	LoadSound(string("Lobby/mu_lobby_dlc_mali_01.ogg"), false, true);
	LoadSound(string("Lobby/Ambience_Menus_am_burning_02.ogg"), false, true);
	LoadSound(string("Lobby/Ambience_Menus_am_lobby_01.ogg"), false, true);
	LoadSound(string("Lobby/Ambience_Menus_am_ui_background_04.ogg"), false, true);
	LoadSound(string("Lobby/BlackSmoke_Appear_VFX_Bounce_01.ogg"), false);
	LoadSound(string("Lobby/BlackSmoke_Appear_VFX_Bounce_02.ogg"), false);

	LoadSound(string("Lobby/Lobby_Start.ogg"), false);
	LoadSound(string("Lobby/Heart_Bounce.ogg"), false);

	LoadSound(string("Lobby/ui_select_press_01.ogg"), false);
	LoadSound(string("Lobby/ui_select_press_bones_01.ogg"), false);

	LoadSound(string("Lobby/ui_equip_01.ogg"), false);
	LoadSound(string("Lobby/ui_unequip_01.ogg"), false);

	LoadSound(string("Logo/theentity_long_02.ogg"), false);
	LoadSound(string("Logo/Logo.ogg"), false, true);

	LoadSound(string("Lobby/theentity_short_01.ogg"), false);
	LoadSound(string("Lobby/theentity_short_02.ogg"), false);
	LoadSound(string("Lobby/Ambience_Menus_am_ui_roleselection_01.ogg"), false, true);

	LoadSound(string("Lobby/BlackSmoke_Dissolve_Bounce_01.ogg"), false);
	LoadSound(string("Lobby/BlackSmoke_Dissolve_Bounce_02.ogg"), false);
	LoadSound(string("Lobby/BlackSmoke_Dissolve_Bounce_03.ogg"), false);
}

void CSoundManager::Load_StageSound()
{
	LoadSound(string("Logo/theentity_long_01.ogg"), false);
	LoadSound(string("Logo/theentity_long_02.ogg"),false);
	// FootStep
	LoadSound(string("Camper/footsteps_barefoot_bush_01.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_02.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_03.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_04.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_05.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_06.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_07.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_08.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_bush_09.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_concrete_run_01.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_concrete_run_02.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_concrete_walk_01.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_concrete_walk_02.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_dirt_run_01.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_dirt_run_02.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_dirt_walk_01.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_dirt_walk_02.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_wood_run_01.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_wood_run_02.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_wood_walk_01.ogg"));
	LoadSound(string("Camper/footsteps_barefoot_wood_walk_02.ogg"));

	// Generator Repair
	LoadSound(string("Generator/generator_repair_crank_01.ogg"));
	LoadSound(string("Generator/generator_repair_crank_02.ogg"));
	LoadSound(string("Generator/generator_repair_crank_03.ogg"));
	LoadSound(string("Generator/generator_repair_crank_04.ogg"));
	LoadSound(string("Generator/generator_repair_crank_05.ogg"));
	LoadSound(string("Generator/generator_repair_crank_06.ogg"));
	LoadSound(string("Generator/generator_repair_electric_01.ogg"));
	LoadSound(string("Generator/generator_repair_electric_02.ogg"));
	LoadSound(string("Generator/generator_repair_electric_03.ogg"));
	LoadSound(string("Generator/generator_repair_electric_04.ogg"));
	LoadSound(string("Generator/generator_repair_electric_05.ogg"));
	LoadSound(string("Generator/generator_repair_pull_01.ogg"));
	LoadSound(string("Generator/generator_repair_pull_02.ogg"));
	LoadSound(string("Generator/generator_repair_pull_03.ogg"));
	LoadSound(string("Generator/generator_repair_pull_04.ogg"));
	LoadSound(string("Generator/generator_repair_pull_05.ogg"));
	LoadSound(string("Generator/generator_repair_push_01.ogg"));
	LoadSound(string("Generator/generator_repair_push_02.ogg"));
	LoadSound(string("Generator/generator_repair_push_03.ogg"));
	LoadSound(string("Generator/generator_repair_push_04.ogg"));

	// Plank
	LoadSound(string("Camper/body_rush_slide_wood_02.ogg"));
	LoadSound(string("Camper/body_rush_slide_wood_03.ogg"));

	// Window
	LoadSound(string("Window/windows_hit_01.ogg"));
	LoadSound(string("Window/windows_bloc_start_01.ogg"));

	// Camper_Male
	// PickUp
	LoadSound(string("Camper/M_HookOut.ogg"));
	LoadSound(string("Camper/M_Wiggle.ogg"));
	LoadSound(string("Camper/M_WiggleBreathing.ogg"));
	LoadSound(string("Camper/M_DeadHard.ogg"));
	LoadSound(string("Camper/M_Detectched.ogg"));
	LoadSound(string("Camper/M_Crying.ogg"));
	LoadSound(string("Camper/M_HookedScream.ogg"), false);
	LoadSound(string("Camper/M_Suprised.ogg"));
	LoadSound(string("Camper/M_Attacked.ogg"));
	LoadSound(string("Camper/M_Attacked2D.ogg"), false);
	LoadSound(string("Camper/M_BeingHeal.ogg"));
	LoadSound(string("Camper/M_SpiderReaction.ogg"));
	LoadSound(string("Camper/M_HookOutLand.ogg"));
	LoadSound(string("Camper/M_HealFailed.ogg"));

	LoadSound(string("Camper/FailedSound.ogg"), false);
	LoadSound(string("Camper/Female_LongScream.ogg"), false);
	LoadSound(string("Camper/Male_LongScream.ogg"), false);
	LoadSound(string("Camper/Female_Scream.ogg"), false);
	LoadSound(string("Camper/Male_Scream.ogg"), false);
	// Camper_Female
	// PickUp
	LoadSound(string("Camper/F_HookOut.ogg"));
	LoadSound(string("Camper/F_Wiggle.ogg"));
	LoadSound(string("Camper/F_DeadHard.ogg"));
	LoadSound(string("Camper/F_Suprised.ogg"));
	LoadSound(string("Camper/F_Crying.ogg"));
	LoadSound(string("Camper/F_Attacked.ogg"));
	LoadSound(string("Camper/F_Attacked2D.ogg"), false);
	LoadSound(string("Camper/F_HookIn.ogg"), false);
	LoadSound(string("Camper/HookedSound.ogg"));
	LoadSound(string("Camper/F_BreathIn.ogg"));
	LoadSound(string("Camper/F_DeepBreathing.ogg"));
	LoadSound(string("Camper/F_SpiderReaction.ogg"));
	LoadSound(string("Camper/F_HookOutLand.ogg"));
	LoadSound(string("Camper/F_HealFailed.ogg"));
	LoadSound(string("Camper/F_WiggleBreathing.ogg"));

	//Generator
	LoadSound(string("Generator/Generator_1.mp3"));
	LoadSound(string("Generator/Generator_2.mp3"));
	LoadSound(string("Generator/Generator_3.mp3"));
	LoadSound(string("Generator/Generator_4.mp3"));
	LoadSound(string("Generator/Generator_On.mp3"));
	LoadSound(string("IngameUI/generator_explode_01.ogg"),false);
	LoadSound(string("IngameUI/GeneratorFin.ogg"), false);
	LoadSound(string("Generator/generator_explode_02.ogg"));

	LoadSound(string("MeatHook/meathook_mechanism_break_02.ogg"));
	LoadSound(string("MeatHook/meathook_metal_drop_01.ogg"));
	LoadSound(string("MeatHook/meathook_metal_drop_02.ogg"));


	//Closet
	LoadSound(string("Closet/closet_squeak_01.ogg"));
	LoadSound(string("Closet/closet_open_slow_01.ogg"));
	LoadSound(string("Closet/closet_close_slow_01.ogg"));
	LoadSound(string("Closet/closet_impact_wood_hit_01.ogg"));
	LoadSound(string("Closet/closet_open_slow_02.ogg"));
	LoadSound(string("Closet/closet_open_fast_01.ogg"));
	LoadSound(string("Closet/closet_close_fast_01.ogg"));

	// Window
	LoadSound(string("Window/windows_hit_01.ogg"));
	LoadSound(string("Window/windows_bloc_start_01.ogg"));

	//Plank
	LoadSound(string("Plank/plank_land_01.ogg"));
	LoadSound(string("Plank/plank_break_hit_03.ogg"));

	//Chest
	LoadSound(string("Chest/chest_open_01.mp3"));
	LoadSound(string("Chest/chest_open_02.mp3"));
	LoadSound(string("Chest/chest_open_03.mp3"));
	LoadSound(string("Chest/chest_open_04.mp3"));
	LoadSound(string("Chest/chest_open_05.mp3"));
	LoadSound(string("Chest/chest_open_06.mp3"));
	LoadSound(string("Chest/chest_open_07.mp3"));
	LoadSound(string("Chest/chest_close_01.mp3"));

	//ExitDoor
	LoadSound(string("ExitDoor/exitdoor_switch_lever_pull_01.ogg"));
	LoadSound(string("ExitDoor/exitdoor_switch_lever_pull_02.ogg"));
	LoadSound(string("ExitDoor/exitdoor_switch_lever_pull_03.ogg"));
	LoadSound(string("ExitDoor/exitdoor_switch_mechanic_01.ogg"));
	LoadSound(string("ExitDoor/exitdoor_spark_short_01.ogg"));
	LoadSound(string("ExitDoor/exitdoor_spark_short_02.ogg"));
	LoadSound(string("ExitDoor/exitdoor_spark_short_03.ogg"));
	LoadSound(string("ExitDoor/exitdoor_spark_short_04.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_01.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_02.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_03.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_04.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_background_01.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_background_02.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_background_03.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_mechanic_background_04.ogg"));
	LoadSound(string("ExitDoor/exitdoor_alarm_02.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_stop_bounce.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_start_bounce_01.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_start_bounce_02.ogg"));
	LoadSound(string("ExitDoor/exitdoor_open_start_bounce_03.ogg"));

	//Totem
	LoadSound(string("Totem/totem_sabotage_bones_01.ogg"));
	LoadSound(string("Totem/totem_sabotage_bones_02.ogg"));
	LoadSound(string("Totem/totem_sabotage_bones_03.ogg"));
	LoadSound(string("Totem/totem_sabotage_bones_04.ogg"));
	LoadSound(string("Totem/totem_sabotage_bones_05.ogg"));
	LoadSound(string("Totem/totem_sabotage_bones_06.ogg"));
	LoadSound(string("Totem/totem_fire_short_01.ogg"));
	LoadSound(string("Totem/totem_fire_short_02.ogg"));
	LoadSound(string("Totem/totem_fire_short_03.ogg"));

	//Barrel
	LoadSound(string("Barrel/Barrel.ogg"), true, true);

	//Music
	LoadSound(string("Lobby/Heart_Bounce.ogg"), false);

	LoadSound(string("Ingame_Music/Farm_SlaughterHouse_Bounce.ogg"), false, true);
	LoadSound(string("Ingame_Music/AmbienceTilesam_basement_02b.ogg"), false, true);
	LoadSound(string("Ingame_Music/Ambience_Animals_sfx_wolves_01.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Animals_sfx_wolves_02.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Animals_sfx_wolves_03.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Animals_sfx_wolves_04.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Animals_sfx_wolves_06.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Emitters_sfx_crow_01.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Emitters_sfx_crow_02.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Emitters_sfx_crow_03.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Emitters_sfx_crow_04.ogg"), false);
	LoadSound(string("Ingame_Music/Ambience_Emitters_sfx_crow_05.ogg"), false);

	LoadSound(string("Situation/mu_survivor_normal_01.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_normal_02.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_normal_03.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_injured_01.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_injured_02.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_injured_03.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_dying_01.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_dying_02.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_dying_03.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_dying_04.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_captured_01.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_captured_02.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_dead_01.ogg"), false);
	LoadSound(string("Situation/mu_survivor_dead_02.ogg"), false);
	LoadSound(string("Situation/mu_survivor_hooked_part1_01.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_hooked_part1_02.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_hooked_part1_03.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_hooked_part2_01.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_hooked_part2_02.ogg"), false, true);
	LoadSound(string("Situation/mu_survivor_tallyscreen_01.ogg"), false, true);
	LoadSound(string("Situation/mu_victory_01.ogg"), false);
	LoadSound(string("Situation/mu_defeated_01.ogg"), false);
	
	LoadSound(string("Situation/mu_killer_carrying_01.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_carrying_02.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_carrying_03.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_exit_01.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_exit_02.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_exit_03.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_normal_01.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_normal_02.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_normal_03.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_tallyscreen_01.ogg"), false, true);
	
	LoadSound(string("Situation/mu_killer_chase_01.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_chase_02.ogg"), false, true);
	LoadSound(string("Situation/mu_killer_chase_03.ogg"), false, true);
	
	//SkillCheck
	LoadSound(string("Ingame/SkillCheck_End.ogg"), false);
	LoadSound(string("Ingame/SkillCheck_Start.ogg"), false);

	//Slasher
	LoadSound(string("Slasher/Wraith/Belling.ogg"));
	LoadSound(string("Slasher/Wraith/Dissappear.ogg"));
	LoadSound(string("Slasher/Wraith/appear.ogg"));
	LoadSound(string("Slasher/Wraith/Stun.ogg"));
	LoadSound(string("Slasher/Wraith/Swing.ogg"));
	LoadSound(string("Slasher/Wraith/Wipe.ogg"));
	LoadSound(string("Slasher/Spirit/PullOut.ogg"));
	LoadSound(string("Slasher/body_rush_hit_wood_02.ogg"));
	for (_int i = 0; i<10; i++)
		LoadSound(string("Slasher/Wraith/Normal/W_Normal") + to_string(i) + string(".ogg"));
	LoadSound(string("Slasher/Spirit/Normal_Loop.ogg"), false);
	LoadSound(string("Slasher/Spirit/Attack.ogg"));
	LoadSound(string("Slasher/Spirit/Dissapear.ogg"));
	LoadSound(string("Slasher/Spirit/Hurt.ogg"));
	LoadSound(string("Slasher/Spirit/S_Appear.ogg"));
	LoadSound(string("Slasher/Spirit/Scream.ogg"));
	LoadSound(string("Slasher/Spirit/Swing.ogg"));
	LoadSound(string("Slasher/Spirit/Wipe.ogg"));
	LoadSound(string("Slasher/Spirit/HitCamper.ogg"));
	LoadSound(string("Slasher/Spirit/PullOut.ogg"));
	LoadSound(string("Slasher/Spirit/Stab.ogg"));
	LoadSound(string("Slasher/Spirit/S_Power.ogg"));
	LoadSound(string("Slasher/body_rush_hit_wood_02.ogg"));
	LoadSound(string("Spirit/spirit_normal_loop_01.ogg"),false,true);

	for (_int i = 1; i < 4; i++)
	{
		LoadSound(string("Slasher/Wraith/invisibility_off_ghost_0")+to_string(i) +string(".ogg"));
		LoadSound(string("Slasher/Wraith/invisibility_on_ghost_0")+ to_string(i) +string(".ogg"));
	}
	for (_int i = 1; i<14; i++)
		LoadSound(string("Slasher/Spirit/spirit_normal_") + to_string(i) + string(".ogg"));
	LoadSound(string("Crow/Crowfly.ogg"));

	LoadSound(string("Key/keys_move_01.ogg"));
	LoadSound(string("Key/keys_move_02.ogg"));
	LoadSound(string("Key/keys_move_03.ogg"));

	LoadSound(string("Medkit/medkit_get_01.ogg"));
	LoadSound(string("Medkit/medkit_get_02.ogg"));
	LoadSound(string("Medkit/medkit_get_03.ogg"));

	LoadSound(string("Toolkit/toolkit_get_01.ogg"));
	LoadSound(string("Toolkit/toolkit_get_02.ogg"));
	LoadSound(string("Toolkit/toolkit_get_03.ogg"));

	LoadSound(string("Flashlight/flashlight_get_01.ogg"));
	LoadSound(string("Flashlight/flashlight_get_02.ogg"));
	LoadSound(string("Flashlight/flashlight_get_03.ogg"));
	LoadSound(string("Flashlight/flashlight_get_04.ogg"));
	LoadSound(string("Flashlight/flashlight_off_01.ogg"));
	LoadSound(string("Flashlight/flashlight_on_01.ogg"));

	//Perk
	LoadSound(string("Ingame/perks_premonition.ogg"),false);
	for(_int i = 0; i<30; i++)
		LoadSound(string("Male_Injured/Male_Injured") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<54; i++)
		LoadSound(string("Male_Dying/Male_Dying") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<19; i++)
		LoadSound(string("Male_Normal/Male_Normal") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<45; i++)
		LoadSound(string("Male_Hooked/Male_Hooked") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<9; i++)
		LoadSound(string("Male_Hurt_Hard/Male_Hurt_Hard") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<32; i++)
		LoadSound(string("Female_Hooked/Female_Hooked") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<34; i++)
		LoadSound(string("Female_Dying/Female_Dying") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<39; i++)
		LoadSound(string("Female_Injured/Female_Injured") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<22; i++)
		LoadSound(string("Female_Normal/Female_Normal") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<21; i++)
		LoadSound(string("Female_Running/Female_Running") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<32; i++)
		LoadSound(string("Female_Hurt_Hard/Female_Hurt_Hard") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<8; i++)
		LoadSound(string("Slasher/Body_Hit/Body_Hit") + to_string(i) + string(".ogg"));
	for (_int i = 0; i<9; i++)
		LoadSound(string("Slasher/Body_Cut/Body_Cut") + to_string(i) + string(".ogg"));
	LoadSound(string("Hatch/Hatch.ogg"), true, true);

	LoadSound(string("MeatHook/TheEntity_3D_DaddyFinger_Explosion_Bounce_02.ogg"));
}

void CSoundManager::Delete_AllSound()
{
	for (auto& iter : m_MapSound)
		FMOD_Sound_Release(iter.second);
	m_MapSound.clear();
}

void CSoundManager::Free()
{
	Delete_AllSound();
	FMOD_System_Release(m_pSystem);
	FMOD_System_Close(m_pSystem);
}
