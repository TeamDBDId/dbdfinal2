#include "stdafx.h"
#include "Effect_Fail.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_Fail::CEffect_Fail(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_Fail::CEffect_Fail(const CEffect_Fail & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Fail::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Fail::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;



	m_pTransformCom->Scaling(500.f, 500.f, 0.f);


	return NOERROR;
}

_int CEffect_Fail::Update_GameObject(const _float & _fTick)
{
	if (true == m_isDead)
		return 1;

	m_fTime += _fTick;
	if (5.f < m_fTime)
	{
		m_isDead = true;
	}





	return _int();
}

_int CEffect_Fail::LastUpdate_GameObject(const _float & _fTick)
{
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	if (m_fCameraDistance < 500.f)
		return NOERROR;

	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;

	
	vRight = *(_vec3*)&matView.m[0][0] * m_pTransformCom->Get_Scale().x;
	vUp = *(_vec3*)&matView.m[1][0] * m_pTransformCom->Get_Scale().y;
	vLook = *(_vec3*)&matView.m[2][0] * m_pTransformCom->Get_Scale().z;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
	




	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_Fail::Render_GameObject()
{
	for (size_t i = 0; i < 2; ++i)
	{


		if (nullptr == m_pBufferCom)
			return;

		// 셰이더를 이용해서 그려. 
		LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();


		if (FAILED(SetUp_ContantTable(pEffect,i)))
			return;

		pEffect->Begin(nullptr, 0);
		pEffect->BeginPass(_E_FAIL1+i);

		m_pBufferCom->Render_VIBuffer();

		pEffect->EndPass();
		pEffect->End();

		Safe_Release(pEffect);


	}

}

void CEffect_Fail::Set_Param(const _vec3 & _vPos, const void * _pVoid)
{
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vPos);
}




HRESULT CEffect_Fail::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Effect");


	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;
	

	m_pTextureCom[0] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_BG_Smoke");
	if (FAILED(Add_Component(L"Com_Texture0", m_pTextureCom[0])))
		return E_FAIL;

	m_pTextureCom[1] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fail_Radical");
	if (FAILED(Add_Component(L"Com_Texture1", m_pTextureCom[1])))
		return E_FAIL;
	m_pTextureCom[2] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fail_Gradiant");
	if (FAILED(Add_Component(L"Com_Texture2", m_pTextureCom[2])))
		return E_FAIL;
	m_pTextureCom[3] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fail_Noise");
	if (FAILED(Add_Component(L"Com_Texture3", m_pTextureCom[3])))
		return E_FAIL;
	m_pTextureCom[4] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fail_TendrilMask");
	if (FAILED(Add_Component(L"Com_Texture4", m_pTextureCom[4])))
		return E_FAIL;


	Safe_Release(pManagement);
	return NOERROR;
}

//0백그라운드
//1
HRESULT CEffect_Fail::SetUp_ContantTable(LPD3DXEFFECT _pEffect, const _uint& _iIdx)
{


	CBaseEffect::SetUp_ContantTable(_pEffect);


	_float m_fSize = 0.f;
	if (m_fTime < 0.75f)
	{
		m_fBGSize = sqrtf(m_fTime*1.33334f);
		m_fSize = 0.75f - m_fTime;
	}
	else if (4.25f < m_fTime)
	{
		m_fBGSize = sqrtf((5-m_fTime)*1.33334f);
		m_fSize = m_fTime - 4.25f;
	}
	else
		m_fSize = 0.f;


	_pEffect->SetFloat("g_fSize", m_fSize);


	_float m_fSin = (sinf(m_fTime*2.512f)*0.1f) + 0.6f;
	_pEffect->SetFloat("g_fSin", m_fSin);



	_float fDist = m_fCameraDistance*0.001f;

	if (0 == _iIdx)
	{
		fDist *= m_fBGSize;
		m_pTextureCom[0]->SetUp_OnShader(_pEffect, "g_Tex0", _uint(m_fTime*30.f) % 30);
	}
	else
	{
		m_pTextureCom[1]->SetUp_OnShader(_pEffect, "g_Tex0");
		m_pTextureCom[2]->SetUp_OnShader(_pEffect, "g_Tex1");
		m_pTextureCom[3]->SetUp_OnShader(_pEffect, "g_Tex2");
		m_pTextureCom[4]->SetUp_OnShader(_pEffect, "g_Tex3");
		fDist *= 0.4f;
	}


	_matrix matWorld = m_pTransformCom->Get_Matrix();
	matWorld._11 *= fDist;
	matWorld._12 *= fDist;
	matWorld._13 *= fDist;
	matWorld._21 *= fDist;
	matWorld._22 *= fDist;
	matWorld._23 *= fDist;
	_pEffect->SetMatrix("g_matWorld", &matWorld);



	_pEffect->SetFloat("g_fTime", m_fTime);


	


	return NOERROR;
}

CEffect_Fail * CEffect_Fail::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Fail*	pInst = new CEffect_Fail(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}


CGameObject * CEffect_Fail::Clone_GameObject()
{
	CEffect_Fail*	pInst = new CEffect_Fail(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("FailEffect Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Fail::Free()
{
	Safe_Release(m_pBufferCom);


	Safe_Release(m_pTransformCom);

	for (size_t i = 0; i<TEXCNT; ++i)
		Safe_Release(m_pTextureCom[i]);



	CBaseEffect::Free();
}
