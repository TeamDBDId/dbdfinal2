#include "stdafx.h"
#include "..\Headers\Action_LootChest.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Chest.h"

_USING(Client)

CAction_LootChest::CAction_LootChest()
{
}

HRESULT CAction_LootChest::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	m_fIndex = 0.f;
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Chest);
	pCamper->Set_State(AC::LootChestOpen);
	m_LootTime = 0.f;
	m_fOverTime = 0.f;
	m_iState = AC::LootChestOpen;
	if (m_pChest != nullptr)
		m_pChest->SetState(CChest::Open);

	return NOERROR;
}

_int CAction_LootChest::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_LootTime += fTimeDelta;
	m_fIndex += fTimeDelta * 30.f;
	_int iIndex = (int)m_fIndex; 

	if (m_iState == 500)
		m_fOverTime += fTimeDelta;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (nullptr == pCamper)
		return END_ACTION;

	if (m_iState == 500 &&  m_fOverTime > 0.1f)
		return END_ACTION;
	else if (m_iState == AC::LootChestOut && pMeshCom->IsOverTime(0.3f))
	{
		pCamper->Set_State(AC::Idle);
		m_pChest->SetState(CChest::Idle);
		camper_data.InterationObject = m_pChest->GetID();
		camper_data.InterationObjAnimation = CChest::Idle;
		m_iState = 500;
	}

	if (m_iState == AC::LootChestOpen && pMeshCom->IsOverTime(0.3f))
	{
		pCamper->Set_State(AC::LootChest);
		m_iState = AC::LootChest;
		m_pChest->SetState(CChest::IdleOpen);
	}

	if (m_pChest->Get_ProgressTime() >= m_pChest->Get_MaxProgressTime())
	{
		pCamper->Set_State(AC::Idle);
		m_pChest->SetState(CChest::IdleOpen);
		return END_ACTION;
	}

	if (m_iState != 500 && (pCamper->GetCurCondition() == CCamper::DYING || pCamper->GetOldCondition() == CCamper::DYING))
	{
		pCamper->Set_State(AC::LootChestOut);
		m_iState = AC::LootChestOut;
		m_pChest->SetState(CChest::Closing);
	}
	if (m_iState == AC::LootChest)
	{
		if (!KEYMGR->MousePressing(0))
		{
			pCamper->Set_State(AC::LootChestOut);
			m_iState = AC::LootChestOut;
			m_pChest->SetState(CChest::Closing);
		}
	}


	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_LootChest::End_Action()
{
	GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Objective, wstring(L"���� ����"), (_int)(250.f*(m_LootTime / m_pChest->Get_MaxProgressTime())));
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->IsLockKey(false);
	m_pChest = nullptr;
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	Send_ServerData();
}

void CAction_LootChest::Send_ServerData()
{
	if (m_pChest != nullptr)
	{
		camper_data.InterationObject = m_pChest->GetID();
		camper_data.InterationObjAnimation = m_pChest->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_LootChest::Free()
{
	CAction::Free();
}
