#include "stdafx.h"
#include "..\Headers\Camera_Camper.h"
#include "Management.h"
#include "Camera_Slasher.h"
#include "Slasher.h"

_USING(Client)

CCamera_Camper::CCamera_Camper(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CCamera(pGraphic_Device)
{
}

CCamera_Camper::CCamera_Camper(const CCamera_Camper & rhs)
	: CCamera(rhs)
{

}

HRESULT CCamera_Camper::Ready_Prototype()
{
	if (FAILED(CCamera::Ready_Prototype()))
		return E_FAIL;


	return NOERROR;
}

HRESULT CCamera_Camper::Ready_GameObject()
{
	if (FAILED(CCamera::Ready_GameObject()))
		return E_FAIL;

	if (FAILED(CCamera_Camper::Ready_Component()))
		return E_FAIL;


	m_pTransform->SetUp_Speed(5.f, D3DXToRadian(360.f));

	D3DVIEWPORT9			ViewPort;
	m_pGraphic_Device->GetViewport(&ViewPort);


	m_ptMouse.x = ViewPort.Width >> 1;
	m_ptMouse.y = ViewPort.Height >> 1;

	ClientToScreen(g_hWnd, &m_ptMouse);

	m_MoveY = 15;
	m_MoveX = -180;
	m_bStart = false;
	m_bMoriCamera = false;
	bObserver = false;

	m_fCollDist = 200.f;
	m_vCamPos = { 0.f, 140.f, 0.f };
	return NOERROR;
}

_int CCamera_Camper::Update_GameObject(const _float & fTimeDelta)
{
	//SetCursorPos(m_ptMouse.x, m_ptMouse.y);
	CommunicateWithServer();

	if (m_IsLock)
		return 0;

	if (m_bMoriCamera)
	{
		if (nullptr == m_MoriCamera
			|| nullptr == m_PlayerMat)
			return 0;

		//_matrix matRot;
		//D3DXMatrixRotationY(&matRot, D3DXToRadian(90.f));
		//_matrix matMori = *m_MoriCamera * matRot;
		//_matrix pMoriCam = matMori * *m_PlayerMat;

		//m_pTransform->Set_Matrix(pMoriCam);
		_matrix matCamera;

		//memcpy(&matCamera.m[0], &-*(_vec4*)&m_MoriCamera->m[0], sizeof(_vec4));
		//memcpy(&matCamera.m[1], &*(_vec4*)&m_MoriCamera->m[2], sizeof(_vec4));
		//memcpy(&matCamera.m[2], &*(_vec4*)&m_MoriCamera->m[1], sizeof(_vec4));
		//memcpy(&matCamera.m[3], &m_MoriCamera->m[3], sizeof(_vec4));

		memcpy(&matCamera.m[0], &*(_vec4*)&m_MoriCamera->m[0], sizeof(_vec4));
		memcpy(&matCamera.m[1], &*(_vec4*)&m_MoriCamera->m[2], sizeof(_vec4));
		memcpy(&matCamera.m[2], &-*(_vec4*)&m_MoriCamera->m[1], sizeof(_vec4));
		memcpy(&matCamera.m[3], &m_MoriCamera->m[3], sizeof(_vec4));

		matCamera *= *m_PlayerMat;

		m_pTransform->Set_Matrix(matCamera);

		Invalidate_ViewProjMatrix();

		m_pFrustumCom->Transform_ToWorld();

		return 0;
	}

	if (bObserver)
	{
		_matrix matObserverCamera;
		D3DXMatrixIdentity(&matObserverCamera);

		memcpy(&matObserverCamera.m[0], server_data.Campers[exiObservPlayer-1].vCamRight, sizeof(_vec3));
		memcpy(&matObserverCamera.m[1], server_data.Campers[exiObservPlayer-1].vCamUp, sizeof(_vec3));
		memcpy(&matObserverCamera.m[2], server_data.Campers[exiObservPlayer-1].vCamLook, sizeof(_vec3));
		memcpy(&matObserverCamera.m[3], server_data.Campers[exiObservPlayer-1].vCamPos, sizeof(_vec3));

		m_pTransform->Set_Matrix(matObserverCamera);

		Invalidate_ViewProjMatrix();

		m_pFrustumCom->Transform_ToWorld();
		return 0;
	}

	if (nullptr == m_PlayerRoot)
		return 0;

	if (nullptr == m_pInput_Device)
		return -1;

	//if (false == m_bIsControl)
	//	return 1;

	if (m_bLock && nullptr != m_pLockMatrix)
	{
		m_pTransform->Set_Matrix(*m_pLockMatrix);

		Invalidate_ViewProjMatrix();

		m_pFrustumCom->Transform_ToWorld();

		return 0;
	}

	if (m_bStart)
	{
		if (m_MoveX -= (m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_X) * 0.2f))
		{
			m_pTransform->Rotation_Y(D3DXToRadian(m_MoveX) * -fTimeDelta);
		}

		if (m_MoveY += (m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_Y) * 0.2f))
		{
			m_pTransform->Rotation_Axis(D3DXToRadian(m_MoveY) * fTimeDelta, m_pTransform->Get_StateInfo(CTransform::STATE_RIGHT));
		}
	}

	if (m_MoveY > 50.f)
		m_MoveY = 50.f;
	else if (m_MoveY < -50.f)
		m_MoveY = -50.f;

	//float cameraLatitude, cameraLongitude, orbitDistance;
	_vec3 vCamRight, vCamLook, vPlayerPos;
	_matrix matOut;

	vCamLook.x = -sin(D3DXToRadian(m_MoveX)) * cos(D3DXToRadian(m_MoveY));
	vCamLook.y = -sin(D3DXToRadian(m_MoveY));
	vCamLook.z = cos(D3DXToRadian(m_MoveX)) * cos(D3DXToRadian(m_MoveY));

	if (nullptr == m_PlayerRoot)
		return -1;

	vPlayerPos = *(_vec3*)&m_PlayerRoot->m[3][0];
	if (server_data.Campers[exPlayerNumber - 1].iState == AC::CrouchStand
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::CrouchComeHere
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::CrouchWalk
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::CrouchPointTo
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::InjuredCrouchComeHere
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::InjuredCrouchPointTo
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::Injured_CrouchIdle
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::Injured_CrouchStand
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::Injured_CrouchWalk
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::CrawlFT
		|| server_data.Campers[exPlayerNumber - 1].iState == AC::CrawlToStand)
		vPlayerPos.y += 100.f;
	else
		vPlayerPos.y += 140.f;

	if (fabs(vPlayerPos.y - m_fOriginY) > 5.f)
	{
		if (fTimeDelta < 1.f)
		{
			if (vPlayerPos.y > m_fOriginY)
				vPlayerPos.y = m_fOriginY += fTimeDelta * 200.f;
			else if (vPlayerPos.y < m_fOriginY)
				vPlayerPos.y = m_fOriginY -= fTimeDelta * 200.f;
		}
	}
	
	if (m_bFlash)
	{
		vPlayerPos = *(_vec3*)&m_PlayerRHandMat->m[3][0];
		vPlayerPos.y += 10.f;
	}

	D3DXVec3TransformCoord(&vPlayerPos, &vPlayerPos, m_PlayerMat);

	m_fDistance = D3DXVec3Length(&(vPlayerPos - m_vCamPos));
	//m_fSpeed = m_fDistance * m_fDistance * fTimeDelta;

	//if (m_fDistance > 200.01f)
	//	m_fDistance -= m_fSpeed;
	//else
	m_fDistance = m_fCollDist;
	m_fCollDist = 200.f;
	if (m_bFlash)
	{
		m_fDistance = 2.f;
	}

	m_vCamPos = vPlayerPos - (vCamLook * m_fDistance);

	Start_Camera(fTimeDelta * 30.f);

	D3DXMatrixLookAtLH(&matOut, &m_vCamPos, &vPlayerPos, &D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	D3DXMatrixInverse(&matOut, nullptr, &matOut);
	m_pTransform->Set_Matrix(matOut);

	Invalidate_ViewProjMatrix();

	m_pFrustumCom->Transform_ToWorld();

	return _int();
}

_int CCamera_Camper::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (5 == exPlayerNumber && m_bStart)
	{
		CManagement* pManagement = CManagement::GetInstance();
		if (nullptr == pManagement)
			return 0;

		CCamera_Slasher*	pCameraSlasher = nullptr;
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Slasher", SCENE_STAGE, L"Layer_Camera", (CGameObject**)&pCameraSlasher)))
			return 0;

		CAMERADESC		CameraDesc;
		ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
		CameraDesc.vEye = _vec3(3200.f, 20.f, 3200.f);
		CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
		CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);

		PROJDESC		ProjDesc;
		ZeroMemory(&ProjDesc, sizeof(PROJDESC));
		ProjDesc.fFovY = D3DXToRadian(60.0f);
		ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
		ProjDesc.fNear = 5.f;
		ProjDesc.fFar = 10000.f;

		if (FAILED(pCameraSlasher->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
			return 0;

		return DEAD_OBJ;
	}

	return _int();
}

void CCamera_Camper::Render_GameObject()
{
}

void CCamera_Camper::Set_OrbitMatrix()
{
	if (nullptr != m_PlayerRoot)
		return;

	if (5 > exPlayerNumber)
	{
		GET_INSTANCE_MANAGEMENT;

		m_PlayerRoot = ((CMesh_Dynamic*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").front()->Get_ComponentPointer(L"Com_Mesh"))->Find_Frame("joint_Char");
		m_PlayerMat = ((CTransform*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").front()->Get_ComponentPointer(L"Com_Transform"))->Get_Matrix_Pointer();
		m_PlayerRHandMat = ((CMesh_Dynamic*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").front()->Get_ComponentPointer(L"Com_Mesh"))->Find_Frame("joint_WeaponRT_01");
		m_MoriCamera = ((CMesh_Dynamic*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").front()->Get_ComponentPointer(L"Com_Mesh"))->Find_Frame("joint_Cam_03");

		CMesh_Dynamic* pMeshPla = (CMesh_Dynamic*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Camper", L"Com_Mesh");
		Safe_Release(pManagement);
		if (nullptr == pMeshPla)
			return;
		//CTransform* pPlayerTrans = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Player", L"Com_Transform");
		//m_PlayerMat = pPlayerTrans->Get_Matrix_Pointer();

		//m_PlayerRoot = pMeshPla->Find_Frame("joint_Char");
	}
	else if(5 == exPlayerNumber)
	{
		GET_INSTANCE_MANAGEMENT;

		m_PlayerRoot = ((CMesh_Dynamic*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front()->Get_ComponentPointer(L"Com_Mesh"))->Find_Frame("joint_Char");
		m_PlayerMat = ((CTransform*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front()->Get_ComponentPointer(L"Com_Transform"))->Get_Matrix_Pointer();

		CMesh_Dynamic* pMeshPla = (CMesh_Dynamic*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Mesh");
		if (nullptr == pMeshPla)
		{
			Safe_Release(pManagement);
			return;
		}

		Safe_Release(pManagement);
	}
}

_int CCamera_Camper::Do_Coll(CGameObject * _pObj, const _vec3 & _vPos)
{


	return _int();
}

_int CCamera_Camper::Do_Dist(const _float & _fDist)
{
	if(_fDist<=200.f)
		m_fCollDist = _fDist;
	
	return _int();
}

void CCamera_Camper::CommunicateWithServer()
{
	if (exPlayerNumber == 5)
		return;

	if (nullptr == m_pTransform)
		return;

	camper_data.vCamRight =  *m_pTransform->Get_StateInfo(CTransform::STATE_RIGHT);
	camper_data.vCamUp =  *m_pTransform->Get_StateInfo(CTransform::STATE_UP);
	camper_data.vCamLook =  *m_pTransform->Get_StateInfo(CTransform::STATE_LOOK);
	camper_data.vCamPos =  *m_pTransform->Get_StateInfo(CTransform::STATE_POSITION);
}

HRESULT CCamera_Camper::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	//m_vCamPos

	_matrix matLocalTransform;
	D3DXMatrixIdentity(&matLocalTransform);


	m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, nullptr));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		return E_FAIL;
	m_pColliderCom->Set_CamPos(&m_vCamPos);

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_CAMERA, this);

	Safe_Release(pManagement);

	return NOERROR;
}

void CCamera_Camper::Start_Camera(const _float& fTimeDelta)
{
	if (m_bStart)
		return;

	if (GET_INSTANCE(CLoadManager)->Get_IsLoading())
		return;

	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return;
	pManagement->AddRef();

	if (!bStartGameServer)
	{
		Safe_Release(pManagement);
		return;
	}
	
	if (m_MoveX < 0.f)
		m_MoveX += fTimeDelta;
	else
	{
		bCameraReady = true;
		m_bStart = true;
	}
		
	Safe_Release(pManagement);
	
}

_bool CCamera_Camper::CheckCollision(_vec3 * pOut)
{
	

	return _bool();
}

CCamera_Camper * CCamera_Camper::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera_Camper*	pInstance = new CCamera_Camper(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCamera_Camper Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


CGameObject * CCamera_Camper::Clone_GameObject()
{
	CCamera_Camper*	pInstance = new CCamera_Camper(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCamera_Camper Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamera_Camper::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_CAMERA, this);

	Safe_Release(m_pFrustumCom);
	Safe_Release(m_pColliderCom);
	CCamera::Free();
}
