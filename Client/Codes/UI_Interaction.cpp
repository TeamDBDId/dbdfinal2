#include "stdafx.h"
#include "..\Headers\UI_Interaction.h"
#include "Management.h"
#include "UI_Texture.h"
#include "Generator.h"
#include "Math_Manager.h"
#include "Camper.h"
#include "Plank.h"
#include "Totem.h"
#include "Chest.h"
#include "Action_HideCloset.h"
#include "Slasher.h"
#include "Item.h"
#include <random>
_USING(Client);




CUI_Interaction::CUI_Interaction(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Interaction::CUI_Interaction(const CUI_Interaction & rhs)
	: CGameObject(rhs)
	, m_eSkillCheckState(rhs.m_eSkillCheckState)
	, m_fSkillCheckTime(rhs.m_fSkillCheckTime)
	, m_pPlayer(rhs.m_pPlayer)
	, m_pCollisionObjtect(rhs.m_pCollisionObjtect)
	, m_ProgressState(rhs.m_ProgressState)
{
}


HRESULT CUI_Interaction::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_Interaction::Ready_GameObject()
{
	SetUp_ProgressBar();
	m_fProgress = 0.f;
	m_PlayerNum = exPlayerNumber;
	m_InteractionObject = 0;
	m_IsObserve = false;
	SetUp_Spirit();
	return NOERROR;
}

_int CUI_Interaction::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	m_fClickButton -= fTimeDelta;
	if (m_fSkillCheckTime != 0.f)
	{
		m_fSkillCheckTime -= fTimeDelta;
		if (m_fSkillCheckTime <= 0.f)
			m_fSkillCheckTime = 0.f;
	}
	Update_Observe();
	Update_Spirit();
	ColItem_Check();
	CurItem_Check();
	KeyEvent_Check(fTimeDelta);
	Set_SkillCheck();
	Interact_Check();
	Update_ProgressBar();
	Update_SkillCheck(fTimeDelta);
	Update_AddButton_Camper();
	Update_AddButton_Slasher();
	Update_ButtonPos();
	return _int();
}

_int CUI_Interaction::LastUpdate_GameObject(const _float & fTimeDelta)
{
	Check_CollisionObject();
	return _int();
}

void CUI_Interaction::Add_Button(wstring TextureName, wstring TextName)
{
	auto& iter = m_mapButton.find(TextureName);
	if (iter != m_mapButton.end())
		return;

	GET_INSTANCE_MANAGEMENT;
	ButtonInfo Button;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&Button.pButtonTexture)))
		return;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&Button.pButtonText)))
		return;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&Button.pMotionText)))
		return;


	if (TextureName == L"Space")
		Button.pButtonTexture->Set_TexName(L"SkillCheck_SpaceBar.tga");
	else if (TextureName == L"M1" || TextureName == L"M2")
		Button.pButtonTexture->Set_TexName(L"Button_Mouse.tga");
	else
		Button.pButtonTexture->Set_TexName(L"Button_Keyboard.tga");


	Button.pButtonTexture->Set_Scale(_vec2(0.6667f, 0.6667f));
	Button.pButtonText->Set_Font(TextureName, _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	Button.pMotionText->Set_Font(TextName, _vec2(14.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));

	Button.pButtonTexture->Set_OneTimeRendering();
	Button.pMotionText->Set_OneTimeRendering();
	Button.pButtonText->Set_OneTimeRendering();

	m_mapButton.insert({ TextureName,Button });
	Safe_Release(pManagement);
}

void CUI_Interaction::SetUp_Spirit()
{
	if (m_PlayerNum != 5 || server_data.Slasher.iCharacter != 1)
		return;

	GET_INSTANCE_MANAGEMENT;
	
	_vec2 vPos = { 0.16f*g_iBackCX , 0.808f*g_iBackCY };
	CUI_Texture* pTexture = nullptr;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_ItemBase.tga"));
	pTexture->Set_Pos(vPos);
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	m_mapUITexture.insert({ wstring(L"Power_Base"),pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_ItemBase2.tga"));
	pTexture->Set_Pos(vPos);
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	pTexture->IsColor(true);
	pTexture->Set_Color(D3DXCOLOR(0.4196078f, 0.35294117f, 0.2823529f, 1.f));
	m_mapUITexture.insert({ wstring(L"Power_Base2"),pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
	pTexture->Set_Pos(vPos);
	pTexture->Set_TexName(wstring(L"Spirit_Power.png"));
	pTexture->Set_Scale(_vec2(0.2f, 0.2f));
	m_mapUITexture.insert({ wstring(L"Power_Icon"),pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
	pTexture->Set_Pos(vPos);
	pTexture->Set_TexName(wstring(L"UI_CoolTime.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_mapUITexture.insert({ wstring(L"Power_CoolTime"),pTexture });

	Safe_Release(pManagement);
}

void CUI_Interaction::Interact_Check()
{
	if (m_PlayerNum == 5)
		m_InteractionObject = slasher_data.InterationObject;
	else
	{
		m_InteractionObject = camper_data.InterationObject;
		//_uint iItem = camper_data.iItem;
	}

}

void CUI_Interaction::Update_Spirit()
{
	if (m_PlayerNum != 5 || server_data.Slasher.iCharacter != 1 || m_pPlayer == nullptr)
		return;

	_float fSkillTime = ((CSlasher*)m_pPlayer)->Get_SkillTime();

	CUI_Texture* pBase = Find_UITexture(wstring(L"Power_Base"));
	CUI_Texture* pBase2 = Find_UITexture(wstring(L"Power_Base2"));
	CUI_Texture* pIcon = Find_UITexture(wstring(L"Power_Icon"));
	CUI_Texture* pCoolTime = Find_UITexture(wstring(L"Power_CoolTime"));

	pCoolTime->Set_Angle(_vec2(0.000001f, D3DXToRadian(fSkillTime / SPRIIT_POWER_BAR*360.f)));
	if (fSkillTime < SPRIIT_POWER_BAR)
	{
		pBase->Set_Alpha(0.5f);
		pBase2->Set_Alpha(0.5f);
		pIcon->Set_Alpha(0.5f);
		pCoolTime->Set_Alpha(0.5f);
	}
	else
	{
		pBase->Set_Alpha(1.f);
		pBase2->Set_Alpha(1.f);
		pIcon->Set_Alpha(1.f);
		pCoolTime->Set_Alpha(1.f);
	}
}

void CUI_Interaction::Update_SkillCheck(const _float& fTimeDelta)
{
	if (m_eSkillCheckState != SC_Check)
		return;
	if (m_InteractionObject == 0)
	{
		m_eSkillCheckState = SC_NONE;
		for (_int i = 0; i < SkillCheckEnd; i++)
		{
			m_SkillCheck[i]->Set_DeadTime(1.f);
			m_SkillCheck[i] = nullptr;
		}
		return;
	}

	_float fGridRotation = m_SkillCheck[S_GridLine]->Get_Rotation();
	_float fEndRotation = m_SkillCheck[S_End]->Get_Rotation();

	if (KEYMGR->KeyDown(DIK_SPACE) || fGridRotation >fEndRotation)
	{
		m_eSkillCheckState = SC_NONE;
		m_fSkillCheckTime = 3.f;

		_float fStartAngle = m_SkillCheck[S_Full]->Get_Angle().x;
		if (fGridRotation < fStartAngle || fGridRotation > fStartAngle + D3DXToRadian(45.f))
			GET_INSTANCE(CUIManager)->Set_SkillCheckState(FAILED_SKILL_CHECK);//실패
		else if (fGridRotation <= fStartAngle + D3DXToRadian(11.f))
		{
			GET_INSTANCE(CUIManager)->Set_SkillCheckState(SUCCESS_SKILL_CHECK);//대성공
			GET_INSTANCE(CSoundManager)->PlaySound(string("Ingame/SkillCheck_End.ogg"), _vec3(), 0, 1.f);
		}
		else
		{
			GET_INSTANCE(CUIManager)->Set_SkillCheckState(12);//성공
			GET_INSTANCE(CSoundManager)->PlaySound(string("Ingame/SkillCheck_End.ogg"), _vec3(), 0, 1.f);
		}
		for (int i = 0; i < SkillCheckEnd; i++)
		{
			m_SkillCheck[i]->Set_DeadTime(1.f);
			m_SkillCheck[i] = nullptr;
		}
		return;
	}

	m_SkillCheck[S_GridLine]->Set_Rotation(fTimeDelta * 5.f);
}

void CUI_Interaction::Update_ButtonPos()
{
	_float m_StartPos = (0.5f - (m_mapButton.size()*0.05f)) * g_iBackCX;

	for (auto& iter : m_mapButton)
	{
		_float ButtonScaleX = iter.second.pButtonTexture->Get_Scale().x;
		_float MotionTextScaleX = iter.second.pMotionText->Get_Scale().x;
		_float ButtonPosX = m_StartPos + (ButtonScaleX*0.5f);
		_float TextPosX = ButtonPosX + (ButtonScaleX*0.5f) + (g_iBackCX * 0.001f) + (MotionTextScaleX * 0.5f);
		wstring TextureName = iter.second.pButtonTexture->Get_TexName();
		_float ButtonTexPosX = 0.f;
		if (TextureName == L"SkillCheck_SpaceBar.tga")
			ButtonTexPosX = -0.002f *g_iBackCX;
		else if (TextureName == L"SkillCheck_SpaceBar.tga")
			ButtonTexPosX = -0.002f *g_iBackCX;
		else
			ButtonTexPosX = -0.002f *g_iBackCX;

		iter.second.pButtonTexture->Set_Pos(_vec2(ButtonPosX, 0.92f *g_iBackCY));
		iter.second.pButtonText->Set_Pos(_vec2(ButtonPosX + ButtonTexPosX, 0.915f *g_iBackCY));
		iter.second.pMotionText->Set_Pos(_vec2(TextPosX, 0.918f *g_iBackCY));

		m_StartPos = max(m_StartPos + 0.1f * g_iBackCX, (TextPosX + (MotionTextScaleX*0.5f) + (g_iBackCX * 0.005f)));
	}
	m_mapButton.clear();
}

void CUI_Interaction::Update_AddButton_Camper()
{
	if (m_PlayerNum >= 5)
		return;
	if (m_pPlayer == nullptr)
		return;

	if (m_pCollisionObjtect == nullptr)
	{
		if (m_InteractionObject & CLOSET)
		{
			if (((CCamper*)m_pPlayer)->GetState() == AC::ClosetIdle)
				Add_Button(L"Space", L"나가기");
		}
		if (((CCamper*)m_pPlayer)->GetCurCondition() == CCamper::CONDITION::HOOKED)
			Add_Button(L"M1", L"탈출 시도");
		return;
	}

	_int iCollisionObject = m_pCollisionObjtect->GetID();
	if (!(((CCamper*)m_pPlayer)->GetOldCondition() == CCamper::HEALTHY || ((CCamper*)m_pPlayer)->GetOldCondition() == CCamper::INJURED))
		return;

	if (iCollisionObject != m_InteractionObject)
	{
		if (iCollisionObject & GENERATOR)
		{
			if (GET_INSTANCE(CUIManager)->Get_GeneratorNum() <= 0)
				return;
			_float fProgressTime = m_pCollisionObjtect->Get_ProgressTime();
			_float fMaxProgressTime = m_pCollisionObjtect->Get_MaxProgressTime();
			if (fProgressTime >= fMaxProgressTime)
				return;
			Add_Button(L"M1", L"수리");
		}
		else if (iCollisionObject & CLOSET)
			Add_Button(L"Space", L"숨기");
		else if (iCollisionObject & TOTEM)
			Add_Button(L"M1", L"토템 정화");
		else if (iCollisionObject & CHEST)
		{
			_float fProgressTime = m_pCollisionObjtect->Get_ProgressTime();
			_float fMaxProgressTime = m_pCollisionObjtect->Get_MaxProgressTime();
			if (fProgressTime >= fMaxProgressTime)
				return;
			Add_Button(L"M1", L"찾기");
		}
		else if (iCollisionObject & PLANK)
		{
			if (m_pCollisionObjtect->Get_IsBool())
				Add_Button(L"Space", L"넘기");
			else
				Add_Button(L"Space", L"넘어뜨리기");
		}
		else if (iCollisionObject & WINDOW)
			Add_Button(L"Space", L"넘기");
		else if (iCollisionObject & CAMPER)
		{
			CCamper::CONDITION CurCondition = ((CCamper*)m_pCollisionObjtect)->GetCurCondition();

			if (CurCondition == CCamper::INJURED)
			{
				Add_Button(L"M1", L"치료");
				//아이템 구급상자일때 치료+
			}
			else if (CurCondition == CCamper::HOOKED)
			{
				Add_Button(L"M1", L"구출");
			}
		}
	}
}

void CUI_Interaction::Update_AddButton_Slasher()
{
	if (m_PlayerNum != 5)
		return;
	if (m_pPlayer == nullptr)
		return;

	if (m_pCollisionObjtect == nullptr)
	{
		return;
	}


	_int iCollisionObject = m_pCollisionObjtect->GetID();

	if (nullptr == ((CSlasher*)(m_pPlayer))->Get_CarriedCamper())
	{
		if (iCollisionObject != m_InteractionObject)
		{
			if (iCollisionObject & GENERATOR)
			{
				if (GET_INSTANCE(CUIManager)->Get_GeneratorNum() <= 0)
					return;
				_float fProgressTime = m_pCollisionObjtect->Get_ProgressTime();
				_float fMaxProgressTime = m_pCollisionObjtect->Get_MaxProgressTime();
				if (fProgressTime >= fMaxProgressTime || fProgressTime < 0.01f)
					return;
				if (((CGenerator*)m_pCollisionObjtect)->Get_CurProgressTimeForSlasher() > m_pCollisionObjtect->Get_ProgressTime())
					return;
				Add_Button(L"Space", L"손상");
			}
			else if (iCollisionObject & CLOSET)
				Add_Button(L"Space", L"찾기");
			else if (iCollisionObject & PLANK)
			{
				if (m_pCollisionObjtect->Get_IsBool())
					Add_Button(L"Space", L"파괴");
			}
			else if (iCollisionObject & WINDOW)
				Add_Button(L"Space", L"넘기");
		}
	}
	else
	{

	}
}

void CUI_Interaction::Check_CollisionObject()
{
	if (m_pPlayer == nullptr)
	{
		GET_INSTANCE_MANAGEMENT;
		if (m_PlayerNum == 5)
			m_pPlayer = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Slasher", SLASHER);
		else
			m_pPlayer = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Camper", CAMPER + (m_PlayerNum - 1));
		Safe_Release(pManagement);
	}

	if (m_pPlayer == nullptr)
		return;

	m_pCollisionObjtect = nullptr;
	if (m_PlayerNum == 5)
		m_pCollisionObjtect = ((CSlasher*)m_pPlayer)->GetTargetOfInteraction();
	else
		m_pCollisionObjtect = ((CCamper*)m_pPlayer)->GetTargetOfInteraction();
}

void CUI_Interaction::Set_ProgressBar(CUIManager::ProgressState State, _int iItem)
{
	if (m_ProgressState == State)
		return;

	m_ProgressState = State;



	_vec2 Start = { 0.42f* g_iBackCX, 0.827786f* g_iBackCY };
	m_ProgressBar[Bar]->IsRendering(true);
	m_ProgressBar[Base]->IsRendering(true);
	m_ProgressBar[Texture]->IsRendering(true);
	m_ProgressBar[Text]->IsRendering(true);

	if (State == CUIManager::PG_Generator)
	{		
		 m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"수리"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		if (iItem == 0)
			m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
	}
	else if (State == CUIManager::PG_Totem)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"토템 정화"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
	}
	else if (State == CUIManager::PG_Chest)
	{
		m_ProgressBar[Text]->Set_Font(wstring(L"찾기"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
	}
	else if (State == CUIManager::PG_HealCamper)
	{
		m_ProgressBar[Text]->Set_Font(wstring(L"치료"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		if (iItem == 0)
			m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_Hand.tga");
	}
	else if (State == CUIManager::PG_HealSelf)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Red.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"자가 치료"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->Set_TexName(L"ProgressBar_Texture_MedicKit.tga");
	}
	else if (State == CUIManager::PG_BeingHeal)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"치료 중"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));

		m_ProgressBar[Text]->Set_Alpha(0.5f);
		m_ProgressBar[Base]->Set_Alpha(0.5f);
		m_ProgressBar[Bar]->Set_Alpha(0.5f);
		m_ProgressBar[Texture]->IsRendering(false);
	}
	else if (State == CUIManager::PG_HookStruggle)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"탈출 시도"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->IsRendering(false);
	}
	else if (State == CUIManager::PG_HookFree)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"구출"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
	}
	else if (State == CUIManager::PG_HookFreed)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"구출중"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Text]->Set_Alpha(0.5f);
		m_ProgressBar[Base]->Set_Alpha(0.5f);
		m_ProgressBar[Bar]->Set_Alpha(0.5f);
		m_ProgressBar[Texture]->IsRendering(false);
	}
	else if (State == CUIManager::PG_Wiggle)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"몸부림"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
	}
	else if (State == CUIManager::PG_ExitDoor)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"출구 열기"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
	}
	else if (State == CUIManager::PG_Sabotage)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Yellow.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"파괴공작(공구상자)"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
	}
	else if (State == CUIManager::PG_Hide)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"투명화"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->IsRendering(false);
	}
	else if (State == CUIManager::PG_UnHide)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"은신 해제"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->IsRendering(false);
	}
	else if (State == CUIManager::PG_SpiritPower)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Yellow.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"영적 세계로 이동"), _vec2(13.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Texture]->IsRendering(false);
	}
	else if (State == CUIManager::PG_DyingHeal)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"회복"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Text]->Set_Alpha(1.f);
		m_ProgressBar[Base]->Set_Alpha(1.f);
		m_ProgressBar[Bar]->Set_Alpha(1.f);
	}
	else if (State == CUIManager::PG_Dying)
	{
		m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Blue.tga"));
		m_ProgressBar[Text]->Set_Font(wstring(L"회복"), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		_vec2 Scale = m_ProgressBar[Text]->Get_Scale();
		m_ProgressBar[Text]->Set_Pos(_vec2(Start.x + Scale.x*0.5f, Start.y));
		m_ProgressBar[Text]->Set_Alpha(0.5f);
		m_ProgressBar[Base]->Set_Alpha(0.5f);
		m_ProgressBar[Bar]->Set_Alpha(0.5f);
		m_ProgressBar[Texture]->IsRendering(false);
	}
	else if (State == CUIManager::PG_None)
	{
		for (_int i = 0; i < ProgressBarEnd; i++)
		{
			m_ProgressBar[i]->IsRendering(false);
			m_ProgressBar[i]->Set_Alpha(1.f);
		}
	}
}

HRESULT CUI_Interaction::SetUp_ProgressBar()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	for (_uint i = 0; i < ProgressBarEnd; i++)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_ProgressBar[i])))
			return E_FAIL;
		m_ProgressBar[i]->IsRendering(false);
	}

	_vec2 vPos = { 0.5f, 0.85f };
	m_ProgressBar[Base]->Set_TexName(wstring(L"ProgressBar_Base.tga"));
	m_ProgressBar[Base]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_ProgressBar[Base]->Set_Scale(_vec2(0.6667f, 0.6667f));

	vPos = { 0.4938f, 0.85f };
	m_ProgressBar[Bar]->Set_TexName(wstring(L"ProgressBar_Red.tga"));
	m_ProgressBar[Bar]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_ProgressBar[Bar]->Set_Scale(_vec2(0.6667f, 0.5f));
	m_ProgressBar[Bar]->Set_UV(_vec2(0.3f, 1.f));

	vPos = { 0.4076f, 0.8358f };
	m_ProgressBar[Texture]->Set_TexName(wstring(L"ProgressBar_Texture_Hand.tga"));
	m_ProgressBar[Texture]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_ProgressBar[Texture]->Set_Scale(_vec2(0.2f, 0.2f));

	vPos = { 0.42931f, 0.827786f };
	m_ProgressBar[Text]->Set_Font(wstring(L"수리"), _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	m_ProgressBar[Text]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CUI_Interaction::Set_SkillCheck()
{
	if (m_eSkillCheckState != SC_Ready || m_fSkillCheckTime != 0.f)
		return NOERROR;
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	for (_uint i = 0; i < SkillCheckEnd; i++)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_SkillCheck[i])))
			return E_FAIL;
		m_SkillCheck[i]->IsRendering(true);
	}

	random_device rn;
	mt19937_64 rnd(rn());

	uniform_real_distribution <_float> fdis(D3DX_PI* 0.5f, ((2.f*D3DX_PI) - D3DXToRadian(50.f)));
	_float fStartRadian = fdis(rnd);

	_vec2 vPos = { 0.5f, 0.5f };

	if ((server_data.Game_Data.Curse & CRUIN) && (server_data.Campers[exPlayerNumber - 1].InterationObject & GENERATOR))
	{
		m_SkillCheck[S_Base]->Set_TexName(wstring(L"SkillCheck_Base2.tga"));
		m_SkillCheck[S_Base]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_SkillCheck[S_Base]->Set_Scale(_vec2(0.6667f, 0.6667f));

		m_SkillCheck[S_Full]->Set_TexName(wstring(L"SkillCheck_Full2.tga"));
		m_SkillCheck[S_Full]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_SkillCheck[S_Full]->Set_Scale(_vec2(0.6667f, 0.6667f));
		m_SkillCheck[S_Full]->Set_Angle(_vec2(fStartRadian, fStartRadian + D3DXToRadian(11.f)));

		m_SkillCheck[S_NoneFull]->Set_TexName(wstring(L"SkillCheck_NoneFull2.tga"));
		m_SkillCheck[S_NoneFull]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_SkillCheck[S_NoneFull]->Set_Scale(_vec2(0.6667f, 0.6667f));
		m_SkillCheck[S_NoneFull]->Set_Angle(_vec2(fStartRadian + D3DXToRadian(11.f), fStartRadian + D3DXToRadian(45.f)));
	}
	else
	{
		m_SkillCheck[S_Base]->Set_TexName(wstring(L"SkillCheck_Base.tga"));
		m_SkillCheck[S_Base]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_SkillCheck[S_Base]->Set_Scale(_vec2(0.6667f, 0.6667f));

		m_SkillCheck[S_Full]->Set_TexName(wstring(L"SkillCheck_Full.tga"));
		m_SkillCheck[S_Full]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_SkillCheck[S_Full]->Set_Scale(_vec2(0.6667f, 0.6667f));
		m_SkillCheck[S_Full]->Set_Angle(_vec2(fStartRadian, fStartRadian + D3DXToRadian(11.f)));

		m_SkillCheck[S_NoneFull]->Set_TexName(wstring(L"SkillCheck_NoneFull.tga"));
		m_SkillCheck[S_NoneFull]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_SkillCheck[S_NoneFull]->Set_Scale(_vec2(0.6667f, 0.6667f));
		m_SkillCheck[S_NoneFull]->Set_Angle(_vec2(fStartRadian + D3DXToRadian(11.f), fStartRadian + D3DXToRadian(45.f)));
	}

	m_SkillCheck[S_End]->Set_TexName(wstring(L"SkillCheck_End.tga"));
	m_SkillCheck[S_End]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_End]->Set_Scale(_vec2(0.6667f, 0.6667f));
	m_SkillCheck[S_End]->Set_Rotation(fStartRadian + D3DXToRadian(45.f));

	m_SkillCheck[S_GridLine]->Set_TexName(wstring(L"SkillCheck_GridLine.tga"));
	m_SkillCheck[S_GridLine]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_GridLine]->Set_Scale(_vec2(0.6667f, 0.6667f));

	vPos = { 0.5f, 0.5058f };
	m_SkillCheck[S_SpaceBar]->Set_TexName(wstring(L"SkillCheck_SpaceBar.tga"));
	m_SkillCheck[S_SpaceBar]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_SkillCheck[S_SpaceBar]->Set_Scale(_vec2(0.6667f, 0.6667f));

	vPos = { 0.5f, 0.503284f };
	m_SkillCheck[S_Text]->Set_Font(wstring(L"Space"), _vec2(10.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	m_SkillCheck[S_Text]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));


	Safe_Release(pManagement);
	m_eSkillCheckState = SC_Check;
	GET_INSTANCE(CUIManager)->Set_SkillCheckState(SkillCheckState::SC_Check);
	return NOERROR;
}

_bool CUI_Interaction::IsSkillCheck()
{
	if (m_eSkillCheckState != SC_NONE)
		return false;
	if (m_fSkillCheckTime != 0.f)
		return false;

	_int Random = Math_Manager::CalRandIntFromTo(0, 600);

	if (!bCalZZi && Random == 1)
	{
		m_eSkillCheckState = SC_Ready;
		GET_INSTANCE(CUIManager)->Set_SkillCheckState(SkillCheckState::SC_Ready);
		m_fSkillCheckTime = 1.f;
		GET_INSTANCE(CSoundManager)->PlaySound(string("Ingame/SkillCheck_Start.ogg"), _vec3(), 0, 1.f);
		return true;
	}
	else if (bCalZZi)
	{
		m_eSkillCheckState = SC_Ready;
		GET_INSTANCE(CUIManager)->Set_SkillCheckState(SkillCheckState::SC_Ready);
		m_fSkillCheckTime = 1.f;
		GET_INSTANCE(CSoundManager)->PlaySound(string("Ingame/SkillCheck_Start.ogg"), _vec3(), 0, 1.f);
		return true;
	}

	return false;
}

void CUI_Interaction::KeyEvent_Check(const _float& fDeltaTime)
{
	if (m_eKeyEvent == CUIManager::EV_None || m_PlayerNum == 5)
		return;

	if (m_eKeyEvent == CUIManager::EV_IdleResist)
	{
		if (nullptr == Find_UITexture(wstring(L"R_ButtonText")))
			Set_Resistance();
	}
	else if (m_eKeyEvent == CUIManager::EV_ClickResist)
	{
		if (nullptr == Find_UITexture(wstring(L"R_ButtonText")))
			Set_Resistance();
		if (m_fClickButton >= 0.125f)
		{
			CUI_Texture* pUI_Texture = nullptr;
			_vec2 vPos = { 0.4828125f, 0.492778f };
			pUI_Texture = Find_UITexture(wstring(L"R_ButtonText"));
			pUI_Texture->Set_Alpha(0.5f);
			pUI_Texture->Set_Pos(_vec2(vPos.x * g_iBackCX, vPos.y * g_iBackCY));
			pUI_Texture = Find_UITexture(wstring(L"R_Button"));
			pUI_Texture->Set_Alpha(0.5f);
			pUI_Texture->Set_Pos(_vec2(vPos.x * g_iBackCX, vPos.y * g_iBackCY));
		}
		else
		{
			CUI_Texture* pUI_Texture = nullptr;
			_vec2 vPos = { 0.4828125f, 0.502778f };
			pUI_Texture = Find_UITexture(wstring(L"R_ButtonText"));
			pUI_Texture->Set_Alpha(1.f);
			pUI_Texture->Set_Pos(_vec2(vPos.x * g_iBackCX, vPos.y * g_iBackCY));
			pUI_Texture = Find_UITexture(wstring(L"R_Button"));
			pUI_Texture->Set_Alpha(1.f);
			pUI_Texture->Set_Pos(_vec2(vPos.x * g_iBackCX, vPos.y * g_iBackCY));
		}
		if (m_fClickButton <= 0.f)
			m_fClickButton = 0.25f;
	}
	else if (m_eKeyEvent == CUIManager::EV_ResistEnd)
	{
		Delete_Resist();
		m_eKeyEvent = CUIManager::EV_None;
	}
	else if (m_eKeyEvent == CUIManager::EV_SetWiggle)
	{
		Set_Wiggle();
		m_eKeyEvent = CUIManager::EV_IdleWiggle;
	}
	else if (m_eKeyEvent == CUIManager::EV_KeyOnWiggle || m_eKeyEvent == CUIManager::EV_IdleWiggle)
	{
		CUI_Texture* pTexture = nullptr;
		if (m_eKeyEvent == CUIManager::EV_IdleWiggle)
		{
			if (m_fClickButton < 0.f)
			{
				m_fClickButton = 0.2f;
				m_isWiggleA = !m_isWiggleA;
			}
			for (_int i = 0; i < ProgressBarEnd; i++)
				m_ProgressBar[i]->Set_Alpha(0.5f);
			if (m_isWiggleA)
			{
				pTexture = Find_UITexture(wstring(L"W_AButton"));
				pTexture->Set_Alpha(1.f);
				pTexture = Find_UITexture(wstring(L"W_AButtonText"));
				pTexture->Set_Alpha(1.f);
				pTexture = Find_UITexture(wstring(L"W_DButton"));
				pTexture->Set_Alpha(0.5f);
				pTexture = Find_UITexture(wstring(L"W_DButtonText"));
				pTexture->Set_Alpha(0.5f);
			}
			else
			{
				pTexture = Find_UITexture(wstring(L"W_AButton"));
				pTexture->Set_Alpha(0.5f);
				pTexture = Find_UITexture(wstring(L"W_AButtonText"));
				pTexture->Set_Alpha(0.5f);
				pTexture = Find_UITexture(wstring(L"W_DButton"));
				pTexture->Set_Alpha(1.f);
				pTexture = Find_UITexture(wstring(L"W_DButtonText"));
				pTexture->Set_Alpha(1.f);
			}
		}
		else if(m_fClickButton < 0.f)
		{
			m_fClickButton = 0.2f;
			m_eKeyEvent = CUIManager::EV_IdleWiggle;
			CUI_Texture* pTexture = nullptr;
			pTexture = Find_UITexture(wstring(L"W_AButton"));
			pTexture->IsRendering(true);
			pTexture = Find_UITexture(wstring(L"W_AButtonText"));
			pTexture->IsRendering(true);
			pTexture = Find_UITexture(wstring(L"W_DButton"));
			pTexture->IsRendering(true);
			pTexture = Find_UITexture(wstring(L"W_DButtonText"));
			pTexture->IsRendering(true);
			pTexture = Find_UITexture(wstring(L"W_Text"));
			pTexture->IsRendering(true);
			pTexture = Find_UITexture(wstring(L"W_Arrow"));
			pTexture->IsRendering(true);
		}

		if ((KEYMGR->KeyDown(DIK_A) && m_isWiggleA) || (KEYMGR->KeyDown(DIK_D) && !m_isWiggleA))
		{
			m_fClickButton = 0.2f;
			CUI_Texture* pTexture = nullptr;
			pTexture = Find_UITexture(wstring(L"W_AButton"));
			pTexture->IsRendering(false);
			pTexture = Find_UITexture(wstring(L"W_AButtonText"));
			pTexture->IsRendering(false);
			pTexture = Find_UITexture(wstring(L"W_DButton"));
			pTexture->IsRendering(false);
			pTexture = Find_UITexture(wstring(L"W_DButtonText"));
			pTexture->IsRendering(false);
			pTexture = Find_UITexture(wstring(L"W_Text"));
			pTexture->IsRendering(false);
			pTexture = Find_UITexture(wstring(L"W_Arrow"));
			pTexture->IsRendering(false);

			if(m_isWiggleA)
				m_eKeyEvent = CUIManager::EV_LeftWiggle;
			else
				m_eKeyEvent = CUIManager::EV_RightWiggle;
		}
	}
	else if (m_eKeyEvent == CUIManager::EV_LeftWiggle || m_eKeyEvent == CUIManager::EV_RightWiggle)
	{
		for (_int i = 0; i < ProgressBarEnd; i++)
			m_ProgressBar[i]->Set_Alpha(1.f);

		if (m_fClickButton <= 0.f)
		{
			m_fClickButton = 0.2f;
			m_eKeyEvent = CUIManager::EV_KeyOnWiggle;
			m_isWiggleA = !m_isWiggleA;
		}
	}
	else if (m_eKeyEvent == CUIManager::EV_WiggleEnd)
	{
		Delete_Wiggle();
		m_eKeyEvent = CUIManager::EV_None;
		Set_ProgressBar(CUIManager::PG_None);
	}
}

void CUI_Interaction::ColItem_Check()
{
	if (m_PlayerNum == 5 || m_pPlayer == nullptr)
		return;
	
	CItem* pColItem = ((CCamper*)m_pPlayer)->GetColledItem();
	CItem::ITEMTYPE eType = CItem::I_NULL;
	if (pColItem != nullptr)
	{
		eType = pColItem->Get_ItemType();
		Add_Button(wstring(L"M1"), wstring(L"획득"));
	}		
	
	if (eType != m_OldColItem)
	{
		if (eType != CItem::I_NULL)
		{
			Delete_UI_Texture(wstring(L"PickItem_Item"));
			Delete_UI_Texture(wstring(L"PickItem_Base"));
			Delete_UI_Texture(wstring(L"PickItem_Base2"));
			GET_INSTANCE_MANAGEMENT;
			CUI_Texture* pTexture = nullptr;
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"UI_ItemBase.tga"));
			pTexture->Set_Pos(_vec2(0.5f*g_iBackCX, 0.713382f*g_iBackCY));
			pTexture->Set_Scale(_vec2(0.4f, 0.4f));
			m_mapUITexture.insert({ wstring(L"PickItem_Base"),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"UI_ItemBase2.tga"));
			pTexture->Set_Pos(_vec2(0.5f*g_iBackCX, 0.713382f*g_iBackCY));
			pTexture->Set_Scale(_vec2(0.4f, 0.4f));
			pTexture->IsColor(true);
			pTexture->Set_Color(D3DXCOLOR(1.f, 0.3f, 0.3f, 1.f));
			m_mapUITexture.insert({ wstring(L"PickItem_Base2"),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_Pos(_vec2(0.5f*g_iBackCX, 0.713382f*g_iBackCY));
			if (eType == CItem::MEDIKIT)
				pTexture->Set_TexName(wstring(L"UI_MedicKit.png"));
			else if (eType == CItem::TOOLBOX)
				pTexture->Set_TexName(wstring(L"UI_ToolBox.png"));
			else if (eType == CItem::KEY)
				pTexture->Set_TexName(wstring(L"UI_Key.png"));
			else if (eType == CItem::FLASHLIGHT)
				pTexture->Set_TexName(wstring(L"UI_Flash.png"));

			pTexture->Set_Scale(_vec2(0.4f, 0.4f));
			m_mapUITexture.insert({ wstring(L"PickItem_Item"),pTexture });
			Safe_Release(pManagement);
		}
		else
		{
			Delete_UI_Texture(wstring(L"PickItem_Item"));
			Delete_UI_Texture(wstring(L"PickItem_Base"));
			Delete_UI_Texture(wstring(L"PickItem_Base2"));
		}
		m_OldColItem = (_uint)eType;
	}
}

void CUI_Interaction::CurItem_Check()
{
	if (m_PlayerNum == 5 || m_pPlayer == nullptr)
		return;

	CItem* pItem = ((CCamper*)m_pPlayer)->GetCurItem();
	CItem::ITEMTYPE eType = CItem::I_NULL;
	if (pItem != nullptr && !pItem->Get_IsDead())
	{
		eType = pItem->Get_ItemType();
		CUI_Texture* pTexture = nullptr;
		_float Ratio = pItem->Get_Durability() / pItem->Get_MaxDurability();
		pTexture = Find_UITexture(wstring(L"UI_ItemProgressFull"));
		if(pTexture !=nullptr)
			pTexture->Set_Angle(_vec2(D3DXToRadian(180), ((Ratio* 0.722f) + 1.1389f)* D3DX_PI));
	}
	if (eType != m_OldItem)
	{
		if (eType != CItem::I_NULL)
		{
			GET_INSTANCE_MANAGEMENT;
			_vec2 vPos = { 0.16f*g_iBackCX , 0.808f*g_iBackCY };
			CUI_Texture* pTexture = nullptr;
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"UI_ItemBase.tga"));
			pTexture->Set_Pos(vPos);
			pTexture->Set_Scale(_vec2(0.2f, 0.2f));
			m_mapUITexture.insert({ wstring(L"StateItem_Base"),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_TexName(wstring(L"UI_ItemBase2.tga"));
			pTexture->Set_Pos(vPos);
			pTexture->Set_Scale(_vec2(0.2f, 0.2f));
			pTexture->IsColor(true);
			pTexture->Set_Color(D3DXCOLOR(1.f, 0.3f, 0.3f, 1.f));
			m_mapUITexture.insert({ wstring(L"StateItem_Base2"),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_Pos(vPos);
			if (eType == CItem::MEDIKIT)
				pTexture->Set_TexName(wstring(L"UI_MedicKit.png"));
			else if (eType == CItem::TOOLBOX)
				pTexture->Set_TexName(wstring(L"UI_ToolBox.png"));
			else if (eType == CItem::KEY)
				pTexture->Set_TexName(wstring(L"UI_Key.png"));
			else if (eType == CItem::FLASHLIGHT)
				pTexture->Set_TexName(wstring(L"UI_Flash.png"));

			pTexture->Set_Scale(_vec2(0.2f, 0.2f));
			m_mapUITexture.insert({ wstring(L"StateItem_Item"),pTexture });

			vPos = { 0.148f*g_iBackCX , 0.808f*g_iBackCY };
			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_Pos(vPos);
			pTexture->Set_TexName(wstring(L"UI_ItemProgressBase.tga"));
			pTexture->Set_Scale(_vec2(0.666667f, 0.66667f));
			m_mapUITexture.insert({ wstring(L"UI_ItemProgressBase"),pTexture });

			pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&pTexture);
			pTexture->Set_Pos(vPos);
			pTexture->Set_TexName(wstring(L"UI_ItemProgressFull.tga"));
			_float Ratio = pItem->Get_Durability() / pItem->Get_MaxDurability();
			pTexture->Set_Angle(_vec2(D3DXToRadian(180), ((Ratio* 0.722f) + 1.1389f)* D3DX_PI));
			pTexture->Set_Scale(_vec2(0.666667f, 0.66667f));
			m_mapUITexture.insert({ wstring(L"UI_ItemProgressFull"),pTexture });

			Safe_Release(pManagement);
		}
		else
		{
			Delete_UI_Texture(wstring(L"StateItem_Base"));
			Delete_UI_Texture(wstring(L"StateItem_Base2"));
			Delete_UI_Texture(wstring(L"StateItem_Item"));
			Delete_UI_Texture(wstring(L"UI_ItemProgressBase"));
			Delete_UI_Texture(wstring(L"UI_ItemProgressFull"));
		}
		m_OldItem = (_uint)eType;
	}
}

void CUI_Interaction::Set_Resistance()
{
	GET_INSTANCE_MANAGEMENT;

	Delete_Resist();
	CUI_Texture* pTexture;

	_vec2 vPos = { 0.4828125f, 0.462222f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Resistance_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(1.f, 1.f));
	m_mapUITexture.insert({ L"R_Arrow", pTexture });

	vPos = { 0.527343f, 0.502778f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"저항"), _vec2(12.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	m_mapUITexture.insert({ L"R_Text", pTexture });

	vPos = { 0.4828125f, 0.502778f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"SkillCheck_SpaceBar.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_mapUITexture.insert({ L"R_Button", pTexture });

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"Space"), _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	m_mapUITexture.insert({ L"R_ButtonText", pTexture });

	Safe_Release(pManagement);
}

void CUI_Interaction::Set_Wiggle()
{
	GET_INSTANCE_MANAGEMENT;
	CUI_Texture* pTexture;

	Delete_Wiggle();

	_vec2 vPos = { 0.47928f, 0.886f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Wiggle_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_mapUITexture.insert({ L"W_Arrow", pTexture });

	vPos = { 0.525781f, 0.913889f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"몸부림"), _vec2(14.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	m_mapUITexture.insert({ L"W_Text", pTexture });

	vPos = { 0.471093f, 0.913889f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Button_Keyboard.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_mapUITexture.insert({ L"W_AButton", pTexture });
	vPos = { 0.469292f, 0.912381f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"A"), _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	m_mapUITexture.insert({ L"W_AButtonText", pTexture });

	vPos = { 0.492188f, 0.913889f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Button_Keyboard.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	m_mapUITexture.insert({ L"W_DButton", pTexture });

	vPos = { 0.490387f, 0.912381f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"D"), _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.2f);
	m_mapUITexture.insert({ L"W_DButtonText", pTexture });

	m_isWiggleA = false;
	Safe_Release(pManagement);
}

void CUI_Interaction::Delete_Wiggle()
{
	Delete_UI_Texture(L"W_Arrow");
	Delete_UI_Texture(L"W_Text");
	Delete_UI_Texture(L"W_AButton");
	Delete_UI_Texture(L"W_AButtonText");
	Delete_UI_Texture(L"W_DButton");
	Delete_UI_Texture(L"W_DButtonText");
}

void CUI_Interaction::Update_Observe()
{
	if (!bObserver)
		return;
	if (exiObservPlayer != m_PlayerNum)
	{
		m_PlayerNum = exiObservPlayer;
		GET_INSTANCE_MANAGEMENT;
		m_pPlayer = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Camper", CAMPER + m_PlayerNum - 1);
		Safe_Release(pManagement);
	}
}

void CUI_Interaction::Delete_Resist()
{
	Delete_UI_Texture(wstring(L"R_Arrow"));
	Delete_UI_Texture(wstring(L"R_Text"));
	Delete_UI_Texture(wstring(L"R_Button"));
	Delete_UI_Texture(wstring(L"R_ButtonText"));
}

void CUI_Interaction::Delete_AllTexture()
{
	for (auto& iter : m_mapUITexture)
		iter.second->SetDead();
	m_mapUITexture.clear();
	for (_int i = 0; i < 4; i++)
	{
		if (m_ProgressBar[i] != nullptr)
		{
			m_ProgressBar[i]->SetDead();
			m_ProgressBar[i] = nullptr;
		}
	}
}

CUI_Texture * CUI_Interaction::Find_UITexture(wstring UIName)
{
	auto& iter = m_mapUITexture.find(UIName);
	if (iter == m_mapUITexture.end())
		return nullptr;
	return iter->second;
}

void CUI_Interaction::Delete_UI_Texture(wstring UIName)
{
	auto& iter = m_mapUITexture.find(wstring(UIName));
	if (iter == m_mapUITexture.end())
		return;
	iter->second->SetDead();
	m_mapUITexture.erase(iter);
}

void CUI_Interaction::Update_ProgressBar()
{
	if (m_ProgressState == CUIManager::PG_None)
		return;

	if (m_ProgressState == CUIManager::PG_Generator ||
		m_ProgressState == CUIManager::PG_Totem ||
		m_ProgressState == CUIManager::PG_Chest ||
		m_ProgressState == CUIManager::PG_ExitDoor)
	{
		GET_INSTANCE_MANAGEMENT;
		CGameObject* pGameObject = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_GameObject", m_InteractionObject);
		Safe_Release(pManagement);
		if (nullptr == pGameObject)
			return;
		
		m_ProgressBar[Bar]->Set_UV(_vec2(pGameObject->Get_ProgressTime() / pGameObject->Get_MaxProgressTime(), 1.f));
		if(m_ProgressState == CUIManager::PG_Generator)
			IsSkillCheck();
	}
	else if(m_ProgressState == CUIManager::PG_HealCamper)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(m_fProgress / m_fMaxProgress, 1.f));
		IsSkillCheck();
	}
	else if (m_ProgressState == CUIManager::PG_HealSelf)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(((CCamper*)m_pPlayer)->GetHeal() / ((CCamper*)m_pPlayer)->GetMaxHeal(), 1.f));
		IsSkillCheck();
	}
	else if (m_ProgressState == CUIManager::PG_BeingHeal||
			m_ProgressState == CUIManager::PG_DyingHeal ||
			m_ProgressState == CUIManager::PG_Dying)
		m_ProgressBar[Bar]->Set_UV(_vec2(((CCamper*)m_pPlayer)->GetHeal() / ((CCamper*)m_pPlayer)->GetMaxHeal(), 1.f));
	else if (m_ProgressState == CUIManager::PG_HookStruggle)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(m_fProgress / m_fMaxProgress, 1.f));
		if (m_fProgress == 0.f)
		{
			for (int i = 0; i < ProgressBarEnd; i++)
				m_ProgressBar[i]->Set_Alpha(0.5f);
		}
		else
		{
			for (int i = 0; i < ProgressBarEnd; i++)
				m_ProgressBar[i]->Set_Alpha(1.f);
		}
	}
	else if (m_ProgressState == CUIManager::PG_HookFree || 
				m_ProgressState == CUIManager::PG_Wiggle	||
				m_ProgressState == CUIManager::PG_Hide		||
				m_ProgressState == CUIManager::PG_UnHide	||
				m_ProgressState == CUIManager::PG_SpiritPower)
		m_ProgressBar[Bar]->Set_UV(_vec2(m_fProgress / m_fMaxProgress, 1.f));
	else if (m_ProgressState == CUIManager::PG_Sabotage)
	{
		m_ProgressBar[Bar]->Set_UV(_vec2(m_fProgress / m_fMaxProgress, 1.f));
		IsSkillCheck();
	}
}

CUI_Interaction * CUI_Interaction::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Interaction*	pInstance = new CUI_Interaction(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Interaction Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Interaction::Clone_GameObject()
{
	CUI_Interaction*	pInstance = new CUI_Interaction(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Interaction Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_Interaction::Free()
{
	CGameObject::Free();
}