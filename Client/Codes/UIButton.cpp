#include "stdafx.h"
#include "..\Headers\UIButton.h"
#include "Management.h"

_USING(Client)

CUIButton::CUIButton(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUIButton::CUIButton(const CUIButton & rhs)
	: CGameObject(rhs)
{
}

HRESULT CUIButton::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");

	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");

	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Shader_Default");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CUIButton::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUIButton::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;


	m_pTransformCom->SetUp_Speed(5.0f, D3DXToRadian(90.0f));
	m_mapfloat["g_fTime"] = 0.f;
	m_mapbool["g_bFadeIn"] = false;
	m_mapbool["g_bFadeOut"] = false;

	return NOERROR;
}

_int CUIButton::Update_GameObject(const _float & fTimeDelta)
{
	if (!bRender)
		return 0;

	_bool Homing = Button_Homing();
	_bool Clicking = false;

	if (Homing)
		Clicking = Button_Click();

	if (bFirst && bGroupSync)
	{
		for (auto pUI : GroupList)
		{
			if (!Homing && !Clicking)
			{
				pUI->DefaultIdle();
				if (bIdleEvent)
					pUI->IdleEvent();
			}
		}
	}
	else if (!bGroupSync)
	{
		if (!Homing && !Clicking)
		{
			DefaultIdle();
			if (bIdleEvent)
				IdleEvent();

			if (bCustomEvent)
				CustomEvent();
		}
	}

	if (bCustomEvent)
		CustomEvent();

	return _int();
}

_int CUIButton::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (0 == m_vecTexCom.size())
		return 0;

	if (!bRender)
	{
		if(bFadeOut && m_fCurFadeTime < m_fFadeOutTime)
		{
			m_fCurFadeTime += fTimeDelta;
		}
		else
			return 0;
	}
	if (bRender)
	{
		if (bFadeIn && m_fCurFadeTime < m_fFadeInTime)
			m_fCurFadeTime += fTimeDelta;
	}

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return _int();
}

void CUIButton::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;
	_bool ChangePass = true;
	LPD3DXEFFECT pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	for (size_t i = 0; i < m_vecTexCom.size(); ++i)
	{
		if (!bUseMultiTex)
		{
			if (FAILED(SetUp_ConstantTable(pEffect, i)))
				return;
		}
		else
		{
			if (FAILED(SetUp_ConstantMultiTable(pEffect, m_iMultiTextureIndex)))
				return;	
		}
		SetUp_ConstantTableCustom(pEffect);

		pEffect->Begin(nullptr, 0);
		if (bRender && bFadeIn && m_fCurFadeTime < m_fFadeInTime)
		{
			_float fFadeIn = (1.f / m_fFadeInTime) * m_fCurFadeTime;
			pEffect->SetFloat("g_fTime", fFadeIn);
			pEffect->BeginPass(5);
			ChangePass = false;
		}		
		else if (!bRender && bFadeOut && m_fCurFadeTime < m_fFadeOutTime)
		{
			_float fFadeOut = 1.f - ((1.f / m_fFadeOutTime) * m_fCurFadeTime);
			pEffect->SetFloat("g_fTime", fFadeOut);
			pEffect->BeginPass(5);
			ChangePass = false;
		}		
		
		if (ChangePass)
		{
			if (!isCheck)
				pEffect->BeginPass(m_iPass);
			else
				pEffect->BeginPass(4);
		}

		pEffect->CommitChanges();

		m_pBufferCom->Render_VIBuffer();

		pEffect->EndPass();
		pEffect->End();
	}
	Safe_Release(pEffect);
}

void CUIButton::SetInfo(_float fX, _float fY, _float SizeX, _float SizeY, SCENEID eID, vector<_tchar*> vecTexTag
	, _uint ShaderPass, void(*funcClickEvent)(), void(*funcHomingEvent)(), void(*funcCustomEvent)())
{
	m_vecTexCom.reserve(vecTexTag.size());

	m_fX = fX;
	m_fY = fY;
	m_fSizeX = SizeX;
	m_fSizeY = SizeY;

	m_iPass = ShaderPass;

	if (NULL != funcClickEvent)
		bClickEvent = true;

	if (NULL != funcHomingEvent)
		bHomingEvent = true;

	if (NULL != funcCustomEvent)
		bCustomEvent = true;

	ClickEvent = funcClickEvent;
	HomingEvent = funcHomingEvent;
	CustomEvent = funcCustomEvent;

	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return;
	pManagement->AddRef();

	m_iTextureIndex = 0;

	for (auto pTexTag : vecTexTag)
	{
		_tchar TexComName[MAX_PATH] = L"";

		swprintf_s(TexComName, L"Com_Texture%d", m_iTextureIndex);

		// For.Com_Texture
		CTexture* pTexture = (CTexture*)pManagement->Clone_Component(eID, pTexTag);

		if (FAILED(Add_Component(TexComName, pTexture)))
			return;

		m_vecTexCom.push_back(pTexture);

		m_iTextureIndex++;
	}

	Safe_Release(pManagement);
}

void CUIButton::SetInfo(_float fX, _float fY, _float SizeX, _float SizeY, SCENEID eID, _tchar * TextureTag
	, _uint ShaderPass, void(*funcClickEvent)(), void(*funcHomingEvent)(), void(*funcCustomEvent)())
{
	m_fX = fX;
	m_fY = fY;
	m_fSizeX = SizeX;
	m_fSizeY = SizeY;

	m_iPass = ShaderPass;

	if (NULL != funcClickEvent)
		bClickEvent = true;

	if (NULL != funcHomingEvent)
		bHomingEvent = true;

	if (NULL != funcCustomEvent)
		bCustomEvent = true;

	ClickEvent = funcClickEvent;
	HomingEvent = funcHomingEvent;
	CustomEvent = funcCustomEvent;

	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return;
	pManagement->AddRef();

	// For.Com_Texture
	CTexture* pTexture = (CTexture*)pManagement->Clone_Component(eID, TextureTag);
	if (FAILED(Add_Component(L"Com_Texture0", pTexture)))
		return;

	m_vecTexCom.push_back(pTexture);

	m_iTextureIndex = 1;

	Safe_Release(pManagement);
}

void CUIButton::SetOriginalSize(_float Value)
{
}

void CUIButton::AddClickEvent(void(*funcClickEvent)())
{
	if (NULL != funcClickEvent)
		bClickEvent = true;

	ClickEvent = funcClickEvent;
}

void CUIButton::AddHomingEvent(void(*funcHomingEvent)())
{
	if (NULL != funcHomingEvent)
		bHomingEvent = true;

	HomingEvent = funcHomingEvent;
}

void CUIButton::AddCustomEvent(void(*funcCustomEvent)())
{
	if (NULL != funcCustomEvent)
		bCustomEvent = true;

	CustomEvent = funcCustomEvent;
}

void CUIButton::AddIdleEvent(void(*funcIdleEvent)())
{
	if (NULL != funcIdleEvent)
		bIdleEvent = true;

	IdleEvent = funcIdleEvent;
}

void CUIButton::SetRender(_bool isRender)
{
	m_fCurFadeTime = 0.f;

	bRender = isRender;
}

void CUIButton::DefaultHoming()
{
	SetAlpha(false);
	if (m_mapfloat["g_fTime"] < 0.5f)
		m_mapfloat["g_fTime"] += fWorldTimer;
	else if (m_mapfloat["g_fTime"] >= 0.5f)
		m_mapfloat["g_fTime"] = 0.5f;
}

void CUIButton::DefaultIdle()
{
	SetAlpha(true);
	if (m_mapfloat["g_fTime"] > 0.f)
		m_mapfloat["g_fTime"] -= fWorldTimer;
	else if (m_mapfloat["g_fTime"] <= 0.f)
		m_mapfloat["g_fTime"] = 0.f;
}

HRESULT CUIButton::SetUp_ConstantTable(LPD3DXEFFECT pEffect, _uint iIndex)
{
	_matrix			matWorld, matRot;
	D3DXMatrixIdentity(&matWorld);

	matWorld._11 = m_fSizeX;
	matWorld._22 = m_fSizeY;

	D3DXMatrixRotationZ(&matRot, m_fDegree);
	matWorld *= matRot;

	matWorld._41 = m_fX - (g_iBackCX >> 1);
	matWorld._42 = -m_fY + (g_iBackCY >> 1);

	pEffect->SetMatrix("g_matWorld", &matWorld);


	_matrix			matView;
	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	_matrix			matProj;
	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	m_vecTexCom[iIndex]->SetUp_OnShader(pEffect, "g_DiffuseTexture");
	pEffect->SetBool("g_bAlpha", bAlpha);

	return NOERROR;
}

HRESULT CUIButton::SetUp_ConstantMultiTable(LPD3DXEFFECT pEffect, _uint iIndex)
{
	_matrix			matWorld, matRot;
	D3DXMatrixIdentity(&matWorld);

	matWorld._11 = m_fSizeX;
	matWorld._22 = m_fSizeY;

	D3DXMatrixRotationZ(&matRot, m_fDegree);
	matWorld *= matRot;

	matWorld._41 = m_fX - (g_iBackCX >> 1);
	matWorld._42 = -m_fY + (g_iBackCY >> 1);

	pEffect->SetMatrix("g_matWorld", &matWorld);


	_matrix			matView;
	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	_matrix			matProj;
	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	m_vecTexCom[0]->SetUp_OnShader(pEffect, "g_DiffuseTexture", iIndex);
	pEffect->SetBool("g_bAlpha", bAlpha);

	return NOERROR;
}

HRESULT CUIButton::SetUp_ConstantTableCustom(LPD3DXEFFECT pEffect)
{
	for (auto mbool : m_mapbool)
		pEffect->SetBool(mbool.first, mbool.second);

	for (auto mint : m_mapint)
		pEffect->SetInt(mint.first, mint.second);

	for (auto mfloat : m_mapfloat)
		pEffect->SetFloat(mfloat.first, mfloat.second);

	for (auto mvector : m_mapvector)
		pEffect->SetVector(mvector.first, &mvector.second);

	for (auto mtextures : m_mapTextures)
		pEffect->SetTexture(mtextures.first, mtextures.second);

	return NOERROR;
}

_bool CUIButton::Button_Click()
{
	if (KEYMGR->MouseDown(0) && bClickEvent)
	{
		if (bFirst && bGroupSync)
		{
			for (auto pUI : GroupList)
			{
				if(pUI->bClickEvent)
					pUI->ClickEvent();
			}
		}
		else if(!bGroupSync)
			ClickEvent();
		return true;
	}
	return false;
}

_bool CUIButton::Button_Homing()
{
	POINT mousePT;

	GetCursorPos(&mousePT);

	ScreenToClient(g_hWnd, &mousePT);

	_vec4 vRect = { (m_fX - m_fSizeX * 0.5f), (m_fX + m_fSizeX * 0.5f)
		,(m_fY - m_fSizeY * 0.5f) ,(m_fY + m_fSizeY * 0.5f) };

	if ((_float)mousePT.x >= vRect.x && (_float)mousePT.x <= vRect.y
		&& (_float)mousePT.y >= vRect.z && (_float)mousePT.y <= vRect.w)
	{
		bHoming = true;

		if (bFirst && bGroupSync)
		{
			for (auto pUI : GroupList)
			{
				if (bHomingEvent)
					pUI->HomingEvent();

				pUI->DefaultHoming();
			}
		}
		else if (!bGroupSync)
		{
			if (bHomingEvent)
				HomingEvent();

			DefaultHoming();
		}

		return true;
	}

	bHoming = false;
	return false;
}


CUIButton * CUIButton::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUIButton*	pInstance = new CUIButton(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUIButton Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUIButton::Clone_GameObject()
{
	CUIButton*	pInstance = new CUIButton(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUIButton Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUIButton::Free()
{
	ClickEvent = nullptr;
	HomingEvent = nullptr;
	CustomEvent = nullptr;
	IdleEvent = nullptr;

	Safe_Release(m_pShaderCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	for (auto pTexCom : m_vecTexCom)
	{
		Safe_Release(pTexCom);
	}
	m_vecTexCom.clear();

	m_mapbool.clear();
	m_mapint.clear();
	m_mapfloat.clear();
	GroupList.clear();

	CGameObject::Free();
}

void CUIButton::SetCheck(_bool b)
{
	isCheck = b;
}
