#include "stdafx.h"
#include "..\Headers\Scene_Stage.h"
#include "Management.h"
#include "Camera_Debug.h"
#include "Camera_Camper.h"
#include "Camera_Slasher.h"
#include "Light_Manager.h"
#include "Scene_Lobby.h"
#include "Camper.h"
#include "Slasher.h"
#include "LoadManager.h"
#include "Moon.h"
#include "Machete.h"
#include "UI_Interaction.h"
#include "MeshTexture.h"
#include "Game_Sound.h"
_USING(Client)

CScene_Stage::CScene_Stage(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{

}

HRESULT CScene_Stage::Ready_Scene()
{
	bGameServer = true;


	if (bUseServer)
	{
		Gamesock = CServerManager::GetInstance()->Ready_Server();

		hThread = (HANDLE)_beginthreadex(nullptr, 0, Game_Thread, 0, 0, nullptr);
	}

	GET_INSTANCE(CLoadManager)->Set_IsLoding(true);
		GET_INSTANCE(CLoadManager)->S1_Camera();
	if(FAILED(GET_INSTANCE(CLoadManager)->S1_BackGround()))
		return E_FAIL;
	GET_INSTANCE(CLoadManager)->LoadMapData(0);




    if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;
	//GET_INSTANCE(CLoadManager)->S1_LightInfo();
	//GET_INSTANCE(CLoadManager)->S1_Camera();
	//GET_INSTANCE(CLoadManager)->S1_BackGround();
	//GET_INSTANCE(CLoadManager)->S1_LightInfo();
	////GET_INSTANCE(CLoadManager)->Set_Progress(0.5f);
	//while (true)
	//{
	//	if (GET_INSTANCE(CLoadManager)->LoadMapData(0))
	//		break;
	//}
	//if (FAILED(Ready_Layer_Camera(L"Layer_Camera")))
	//	return E_FAIL;


	//if (FAILED(Ready_Layer_BackGround(L"Layer_BackGround")))
	//	return E_FAIL;

	if (FAILED(Ready_Layer_AfterImage(L"Layer_AfterImage")))
		return E_FAIL;

	if (FAILED(Ready_LightInfo()))
		return E_FAIL;

	GET_INSTANCE(CSoundManager)->Set_PlayerNum(exPlayerNumber);

	return NOERROR;
}

_int CScene_Stage::Update_Scene(const _float & fTimeDelta)
{
	m_fTime += fTimeDelta;

	if (!m_IsLastInit)
	{
		if (exPlayerNumber > 0 && exPlayerNumber < 5)
		{
			if (server_data.Campers[exPlayerNumber - 1].bConnect)
			{
				if (FAILED(Ready_Layer_Camper(L"Layer_Camper", (_uint)exPlayerNumber)))
					return E_FAIL;

				if (FAILED(Ready_UI()))
					return E_FAIL;
				m_IsLastInit = true;
				bStartGameServer = true;
			}
		}

		else if (exPlayerNumber == 5)
		{
			if (server_data.Slasher.bConnect)
			{
				if (FAILED(Ready_Layer_Slasher(L"Layer_Slasher")))
					return E_FAIL;

				if (FAILED(Ready_UI()))
					return E_FAIL;
				m_IsLastInit = true;
				bStartGameServer = true;
			}
		}
	}
	_float fProgress = GET_INSTANCE(CLoadManager)->Get_Progress();
	if (fProgress >= 5.f)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("BGM"), string("Ingame_Music/Farm_SlaughterHouse_Bounce.ogg"),_vec3(), 0, 0.3f);
		GET_INSTANCE(CLoadManager)->Set_IsLoding(false);
		GET_INSTANCE(CLoadManager)->Set_Progress(0.f);
		GET_INSTANCE_MANAGEMENTR(-1);
		pManagement->Clear_Layers(SCENE_LOBBY);
		Safe_Release(pManagement);
	}
	else if(fProgress >= 1.f)
		GET_INSTANCE(CLoadManager)->Set_Progress(fProgress + fTimeDelta);

	Check_Connection_OtherPlayers();
	EFFMGR->Make_Mist(fTimeDelta);

	Set_Ending();

	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Stage::LastUpdate_Scene(const _float & fTimeDelta)
{
	CColl_Manager::GetInstance()->Coll(fTimeDelta);

	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Stage::Render_Scene()
{

}

size_t CScene_Stage::Game_Thread(LPVOID lpParam)
{
	sendPacket = 0;
	QueryPerformanceFrequency(&GtickPerSecond);
	QueryPerformanceCounter(&GstartTick);
	while (bGameServer)
	{
		QueryPerformanceCounter(&GendTick);
		dGTime = (double)(GendTick.QuadPart - GstartTick.QuadPart) / GtickPerSecond.QuadPart;

		while (dGTime >= 0.011000) {
			sendPacket = 0;
			QueryPerformanceCounter(&GstartTick);
			dGTime = 0;
			if (exPlayerNumber < 5)
				camper_data.Number = exPlayerNumber;
			else
				slasher_data.Number = exPlayerNumber;
			CServerManager::GetInstance()->Data_Exchange(Gamesock, camper_data, client_data, server_data, slasher_data);
			sendPacket++;
		}
	}

	return 0;
}

HRESULT CScene_Stage::Ready_LightInfo()
{
	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return E_FAIL;

	pLight_Manager->AddRef();
	pLight_Manager->Delete_AllLight();
	D3DLIGHT9			LightInfo;
	ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	LightInfo.Type = D3DLIGHT_DIRECTIONAL;
	//LightInfo.Diffuse = D3DXCOLOR(0.185f, 0.166175f, 0.1492f, 1.f);
	//LightInfo.Specular = D3DXCOLOR(0.225f, 0.206175f, 0.1892f, 1.f);
	//LightInfo.Ambient = D3DXCOLOR(0.155f, 0.136175f, 0.1192f, 1.f);

	LightInfo.Diffuse = D3DXCOLOR(0.21f, 0.21f, 0.22f, 1.f);
	LightInfo.Specular = D3DXCOLOR(0.225f, 0.225f, 0.235f, 1.f);
	LightInfo.Ambient = D3DXCOLOR(0.2f, 0.2f, 0.21f, 1.f);

	if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
		return E_FAIL;

	//LightInfo.Type = D3DLIGHT_SPOT;
	//LightInfo.Diffuse = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	//LightInfo.Specular = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	//LightInfo.Ambient = D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.f);

	////LightInfo.Diffuse = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.f);
	////LightInfo.Specular = D3DXCOLOR(0.6f, 0.6f, 0.6f, 1.f);
	////LightInfo.Ambient = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.f);

	//LightInfo.Position = _vec3(3200.f, 50.f, 3200.f);
	//LightInfo.Direction = _vec3(0.f, -1.f, 0.f);
	//LightInfo.Theta = D3DXToRadian(6.5f);
	//LightInfo.Phi = D3DXToRadian(10.5f);
	//LightInfo.Range = 1000.f;

	//if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
	//	return E_FAIL;

	//for (int i = 0; i < 2; ++i)

	//ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	//LightInfo.Type = D3DLIGHT_POINT;
	//LightInfo.Diffuse = D3DXCOLOR(4.f, 4.f, 4.f, 1.f);
	//LightInfo.Specular = LightInfo.Diffuse;
	//LightInfo.Ambient = D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.f);

	//LightInfo.Position = _vec3(3500.f, 5.f, 3500.f);
	//LightInfo.Range = 3000.0f;

	//if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
	//	return E_FAIL;

	//ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	//LightInfo.Type = D3DLIGHT_SPOT;
	//LightInfo.Diffuse = D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f);
	//LightInfo.Specular = LightInfo.Diffuse;
	//LightInfo.Ambient = D3DXCOLOR(1.f, 0.1f, 0.1f, 1.f);
	//LightInfo.Attenuation0 = 0.0001f;
	//LightInfo.Attenuation1 = 0.0001f;
	//LightInfo.Attenuation2 = 0.0001f;

	//LightInfo.Position = _vec3(3500.f, 25.f, 3500.f);
	//LightInfo.Range = 10000.0f;
	//LightInfo.Falloff = 1.f;
	//LightInfo.Theta = D3DXToRadian(12.5f);
	//LightInfo.Phi = D3DXToRadian(17.5f);
	//LightInfo.Direction = _vec3(0.f, -1.f, 0.f);

	//if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
	//	return E_FAIL;

	Safe_Release(pLight_Manager);

	return NOERROR;
}

// 원형객체를 생성해 놓는다.
HRESULT CScene_Stage::Ready_Prototype_GameObject()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Item", SCENE_STAGE, L"Layer_Item")))
		return E_FAIL;

	pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Item").front()->SetDead();

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Prototype_Component()
{
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Camera(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Camera_Debug
	CCamera_Debug*		pCameraObject = nullptr;
	CCamera_Camper*		pCameraCamper = nullptr;
	//CCamera_Slasher*	pCameraSlasher = nullptr;

	// 원형카메라를 복제해서 레이어에 추가할(실제 사용할)객체를 생성한다.레이어에 추가한다ㅏ.
	// 그렇게 복제된 객체를 건져온다.

	/*if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Debug", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraObject)))
		return E_FAIL;*/

	if (0 == exPlayerNumber)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Debug", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraObject)))
			return E_FAIL;
	}
	else/* if (exPlayerNumber < 5 && exPlayerNumber > 0)*/
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Camper", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraCamper)))
			return E_FAIL;
	}
	//else
	//{
	//	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Slasher", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraSlasher)))
	//		return E_FAIL;
	//}

	CAMERADESC		CameraDesc;
	ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
	CameraDesc.vEye = _vec3(3200.f, 20.f, 3200.f);
	CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
	CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);
		
	PROJDESC		ProjDesc;
	ZeroMemory(&ProjDesc, sizeof(PROJDESC));
	ProjDesc.fFovY = D3DXToRadian(87.0f);
	ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
	ProjDesc.fNear = 5.f;
	ProjDesc.fFar = 30000.f;

	if (0 == exPlayerNumber)
	{
		if (FAILED(pCameraObject->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
			return E_FAIL;
	}
	else/* if (exPlayerNumber < 5 && exPlayerNumber > 0)*/
	{
		if (FAILED(pCameraCamper->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
			return E_FAIL;
	}
	//else
	//{
	//	if (FAILED(pCameraSlasher->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
	//		return E_FAIL;
	//}

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_BackGround(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Sky
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Sky", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Moon", SCENE_STAGE, L"Layer_Moon")))
		return E_FAIL;

	//EFFMGR->Make_Effect(CEffectManager::E_Fire,_vec3(3200.f,500.f,3200.f));
	EFFMGR->Make_Effect(CEffectManager::E_Mist);
	
	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Camper(const _tchar * pLayerTag, _uint Index)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Player
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camper", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	((CCamper*)pManagement->Get_ObjectList(SCENE_STAGE, pLayerTag).back())->Set_Index(Index);
	Safe_Release(pManagement);


	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Slasher(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	if (server_data.Slasher.iCharacter == 0)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Slasher", SCENE_STAGE, pLayerTag)))
			return E_FAIL;

		((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, pLayerTag).back())->Set_Index(server_data.Slasher.iCharacter);

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Machete", SCENE_STAGE, L"Layer_Machete")))
			return E_FAIL;
		((CMachete*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Machete").back())->Set_Index(server_data.Slasher.iCharacter);

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Bell", SCENE_STAGE, L"Layer_Bell")))
			return E_FAIL;

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_WraithEffect", SCENE_STAGE, L"Layer_wra")))
			return E_FAIL;
	}
	else
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Slasher", SCENE_STAGE, pLayerTag)))
			return E_FAIL;

		((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, pLayerTag).back())->Set_Index(server_data.Slasher.iCharacter);

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Machete", SCENE_STAGE, L"Layer_Machete")))
			return E_FAIL;
		((CMachete*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Machete").back())->Set_Index(server_data.Slasher.iCharacter);
	}
	
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_AngerEffect", SCENE_STAGE, L"Layer_AngerEffect")))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_AfterImage(const _tchar * pLayerTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_AfterImage", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_UI()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_GameState", SCENE_STAGE, L"Layer_UI_State")))
		return E_FAIL;
	CUI_Interaction* pUI_Interaction = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Interaction", SCENE_STAGE, L"Layer_UI_Interaction", (CGameObject**)&pUI_Interaction)))
		return E_FAIL;
	CUI_Score* pUI_Score = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Score", SCENE_STAGE, L"Layer_UI_Score",(CGameObject**)&pUI_Score)))
		return E_FAIL;
	GET_INSTANCE(CUIManager)->Set_UI(pUI_Interaction, pUI_Score);
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Perk", SCENE_STAGE, L"Layer_UI_Perk")))
		return E_FAIL;

	CGame_Sound* pGame_Sound= nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Game_Sound", SCENE_STAGE, L"Layer_Game_Sound", (CGameObject**)&pGame_Sound)))
		return E_FAIL;
	pGame_Sound->Set_Player(exPlayerNumber);
	Safe_Release(pManagement);
	return NOERROR;
}

void CScene_Stage::Set_Ending()
{
	if (GET_INSTANCE(CLoadManager)->Get_Progress() != 0.f)
		return;
	if (m_bEnding)
		return;
	if (KEYMGR->KeyDown(DIK_F3));
	else
		return;
	//else if (exPlayerNumber != 5)
	//{
	//	if (!((camper_data.iCondition == CCamper::BLOODDEAD ||
	//		camper_data.iCondition == CCamper::HOOKDEAD ||
	//		camper_data.iCondition == CCamper::ESCAPE) &&
	//		!camper_data.bLive))
	//		return;
	//}             
	//else
	//{
	//	_int Connect = 0;
	//	for (int i = 0; i < 4; i++)
	//	{
	//		if (server_data.Campers[i].bLive)
	//			return;
	//		if (server_data.Campers[i].bConnect)
	//			Connect++;
	//	}
	//	if (Connect == 0)
	//		return;
	//}
	m_bEnding = true;
	GET_INSTANCE_MANAGEMENT;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Ending", SCENE_STAGE, L"Layer_UI_Ending")))
		return;
	Safe_Release(pManagement);
}

CScene_Stage * CScene_Stage::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Stage*	pInstance = new CScene_Stage(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Stage Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);		
	}
	return pInstance;
 }

void CScene_Stage::Check_Connection_OtherPlayers()
{
	if (m_fTime >= 40.f)
	{
		m_fTime = 41.f;
		return;
	}

	if (exPlayerNumber < 5)
		bConnectedCamper[exPlayerNumber - 1] = true;
	else if (exPlayerNumber == 5)
		bConnectedSlasher = true;

	for (int i = 0; i < 4; ++i)
	{
		if (server_data.Campers[i].bConnect && server_data.Campers[i].bLive && !bConnectedCamper[i])
		{
			bConnectedCamper[i] = true;
			Ready_Layer_Camper(L"Layer_Camper", i+1);
		}
		else if (!server_data.Campers[i].bConnect && bConnectedCamper[i])
		{
			bConnectedCamper[i] = false;
		}
	}

	if (server_data.Slasher.bConnect && !bConnectedSlasher)
	{
		bConnectedSlasher = true;
		Ready_Layer_Slasher(L"Layer_Slasher");
	}
	else if (!server_data.Slasher.bConnect && bConnectedSlasher)
	{
		bConnectedSlasher = false;
	}
}

void CScene_Stage::Free()
{

	if (bUseServer)
	{
		CServerManager::GetInstance()->Close_Game();

		CloseHandle(hThread);
	}

	bGameServer = false;
	GET_INSTANCE_MANAGEMENT
	pManagement->Clear_Layers(SCENE_STAGE);

	Safe_Release(pManagement);

	//CScene_Lobby*	pNewScene = CScene_Lobby::Create(m_pGraphic_Device);
	//if (nullptr == pNewScene)
	//	return;

	//if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
	//	return;

	//Safe_Release(pNewScene);

	CScene::Free();
}
