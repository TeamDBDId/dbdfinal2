#include "stdafx.h"
#include "..\Headers\Lamp_02.h"
#include "Management.h"
#include "Defines.h"
#include "CustomLight.h"
#include "Light_Manager.h"
#include "MeshTexture.h"

_USING(Client)

CLamp_02::CLamp_02(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CLamp_02::CLamp_02(const CLamp_02 & rhs)
	: CGameObject(rhs)
{
}

HRESULT CLamp_02::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CLamp_02::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_LateInit = true;
	return NOERROR;
}

_int CLamp_02::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	Init_Light();
	return _int();
}

_int CLamp_02::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;
	if (m_pCustomLight == nullptr)
		return 0;
	m_pCustomLight->SetRender(false);
	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	Compute_CameraDistance(&vPosition);

	if (m_fCameraDistance > m_Max* 100.f)
		return 0;

	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	if ((m_iOtherOption == 2 && matView._42 <= 200.f) ||
		(m_iOtherOption == 1 && matView._42 >= 400.f) ||
		(matView._42 >= 500.f) ||
		matView._42 <= -200.f)
		return 0;

	_matrix		matLocal, matWorld;
	matWorld = m_pTransformCom->Get_Matrix();
	matLocal = m_pMeshCom->Get_LocalTransform();

	matWorld = matLocal * matWorld;
	vPosition = *(_vec3*)&matWorld.m[3][0];
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);

	if (!m_pFrustumCom->Culling_Frustum(&m_vLocalPos, m_matLocalInv, m_Max))
		return 0;

	m_pCustomLight->SetRender(true);
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	if (FAILED(m_pRendererCom->Add_StemGroup(this)))
		return -1;

	//m_pCustomLight->LastUpdate_GameObject(fTimeDelta);
	return _int();
}

void CLamp_02::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(3);

	pEffect->SetTexture("g_LightEmissive", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_NeonLight_E.tga")));


	if (FAILED(SetUp_ConstantTable(pEffect, 0)))
		return;

	pEffect->CommitChanges();

	m_pMeshCom->Render_Mesh(0);

	pEffect->EndPass();

	pEffect->BeginPass(0);

	if (FAILED(SetUp_ConstantTable(pEffect, 1)))
		return;

	pEffect->CommitChanges();

	m_pMeshCom->Render_Mesh(1);

	pEffect->EndPass();

	pEffect->End();

	Safe_Release(pEffect);


	m_pColliderCom->Render_Collider();

	m_pLightCollider->Render_Collider();
}

void CLamp_02::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
}

void CLamp_02::Render_Stemp()
{
	LPD3DXEFFECT	pEffect = m_pRendererCom->GetCubeEffectHandle()->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(13);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(i);
		if (nullptr == pSubSet)
			return;

		m_pMeshCom->Render_Mesh(i);

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CLamp_02::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);
	m_iOtherOption = OtherOption;
	if (Key == nullptr)
		return;

	m_Key = Key;

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_Lamp_02");
	Add_Component(L"Com_Mesh", m_pMeshCom);

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_AABB, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);

	_vec3 vMax = m_pMeshCom->GetMax();
	_vec3 vMin = m_pMeshCom->GetMin();
	_vec3 vResult = (vMax - vMin) * 1.f;

	m_Max = max(vResult.x, max(vResult.y, vResult.z));

	m_matLocalInv = m_pMeshCom->Get_LocalTransform();

	m_matLocalInv = m_matLocalInv * matWorld;
	m_vLocalPos = *(_vec3*)&m_matLocalInv.m[3][0];
	D3DXMatrixInverse(&m_matLocalInv, nullptr, &m_matLocalInv);
}

_matrix CLamp_02::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CLamp_02::Get_Key()
{
	return m_Key.c_str();
}

_int CLamp_02::Get_OtherOption()
{
	return 0;
}

void CLamp_02::Init_Light()
{
	if (m_LateInit)
		return;
	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 700.f, 700.f, 700.f);
	_vec3 vLocalPos = { 0.f, -10.f, -30.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_OBB, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider", m_pLightCollider);

	//D3DXVec3TransformCoord(&vLocalPos, &vLocalPos, m_pTransformCom->Get_Matrix_Pointer());

	_vec3 vCenter = m_pLightCollider->Get_Center();

	m_pCustomLight = nullptr;

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight)))
		return;

	m_pCustomLight->Add_Light();
	m_pCustomLight->AddRef();

	m_pCustomLight->Set_Position(vCenter);
	m_pCustomLight->Set_Type(D3DLIGHT_SPOT);

	//m_pCustomLight->Set_Diffuse(D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f));
	m_pCustomLight->Set_Range(200.f);

	m_pCustomLight->Set_Diffuse(D3DXCOLOR(3.6f, 3.6f, 3.5f, 1.f));
	m_pCustomLight->Set_Theta(D3DXToRadian(17.5f));
	m_pCustomLight->Set_Phi(D3DXToRadian(21.5f));

	//_vec3 vLightDir, vAxis;
	//vLightDir = -*m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	//vAxis = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
	_vec3 vLightDir = -*m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	vLightDir.y -= 0.95f;

	//_matrix matLot = m_pTransformCom->Get_Matrix();

	//D3DXMatrixRotationX(&matLot, D3DXToRadian(-300.f));

	//D3DXVec3TransformCoord(&vLightDir, &_vec3(0.f, -1.f, 0.f), m_pTransformCom->Get_Matrix_Pointer());
	m_pCustomLight->Set_Direction(vLightDir);


	//AddShadowMap
	GET_INSTANCE_MANAGEMENT;

	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static"))
	{
		m_pCustomLight->Add_ShadowCubeGroup(pObj);
	}
	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject"))
	{
		m_pCustomLight->Add_ShadowCubeGroup(pObj);
	}
	Safe_Release(pManagement);
	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return;

	pLight_Manager->AddRef();

	pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());
	m_LateInit = true;
	Safe_Release(pLight_Manager);
	
}

HRESULT CLamp_02::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CLamp_02::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	return NOERROR;
}

CLamp_02 * CLamp_02::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CLamp_02*	pInstance = new CLamp_02(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CLamp_02 Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CLamp_02::Clone_GameObject()
{
	CLamp_02*	pInstance = new CLamp_02(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CLamp_02 Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CLamp_02::Free()
{
	Safe_Release(m_pLightCollider);
	Safe_Release(m_pCustomLight);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
