#include "stdafx.h"
#include "Effect_BloodAnim.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_BloodAnim::CEffect_BloodAnim(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_BloodAnim::CEffect_BloodAnim(const CEffect_BloodAnim & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_BloodAnim::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_BloodAnim::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;


	return NOERROR;
}

_int CEffect_BloodAnim::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (2.5f < m_fTime)
		m_isDead = true;


	return _int();
}

_int CEffect_BloodAnim::LastUpdate_GameObject(const _float & _fTick)
{
	if (nullptr == m_pRendererCom)
		return -1;


	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;



	return _int();
}

void CEffect_BloodAnim::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(m_iPass);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CEffect_BloodAnim::Set_Param(const _vec3 & _vPos, const void* _pVoid)
{
}

void CEffect_BloodAnim::Set_Param(const BloodType & _eType)
{
	m_eType = _eType;

	switch (m_eType)
	{
	case B_Anim:
		Make_Anim();
		break;
	case B_DropAnim:
		Make_DropAnim();
		break;
	case B_Drop:
		Make_Drop();
		break;
	case B_DropBig:
		Make_DropBig();
		break;
	}




}

void CEffect_BloodAnim::Make_Drop()
{
	_float fSize = Math_Manager::CalRandFloatFromTo(20.f, 30.f);

	m_uiSize = {
		Math_Manager::CalRandFloatFromTo(0,g_iBackCX),
		Math_Manager::CalRandFloatFromTo(0,g_iBackCY),
		fSize,fSize };

}

void CEffect_BloodAnim::Make_DropBig()
{
	m_uiSize = {
		Math_Manager::CalRandFloatFromTo(0,g_iBackCX),
		Math_Manager::CalRandFloatFromTo(0,g_iBackCY),
		800.f,800.f };
}


void CEffect_BloodAnim::Make_DropAnim()
{
	_float fSize = Math_Manager::CalRandFloatFromTo(20.f, 30.f);

	m_uiSize = {
		Math_Manager::CalRandFloatFromTo(0,g_iBackCX),
		Math_Manager::CalRandFloatFromTo(0,g_iBackCY),
		fSize,fSize*4.f };

	m_iPass = 1;
}


void CEffect_BloodAnim::Make_Anim()
{
	m_iNum = Math_Manager::CalRandIntFromTo(3,5);

	m_uiSize = { 
		Math_Manager::CalRandFloatFromTo(256,g_iBackCX-256),
		Math_Manager::CalRandFloatFromTo(144,g_iBackCY-144),
		600.f,600.f };

	m_iPass = 1;
}




HRESULT CEffect_BloodAnim::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Blood");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;


	
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_BloodAnim");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;
	//m_pTextureCom[1] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_BloodAnim2");
	//if (FAILED(Add_Component(L"Com_Texture1", m_pTextureCom[1])))
	//	return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_BloodAnim::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	_matrix			matWorld;
	D3DXMatrixIdentity(&matWorld);


	matWorld._11 = m_uiSize.fSizeX;// +m_fPer*100.f;
	matWorld._22 = m_uiSize.fSizeY;


	matWorld._41 = m_uiSize.fX - (g_iBackCX >> 1);
	matWorld._42 = -m_uiSize.fY + (g_iBackCY >> 1);

	_pEffect->SetMatrix("g_matWorld", &matWorld);

	_matrix			matView;
	D3DXMatrixIdentity(&matView);
	_pEffect->SetMatrix("g_matView", &matView);

	_matrix			matProj;
	D3DXMatrixOrthoLH(&matProj, g_iBackCX, g_iBackCY, 0.f, 1.f);
	_pEffect->SetMatrix("g_matProj", &matProj);


	_float	fAlpha = 1.f;

	switch (m_eType)
	{
	case B_Drop:
		m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0", 0);
		break;
	case B_DropBig:
		m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0", 1);
		break;
	case B_DropAnim:
		m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0", 2);
		_pEffect->SetFloat("g_fDot", 0.25f);
		_pEffect->SetFloat("g_iColumn", _uint(m_fTime*6.f) / 4);
		_pEffect->SetFloat("g_iRow", _uint(m_fTime*6.f) % 4);
		break;
	case B_Anim:
		m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0", m_iNum);
		_pEffect->SetFloat("g_fDot",0.1f);
		_pEffect->SetFloat("g_iColumn", _uint(m_fTime*38.f) / 10);
		_pEffect->SetFloat("g_iRow", _uint(m_fTime*38.f) % 10);
		fAlpha = 0.7f;
		break;
	}
	
	if (2.f < m_fTime)
	{
		fAlpha *= (2.5f - m_fTime)*2.f;
	}
	_pEffect->SetFloat("g_fAlpha", fAlpha);

	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_BloodAnim * CEffect_BloodAnim::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_BloodAnim*	pInst = new CEffect_BloodAnim(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_BloodAnim::Clone_GameObject()
{
	CEffect_BloodAnim*	pInst = new CEffect_BloodAnim(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_BloodAnim Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_BloodAnim::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pTextureCom);
	//지울것들



	CBaseEffect::Free();
}
