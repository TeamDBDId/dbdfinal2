#include "stdafx.h"
#include "..\Headers\Action_jesture.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Action_HealCamper.h"

_USING(Client)

CAction_Jesture::CAction_Jesture()
{
}

HRESULT CAction_Jesture::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_fDelay = 0.f;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	CCamper::CONDITION eCondition = pCamper->GetCurCondition();

	if (KEYMGR->KeyPressing(DIK_1))
	{
		if (KEYMGR->KeyPressing(DIK_LCONTROL))
		{
			if (eCondition == CCamper::HEALTHY)
			{
				pCamper->Set_State(AC::CrouchPointTo);
				m_iState = AC::CrouchPointTo;
			}
			else if (eCondition == CCamper::INJURED)
			{
				pCamper->Set_State(AC::InjuredCrouchPointTo);
				m_iState = AC::InjuredCrouchPointTo;
			}
		}
		else
		{
			if (eCondition == CCamper::HEALTHY)
			{
				pCamper->Set_State(AC::PointTo);
				m_iState = AC::PointTo;
			}
			else if (eCondition == CCamper::INJURED)
			{
				pCamper->Set_State(AC::InjuredPointTo);
				m_iState = AC::InjuredPointTo;
			}
		}
	}
	else if (KEYMGR->KeyPressing(DIK_2))
	{
		if (KEYMGR->KeyPressing(DIK_LCONTROL))
		{
			if (eCondition == CCamper::HEALTHY)
			{
				pCamper->Set_State(AC::CrouchComeHere);
				m_iState = AC::CrouchPointTo;
			}
			else if (eCondition == CCamper::INJURED)
			{
				pCamper->Set_State(AC::InjuredCrouchComeHere);
				m_iState = AC::InjuredCrouchPointTo;
			}
		}
		else
		{
			if (eCondition == CCamper::HEALTHY)
			{
				pCamper->Set_State(AC::ComeHere);
				m_iState = AC::PointTo;
			}
			else if (eCondition == CCamper::INJURED)
			{
				pCamper->Set_State(AC::InjuredComeHere);
				m_iState = AC::InjuredPointTo;
			}
		}
	}

	return NOERROR;
}

_int CAction_Jesture::Update_Action(const _float & fTimeDelta)
{


	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	m_fDelay += fTimeDelta;

	if (m_fDelay > 0.3f)
	{
		if (KEYMGR->KeyDown(DIK_W) || KEYMGR->KeyDown(DIK_A) || KEYMGR->KeyDown(DIK_S) || KEYMGR->KeyDown(DIK_D))
			return END_ACTION;

	}
	

	if (pMeshCom->IsOverTime(0.3f))
	{
		if (m_iState == AC::PointTo || m_iState == AC::ComeHere)
			((CCamper*)m_pGameObject)->Set_State(AC::Idle);
		else if (m_iState == AC::CrouchPointTo)
			((CCamper*)m_pGameObject)->Set_State(AC::CrouchStand);
		else if (m_iState == AC::InjuredCrouchPointTo)
			((CCamper*)m_pGameObject)->Set_State(AC::Injured_CrouchIdle);
		else if (m_iState == AC::InjuredPointTo)
			((CCamper*)m_pGameObject)->Set_State(AC::Injured_Idle);

		return END_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_Jesture::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
}

void CAction_Jesture::Send_ServerData()
{
}

void CAction_Jesture::Free()
{
	CAction::Free();
}
