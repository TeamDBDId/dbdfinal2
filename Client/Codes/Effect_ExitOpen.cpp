#include "stdafx.h"
#include "Effect_ExitOpen.h"
#include "Management.h"

#include "Effect_Particle.h"

#include "Math_Manager.h"
_USING(Client)

CEffect_ExitOpen::CEffect_ExitOpen(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_ExitOpen::CEffect_ExitOpen(const CEffect_ExitOpen & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_ExitOpen::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_ExitOpen::Ready_GameObject()
{

	return NOERROR;
}

_int CEffect_ExitOpen::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;



	return _int();
}

_int CEffect_ExitOpen::LastUpdate_GameObject(const _float & _fTick)
{



	return _int();
}

void CEffect_ExitOpen::Render_GameObject()
{


}

void CEffect_ExitOpen::Set_Param(const _vec3* _vPos)
{
	m_pPos = _vPos;
	Make_Particle();
	m_isDead = true;
}


void CEffect_ExitOpen::Make_Particle()
{

	GET_INSTANCE_MANAGEMENT;
	
	

	for (size_t i = 0; i < 70; ++i)
	{
		CEffect_Particle* pEffect = nullptr;
		if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_Particle", SCENE_STAGE, L"Layer_EParticle", (CGameObject**)&pEffect)))
			_MSG_BOX("Fail Make_Particle");
		pEffect->Set_Param(*m_pPos);
	}

	Safe_Release(pManagement);

}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_ExitOpen * CEffect_ExitOpen::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_ExitOpen*	pInst = new CEffect_ExitOpen(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_ExitOpen::Clone_GameObject()
{
	CEffect_ExitOpen*	pInst = new CEffect_ExitOpen(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_ExitOpen Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_ExitOpen::Free()
{

	CGameObject::Free();
}