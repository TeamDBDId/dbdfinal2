#include "stdafx.h"
#include "..\Headers\Slasher.h"
#include "Management.h"
#include "Light_Manager.h"
#include "Camera_Camper.h"
#include "Machete.h"
#include "CustomLight.h"
#include "Action_Drop.h"
#include "Action_Grab_Generic_Fast.h"
#include "Action_Grab_Obstacles.h"
#include "Action_HookIn.h"
#include "Action_PickUp.h"
#include "Action_Damage_Generator.h"
#include "Action_Destroy_Pallet.h"
#include "Action_Search_Locker.h"
#include "Action_Stun_Pallet.h"
#include "Action_StunDrop.h"
#include "Action_WindowVault.h"
#include "Action_Hit.h"
#include "Action_Mori.h"
#include "Action_MoveLerp.h"
#include "Camper.h"
#include "Plank.h"
#include "MeshTexture.h"
#include "Generator.h"
#include "AfterImage.h"
#include <string>
#include "Math_Manager.h"
_USING(Client)

CSlasher::CSlasher(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CSlasher::CSlasher(const CSlasher & rhs)
	: CGameObject(rhs)
{
}

HRESULT CSlasher::Ready_Prototype()
{
	m_eObjectID = SLASHER;
	return NOERROR;
}

HRESULT CSlasher::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (FAILED(Ready_Action()))
		return E_FAIL;

	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	m_eObjectID = SLASHER;

	m_fMoveSpeed = 460.f;
	m_fRotateSpeed = D3DXToRadian(360.f);
	m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);
	m_fSpiritSkillTime = SPRIIT_POWER_BAR;
	
	//m_pTransformCom->Scaling(0.02f, 0.02f, 0.02f);
	m_SkillBreath = false;
	m_BreathTime = 0.f;
	m_fSkillTime = 15.f;
	m_bSkillUse = false;
	m_iCharacter = C_WRAITH;
	if (m_iCharacter == C_WRAITH)
	{
		m_vecFPVAnim.reserve(16);
		m_vecFPVAnim.push_back(TW_Attack_Bow_FPV);
		m_vecFPVAnim.push_back(TW_Attack_In_FPV);
		m_vecFPVAnim.push_back(TW_Attack_Miss_Out_FPV);
		m_vecFPVAnim.push_back(TW_Attack_Swing_FPV);
		m_vecFPVAnim.push_back(TW_Attack_Wipe_FPV);
		//m_vecFPVAnim.push_back(TW_Bell_In_FPV);
		//m_vecFPVAnim.push_back(TW_Bell_Loop_FPV);
		m_vecFPVAnim.push_back(TW_Bell_Out_FPV);
		m_vecFPVAnim.push_back(TW_Hook_In_FPV);
		m_vecFPVAnim.push_back(TW_Close_Hatch_FPV);
		m_vecFPVAnim.push_back(TT_Carry_Attack_BOW_FPV);
		m_vecFPVAnim.push_back(TT_Carry_Attack_IN_FPV);
		m_vecFPVAnim.push_back(TT_Carry_Attack_Out_FPV);
		m_vecFPVAnim.push_back(TT_Carry_Attack_Swing_FPV);
		m_vecFPVAnim.push_back(TT_Grab_Generic_Fast_FPV);
		m_vecFPVAnim.push_back(TT_Grab_Obstacles_BK_FPV);
		m_vecFPVAnim.push_back(TT_Grab_Obstacles_FT_FPV);
	}
	
	if (!bUseServer)
		return NOERROR;

	m_pInput_Device = CInput_Device::GetInstance();

	// Light
	m_pCustomLight = nullptr;

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight)))
		return E_FAIL;

	m_pCustomLight->Add_Light();
	m_pCustomLight->AddRef();

	m_pCustomLight->Set_Position(_vec3(3200.f, 300.f, 3200.f));
	m_pCustomLight->Set_Type(D3DLIGHT_SPOT);

	m_pCustomLight->Set_Diffuse(D3DXCOLOR(5.f, 0.81f, 0.81f, 1.f));
	//m_pCustomLight->Set_Ambient(D3DXCOLOR(1.f, 0.18f, 0.18f, 1.f));
	m_pCustomLight->Set_Range(600.f);
	m_pCustomLight->Set_Theta(D3DXToRadian(43.5f));
	m_pCustomLight->Set_Phi(D3DXToRadian(53.5f));
	m_pCustomLight->Set_Direction(_vec3(0,1,0));
	m_pCustomLight->SetMovable(true);
	m_pCustomLight->SetRender(true);



	//AddShadowMap
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	m_pCustomLight->Add_ShadowCubeGroup(pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static").front());
	Safe_Release(pManagement);

	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return E_FAIL;

	pLight_Manager->AddRef();

	pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());

	Safe_Release(pLight_Manager);

	if (5 == exPlayerNumber)
		m_pCustomLight->SetRender(false);
	//m_pRendererCom->Set_LutTexture(GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"LUT_Greenish.jpg")));




	return NOERROR;
}

_int CSlasher::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return DEAD_OBJ;

	if (!bUseServer)
	{
		m_fTimeDelta = fTimeDelta;
		return 0;
	}
	m_fTimeDelta = fTimeDelta;

	Init_Camera();
	Key_Input(fTimeDelta);

	CommunicationWithServer();
	State_Check();
	LowerState_Check();
	Compute_Map_Index();
	Check_Spirit_MoriEffect();
	Check_Wraith_MoriEffect();
	Cal_LightPos();
	Update_Actions(fTimeDelta);
	Update_Breath(fTimeDelta);
	if(server_data.Slasher.iCharacter == C_SPIRIT)
		Check_AfterImage(fTimeDelta);
	
	Processing_Others(fTimeDelta);
	return _int();
}

_int CSlasher::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return DEAD_OBJ;
	Check_AngerGauge();
	
	/*cout << m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->x << " ";
	cout << m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->z << endl;
*/

	Change_TimeOfAnimation(fTimeDelta);
	m_pMeshCom->Play_Animation(m_fTimeDelta);
	m_pMeshCom->Play_ServeAnimation(m_fTimeDelta);

	if (exPlayerNumber == 5 && m_iCharacter == 0 && m_eCurState != AS::WR_Bell_Loop)
		m_pMeshCom->Play_ArmAnimation(m_fTimeDelta);

	if (nullptr == m_pRendererCom)
		return -1;

	if (m_bNotRenderSlasher)
		return 0;
	
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance < 1000.f)
		m_pRendererCom->Add_CubeGroup(this);

	if (m_bPenetration)
	{
		m_pRendererCom->Add_StencilGroup(this);
	}

	if (C_WRAITH == server_data.Slasher.iCharacter && !m_bSkillUse && m_bSkill)
	{
		if (m_pCustomLight != nullptr)
			m_pCustomLight->SetRender(false);

		if (5 == exPlayerNumber)
		{
			if (m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y >= -10.f)
			{
				if (m_pRendererCom->Get_Brightness() > 25.f)
					m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() - fTimeDelta * 40.f);
				else
					m_pRendererCom->Set_Brightness(25.f);
			}
		}

		return 0;
	}
	else if(C_WRAITH == server_data.Slasher.iCharacter && !m_bSkillUse && !m_bSkill)
	{
		if (exPlayerNumber != 5 && m_pCustomLight != nullptr)
			m_pCustomLight->SetRender(true);

		if (5 == exPlayerNumber)
		{
			if (m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y < -10.f)
			{
				if (m_pRendererCom->Get_Brightness() > 17.f)
					m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() - fTimeDelta * 40.f);
				else
					m_pRendererCom->Set_Brightness(17.f);

				_vec3 vRGB = m_pRendererCom->Get_RGB();
				_vec3 vCalRGB = vRGB;
				if (vRGB.x < 3.5f)
					vCalRGB.x += fTimeDelta * 0.35f;
				else
					vCalRGB.x = 3.5f;
				if (vRGB.y > 0.1f)
					vCalRGB.y -= fTimeDelta * 0.1f;
				else
					vCalRGB.y = 0.1f;
				if (vRGB.z > 0.1f)
					vCalRGB.z -= fTimeDelta * 0.1f;
				else
					vCalRGB.z = 0.1f;

				m_pRendererCom->Set_RGB(vCalRGB);
			}
			else
			{
				if (m_pRendererCom->Get_Brightness() < 35.f)
					m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() + fTimeDelta * 40.f);
				else
					m_pRendererCom->Set_Brightness(35.f);


				_vec3 vRGB = m_pRendererCom->Get_RGB();
				_vec3 vCalRGB = vRGB;
				if (vRGB.x > 0.5f)
					vCalRGB.x -= fTimeDelta * 0.35f;
				else
					vCalRGB.x = 0.5f;
				if (vRGB.y < 0.5f)
					vCalRGB.y += fTimeDelta * 0.1f;
				else
					vCalRGB.y = 0.5f;
				if (vRGB.z < 0.5f)
					vCalRGB.z += fTimeDelta * 0.1f;
				else
					vCalRGB.z = 0.5f;

				m_pRendererCom->Set_RGB(vCalRGB);
			}
		}
	}
	
	if (C_WRAITH == server_data.Slasher.iCharacter && m_bSkillUse)
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
			return -1;
	}
	else
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;
	}

	return _int();
}

void CSlasher::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	if (nullptr == m_pSkinTextures)
		return;

	_bool Aft = false;
	_matrix matRand;

	if (m_bAfterImaged && exPlayerNumber < 5)
		m_fSpiritSkillTime += m_fTimeDelta;

	else if (!m_bAfterImaged && C_SPIRIT == m_iCharacter)
	{
		if (m_fSpiritNoise < 0.f)
			m_fSpiritNoise = 7.f;
		else
			m_fSpiritNoise -= m_fTimeDelta;

		_int RandX = (rand() % 60) - 30;
		_int RandZ = (rand() % 60) - 30;

		matRand = m_pTransformCom->Get_Matrix();

		matRand.m[3][0] += RandX;
		matRand.m[3][2] += RandZ;
	}

	_uint iPass = 2;
	if (C_WRAITH == m_iCharacter && m_bSkillUse)
		iPass = 7;

	if(exPlayerNumber != 5 && m_bPenetration)
		iPass = 4;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	vector<_matrix*> vecPrev;
	vector<_uint> vecNumFrame;

	pEffect->Begin(nullptr, 0);

  	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		if (m_iCharacter == C_WRAITH)
		{
			if (!m_bRendHead && i == 1)
				continue;
		}
		else
		{	
			if ((m_eCurState != AS::Mori) && !m_bRendHead && i == 1)
				continue;
		}

		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		
		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			if (m_iCharacter == C_SPIRIT)
			{
				if (j == 1 && i == 1 && exPlayerNumber == 5 && m_eCurState != AS::Mori)
				{
					pEffect->BeginPass(20);
				}
				else
					pEffect->BeginPass(iPass);
			}
			else
				pEffect->BeginPass(iPass);

			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			if (j == 2 && i == 1 && 5 == exPlayerNumber && (m_eCurState != AS::Mori) && bUseServer)
			{
				//m_pMeshCom->Update_Hair((_uint)i, (_uint)j);
				//m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);
				continue;
			}

			_matrix* pRenderMat = m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices;

			if (m_bAfterImaged && m_fSpiritSkillTime >= 0.12f)
			{
				_matrix* pNewMat = new _matrix[pMeshContainer->dwNumFrames];
				memcpy(pNewMat, pRenderMat, sizeof(_matrix) * pMeshContainer->dwNumFrames);
				vecPrev.push_back(pNewMat);
				vecNumFrame.push_back(pMeshContainer->dwNumFrames);
			}
				
			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
				return;

			memcpy(lock_Rect.pBits, pRenderMat, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);
			pEffect->SetInt("numBoneInf", 4);
			if (exPlayerNumber < 5 && server_data.Slasher.iSkill == 1 && server_data.Slasher.iCharacter == C_SPIRIT) 
				pEffect->SetMatrix("g_matWorld", &m_matSpritMatrix);

			if(exPlayerNumber < 5 && server_data.Slasher.iSkill == 0 && !m_bAfterImaged && m_fSpiritNoise < 0.25f && server_data.Slasher.iCharacter == C_SPIRIT)
				pEffect->SetMatrix("g_matWorld", &matRand);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);

			pEffect->EndPass();
		}

		//pEffect->EndPass();

	}
	if (m_bAfterImaged && m_fSpiritSkillTime > 0.12f)
	{
		m_fSpiritSkillTime = 0.f;
		//if (m_pSkillMatrix.size() < 15)
		//{
			m_pFrameNum.push_back(vecNumFrame);
			m_pSkillMatrix.push_back(vecPrev);
			m_pSkillWorld.push_back(m_pTransformCom->Get_Matrix());
			m_pDisapearTime.push_back(0.8f);
		//}
		//else
		//{
		//	for (auto pMat : m_pSkillMatrix.front())
		//	{
		//		Safe_Delete_Array(pMat);
		//	}
		//	m_pSkillMatrix.pop_front();
		//	m_pSkillMatrix.push_back(vecPrev);
		//	m_pSkillWorld.pop_front();
		//	m_pSkillWorld.push_back(m_pTransformCom->Get_Matrix());
		//	m_pFrameNum.pop_front();
		//	m_pFrameNum.push_back(vecNumFrame);
		//}
	}

	pEffect->End();

	if(m_pSkillMatrix.size() != 0)
		Render_SpiritAfterImage();

	Safe_Release(pEffect);
}

void CSlasher::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);


	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);


		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);
			pEffect->SetInt("numBoneInf", 4);
			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}
	}
	
	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CSlasher::Render_Stencil()
{
	if (nullptr == m_pSkinTextures)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(5);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			pEffect->SetVector("g_DiffuseColor", (_vec4*)&m_Color);
			pEffect->SetInt("numBoneInf", 4);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CSlasher::Render_ShadowMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			pEffect->SetMatrix("g_matVP", VP);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);
			pEffect->SetInt("numBoneInf", 4);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CSlasher::Render_CubeMap(_matrix * View, _matrix * Proj)
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			_matrix matWVP = m_pTransformCom->Get_Matrix() * (*View) * (*Proj);

			pEffect->SetMatrix("g_matWVP", &matWVP);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CSlasher::Render_SpiritAfterImage()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(19);

	for (size_t k = 0; k < m_pSkillMatrix.size(); ++k)
	{
		auto pMatWld = m_pSkillWorld.begin();
		advance(pMatWld, k);

		auto pTime = m_pDisapearTime.begin();
		advance(pTime, k);

		_matrix matRenderWorld = (*pMatWld);
		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				auto pMat = m_pSkillMatrix.begin();
				advance(pMat, k);

				_matrix* pRenderMat = (*pMat)[(i * 3) + j];

				auto pFrm = m_pFrameNum.begin();
				advance(pFrm, k);

				_uint dwNumFrm = (*pFrm)[(i * 3) + j];

				_float fDisapearTime = *pTime / 0.8f;

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
					return;

				memcpy(lock_Rect.pBits, pRenderMat, sizeof(_matrix) * dwNumFrm);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;

				pEffect->SetMatrix("g_matWorld", &matRenderWorld);

				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->SetFloat("g_fTimeAcc", fDisapearTime);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}
		}
		//if (m_fDissolveTime > (k + 1) * 0.111f)
		//{
		//	pEffect->EndPass();
		//	pEffect->End();

		//	Safe_Release(pEffect);
		//	return;
		//}
	}
	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

_int CSlasher::Do_Coll(CGameObject * _pObj, const _vec3 & _vPos)
{
	if (exPlayerNumber != 5)
		return 0;

	if (0 == _pObj->GetID())
		return 0;

	if (_pObj->GetID() & HOOK)
	{
		if (server_data.Game_Data.Hook[_pObj->GetID() - HOOK] == 13
			|| server_data.Game_Data.Hook[_pObj->GetID() - HOOK] == 14)
		{
			m_pTargetOfInteraction = nullptr;
			return 0;
		}

		for (int i = 0; i < 4; ++i)
		{
			if (!server_data.Campers[i].bConnect || !server_data.Campers[i].bLive)
				continue;

			if (server_data.Campers[i].iCondition == CCamper::HOOKED)
			{
				_vec3 vPos = server_data.Campers[i].vPos;
				CTransform* pTransform = (CTransform*)_pObj->Get_ComponentPointer(L"Com_Transform");
				_vec3 vHookPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

				_float fLength = D3DXVec3Length(&(vPos - vHookPos));
				if (fLength <= 300.f)
					return 0;
			}		
		}
	}

	m_pTargetOfInteraction = _pObj;
	m_vTargetPos = _vPos;

	return _int();
}

_int CSlasher::Do_Coll_Player(CGameObject * _pObj, const _vec3 & _vPos)
{
	if (exPlayerNumber != 5)
		return 0;

	if (0 == _pObj->GetID())
		return 0;

	if (_pObj->GetID() & CAMPER)
	{
		m_pCamperOfInteraction = _pObj;
		m_vTargetCamperPos = _vPos;

		//if(server_data.Campers[m_pCamperOfInteraction->GetID() - CAMPER].iCondition == CCamper::DYING)
		//if(bCanGoOut && ((server_data.Slasher.Perk & SWALLOWEDHOPE) || (server_data.Slasher.Perk & RESENTMENT)))

		return 0;
	}

	return _int();
}

HRESULT CSlasher::Set_Index(_uint index)
{
	m_iCharacter = (CHARACTER)index;
	m_eObjectID = SLASHER;

	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	switch (m_iCharacter)
	{
	case C_WRAITH:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Slasher_Wraith");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;

		m_fMoveSpeed = WRAITH_CLOAKED_SPEED;
		m_eCurState = AS::IdleMenu;
		m_pMeshCom->Set_NoBleningAnimationSet(TW_IdleMenu);
		if (bGameServer && nullptr != m_pRendererCom)
			m_pRendererCom->Set_FogColor(0.17f);
	}
	break;
	case C_SPIRIT:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Slasher_Spirit");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;

		//for (int i = 0; i < 4; ++i)
		//{
		//	if (FAILED(m_pGraphic_Device->CreateTexture(128, 128, 1, 0, D3DFMT_A32B32G32R32F, D3DPOOL_MANAGED, &m_pSkillTexture[i], nullptr)))
		//		return E_FAIL;
		//}
		m_fMoveSpeed = SPIRIT_SPEED;
		m_eCurState = AS::IdleMenu;
		m_pMeshCom->Set_NoBleningAnimationSet(HK_IdleMenuBack);
		if (bGameServer && nullptr != m_pRendererCom)
			m_pRendererCom->Set_FogColor(0.0f);
	}
	break;
	default:
		break;
	}

	if (bUseServer)
	{
		//	m_pMatCamera = m_pMeshCom->Find_Frame("joint_TorsoC_01");
		m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);
		_matrix			matLocalTransform;
		D3DXMatrixScaling(&matLocalTransform, 150.f, 150.f, 150.f);
		matLocalTransform._42 = 75.f;

		m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
			return E_FAIL;
		m_pColliderCom->Set_CircleRad(60.f);

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_PLAYER, this);
		EFFMGR->Ready_PlayerPos(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

		//m_pCamMatrix = m_pMeshCom->Find_Frame("jointCam_01");
		//m_pCamMatrix = m_pMeshCom->Find_Frame("joint_CamOff_01");
		m_pRHandMatrix = m_pMeshCom->Find_Frame("joint_WeaponRT_01");
		m_pLHandMatrix = m_pMeshCom->Find_Frame("joint_WeaponLT_01");
		if (0 == index)
			m_pHeadMatrix = m_pMeshCom->Find_Frame("joint_Head_01");
		else
			m_pHeadMatrix = m_pMeshCom->Find_Frame("joint_Nose_01");

		m_pCamperAttachMatrix = m_pMeshCom->Find_Frame("joint_CamperAttach_01");
		m_pCarryMatrix = m_pMeshCom->Find_Frame("joint_CarryLT_01");
		D3DXMatrixIdentity(&matLocalTransform);
		D3DXMatrixScaling(&matLocalTransform, 180.f, 180.f, 180.f);

		m_pHeadColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, m_pHeadMatrix, m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_HeadCollider", m_pHeadColliderCom)))
			return E_FAIL;

		SetUp_Sound();
	}
	Safe_Release(pManagement);

	if (server_data.Slasher.Perk & RESENTMENT)
		m_iMoriCount = 1;
	Set_StartPosition();
	return NOERROR;
}

void CSlasher::Set_WeaponColl(_bool bColl)
{
	if (nullptr == m_pWeapon)
		return;
	
	((CMachete*)m_pWeapon)->Set_CollState(bColl);

}

void CSlasher::Set_CarriedCamper(CGameObject * pCamper)
{
	m_pCarriedCamper = pCamper;
}

void CSlasher::State_Check()
{
	Update_Sound();

	if (exPlayerNumber != 5 && server_data.Slasher.iCharacter == C_SPIRIT && m_fSTime > 0.f && !m_bIsLockKey)
	{
		m_pMeshCom->Set_AnimationSet(SSTATE::HK_Power_Out_FT);
		return;
	}

	if (m_eCurState == m_eOldState)
		return;

	m_fAnimationIndex = 0.f;
	if (server_data.Slasher.bConnect && server_data.Slasher.iCharacter == C_WRAITH)
	{
		WSTATE WraithAnim = Change_Animation_Wriath(m_eCurState);
		if (exPlayerNumber == 5 && !m_bIsLockKey && m_eCurState != AS::WR_Bell_Loop)
		{
			m_pMeshCom->Set_AnimationSet(WSTATE::TW_Stand);
			m_pMeshCom->Set_ArmAnimationSet(WSTATE::TT_SetArmOffset);
		}
		else
		{
			if (m_bIsNoBlending)
			{
				m_pMeshCom->Set_NoBleningAnimationSet(WraithAnim);
			}
			else
			{
				m_pMeshCom->Set_AnimationSet(WraithAnim);
				m_pMeshCom->Set_ArmAnimationSet(WraithAnim);
			}
		}
	}
	else if(server_data.Slasher.bConnect && server_data.Slasher.iCharacter == C_SPIRIT)
	{
		SSTATE SpiritAnim = Change_Animation_Spirit(m_eCurState);
		if (m_bIsNoBlending)
			m_pMeshCom->Set_NoBleningAnimationSet(SpiritAnim);
		else
			m_pMeshCom->Set_AnimationSet(SpiritAnim);
	}

	m_eOldState = m_eCurState;
}

void CSlasher::LowerState_Check()
{
	if (exPlayerNumber != 5 && server_data.Slasher.iCharacter == C_SPIRIT && m_fSTime >= 0.f && !m_bIsLockKey)
	{
		m_pMeshCom->Set_ServeAnimationSet(SSTATE::HK_Power_Out_FT);
		return;
	}

	if (m_eCurLowerState == m_eOldLowerState)
		return;

	if (server_data.Slasher.iCharacter == C_WRAITH)
	{
		WSTATE Wraith_LowerAnim = Change_Animation_Wriath(m_eCurLowerState);
		if (!m_bIsNoBlending)
			m_pMeshCom->Set_ServeAnimationSet(Wraith_LowerAnim);
	}
	else
	{
		SSTATE SpiritAnim = Change_Animation_Spirit(m_eCurLowerState);
		if (!m_bIsNoBlending)
			m_pMeshCom->Set_ServeAnimationSet(SpiritAnim);
	}

	m_eOldLowerState = m_eCurLowerState;
}

void CSlasher::Key_Input(const _float & fTimeDelta)
{
	if (exPlayerNumber != 5)
		return;

	if (!bCameraReady)
		return;

	Interact_Object();

	if (m_bIsLockKey)
		return;

	RotateY(fTimeDelta);

	_float AngerSpeed = 0.0f;

	switch (m_iCurAnger)
	{
	case 1:
		AngerSpeed = 0.03f;
		break;
	case 2:
		AngerSpeed = 0.06f;
		break;
	case 3:
		AngerSpeed = 0.12f;
		break;
	default:
		AngerSpeed = 0.0f;
		break;
	}

	if (m_iCharacter == C_WRAITH)
	{
		if (server_data.Slasher.iSkill == 0)
		{
			AngerSpeed *= WRAITH_SPEED;
			m_fMoveSpeed = WRAITH_SPEED + AngerSpeed;
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);
			if (m_bSpeedUp && m_fSpeedUpTime >= 0.f)
			{
				m_fSpeedUpTime -= fTimeDelta;
				m_fMoveSpeed = WRAITH_SPEED + AngerSpeed + 120.f;
				m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);
			}
			else if (m_bSpeedUp && m_fSpeedUpTime <= 0.1f)
				m_bSpeedUp = false;
		}

		if (KEYMGR->KeyPressing(DIK_W))
		{
			m_pTransformCom->Go_Straight(fTimeDelta);
			m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::WR_Carry_Run : m_eCurState =  m_eCurLowerState = AS::RunFT;
		}
		else if (KEYMGR->KeyPressing(DIK_S))
		{
			m_pTransformCom->BackWard(fTimeDelta);
			m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::WR_Carry_Run : m_eCurState = m_eCurLowerState = AS::RunBK;
		}
		if (KEYMGR->KeyPressing(DIK_A))
		{
			m_pTransformCom->Go_Left(fTimeDelta);
			m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::WR_Carry_Run : m_eCurState = m_eCurLowerState = AS::RunFT;
		}
		else if (KEYMGR->KeyPressing(DIK_D))
		{
			m_pTransformCom->Go_Right(fTimeDelta);
			m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::WR_Carry_Run : m_eCurState = m_eCurLowerState = AS::RunFT;
		}

		if (!KEYMGR->KeyPressing(DIK_W) && !KEYMGR->KeyPressing(DIK_A)
			&& !KEYMGR->KeyPressing(DIK_S) && !KEYMGR->KeyPressing(DIK_D))
			m_bIsCarry ? m_eCurState = m_eCurLowerState = AS::Carry_Idle : m_eCurState = m_eCurLowerState = AS::Idle;

		if (m_fSkillTime != 0.f && KEYMGR->MouseUp(1))
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);

		if (!KEYMGR->MousePressing(1))
		{
			m_bFlagOfKeyInput = false;
			m_bSkillUse = false;
			m_fSkillTime = 0.f;
			if (!m_bSkill)
			{
				m_fDissolveTime = 0.0f;
				slasher_data.iSkill = 0;
			}		
			else
				m_fDissolveTime = 1.f;
		}

		if (!m_bFlagOfKeyInput && !m_bIsCarry)
		{
			if (KEYMGR->MousePressing(1))
				Use_Skill_Wraith(fTimeDelta);
			else if (m_bSkill)
				GET_INSTANCE(CUIManager)->Add_Button(wstring(L"M2"), wstring(L"은신 해제"));
			else
				GET_INSTANCE(CUIManager)->Add_Button(wstring(L"M2"), wstring(L"투명화"));
		}
	}
	else
	{
		if (server_data.Slasher.iSkill == 0)
		{
			AngerSpeed *= SPIRIT_SPEED;
			m_fMoveSpeed = SPIRIT_SPEED + AngerSpeed;
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);
		}
		else
		{
			m_fMoveSpeed = SPIRIT_PHASE_WALK_SPEED;
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);
		}

		if (!m_bMoveLock && KEYMGR->KeyPressing(DIK_W))
		{
			m_pTransformCom->Go_Straight(fTimeDelta);
			if (!m_bSkill && m_fSkillCoolTime <= 0.3f)
				m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::RunFT : m_eCurState = m_eCurLowerState = AS::RunFT;
			else
				m_eCurState = m_eCurLowerState = AS::SP_Power_Run_FT;
		}
		else if (!m_bMoveLock && KEYMGR->KeyPressing(DIK_S))
		{
			m_pTransformCom->BackWard(fTimeDelta);
			if (!m_bSkill  && m_fSkillCoolTime <= 0.3f)
				m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::RunBK : m_eCurState = m_eCurLowerState = AS::RunBK;
			else
				m_eCurState = m_eCurLowerState = AS::SP_Power_Run_BK;
		}
		

		if (!m_bMoveLock && KEYMGR->KeyPressing(DIK_A))
		{
			m_pTransformCom->Go_Left(fTimeDelta);
			if (!m_bSkill  && m_fSkillCoolTime <= 0.3f)
				m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::RunFT : m_eCurState = m_eCurLowerState = AS::RunFT;
			else
				m_eCurState = m_eCurLowerState = AS::SP_Power_Run_FT;
		}
		else if (!m_bMoveLock && KEYMGR->KeyPressing(DIK_D))
		{
			m_pTransformCom->Go_Right(fTimeDelta);
			if (!m_bSkill  && m_fSkillCoolTime <= 0.3f)
				m_bIsCarry ? m_eCurState = AS::Carry_Idle, m_eCurLowerState = AS::RunFT : m_eCurState = m_eCurLowerState = AS::RunFT;
			else
				m_eCurState = m_eCurLowerState = AS::SP_Power_Run_FT;
		}

		if (!m_bMoveLock && !KEYMGR->KeyPressing(DIK_W) && !KEYMGR->KeyPressing(DIK_A)
			&& !KEYMGR->KeyPressing(DIK_S) && !KEYMGR->KeyPressing(DIK_D))
		{
			if (!m_bSkill && m_fSkillCoolTime <= 0.3f)
				m_bIsCarry ? m_eCurState = m_eCurLowerState = AS::Carry_Idle : m_eCurState = m_eCurLowerState = AS::Idle;
			else if (m_fSkillCoolTime <= 0.3f)
				m_eCurState = m_eCurLowerState = AS::SP_Power_Idle;
		}

		if (m_fSpiritChargingTime != 0.f && KEYMGR->MouseUp(1))
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
		if (!KEYMGR->MousePressing(1))
		{
			if (m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y >= -10.f)
			{
				if (m_pRendererCom->Get_Brightness() < 35.f)
					m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() + fTimeDelta * 40.f);
				else
					m_pRendererCom->Set_Brightness(35.f);

				_vec3 vRGB = m_pRendererCom->Get_RGB();
				_vec3 vCalRGB = vRGB;
				if (vRGB.x > 0.5f)
					vCalRGB.x -= fTimeDelta * 0.35f;
				else
					vCalRGB.x = 0.5f;
				if (vRGB.y < 0.5f)
					vCalRGB.y += fTimeDelta * 0.1f;
				else
					vCalRGB.y = 0.5f;
				if (vRGB.z < 0.5f)
					vCalRGB.z += fTimeDelta * 0.1f;
				else
					vCalRGB.z = 0.5f;

				m_pRendererCom->Set_RGB(vCalRGB);
			}
			else
			{
				if (m_pRendererCom->Get_Brightness() > 17.f)
					m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() - fTimeDelta * 40.f);
				else
					m_pRendererCom->Set_Brightness(17.f);

				_vec3 vRGB = m_pRendererCom->Get_RGB();
				_vec3 vCalRGB = vRGB;
				if (vRGB.x < 3.5f)
					vCalRGB.x += fTimeDelta * 0.35f;
				else
					vCalRGB.x = 2.5f;
				if (vRGB.y > 0.1f)
					vCalRGB.y -= fTimeDelta * 0.1f;
				else
					vCalRGB.y = 0.1f;
				if (vRGB.z > 0.1f)
					vCalRGB.z -= fTimeDelta * 0.1f;
				else
					vCalRGB.z = 0.1f;

				m_pRendererCom->Set_RGB(vCalRGB);
			}
			m_bMoveLock = false;
			m_fSpiritChargingTime = 0.f;

			if (m_bSkill)						
			{
				m_fSkillCoolTime = 1.f;
				slasher_data.iSkill = SPIRIT_NOSKILL;
				m_bSkill = false;
			}
			else
			{
				m_fSkillTime += fTimeDelta;

				if (m_fSkillTime > SPRIIT_POWER_BAR)
					m_fSkillTime = SPRIIT_POWER_BAR;

				if (m_fSkillTime < 0.f)
					m_fSkillTime = 0.f;

				if (m_fSkillCoolTime > 0.f)
				{
					m_fSkillCoolTime -= fTimeDelta;
				}

				if (m_fSkillCoolTime <= 0.f)
				{
					m_fSkillCoolTime = 0.f;
					if(!Find_Action(L"Action_Hit")->m_bIsPlaying)
						m_pTransformCom->SetUp_Speed(m_fMoveSpeed = SPIRIT_SPEED, m_fRotateSpeed);
				}
			}
		}

		if (!m_bIsCarry)
		{
			if (KEYMGR->MousePressing(1))
				Use_Skill_Spirit(fTimeDelta);
			else if (m_fSkillTime >= SPRIIT_POWER_BAR && !m_bSkill)
				GET_INSTANCE(CUIManager)->Add_Button(wstring(L"M2"), wstring(L"영적 세계로 이동"));
		}
	}
	if (KEYMGR->KeyPressing(DIK_O))
		Set_Action(L"Action_Damage_Generator", 10.f);
	if (m_bIsCarry)
	{
		_uint CamperID = 0;

		if (m_fWiggleTime > 0.f)
		{
			m_fWiggleTime -= fTimeDelta;
			if (m_fWiggleTime < 0.f)
				m_fWiggleTime = 0.f;
		}
		else if (m_fWiggleTime < 0.f)
		{
			m_fWiggleTime += fTimeDelta;
			if (m_fWiggleTime > 0.f)
				m_fWiggleTime = 0.f;
		}

		if (nullptr == m_pCarriedCamper || m_pCarriedCamper->Get_IsDead())
		{
			m_pCarriedCamper = nullptr;
			m_bIsCarry = false;
		}

		if (m_pCarriedCamper != nullptr)
			CamperID = m_pCarriedCamper->GetID() - CAMPER;
		if (server_data.Campers[CamperID].Packet == WIGGLE_RIGHT)
			m_fWiggleTime = 1.f;
		else if (server_data.Campers[CamperID].Packet == WIGGLE_LEFT)
			m_fWiggleTime = -1.f;

		if (m_fWiggleTime > 0.f)
			m_pTransformCom->Go_Right(fTimeDelta * 0.3f);
		else if (m_fWiggleTime < 0.f)
			m_pTransformCom->Go_Left(fTimeDelta * 0.3f);
	}
}

void CSlasher::Use_Skill_Wraith(const _float & fTimeDelta)
{
	if (nullptr != m_pCarriedCamper)
		return;

	m_fSkillTime += fTimeDelta;

	if (!m_bSkill)		// HIDE
	{
		if (m_fSkillTime > WRAITH_HIDE_TIME)
		{
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
			m_bSkill = true;
			m_bSkillUse = false;
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed = WRAITH_CLOAKED_SPEED, m_fRotateSpeed);
			m_bFlagOfKeyInput = true;
			m_fSkillTime = 0.f;
			slasher_data.iSkill = WRAITH_HIDEINGSKILL;
			return;
		}

		if (!m_bSkillUse)
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Hide);
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fSkillTime, WRAITH_HIDE_TIME);

		slasher_data.iSkill = WRAITH_HIDESKILL;
		m_fDissolveTime = m_fSkillTime / WRAITH_HIDE_TIME;
		m_bSkill = false;
		m_bSkillUse = true;
	}
	else				// UNHIDE
	{
		if (m_fSkillTime > WRAITH_UNHIDE_TIME)
		{
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed = WRAITH_CLOAKED_SPEED, m_fRotateSpeed);
			m_bFlagOfKeyInput = true;
			m_fSkillTime = 0.f;
			slasher_data.iSkill = WRATIH_NOSKILL;
			m_bSkill = false;
			m_bSkillUse = false;
			
			return;
		}
		if (!m_bSkillUse)
		{
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_UnHide);
		}		
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fSkillTime, WRAITH_UNHIDE_TIME);
		
		slasher_data.iSkill = WRAITH_UNHIDESKILL;
		m_fDissolveTime = 1.f - (m_fSkillTime / WRAITH_UNHIDE_TIME);
		m_bSkill = true;
		m_bSkillUse = true;

		if (m_fSkillTime < (WRAITH_UNHIDE_TIME * 0.5f))
		{
			m_fMoveSpeed -= (fTimeDelta * 206.667f);
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);
		}
		else
		{
			m_bSpeedUp = true;
			m_fSpeedUpTime = 1.2f;
			/*m_fMoveSpeed += (fTimeDelta * 186.667f);
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed, m_fRotateSpeed);*/
		}
	}

	m_eCurState = AS::WR_Bell_Loop;
	if(KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_D))
		m_eCurLowerState = AS::WR_Carry_Run;
	else if (KEYMGR->KeyPressing(DIK_S))
		m_eCurLowerState = AS::WR_Carry_Run;
	else
		m_eCurLowerState = AS::WR_Bell_Loop;
}

void CSlasher::Use_Skill_Spirit(const _float & fTimeDelta)
{
	if (nullptr != m_pCarriedCamper)
		return;

	if (!m_bSkill)
	{
		if (m_fSkillTime < SPRIIT_POWER_BAR)
		{
			m_fSkillTime += fTimeDelta;
			return;
		}
		if (m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y >= -10.f)
		{
			if (m_pRendererCom->Get_Brightness() > 20.f)
				m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() - fTimeDelta * 40.f);
			else
				m_pRendererCom->Set_Brightness(20.f);
		}

		if (m_fSpiritChargingTime > SPIRIT_IN_TRAVERSE_TIME)
		{
			m_bSkill = true;
			m_pTransformCom->SetUp_Speed(m_fMoveSpeed = SPIRIT_PHASE_WALK_SPEED, m_fRotateSpeed);
			m_bMoveLock = false;
			slasher_data.iSkill = SPIRIT_SKILLUSE;
			return;
		}

		if (!m_bMoveLock)
			m_matTemp = m_pTransformCom->Get_Matrix();
		if (m_fSpiritChargingTime == 0.f)
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_SpiritPower);
			
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fSpiritChargingTime, SPIRIT_IN_TRAVERSE_TIME);
		m_fSpiritChargingTime += fTimeDelta;
		m_bMoveLock = true;
		m_eCurState = AS::SP_Power_Charge;
	}
	else
	{
		if (m_fSkillTime < 0.001f)
		{
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
			m_bSkill = false;
			slasher_data.iSkill = SPIRIT_NOSKILL;
			if(m_pCarriedCamper == nullptr)
				m_pTransformCom->SetUp_Speed(m_fMoveSpeed = SPIRIT_SPEED, m_fRotateSpeed);
			else
				m_pTransformCom->SetUp_Speed(m_fMoveSpeed = SPIRIT_SPEED * 0.7f, m_fRotateSpeed);
		}

		m_fSkillTime -= (m_fTimeDelta * 3.f);
	}
}

void CSlasher::Update_Breath(const _float & fDeltaTime)
{
	if (server_data.Slasher.iCharacter == C_WRAITH)
	{
		if (!m_SkillBreath && server_data.Slasher.iSkill == WRAITH_HIDEINGSKILL)
		{
			GET_INSTANCE(CSoundManager)->PlaySound(string("Slasher/Wraith/invisibility_on_ghost_0") + to_string(Math_Manager::CalRandIntFromTo(1, 3)) + string(".ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 4000.f, 1.f);
			m_SkillBreath = true;
			return;
		}
		else if (m_SkillBreath &&server_data.Slasher.iSkill == WRATIH_NOSKILL)
		{
			GET_INSTANCE(CSoundManager)->PlaySound(string("Slasher/Wraith/invisibility_off_ghost_0") + to_string(Math_Manager::CalRandIntFromTo(1, 3)) + string(".ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 4000.f, 1.f);
			m_SkillBreath = false;
			return;
		}
		else if (server_data.Slasher.iSkill == WRAITH_HIDEINGSKILL)
			return;
	}
	else
	{
		if (m_SkillBreath && SPIRIT_SKILLUSE)
			return;
		else if (SPIRIT_SKILLUSE &&m_SkillBreath)
		{
			if (exPlayerNumber == 5)
					GET_INSTANCE(CSoundManager)->PlaySound(string("S_Breath"), "Spirit/spirit_normal_loop_01.ogg", *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 0, 1.f);
			m_SkillBreath = true;
			m_BreathTime = 4.f;
			return;
		}
		m_SkillBreath = false;
	}
	m_BreathTime += fDeltaTime;

	if (m_BreathTime <= 3.f)
		return;
	m_BreathTime = 0.f;

	_int Random;
	_float Distance = 1700.f;
	string FileName = "";
	_float Volume = 0.7f;

	if (server_data.Slasher.iCharacter == C_WRAITH)
	{
		Random = Math_Manager::CalRandIntFromTo(0, 9);
		FileName = string("Slasher/Wraith/Normal/W_Normal") + to_string(Random) + string(".ogg");
		if (exPlayerNumber == 5)
			Volume = 0.4f;
		else
			Volume = 0.8f;
	}
	else
	{
			Random = Math_Manager::CalRandIntFromTo(1, 13);
			FileName = string("Slasher/Spirit/spirit_normal_") + to_string(Random) + string(".ogg");
			if (exPlayerNumber == 5)
				Volume = 0.4f;
	}
	if (FileName != "")
		GET_INSTANCE(CSoundManager)->PlaySound(string("S_Breath"), FileName, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), Distance, Volume);
}

void CSlasher::CommunicationWithServer()
{
	if (!bGameServer)
		return;

	if (exPlayerNumber == 5)
	{
		//camper_data.Character =
		slasher_data.bConnect = true;

		if (nullptr != m_pTransformCom)
		{
			slasher_data.vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
			slasher_data.vUp = *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
			slasher_data.vLook = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
			slasher_data.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
		}

		slasher_data.iCharacter = (_uint)m_iCharacter;

		_matrix matView;
		m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
		D3DXMatrixInverse(&matView, nullptr, &matView);

		memcpy(&slasher_data.vCamLook, &matView.m[2][0], sizeof(_vec3));

		slasher_data.iState = (_int)m_eCurState;
		slasher_data.iLowerState = (_int)m_eCurLowerState;

		if (server_data.Game_Data.iPlayerAction[4] != 0)
		{
			_tchar ActionTag[150] = L"";

			if (server_data.Game_Data.iPlayerAction[4] & XXPALOMA)
			{
				Assert_EndAllAction(L"Action_StunDrop");
				lstrcpy(ActionTag, L"Action_StunDrop");

				slasher_data.RecvPacket = XXPALOMA;
			}		
			else if (server_data.Game_Data.iPlayerAction[4] & CALZZI)
			{
				Assert_EndAllAction(L"Action_StunDrop");
				lstrcpy(ActionTag, L"Action_StunDrop");

				slasher_data.RecvPacket = CALZZI;
			}
			else if (server_data.Game_Data.iPlayerAction[4] & STUNDROP)
			{
				Assert_EndAllAction(L"Action_StunDrop");
				lstrcpy(ActionTag, L"Action_StunDrop");

				slasher_data.RecvPacket = STUNDROP;
			}

			Set_Action(ActionTag, 100.f);
		}

	}
	else if (exPlayerNumber < 5)
	{
		if (!server_data.Slasher.bConnect)
			m_isDead = true;

		//_vec3 vCurPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

		_vec3 vRight = server_data.Slasher.vRight;
		_vec3 vUp = server_data.Slasher.vUp;
		_vec3 vLook = server_data.Slasher.vLook;
		_vec3 vPos = server_data.Slasher.vPos;
		
		m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		m_eCurState = (AS::STATE)server_data.Slasher.iState;
		m_eCurLowerState = (AS::STATE)server_data.Slasher.iLowerState;

		switch (server_data.Slasher.iCharacter)
		{
		case C_WRAITH:
		{
			if (WRATIH_NOSKILL == server_data.Slasher.iSkill)
			{
				m_bSkill = false;
				m_bSkillUse = false;
				m_fSkillTime = 0.f;
				m_fDissolveTime = 0.f;
			}
			else if (WRAITH_HIDESKILL == server_data.Slasher.iSkill)
			{
				if (m_fSkillTime <= WRAITH_HIDESKILL)
					m_fSkillTime += m_fTimeDelta;

				m_bSkill = false;
				m_bSkillUse = true;
				m_fDissolveTime = m_fSkillTime / WRAITH_UNHIDE_TIME;
			}
			else if (WRAITH_UNHIDESKILL == server_data.Slasher.iSkill)
			{
				if (m_fSkillTime <= WRAITH_UNHIDE_TIME)
					m_fSkillTime += m_fTimeDelta;

				m_bSkill = true;
				m_bSkillUse = true;
				m_fDissolveTime = 1.f - (m_fSkillTime / WRAITH_UNHIDE_TIME);
			}
			else if (WRAITH_HIDEINGSKILL == server_data.Slasher.iSkill)
			{
				m_bSkill = true;
				m_bSkillUse = false;
				m_fSkillTime = 0.f;
				m_fDissolveTime = 1.f;
			}
		}
		break;
		case C_SPIRIT:
		{
			if (m_iOldSkill != server_data.Slasher.iSkill)
			{
				if (SPIRIT_SKILLUSE == server_data.Slasher.iSkill)
				{
					if (nullptr == m_pTransformCom)
						return;

					m_matSpritMatrix = m_pTransformCom->Get_Matrix();
					m_iOldSkill = server_data.Slasher.iSkill;
				}
				else if (SPIRIT_NOSKILL == server_data.Slasher.iSkill)
				{
					m_fDissolveTime = 0.f;
					m_bAfterImaged = true;
					m_fSTime = 1.f;
					m_iOldSkill = server_data.Slasher.iSkill;
				}
			}
		}
			break;
		default:
			break;
		}
	}
}

void CSlasher::Compute_Map_Index()
{
	const _vec3* pPos = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	_int IndexX = (_int)(pPos->x / _MAP_INTERVAL);
	_int IndexZ = (_int)(pPos->z / _MAP_INTERVAL);

	m_iMapIndex = (IndexX * 14) + IndexZ;
}

HRESULT CSlasher::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fire_Noise");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;



	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CSlasher::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED * pMeshContainer, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetFloat("numBoneInf", 4);

	if (m_bSkillUse && m_iCharacter == C_WRAITH)
	{
		pEffect->SetFloat("g_fFloorHeight", m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y);
		pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
		pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
		
		pEffect->SetFloat("g_fTimeAcc", m_fDissolveTime);
	}
	if (m_iCharacter == C_SPIRIT)
	{
		if(!m_bHairSwitch)
			m_fHairMove += m_fTimeDelta;
		else if (m_bHairSwitch)
			m_fHairMove -= m_fTimeDelta;

		if (!m_bHairSwitch && m_fHairMove > 0.5f)
			m_bHairSwitch = true;
		else if (m_bHairSwitch && m_fHairMove < -0.5f)
			m_bHairSwitch = false;

		pEffect->SetTexture("g_LightEmissive", pSubSet->MeshTexture.pExtendsTexture2);
		pEffect->SetTexture("g_HairTexture", pSubSet->MeshTexture.pExtendsTexture1);
		pEffect->SetFloat("g_fTimeAcc", m_fHairMove);
	}
	pEffect->SetBool("g_isFootPrint", false);
	return NOERROR;
}

HRESULT CSlasher::Ready_Action()
{
	CAction* pAction = new CAction_Drop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Drop", pAction));

	pAction = new CAction_Grab_Generic_Fast;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Generic_Fast", pAction));

	pAction = new CAction_Grab_Obstacles;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Obstacles", pAction));

	pAction = new CAction_HookIn;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookIn", pAction));

	pAction = new CAction_PickUp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PickUp", pAction));

	pAction = new CAction_Damage_Generator;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Damage_Generator", pAction));

	pAction = new CAction_Destroy_Pallet;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Destroy_Pallet", pAction));

	pAction = new CAction_SearchLocker;
	m_mapActions.insert(MAPACTION::value_type(L"Action_SearchLocker", pAction));

	pAction = new CAction_Stun_Pallet;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Stun_Pallet", pAction));

	pAction = new CAction_StunDrop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_StunDrop", pAction));

	pAction = new CAction_WindowVault;
	m_mapActions.insert(MAPACTION::value_type(L"Action_WindowVault", pAction));

	pAction = new CAction_MoveLerp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_MoveLerp", pAction));

	pAction = new CAction_Hit;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Hit", pAction));

	pAction = new CAction_Mori;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Mori", pAction));

	return NOERROR;
}

void CSlasher::Init_Camera()
{
	if (!m_bLaitInit && exPlayerNumber == 5)
	{
		m_bLaitInit = true;

		GET_INSTANCE_MANAGEMENT;

		((CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0))->Set_OrbitMatrix();

		Safe_Release(pManagement);
	}
}

void CSlasher::RotateY(const _float& fTimeDelta)
{
	_long	MouseMove = 0;

	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_X))
	{
		m_pTransformCom->Rotation_Y(D3DXToRadian(MouseMove) * fTimeDelta * 0.5f);
	}
}

void CSlasher::Interact_Object()
{
	if (5 != exPlayerNumber)
		return;

	if (m_bIsLockKey)
		return;

	if (m_pTargetOfInteraction != nullptr)
	{
		if (m_pTargetOfInteraction->GetID() & PLANK)
		{
			if (m_pTargetOfInteraction->Get_ProgressTime() >= 0.01f &&
				m_pTargetOfInteraction->Get_ProgressTime() <= 0.98f)
			{
				Set_Action(L"Action_Stun_Pallet", 10.f);
				return;
			}
		}
	}

	if (nullptr != m_pCamperOfInteraction && !m_bIsCarry)
	{
		_uint CamperID = m_pCamperOfInteraction->GetID();

		if ((CamperID & CAMPER) && CCamper::DYING == ((CCamper*)m_pCamperOfInteraction)->GetCurCondition())
		{
			if ((server_data.Slasher.Perk & SWALLOWEDHOPE) && bCanGoOut)
			{
				if (((CCamper*)m_pCamperOfInteraction)->GetHookStage() != CCamper::EscapeAttempts)
				{
					GET_INSTANCE(CUIManager)->Add_Button(L"E", L"즉시 처치");

					if (KEYMGR->KeyDown(DIK_E))
					{
						CAction_Mori* pAction_Mori = (CAction_Mori*)Find_Action(L"Action_Mori");
						pAction_Mori->SetCamper(m_pCamperOfInteraction);

						slasher_data.SecondInterationObject = m_pCamperOfInteraction->GetID();
						slasher_data.SecondInterationObjAnimation = MORICAMPER;

						Set_Action(L"Action_Mori", 10.f);
						return;
					}
				}
			}
			else if ((server_data.Slasher.Perk & RESENTMENT) && 0 < m_iMoriCount)
			{
				if ((((CCamper*)m_pCamperOfInteraction)->GetHookStage() != CCamper::EscapeAttempts))
				{
					GET_INSTANCE(CUIManager)->Add_Button(L"E", L"즉시 처치");

					if (KEYMGR->KeyDown(DIK_E))
					{
						CAction_Mori* pAction_Mori = (CAction_Mori*)Find_Action(L"Action_Mori");
						pAction_Mori->SetCamper(m_pCamperOfInteraction);

						--m_iMoriCount;
						slasher_data.SecondInterationObject = m_pCamperOfInteraction->GetID();
						slasher_data.SecondInterationObjAnimation = MORICAMPER;

						Set_Action(L"Action_Mori", 10.f);
						return;
					}
				}
			}
		}
	}

	if (KEYMGR->MousePressing(0))
	{
		if (server_data.Slasher.iSkill != 0)
			return;

		if (!Check_CampersState())
		{
			Set_Action(L"Action_Hit", 10.f);
			return;
		}
	}
	else if (KEYMGR->KeyPressing(DIK_SPACE))
	{
		if (m_pTargetOfInteraction == nullptr && m_pCamperOfInteraction == nullptr)
			return;
		else
		{
			_uint CamperID = 0;
			_uint TargetID = 0;
			if (m_pCamperOfInteraction != nullptr && !m_pCamperOfInteraction->Get_IsDead() && !m_bIsCarry)
			{
				CCamper* pCamper = (CCamper*)m_pCamperOfInteraction;
				CCamper::CONDITION CurConditionOfCamper = pCamper->GetCurCondition();
				if (CurConditionOfCamper == CCamper::DYING)
				{
					CAction_PickUp* pAction_PickUp = (CAction_PickUp*)Find_Action(L"Action_PickUp");
					pAction_PickUp->SetCamper(m_pCamperOfInteraction);
					Set_Action(L"Action_PickUp", 10.f);
					return;
				}
			}

			if (m_pTargetOfInteraction != nullptr)
			{
				TargetID = m_pTargetOfInteraction->GetID();
				if (TargetID & HATCH || TargetID & EXITDOOR || TargetID & CHEST || TargetID & TOTEM)
					return;
				if (TargetID & GENERATOR)
				{
					CGenerator* pGenerator = (CGenerator*)m_pTargetOfInteraction;
					if (pGenerator->Get_CurProgressTimeForSlasher() > pGenerator->Get_ProgressTime())
						return;
					if (m_pTargetOfInteraction->Get_ProgressTime() < 0.01f)
						return;

					CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
					pAction_MoveLerp->SetPos(m_vTargetPos);
					pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
					Set_Action(L"Action_MoveLerp", 0.2f);
					return;
				}
				else if (TargetID & CLOSET || TargetID & HOOK || TargetID & PLANK || TargetID & WINDOW)
				{
					if (TargetID & HOOK && !m_bIsCarry)
						return;

					if (TargetID & PLANK)
					{
						if (m_pTargetOfInteraction->Get_ProgressTime() < 1.488f)
							return;
					}

					CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
					pAction_MoveLerp->SetPos(m_vTargetPos);
					pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
					Set_Action(L"Action_MoveLerp", 0.2f);
					return;
				}
			}
		}
	}
	else if (KEYMGR->KeyDown(DIK_R) && m_bIsCarry)
	{
		Set_Action(L"Action_Drop", 5.f);
		return;
	}
}

void CSlasher::Cal_LightPos()
{
	if (5 == exPlayerNumber)
		return;
	
	if (nullptr == m_pMeshCom)
		return;

	_matrix	matWld = m_pTransformCom->Get_Matrix();

	if (server_data.Slasher.iSkill == 1 && C_SPIRIT == m_iCharacter)
	{
		matWld = m_matSpritMatrix;
	}

	_matrix matWorldLight = *m_pHeadMatrix * matWld;
	if (m_iCharacter == C_SPIRIT)
		matWorldLight.m[3][1] -= 60.f;
	_vec3	vLightPos = *(_vec3*)&matWorldLight.m[3][0] + *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK) * 30.f;

	_vec3 vLightDir = _vec3(0.4f, -0.6f, 0.f);

	_vec3 vLookUp = _vec3(1, 0, 0);

	_vec3 vLook = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	if (server_data.Slasher.iSkill == 1 && C_SPIRIT == m_iCharacter)
		memcpy(&vLook, &m_matSpritMatrix.m[2], sizeof(_vec3));
	vLook.y = 0.f;
	
	D3DXVec3Normalize(&vLook, &vLook);
	_float fAngle = acosf((vLook * vLookUp));
	_vec3	vLookUpY = vLookUp ^ vLook;

	if (vLookUpY.y < 0.f)
		fAngle = D3DXToRadian(360.f) - fAngle;

	_matrix matRot;
	D3DXMatrixRotationY(&matRot, fAngle);
	D3DXVec3TransformNormal(&vLightDir, &vLightDir, &matRot);

	D3DXVec3Normalize(&vLightDir, &vLightDir);
	m_pCustomLight->Set_Position(vLightPos);
	m_pCustomLight->Set_Direction(vLightDir);
}
void CSlasher::Check_AngerGauge()
{
	if (exPlayerNumber != 5)
		return;

	if (server_data.Slasher.iCharacter == 0 && server_data.Slasher.iSkill == 3)
		m_fAngerGauge = 0.f;
	if (server_data.Slasher.iCharacter == 1 && server_data.Slasher.iSkill == 1)
		m_fAngerGauge = 0.f;

	if (m_isLook)
	{
		m_fAngerGauge += m_fTimeDelta;
		
		if (m_fAngerGauge > 35.f)
			m_fAngerGauge = 35.f;
	}
	else
	{
		if(m_fAngerGauge >= 0.0f)
			m_fAngerGauge -= m_fTimeDelta * 2.f;
		if (m_fAngerGauge < 0.0f)
			m_fAngerGauge = 0.0f;
	}

	if (m_fAngerGauge >= 0.f && m_fAngerGauge <= 10.f)
	{
		m_iCurAnger = 0;
	}
	else if (m_fAngerGauge > 10.f && m_fAngerGauge <= 20.f)
	{
		m_iCurAnger = 1;
	}
	else if (m_fAngerGauge > 20.f && m_fAngerGauge <= 30.f)
	{
		m_iCurAnger = 2;
	}
	else if (m_fAngerGauge > 30.f)
		m_iCurAnger = 3;

	//cout << "isLook : " << m_isLook << endl;
	//cout << "Gauge : " << m_fAngerGauge << endl;
	//cout << "State : " << m_iCurAnger << endl;
}

_bool CSlasher::Check_CampersState()
{
	if (exPlayerNumber != 5)
		return false;

	CManagement* pManagement = CManagement::GetInstance();
	list<CGameObject*>* pObjList = pManagement->Get_ObjList(SCENE_STAGE, L"Layer_Camper");

	if (pObjList == nullptr)
		return false;

	_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	for (auto pObj : *pObjList)
	{
		if (pObj->GetID() == m_eObjectID)
			continue;

		CTransform* pOtherCamperTransform = (CTransform*)pObj->Get_ComponentPointer(L"Com_Transform");
		_vec3 vOtherCamperPos = *pOtherCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);

		_float fDist = D3DXVec3Length(&(vMyPos - vOtherCamperPos));
		if (fDist <= 150.f)
		{
			CCamper* pOtherCamper = (CCamper*)pObj;
			CCamper::CONDITION eCamperCurCondition = pOtherCamper->GetCurCondition();
			if (eCamperCurCondition == CCamper::SPECIAL)
			{
				CAction_Grab_Generic_Fast* pAction = (CAction_Grab_Generic_Fast*)Find_Action(L"Action_Grab_Generic_Fast");
				pAction->SetCamper(pOtherCamper);
				Set_Action(L"Action_Grab_Generic_Fast", 10.f);
				return true;
			}
			else if (eCamperCurCondition == CCamper::OBSTACLE)
			{
				CCamper::CONDITION eCamperOldCondition = pOtherCamper->GetOldCondition();
				if (eCamperOldCondition == CCamper::INJURED)
				{
					CAction_Grab_Obstacles* pAction = (CAction_Grab_Obstacles*)Find_Action(L"Action_Grab_Obstacles");
					pAction->SetCamper(pOtherCamper);
					Set_Action(L"Action_Grab_Obstacles", 10.f);
					return true;
				}
			} 
		}
	}
	return false;
}

void CSlasher::Processing_Others(const _float & fTimeDelta)
{
	m_vTargetPos = { 0.f, 0.f, 0.f };
	m_vTargetCamperPos = { 0.f, 0.f, 0.f };
	
	m_pTargetOfInteraction = nullptr;
	m_pCamperOfInteraction = nullptr;

	if(nullptr != m_pCarriedCamper)
		GET_INSTANCE(CUIManager)->Add_Button(L"R", L"버리기");

	if (C_WRAITH == m_iCharacter)
	{
		if (m_fSkillCoolTime > 0.f)
			m_fSkillCoolTime -= fTimeDelta;

		if (m_pCarriedCamper == nullptr)
		{
			if (!m_bSkill && m_fSkillCoolTime < 0.f)
				m_pTransformCom->SetUp_Speed(m_fMoveSpeed = WRAITH_SPEED, m_fRotateSpeed);
			else if (m_bSkill && m_fSkillTime <= 0.f)
				m_pTransformCom->SetUp_Speed(m_fMoveSpeed = WRAITH_CLOAKED_SPEED, m_fRotateSpeed);
		}
		else
		{
			if (!m_bSkill && m_fSkillCoolTime < 0.f)
				m_pTransformCom->SetUp_Speed(m_fMoveSpeed = WRAITH_SPEED * 0.7f, m_fRotateSpeed);
			else if (m_bSkill && m_fSkillTime <= 0.f)
				m_pTransformCom->SetUp_Speed(m_fMoveSpeed = WRAITH_CLOAKED_SPEED * 0.7f, m_fRotateSpeed);
		}
	}
	else
	{
		if (exPlayerNumber != 5)
		{
			if (m_fSTime > 0.f)
				m_fSTime -= fTimeDelta;
		}
	}

	m_bPenetration = false;
	if (m_fPenetrationTime > 0.f)
	{
		m_bPenetration = true;
		m_fPenetrationTime -= fTimeDelta;
	}
	else
		m_fPenetrationTime = 0.0f;

	if (exPlayerNumber != 5)
	{
		if (server_data.Campers[exPlayerNumber - 1].Perk & DARKSENSE)
		{
			if (bCanGoOut && !m_bCanGoOut)
			{
				m_bCanGoOut = true;
				m_fPenetrationTime = 10.f;
			}
		}
	}
	
}

_bool CSlasher::Find_FPVAnim(WSTATE Anim)
{
	for (auto& anim : m_vecFPVAnim)
	{
		if (anim == Anim)
			return true;
	}

	return false;
}

void CSlasher::Check_AfterImage(const _float& fTimeDelta)
{
	if (m_bAfterImaged)
	{
		m_fDissolveTime += fTimeDelta;
		if (m_fDissolveTime > 1.42f)
		{
			m_bAfterImaged = false;
			m_fDissolveTime = 0.f;
		}
	}

	if (m_pSkillMatrix.size() == 0)
		return;

	_float fAccTime = 1.f;
	for (auto& fDis : m_pDisapearTime)
	{
		fDis -= fTimeDelta * fAccTime;
		fAccTime -= 0.05f;
	}

	if (m_pDisapearTime.front() <= 0.f)
	{
		m_pDisapearTime.pop_front();
		for (auto& pMat : m_pSkillMatrix.front())
		{
			Safe_Delete_Array(pMat);
		}
		m_pSkillMatrix.pop_front();
		m_pFrameNum.front().clear();
		m_pFrameNum.pop_front();
		m_pSkillWorld.pop_front();
	}
}

void CSlasher::Check_Spirit_MoriEffect()
{
	if (nullptr == m_pMeshCom)
		return;

	if (server_data.Slasher.iCharacter != C_SPIRIT)
		return;

	if (m_eCurState != AS::Mori)
	{
		m_iMoriSound = 0;
		m_fMoriTime = 0.f;
		return;
	}

	m_fMoriTime += m_fTimeDelta;

	// Render Katana
	if (m_fMoriTime >= (41.0f / 321.0f) * 10.03f && m_fMoriTime <= (307.0f / 321.0f) * 10.03f)
	{
		if (nullptr != m_pWeapon)
			((CMachete*)m_pWeapon)->Set_SpiritAttack(false);
	}
	else if (m_fMoriTime > (307.0f / 321.0f) * 10.03f)
	{
		if (nullptr != m_pWeapon)
			((CMachete*)m_pWeapon)->Set_SpiritAttack(true);
	}

	// Render Off
	if (m_fMoriTime >= (148.0f / 321.0f) * 10.03f  && m_fMoriTime <= (171.4f / 321.0f) * 10.03f)
	{
		m_bNotRenderSlasher = true;
		if (nullptr != m_pWeapon)
			((CMachete*)m_pWeapon)->Set_Render(false);
	}
	else
	{
		m_bNotRenderSlasher = false;
		if (nullptr != m_pWeapon)
			((CMachete*)m_pWeapon)->Set_Render(true);
	}

	// Render Katana AfterImage
	if ((m_fMoriTime >= (167.4f / 321.0f) * 10.03f && /*m_fMoriTime <= (205.0f / 321.0f) * 10.03f)
		|| (m_fMoriTime >= (219.0f / 321.0f) * 10.03f  && m_fMoriTime <= (230.0f / 321.0f) * 10.03f)
		|| (m_fMoriTime >= (242.0f / 321.0f) * 10.03f  &&*/ m_fMoriTime <= (299.0f / 321.0f) * 10.03f))
	{
		GET_INSTANCE_MANAGEMENT;

		CAfterImage* pAfterImage = (CAfterImage*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_AfterImage").front();
		if (nullptr == pAfterImage)
		{
			Safe_Release(pManagement);
			return;
		}

		pAfterImage->Set_Render(true);

		Safe_Release(pManagement);
	}
	else
	{
		GET_INSTANCE_MANAGEMENT;

		CAfterImage* pAfterImage = (CAfterImage*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_AfterImage").front();
		if (nullptr == pAfterImage)
		{
			Safe_Release(pManagement);
			return;
		}

		pAfterImage->Set_Render(false);

		Safe_Release(pManagement);
	}

	if ((m_fMoriTime >= (41.0f / 321.0f) * 10.03f) && (m_iMoriSound == 0))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/S_Appear.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (101.0f / 321.0f) * 10.03f) && (m_iMoriSound == 1))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/Stab.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::FRONT);

		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (141.0f / 321.0f) * 10.03f) && (m_iMoriSound == 2))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (110.0f / 321.0f) * 10.03f) && m_iMoriSound == 3)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/HitCamper.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}	
	if ((m_fMoriTime >= (197.0f / 321.0f) * 10.03f) && m_iMoriSound == 4)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/Swing.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (199.0f / 321.0f) * 10.03f) && m_iMoriSound == 5)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::FRONT);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (239.0f / 321.0f) * 10.03f) && m_iMoriSound == 6)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/Swing.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (241.0f / 321.0f) * 10.03f) && m_iMoriSound == 7)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::BACK);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (262.0f / 321.0f) * 10.03f) && m_iMoriSound == 8)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/Swing.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (264.0f / 321.0f) * 10.03f) && m_iMoriSound == 9)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::FRONT);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}

	if ((m_fMoriTime >= (278.0f / 321.0f) * 10.03f) && m_iMoriSound == 10)
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/Scream.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}
}

void CSlasher::Check_Wraith_MoriEffect()
{
	if (nullptr == m_pMeshCom)
		return;

	if (server_data.Slasher.iCharacter != C_WRAITH)
		return;

	if (m_eCurState != AS::Mori)
	{
		m_iMoriSound = 0;
		m_fMoriTime = 0.f;
		return;
	}

	m_fMoriTime += m_fTimeDelta;

	if (m_fMoriTime >= 4.6f && (m_iMoriSound == 0))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Wraith/Swing.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}
	else if (m_fMoriTime >= 4.65f && (m_iMoriSound == 1))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::BACK);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}
	else if (m_fMoriTime >= 5.1f && (m_iMoriSound == 2))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Wraith/Swing.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}
	else if (m_fMoriTime >= 5.14f && (m_iMoriSound == 3))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::BACK);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}
	else if (m_fMoriTime >= 6.0f && (m_iMoriSound == 4))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Wraith/Swing.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}
	else if (m_fMoriTime >= 6.05f && (m_iMoriSound == 5))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::BACK);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}
	else if (m_fMoriTime >= 6.7f && (m_iMoriSound == 6))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Wraith/Swing.ogg", server_data.Slasher.vPos);
		++m_iMoriSound;
	}
	else if (m_fMoriTime >= 6.75f && (m_iMoriSound == 7))
	{
		GET_INSTANCE(CSoundManager)->PlaySound("Slasher/Spirit/PullOut.ogg", server_data.Slasher.vPos);
		if (nullptr != m_pMoriCamper)
			((CCamper*)m_pMoriCamper)->BloodSpread(CCamper::BACK);

		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		++m_iMoriSound;
	}
}

void CSlasher::Change_Mesh(_uint iNum)
{
	Safe_Release(m_pMeshCom);

	GET_INSTANCE_MANAGEMENT;

	switch (iNum)
	{
	case C_WRAITH:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Slasher_Wraith");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return;
	}
	break;
	case C_SPIRIT:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Slasher_Spirit");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return;
	}
	break;
	default:
		break;
	}



	m_pTransformCom->SetUp_RotationY(D3DXToRadian(270.f));

	Safe_Release(pManagement);
}

void CSlasher::Set_StartPosition()
{
	if (exPlayerNumber != 5)
		return;

	_int Random = Math_Manager::CalRandIntFromTo(0, 2);
	_vec3 vPos = { 0.f,0.f,0.f };
	if (Random == 0)
		vPos = { 7088.f, 0.f,  3061.f };
	else if (Random == 1)
		vPos = { 8986.f, 0.f,6991.f };
	else
		vPos = { 9894, 0.f ,9237.f };

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}

void CSlasher::SetUp_Sound()
{
	if (C_WRAITH == server_data.Slasher.iCharacter)
	{
		Add_SoundList(AS::WR_Bell_Loop, 0.2f, "Slasher/Wraith/Belling.ogg", 3200.f, 1.f);
		Add_SoundList(AS::WR_Bell_Loop, 0.7f, "Slasher/Wraith/Belling.ogg", 3200.f, 1.f);

		Add_SoundList(AS::Stun_Drop_In, 0.1f, "Slasher/Wraith/Stun.ogg");
		Add_SoundList(AS::Stun_Pallet, 0.1f, "Slasher/Wraith/Stun.ogg");

		Add_SoundList(AS::Attack_Swing, 0.1f, "Slasher/Wraith/Swing.ogg");
		Add_SoundList(AS::CarryAttack_Swing, 0.1f, "Slasher/Wraith/Swing.ogg");

		Add_SoundList(TW_Attack_Wipe, 0.1f, "Slasher/Wraith/Wipe.ogg");

		Add_SoundList(AS::Damage_Generator, 0.345f, "Slasher/body_rush_hit_wood_02.ogg", 850.f);
		Add_SoundList(AS::Damage_Generator, 0.63f, "Slasher/body_rush_hit_wood_02.ogg", 850.f);
		Add_SoundList(AS::Damage_Generator, 0.64f, "Generator/generator_explode_02.ogg", 1700.f);


		//Add_SoundList(TW_Bell_Loop_FPV, 0.45f, "Slasher/Wraith/Belling.ogg", 3200.f, 1.f);
		//	Add_SoundList(TW_Bell_Loop_FPV, 0.45f, "Slasher/Wraith/Belling.ogg", 3200.f, 1.f);
		//	Add_SoundList(TW_Bell_Loop, 0.9f, "Slasher/Wraith/Belling.ogg", 3200.f, 1.f);
		//	Add_SoundList(TW_Bell_Loop_FPV, 0.9f, "Slasher/Wraith/Belling.ogg", 3200.f, 1.f);

		//Add_SoundList(TT_Stun_Drop_IN, 0.1f, "Slasher/Wraith/Stun.ogg");
		//Add_SoundList(TT_Stun_Pallet, 0.1f, "Slasher/Wraith/Stun.ogg");

		//Add_SoundList(TW_Attack_Swing_FPV, 0.1f, "Slasher/Wraith/Swing.ogg");
		//Add_SoundList(TW_Attack_Swing, 0.1f, "Slasher/Wraith/Swing.ogg");
		//Add_SoundList(TT_Carry_Attack_Swing_FPV, 0.1f, "Slasher/Wraith/Swing.ogg");
		//Add_SoundList(TT_Carry_Attack_Swing, 0.1f, "Slasher/Wraith/Swing.ogg");

		//Add_SoundList(TW_Attack_Wipe, 0.1f, "Slasher/Wraith/Wipe.ogg");
		//Add_SoundList(TW_Attack_Wipe_FPV, 0.1f, "Slasher/Wraith/Wipe.ogg");

		//Add_SoundList(AW::TW_Damage_Generator, 0.4f,"Slasher/body_rush_hit_wood_02.ogg", 850.f);
		//Add_SoundList(TW_Damage_Generator, 0.8f, "Slasher/body_rush_hit_wood_02.ogg", 850.f);
	}
	else
	{


		Add_SoundList(AS::Stun_Drop_In, 0.1f, "Slasher/Spirit/Hurt.ogg");
		Add_SoundList(AS::Stun_Pallet, 0.1f, "Slasher/Spirit/Hurt.ogg");

		Add_SoundList(AS::Attack_Swing, 0.2f, "Slasher/Spirit/Swing.ogg");
		Add_SoundList(AS::CarryAttack_Swing, 0.2f, "Slasher/Spirit/Swing.ogg");

		Add_SoundList(AS::Attack_Swing, 0.1f, "Slasher/Spirit/Scream.ogg");
		Add_SoundList(AS::CarryAttack_Swing, 0.1f, "Slasher/Spirit/Scream.ogg");

		Add_SoundList(AS::Attack_In, 0.1f, "Slasher/Spirit/S_Appear.ogg", 500.f, 0.1f);
		Add_SoundList(AS::Attack_Miss_Out, 0.1f, "Slasher/Spirit/Dissapear.ogg", 500.f, 0.1f);
		Add_SoundList(AS::Attack_Bow, 0.1f, "Slasher/Spirit/Dissapear.ogg", 500.f, 0.1f);
		Add_SoundList(AS::Attack_Wipe, 0.1f, "Slasher/Spirit/Wipe.ogg", 500.f, 0.1f);
		
		Add_SoundList(AS::SP_Power_Charge, 0.1f, "Slasher/Spirit/S_Power.ogg", 1700.f, 1.f);
		
		Add_SoundList(AS::Damage_Generator, 0.345f, "Slasher/body_rush_hit_wood_02.ogg", 850.f);
		Add_SoundList(AS::Damage_Generator, 0.63f, "Slasher/body_rush_hit_wood_02.ogg", 850.f);
		Add_SoundList(AS::Damage_Generator, 0.64f, "Generator/generator_explode_02.ogg", 1700.f);

		//Add_SoundList(AS::HK_Stun_Drop_IN, 0.1f, "Slasher/Spirit/Hurt.ogg");
		//Add_SoundList(AS::HK_Stun_Drop_IN_FPV, 0.1f, "Slasher/Spirit/Hurt.ogg");
		//Add_SoundList(AS::HK_Stun_Pallet, 0.1f, "Slasher/Spirit/Hurt.ogg");
		//Add_SoundList(AS::HK_Stun_Pallet_FPV, 0.1f, "Slasher/Spirit/Hurt.ogg");

		//Add_SoundList(AS::HK_Attack_Swing, 0.2f, "Slasher/Spirit/Swing.ogg");
		//Add_SoundList(AS::HK_Attack_Swing_FPV, 0.2f, "Slasher/Spirit/Swing.ogg");
		//Add_SoundList(AS::HK_Carry_Attack_Swing, 0.2f, "Slasher/Spirit/Swing.ogg");
		//Add_SoundList(AS::HK_Carry_Attack_Swing_FPV, 0.2f, "Slasher/Spirit/Swing.ogg");

		//Add_SoundList(AS::HK_Attack_Swing, 0.1f, "Slasher/Spirit/Scream.ogg");
		//Add_SoundList(AS::HK_Attack_Swing_FPV, 0.1f, "Slasher/Spirit/Scream.ogg");
		//Add_SoundList(AS::HK_Carry_Attack_Swing, 0.1f, "Slasher/Spirit/Scream.ogg");
		//Add_SoundList(AS::HK_Carry_Attack_Swing_FPV, 0.1f, "Slasher/Spirit/Scream.ogg");

		//Add_SoundList(AS::HK_Attack_IN, 0.1f, "Slasher/Spirit/S_Appear.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Attack_IN_FPV, 0.1f, "Slasher/Spirit/S_Appear.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Attack_Miss, 0.1f, "Slasher/Spirit/Dissapear.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Attack_Miss_FPV, 0.1f, "Slasher/Spirit/Dissapear.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Attack_BOW, 0.1f, "Slasher/Spirit/Dissapear.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Attack_BOW_FPV, 0.1f, "Slasher/Spirit/Dissapear.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Attack_Wipe, 0.1f, "Slasher/Spirit/Wipe.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Attack_Wipe_FPV, 0.1f, "Slasher/Spirit/Wipe.ogg", 500.f, 0.1f);
		//Add_SoundList(AS::HK_Damage_Generator, 0.8f, "Slasher/body_rush_hit_wood_02.ogg", 850.f);
		//Add_SoundList(AS::HK_Damage_Generator_FPV, 0.8f, "Slasher/body_rush_hit_wood_02.ogg", 850.f);

		//167.4f / 321.0f
		//	219.0f
		//	242.0f
		// Ratio 268.0 -> Scream
		// Ratio 180, 224, 245 -> Blood(Hit Camper)
	}
}

void CSlasher::Update_Sound()
{
	_float TimeAcc = (_float)m_pMeshCom->GetTimeAcc();
	_int Animation = 0;
	if(server_data.Slasher.iCharacter == C_WRAITH)
		Animation = Change_Animation_Wriath(m_eCurState);
	else
		Animation = Change_Animation_Spirit(m_eCurState);
	_float Period = (_float)m_pMeshCom->GetPeriod(Animation);
	_float Ratio = TimeAcc / Period;
	Ratio = (Ratio - (int)Ratio);
	
	//cout << "T : " << TimeAcc<<" P : "<< Period<< " R : "<<Ratio<< endl;
	if (m_fRatio > Ratio || m_eOldState != m_eCurState)
	{
		m_iSoundCheck = 0;	//중복체크
		m_fRatio = Ratio;
	}
	else if (m_fRatio < Ratio)
	{
		if (AS::Damage_Generator == m_eCurState)
		{
		//	cout << Ratio << endl;
		}
		m_fRatio = Ratio;
		_int SoundCount = 0;

		for (auto& iter : m_SoundList)
		{
			if (m_eCurState == iter.m_iState)
			{
				if (iter.OtherOption != 0)
					continue;
				if (Ratio >= iter.Ratio && SoundCount == m_iSoundCheck)
				{
					if (m_eCurState == AS::WR_Bell_Loop)
						GET_INSTANCE(CSoundManager)->ChannelStop(m_CurChannel);
					m_CurChannel = GET_INSTANCE(CSoundManager)->PlaySound(string(iter.SoundName), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), iter.SoundDistance, iter.SoundValue);
					m_iSoundCheck++;
					return;
				}
				SoundCount++;
			}
		}
	}
}

void CSlasher::Add_SoundList(const _uint & m_iState, const _float Ratio, const char * SoundName, const _float & SoundDistance, const _float & SoundValue, const _int & OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundDistance = SoundDistance;
	Data.SoundValue = SoundValue;
	Data.OtherOption = OtherOption;

	m_SoundList.push_back(Data);
}

WSTATE CSlasher::Change_Animation_Wriath(AS::STATE Anim)
{
	WSTATE Anim_Wraith;
	m_bIsNoBlending = false;
	switch (Anim)
	{
	case AS::Attack_Bow:
		Anim_Wraith = TW_Attack_Bow_FPV;
		break;
	case AS::Attack_In:
		Anim_Wraith = TW_Attack_In_FPV;
		break;
	case AS::Attack_Miss_Out:
		Anim_Wraith = TW_Attack_Miss_Out_FPV;
		break;
	case AS::Attack_Swing:
		Anim_Wraith = TW_Attack_Swing_FPV;
		break;
	case AS::Attack_Wipe:
		Anim_Wraith = TW_Attack_Wipe_FPV;
		break;
	case AS::Carry_Attack_Bow:
		Anim_Wraith = TT_Carry_Attack_BOW_FPV;
		break;
	case AS::Carry_Attack_In:
		Anim_Wraith = TT_Carry_Attack_IN_FPV;
		break;
	case AS::Carry_Attack_MissOut:
		Anim_Wraith = TT_Carry_Attack_Out_FPV;
		break;
	case AS::CarryAttack_Swing:
		Anim_Wraith = TT_Carry_Attack_Swing_FPV;
		break;
	case AS::Drop:
		Anim_Wraith = TW_Drop;
		break;
	case AS::Hook_In:
		m_bIsNoBlending = true;
		Anim_Wraith = TW_Hook_In_FPV;
		break;
	case AS::Mori:
		Anim_Wraith = TW_Mori01;
		break;
	case AS::PickUp:
		Anim_Wraith = TW_PickUp;
		break;
	case AS::PickUp_In:
		Anim_Wraith = TW_PcikUp_IN;
		break;
	case AS::Idle:
		Anim_Wraith = TW_Idle;
		break;
	case AS::IdleMenu:
		Anim_Wraith = TW_IdleMenu;
		break;
	case AS::IdleMenu_Interrupt01:
		Anim_Wraith = TW_IdleMenu_Intterrupt01;
		break;
	case AS::IdleMenu_Interrupt02:
		Anim_Wraith = TW_IdleMenu_Intterrupt02;
		break;
	case AS::RunFT:
		Anim_Wraith = TW_RunFTTrans;
		break;
	case AS::RunBK:
		Anim_Wraith = TW_RunBKTrans;
		break;
	case AS::RunLT:
		Anim_Wraith = TW_RunLTTrans;
		break;
	case AS::RunRT:
		Anim_Wraith = TW_RunRTTrans;
		break;
	case AS::Search_locker:
		Anim_Wraith = TT_SearchLocker;
		break;
	case AS::Damage_Generator:
		Anim_Wraith = TW_Damage_Generator;
		break;
	case AS::Destroy_Pallet:
		Anim_Wraith = TW_Destroy_Pallet;
		break;
	case AS::Carry_Idle:
		Anim_Wraith = TT_Carry_Idle;
		break;
	case AS::Stun_Drop_In:
		Anim_Wraith = TT_Stun_Drop_IN;
		break;
	case AS::Stun_Drop_Out:
		Anim_Wraith = TT_Stun_Drop_Out;
		break;
	case AS::Stun_Pallet:
		Anim_Wraith = TT_Stun_Pallet;
		break;
	case AS::WindowVault:
		Anim_Wraith = TT_WindowVault;
		break;
	case AS::Fall:
		Anim_Wraith = TT_Fall;
		break;
	case AS::Grab_Generic_Fast:
		Anim_Wraith = TT_Grab_Generic_Fast_FPV;
		m_bIsNoBlending = true;
		break;
	case AS::Grab_Locker:
		Anim_Wraith = TT_Grab_Locker;
		m_bIsNoBlending = true;
		break;
	case AS::Grab_Obstacle_BK:
		m_bIsNoBlending = true;
		Anim_Wraith = TT_Grab_Obstacles_BK_FPV;
		break;
	case AS::Grab_Obstacle_FT:
		m_bIsNoBlending = true;
		Anim_Wraith = TT_Grab_Obstacles_FT_FPV;
		break;
	case AS::LandLight:
		Anim_Wraith = TT_LandLight;
		break;
	case AS::Run_End_Game:
		Anim_Wraith = TW_RunFT;
		break;
	case AS::WR_SetArmOffset:
		Anim_Wraith = TT_SetArmOffset;
		break;
	case AS::WR_Bell_Loop:
		Anim_Wraith = TW_Bell_Loop;
		break;
	case AS::WR_Carry_Run:
		Anim_Wraith = TT_RunFTTrans;
		break;
	case AS::Stand:
		Anim_Wraith = TW_Stand;
		break;
	}
	
	if (exPlayerNumber != 5 && Find_FPVAnim(Anim_Wraith))
		Anim_Wraith = (WSTATE)(_int(Anim_Wraith) - 1);

	if (m_eOldState == AS::Grab_Generic_Fast)
		m_bIsNoBlending = true;

	return Anim_Wraith;
}

SSTATE CSlasher::Change_Animation_Spirit(AS::STATE Anim)
{
	SSTATE SpiritAnim;
	m_bIsNoBlending = false;
	switch (Anim)
	{
	case AS::Attack_Bow:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Attack_BOW_FPV;
		break;
	case AS::Attack_In:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Attack_IN_FPV;
		break;
	case AS::Attack_Miss_Out:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Attack_Miss_FPV;
		break;
	case AS::Attack_Swing:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Attack_Swing_FPV;
		break;
	case AS::Attack_Wipe:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Attack_Wipe_FPV;
		break;
	case AS::Carry_Attack_Bow:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Carry_Attack_BOW_FPV;
		break;
	case AS::Carry_Attack_In:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Carry_Attack_IN_FPV;
		break;
	case AS::Carry_Attack_MissOut:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Carry_Attack_OUT_FPV;
		break;
	case AS::CarryAttack_Swing:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Carry_Attack_Swing_FPV;
		break;
	case AS::Drop:
		SpiritAnim = HK_Drop_FPV;
		break;
	case AS::Hook_In:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Hook_IN_FPV;
		break;
	case AS::Mori:
		SpiritAnim = HK_Mori;
		break;
	case AS::PickUp:
		SpiritAnim = HK_PickUp_FPV;
		break;
	case AS::PickUp_In:
		SpiritAnim = HK_PickUp_IN_FPV;
		break;
	case AS::Idle:
		SpiritAnim = HK_Idle_FPV;
		break;
	case AS::IdleMenu:
		SpiritAnim = HK_IdleMenuBack;
		break;
	case AS::IdleMenu_Interrupt01:
		SpiritAnim = HK_IdleMenu_Interrupt01;
		break;
	case AS::IdleMenu_Interrupt02:
		SpiritAnim = HK_IdleMenu_Interrupt02;
		break;
	case AS::RunFT:
		SpiritAnim = HK_RunFT_FPV;
		break;
	case AS::RunBK:
		SpiritAnim = HK_RunBK_FPV;
		break;
	case AS::RunLT:
		SpiritAnim = HK_RunLT_FPV;
		break;
	case AS::RunRT:
		SpiritAnim = HK_RunRT_FPV;
		break;
	case AS::Damage_Generator:
		SpiritAnim = HK_Damage_Generator_FPV;
		break;
	case AS::Destroy_Pallet:
		SpiritAnim = HK_Destroy_Pallet_FPV;
		break;
	case AS::Carry_Idle:
		SpiritAnim = HK_Carry_Idle_FPV;
		break;
	case AS::Stun_Drop_In:
		SpiritAnim = HK_Stun_Drop_IN_FPV;
		break;
	case AS::Stun_Drop_Out:
		SpiritAnim = HK_Stun_Drop_Out_FPV;
		break;
	case AS::Stun_Pallet:
		SpiritAnim = HK_Stun_Pallet_FPV;
		break;
	case AS::WindowVault:
		SpiritAnim = HK_WindowVault_FPV;
		break;
	case AS::Fall:
		SpiritAnim = HK_Fall_FPV;
		break;
	case AS::Grab_Generic_Fast:
		SpiritAnim = HK_Grab_Generic_Fast_FPV;
		m_bIsNoBlending = true;
		break;
	case AS::Grab_Locker:
		SpiritAnim = HK_Grab_Locker_FPV;
		m_bIsNoBlending = true;
		break;
	case AS::Grab_Obstacle_BK:
		SpiritAnim = HK_Grab_Obstacles_BK_FPV;
		m_bIsNoBlending = true;
		break;
	case AS::Grab_Obstacle_FT:
		m_bIsNoBlending = true;
		SpiritAnim = HK_Grab_Obstacles_FT_FPV;
		break;
	case AS::LandLight:
		SpiritAnim = HK_Search_Locker_FPV;
		break;
	case AS::Run_End_Game:
		SpiritAnim = HK_RunEndGame;
		break;
	case AS::Search_locker:
		SpiritAnim = HK_Search_Locker_FPV;
		break;
	case AS::SP_Power_Charge:
		SpiritAnim = HK_Power_Charge_FPV;
		break;
	case AS::SP_Power_Idle:
		SpiritAnim = HK_Power_Idle_FPV;
		break;
	case AS::SP_Power_Run_FT:
		SpiritAnim = HK_Power_RunFT_FPV;
		break;
	case AS::SP_Power_Run_BK:
		SpiritAnim = HK_Power_RunBK_FPV;
		break;
	case AS::SP_Power_Out:
		SpiritAnim = HK_Power_Out_FT;
		break;
	default:
		SpiritAnim = (SSTATE)Anim;
		break;
	}

	if (exPlayerNumber != 5)
	{
		if (m_fSTime >= 0.3f)
			return HK_Power_Out_FT;

		if (SpiritAnim == HK_Power_Charge_FPV || SpiritAnim == HK_Power_Idle_FPV || SpiritAnim == HK_Power_RunFT_FPV || SpiritAnim == HK_Power_RunBK_FPV)
			return HK_Idle;
		
		if(SpiritAnim != HK_Mori)
			SpiritAnim = (SSTATE)(_int(SpiritAnim) - 1);
	}  

	if (m_eOldState == AS::Grab_Generic_Fast)
		m_bIsNoBlending = true;

	return SpiritAnim;
}

void CSlasher::Change_TimeOfAnimation(const _float & fTimeDelta)
{
	m_fAnimationIndex += fTimeDelta * 30.f;
	_int iIndex = (_int)m_fAnimationIndex;
	if (server_data.Slasher.iCharacter == C_WRAITH)
	{
		if (m_eCurState == AS::Attack_In && iIndex >= 7)
			m_fTimeDelta = 0.000001f;
		else if (m_eCurState == AS::Attack_Swing)
			m_fTimeDelta = fTimeDelta * 1.5f;
		else if (m_eCurState == AS::WR_Bell_Loop)
			m_fTimeDelta = fTimeDelta * 0.5f;
		else if (m_eCurState == AS::Attack_Wipe)
			m_fTimeDelta = fTimeDelta * 0.8f;
		else if (m_eCurState == AS::WindowVault)
			m_fTimeDelta = fTimeDelta * 0.9f;
	}
	else if(server_data.Slasher.iCharacter == C_SPIRIT)
	{
		if (m_eCurState == AS::Attack_In && iIndex >= 9)
			m_fTimeDelta = 0.000001f;
		else if (m_eCurState == AS::Attack_Swing)
			m_fTimeDelta = fTimeDelta * 0.9f;
		else if (m_eCurState == AS::Stun_Drop_Out)
			m_fTimeDelta = fTimeDelta * 0.3f;
		else if (m_eCurState == AS::Attack_Wipe)
			m_fTimeDelta = fTimeDelta * 0.8f;
		else if (m_eCurState == AS::Attack_Miss_Out)
			m_fTimeDelta = fTimeDelta * 0.8f;
		else if (m_eCurState == AS::WindowVault)
			m_fTimeDelta = fTimeDelta * 0.9f;


	}

}

CSlasher * CSlasher::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CSlasher*	pInstance = new CSlasher(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CSlasher Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CSlasher::Clone_GameObject()
{
	CSlasher*	pInstance = new CSlasher(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CSlasher Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CSlasher::Free()
{
	m_vecFPVAnim.clear();

	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_PLAYER, this);

	m_pSkillWorld.clear();
	for (auto& pSour : m_pFrameNum)
		pSour.clear();
	m_pFrameNum.clear();

	for (auto pSour : m_pSkillMatrix)
	{
		for (auto& pMat : pSour)
			Safe_Delete_Array(pMat);
		pSour.clear();
	}
	m_pSkillMatrix.clear();

	//Safe_Release(m_pSkillTexture);
	Safe_Release(m_pHeadColliderCom);
	Safe_Release(m_pCustomLight);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pColliderCom);

	CGameObject::Free();
}