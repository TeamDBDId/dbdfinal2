#include "stdafx.h"
#include "..\Headers\Action_HealingSelf.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Item.h"

_USING(Client)

CAction_HealingSelf::CAction_HealingSelf()
{
}

HRESULT CAction_HealingSelf::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;
	m_fHealTime = 0.f;
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_HealSelf);
	CCamper* pCamper = (CCamper*)m_pGameObject;

	if (nullptr == (CCamper*)pCamper->GetCurItem()
		&& !(server_data.Campers[exPlayerNumber - 1].Perk & SELFHEAL))
		return NOERROR;

	if (pCamper->GetCurCondition() == CCamper::HEALTHY)
		return NOERROR;

	pCamper->IsLockKey(true);
	
	m_bIsPlaying = true;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->Set_State(AC::HealingSelf);
	m_iState = AC::HealingSelf;
	m_bIsFinished = false;

	return NOERROR;
}

_int CAction_HealingSelf::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	_float fAcc = 1.0f;
	if (server_data.Campers[exPlayerNumber - 1].Perk & SELFHEAL)
		fAcc = 0.7f;

	if (nullptr != pCamper->GetCurItem() && pCamper->GetCurItem()->Get_ItemType() == CItem::MEDIKIT)
	{
		if (!pCamper->GetCurItem()->Get_IsDead())
		{
			camper_data.bUseItem = true;
			_bool bOverDurability = false;

			bOverDurability = pCamper->GetCurItem()->Use_Item(fTimeDelta, nullptr, nullptr);

			if (KEYMGR->MousePressing(1) && pCamper->GetCurItem()->Get_ItemType() == CItem::MEDIKIT && !(server_data.Campers[exPlayerNumber - 1].Perk & SELFHEAL))
				fAcc = 1.4f;
			
			else if (KEYMGR->MousePressing(1) && pCamper->GetCurItem()->Get_ItemType() == CItem::MEDIKIT && (server_data.Campers[exPlayerNumber - 1].Perk & SELFHEAL))
				fAcc = 1.9f;
		}
	}

	pCamper->SetHeal(pCamper->GetHeal() + fTimeDelta * fAcc);
	//camper_data.fHeal = pCamper->GetHeal() + fTimeDelta * fAcc;
	m_fHealTime += fTimeDelta;
	GET_INSTANCE(CUIManager)->Set_ProgressPoint(pCamper->GetHeal(), pCamper->GetMaxHeal());

	_int SkillCheck = GET_INSTANCE(CUIManager)->Get_SkillCheckState();
	if (SkillCheck == FAILED_SKILL_CHECK)
	{
		m_iState = AC::BeingHealFail;
		((CCamper*)m_pGameObject)->Set_State(AC::BeingHealFail);
	}
	else if (SkillCheck == SUCCESS_SKILL_CHECK)
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Altruism, L"스킬 체크(훌륭함)", 150);
	else if(SkillCheck == 12)
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Altruism, L"스킬 체크(좋음)", 50);

	if (m_iState == AC::BeingHealFail)
	{
		CMesh_Dynamic* pMesh_Com = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMesh_Com->IsOverTime(0.2f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::HealingSelf);
			m_iState = AC::HealingSelf;
		}
	}

	if (pCamper->GetHeal() > pCamper->GetMaxHeal())
	{
		pCamper->SetHeal(0.f);
		m_bIsFinished = true;
		return END_ACTION;
	}

	if (!KEYMGR->MousePressing(1))
		return END_ACTION;
	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_HealingSelf::End_Action()
{
	CCamper* pCamper = (CCamper*)m_pGameObject;
	GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Survival, wstring(L"치료"), _int(m_fHealTime* 400.f / pCamper->GetMaxHeal()));
	m_bIsPlaying = false;
	pCamper->IsLockKey(false);
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	if (m_bIsFinished)
		pCamper->SetCurCondition(CCamper::HEALTHY);
	Send_ServerData();
}

void CAction_HealingSelf::Send_ServerData()
{
	if (m_pGameObject != nullptr)
		camper_data.InterationObject = m_pGameObject->GetID();
	else
		camper_data.InterationObject = 0;
}

void CAction_HealingSelf::Free()
{
	CAction::Free();
}
