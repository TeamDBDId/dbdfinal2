#include "stdafx.h"
#include "Effect_BloodUI.h"
#include "Management.h"

#include "Effect_BloodAnim.h"


#include "Math_Manager.h"
_USING(Client)

CEffect_BloodUI::CEffect_BloodUI(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_BloodUI::CEffect_BloodUI(const CEffect_BloodUI & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_BloodUI::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_BloodUI::Ready_GameObject()
{

	return NOERROR;
}

_int CEffect_BloodUI::Update_GameObject(const _float & _fTick)
{
	if (true == m_isDead)
		return DEAD_OBJ;

	m_fLifeTime += _fTick;
	if (1.1f < m_fLifeTime)
		m_isDead = true;
	

	Make_Anim(_fTick);
	Make_DropAnim(_fTick);
	Make_Drop(_fTick);
	Make_DropBig(_fTick);


	return _int();
}

_int CEffect_BloodUI::LastUpdate_GameObject(const _float & _fTick)
{



	return _int();
}

void CEffect_BloodUI::Render_GameObject()
{

}

void CEffect_BloodUI::Make_Drop(const _float & _fTick)
{
	m_fDropTime += _fTick;
	if (m_fDropTime < DROPDELAY)
		return;
	m_fDropTime = 0.f;

	GET_INSTANCE_MANAGEMENT;

	CEffect_BloodAnim* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_BloodAnim", SCENE_STAGE, L"Layer_BloodAnim", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_BloodAnim");
	pEffect->Set_Param(CEffect_BloodAnim::B_Drop);

	Safe_Release(pManagement);
}

void CEffect_BloodUI::Make_DropBig(const _float & _fTick)
{
	m_fDropBigTime += _fTick;
	if (m_fDropBigTime < DROPBIGDELAY)
		return;
	m_fDropBigTime = 0.f;

	GET_INSTANCE_MANAGEMENT;

	CEffect_BloodAnim* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_BloodAnim", SCENE_STAGE, L"Layer_BloodAnim", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_BloodAnim");
	pEffect->Set_Param(CEffect_BloodAnim::B_DropBig);

	Safe_Release(pManagement);
}

void CEffect_BloodUI::Make_DropAnim(const _float & _fTick)
{
	m_fDropAnimTime += _fTick;
	if (m_fDropAnimTime < DROPANIMDELAY)
		return;
	m_fDropAnimTime = 0.f;

	GET_INSTANCE_MANAGEMENT;

	CEffect_BloodAnim* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_BloodAnim", SCENE_STAGE, L"Layer_BloodAnim", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_BloodAnim");
	pEffect->Set_Param(CEffect_BloodAnim::B_DropAnim);

	Safe_Release(pManagement);
}

void CEffect_BloodUI::Make_Anim(const _float & _fTick)
{
	m_fAnimTime += _fTick;
	if (m_fAnimTime < ANIMDELAY)
		return;
	m_fAnimTime = 0.f;

	GET_INSTANCE_MANAGEMENT;

	CEffect_BloodAnim* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_BloodAnim", SCENE_STAGE, L"Layer_BloodAnim", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_BloodAnim");

	
	pEffect->Set_Param(CEffect_BloodAnim::B_Anim);


	Safe_Release(pManagement);
}



// 원형객체를 생성하기위해 만들어진 함수.
CEffect_BloodUI * CEffect_BloodUI::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_BloodUI*	pInst = new CEffect_BloodUI(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_BloodUI::Clone_GameObject()
{
	CEffect_BloodUI*	pInst = new CEffect_BloodUI(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_BloodUI Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_BloodUI::Free()
{

	CGameObject::Free();
}