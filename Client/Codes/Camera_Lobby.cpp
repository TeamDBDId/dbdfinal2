#include "stdafx.h"
#include "..\Headers\Camera_Lobby.h"
#include "Management.h"
#include "Math_Manager.h"

_USING(Client)

CCamera_Lobby::CCamera_Lobby(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CCamera(pGraphic_Device)
{
}

CCamera_Lobby::CCamera_Lobby(const CCamera_Lobby & rhs)
	: CCamera(rhs)
{
}

HRESULT CCamera_Lobby::Ready_Prototype()
{
	if (FAILED(CCamera::Ready_Prototype()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCamera_Lobby::Ready_GameObject()
{
	if (FAILED(CCamera::Ready_GameObject()))
		return E_FAIL;

	if (FAILED(CCamera_Lobby::Ready_Component()))
		return E_FAIL;

	m_pTransform->SetUp_Speed(450.f, D3DXToRadian(360.f));

	D3DVIEWPORT9			ViewPort;
	m_pGraphic_Device->GetViewport(&ViewPort);

	m_ptMouse.x = ViewPort.Width >> 1;
	m_ptMouse.y = ViewPort.Height >> 1;

	ClientToScreen(g_hWnd, &m_ptMouse);

	m_StatePos[L_Start] = {16.2678f, 300.29f, -500.f};
	m_StateLook[L_Start] = { 0.374619f, -0.212417f, 0.902514f };

	m_StatePos[L_RoleSelection] = { 1524.22f, 230.402f, -171.12f };
	m_StateLook[L_RoleSelection] = { 0.0630007f, 0.0133554f, 0.997919f };

	m_StatePos[L_Camper] = { 1317.68f, 176.008f, 400.182f };
	m_StateLook[L_Camper] = { -0.0446666f, 0.132312f, 0.990196f };

	m_StatePos[L_Slasher] = { 1378.4f, 346.29f, -346.64f };
	m_StateLook[L_Slasher] = { 0.117845f, -0.21575f, 0.969386f};

	m_CurPos = m_StatePos[L_Start];
	m_CurLook = m_StateLook[L_Start];
	m_OldLobbyState = L_Start;
	m_CurLobbyState = L_RoleSelection;

	return NOERROR;
}

_int CCamera_Lobby::Update_GameObject(const _float & fTimeDelta)
{
	State_Check(fTimeDelta);
	



	if (nullptr == m_pInput_Device)
		return -1;



	Invalidate_ViewProjMatrix();
	m_pFrustumCom->Transform_ToWorld();

	return _int();
}

_int CCamera_Lobby::LastUpdate_GameObject(const _float & fTimeDelta)
{


	return _int();
}

void CCamera_Lobby::Render_GameObject()
{
}

void CCamera_Lobby::Set_State(LobbyState State)
{
	if (m_iSoundNum != 0)
		GET_INSTANCE(CSoundManager)->ChannelStop(m_iSoundNum);
	if (State == L_RoleSelection)
		m_iSoundNum = GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/BlackSmoke_Dissolve_Bounce_01.ogg"), _vec3(), 0, 0.5f);
	else if(State == L_Camper)
		m_iSoundNum = GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/BlackSmoke_Dissolve_Bounce_02.ogg"), _vec3(), 0, 0.5f);
	else if(State == L_Slasher)
		m_iSoundNum = GET_INSTANCE(CSoundManager)->PlaySound(string("Lobby/BlackSmoke_Dissolve_Bounce_03.ogg"), _vec3(), 0, 0.5f);
	m_CurLobbyState = State;
}

void CCamera_Lobby::Move_StateToState(const LobbyState & _From, const LobbyState & _To)
{
	if (m_CurPos.x == m_StatePos[_To].x)
	{
		m_fTime = 0.f;
		m_OldLobbyState = m_CurLobbyState;
		return;
	}
	_vec3 vUp;
	D3DXVec3Lerp(&m_CurPos, &m_CurPos, &m_StatePos[_To], m_fTime);
	D3DXVec3Lerp(&m_CurLook, &m_CurLook, &m_StateLook[_To], m_fTime);
	
	//vPosition = Math_Manager::ProgressToPersent(m_StatePos[_From], m_StatePos[_To], m_fTime);
	//vLook = Math_Manager::ProgressToPersent(m_StateLook[_From], m_StateLook[_To], m_fTime);

	D3DXVec3Normalize(&m_CurLook, &m_CurLook);
	D3DXVec3Cross(&vUp, &m_CurLook, &_vec3(1.f, 0.f, 0.f));
	D3DXVec3Normalize(&vUp, &vUp);

	m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &_vec3(1.f, 0.f, 0.f));
	m_pTransform->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &m_CurLook);
	m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &m_CurPos);
}

void CCamera_Lobby::State_Check(const _float& fTimeDelta)
{
	if (m_OldLobbyState == m_CurLobbyState)
		return;
	
	//m_fTime += fTimeDelta * 0.4f;
	m_fTime = fTimeDelta * 0.4f;
	Move_StateToState(m_OldLobbyState, m_CurLobbyState);
	
	

}



HRESULT CCamera_Lobby::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

CCamera_Lobby * CCamera_Lobby::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera_Lobby*	pInstance = new CCamera_Lobby(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCamera_Lobby Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


CGameObject * CCamera_Lobby::Clone_GameObject()
{
	CCamera_Lobby*	pInstance = new CCamera_Lobby(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCamera_Lobby Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamera_Lobby::Free()
{
	Safe_Release(m_pFrustumCom);
	CCamera::Free();
}
