#include "stdafx.h"
#include "..\Headers\Action_JumpPlank.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Plank.h"

_USING(Client)

CAction_JumpPlank::CAction_JumpPlank()
{
}

HRESULT CAction_JumpPlank::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(true);
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::OBSTACLE);
	pCamper->SetColl(false);
	m_bIsPlaying = true;
	m_fIndex = 0.f;

	if (!m_bIsInit)
		SetVectorPos();

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	CTransform* pPlankTransform = (CTransform*)m_pPlank->Get_ComponentPointer(L"Com_Transform");
	_vec3 vPlankPos = *pPlankTransform->Get_StateInfo(CTransform::STATE_POSITION);

	_vec3 vRight = *pTransform->Get_StateInfo(CTransform::STATE_RIGHT);
	_vec3 vDir;
	D3DXVec3Normalize(&vDir, &(vPlankPos - m_vOriginPos));

	_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vRight, &vDir)));

	if (KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		if (fAngle >= 90.f)
			pCamper->Set_State(AC::JumpOverAngle60LTFast);
		else
			pCamper->Set_State(AC::JumpOverAngle60RTFast);

		m_iState = AC::JumpOverAngle60RTFast;
	}
	else
	{
		if (fAngle >= 90.f)
			pCamper->Set_State(AC::JumpOverAngle60LT);
		else
			pCamper->Set_State(AC::JumpOverAngle60RT);
		m_iState = AC::JumpOverAngle60RT;
	}


	_vec3 vPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION) +
		*pTransform->Get_StateInfo(CTransform::STATE_LOOK)*45.f;
	vPos.y += 100.f;
	EFFMGR->Make_Effect(CEffectManager::E_Jump, vPos);


	return NOERROR;
}

_int CAction_JumpPlank::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

		m_fIndex += fTimeDelta * 30.f;

	size_t iIndex = (size_t)m_fIndex;

	if (m_iState == AC::JumpOverAngle60RT)
	{
		if (m_vecPos.size() <= iIndex)
			return END_ACTION;

		if (m_fIndex >= 56.f)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		return UPDATE_ACTION;
	}
	else
	{
		if (m_vecFastPos.size() <= iIndex)
			return END_ACTION;

		if (m_fIndex >= 31.f)
			return END_ACTION;

		_vec3 vLocalPos = m_vecFastPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		return UPDATE_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_JumpPlank::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (pCamper->GetCurCondition() != CCamper::DYING)
		pCamper->SetCurCondition(pCamper->GetOldCondition());
	else
		pCamper->SetCurCondition(CCamper::DYING);
	pCamper->Set_State(AC::Idle);
	pCamper->IsLockKey(false);
	pCamper->SetColl(true);
}

void CAction_JumpPlank::Send_ServerData()
{
}

void CAction_JumpPlank::SetVectorPos()
{
	m_vecPos.reserve(56);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.018f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.023f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.028f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.028f));
	m_vecPos.push_back(_vec3(0.f, 2.f, 0.029f));;
	m_vecPos.push_back(_vec3(0.f, 4.f, 1.586f));
	m_vecPos.push_back(_vec3(0.f, 6.f, 5.855f));
	m_vecPos.push_back(_vec3(0.f, 8.f, 12.237f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 20.132f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 28.939f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 38.059f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 46.89f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 54.834f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 61.29f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 66.577f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 71.463f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 76.02f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 80.318f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 84.43f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 88.427f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 92.38f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 96.362f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 100.444f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 104.697f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 109.193f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 114.003f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 119.2f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 124.854f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 131.038f));
	m_vecPos.push_back(_vec3(0.f, 10.f, 137.822f));
	m_vecPos.push_back(_vec3(0.f, 8.f, 148.183f));
	m_vecPos.push_back(_vec3(0.f, 6.f, 163.275f));
	m_vecPos.push_back(_vec3(0.f, 4.f, 180.434f));
	m_vecPos.push_back(_vec3(0.f, 2.f, 196.996f));
	m_vecPos.push_back(_vec3(0.f, 1.f, 210.297f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 217.676f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 220.992f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 221.992f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 223.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 224.067f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 225.087f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 226.059f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 226.957f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 227.752f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 228.418f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 228.927f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 229.252f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 229.367f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 229.367f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 229.367f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 229.367f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 229.367f));

	m_vecFastPos.reserve(31);

	m_vecFastPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecFastPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecFastPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecFastPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecFastPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecFastPos.push_back(_vec3(0.f, 2.f, 0.f));
	m_vecFastPos.push_back(_vec3(0.f, 4.f, 1.363f));
	m_vecFastPos.push_back(_vec3(0.f, 6.f, 5.239f));
	m_vecFastPos.push_back(_vec3(0.f, 8.f, 11.312f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 19.265f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 28.781f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 39.542f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 51.231f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 63.531f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 76.126f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 88.697f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 100.929f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 116.042f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 135.783f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 157.616f));
	m_vecFastPos.push_back(_vec3(0.f, 10.f, 179.008f));
	m_vecFastPos.push_back(_vec3(0.f, 9.f, 197.425f));
	m_vecFastPos.push_back(_vec3(0.f, 8.f, 210.334f));
	m_vecFastPos.push_back(_vec3(0.f, 7.f, 215.288f));
	m_vecFastPos.push_back(_vec3(0.f, 6.f, 215.288f));
	m_vecFastPos.push_back(_vec3(0.f, 5.f, 215.288f));
	m_vecFastPos.push_back(_vec3(0.f, 4.f, 215.288f));
	m_vecFastPos.push_back(_vec3(0.f, 3.f, 215.288f));
	m_vecFastPos.push_back(_vec3(0.f, 2.f, 215.288f));
	m_vecFastPos.push_back(_vec3(0.f, 1.f, 215.288f));
	m_vecFastPos.push_back(_vec3(0.f, 0.f, 215.288f));


	m_bIsInit = true;
}

void CAction_JumpPlank::Free()
{
	CAction::Free();
}
