#include "stdafx.h"
#include "Effect_Death.h"
#include "Management.h"

#include "Effect_DeathSmoke.h"
//#include "Effect_Flash.h"


#include "Math_Manager.h"
_USING(Client)

CEffect_Death::CEffect_Death(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_Death::CEffect_Death(const CEffect_Death & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Death::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Death::Ready_GameObject()
{
	m_vSmokeDir = _vec3(90.f, 0.f, 0.f);



	return NOERROR;
}

_int CEffect_Death::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;

	m_fTick += _fTick;
	if (12.f < m_fTick)
		m_isDead = true;



	Make_Smoke(_fTick);


	return _int();
}

_int CEffect_Death::LastUpdate_GameObject(const _float & _fTick)
{



	return _int();
}

void CEffect_Death::Render_GameObject()
{


}

void CEffect_Death::Set_Param(const _vec3 & _vPos)
{
	m_vPos = _vPos;
}





void CEffect_Death::Make_Smoke(const _float& _fTick)
{
	m_fDelay += _fTick;
	if (0.1f < m_fDelay)
		m_fDelay = 0.f;



	GET_INSTANCE_MANAGEMENT;


	_matrix matRot;
	D3DXMatrixRotationY(&matRot, Math_Manager::CalRandFloatFromTo(0.f,6.18f));


	CEffect_DeathSmoke* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_DeathSmoke", SCENE_STAGE, L"Layer_EDeathSmoke", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_DeathSmoke");
	pEffect->Set_Param(m_vPos, &m_vSmokeDir);
	//pEffect->Set_Type(0);

	D3DXVec3TransformNormal(&m_vSmokeDir, &m_vSmokeDir, &matRot);


	Safe_Release(pManagement);
}

void CEffect_Death::Make_Smoke2()
{
	//GET_INSTANCE_MANAGEMENT;

	//_vec3 vSmokeDir = _vec3(20.f, 0.f, 0.f);
	//_vec3 vPos = m_vPos;


	//_matrix matRot;
	//D3DXMatrixRotationY(&matRot, D3DXToRadian(60.f));

	//for (size_t i = 0; i < 8; ++i)
	//{

	//	for (size_t i = 0; i < SMOKE_CNT/2; ++i)
	//	{
	//		CEffect_SmokeBall* pEffect = nullptr;
	//		if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_SmokeBall", SCENE_STAGE, L"Layer_ESmokeBall", (CGameObject**)&pEffect)))
	//			_MSG_BOX("Fail Make_SmokeBall");
	//		pEffect->Set_Param(vPos + vSmokeDir, &vSmokeDir);

	//		D3DXVec3TransformNormal(&vSmokeDir, &vSmokeDir, &matRot);
	//	}



	//	vPos.y += 20.f;
	//}
	//
	//Safe_Release(pManagement);




	//GET_INSTANCE_MANAGEMENT;

	//_vec3 vSmokeDir = _vec3(0.f, -120.f, 0.f);
	//_vec3 vRand = _vec3();


	//for (size_t i = 0; i < SMOKE_CNT * 4; ++i)
	//{

	//	vRand = _vec3(Math_Manager::CalRandIntFromTo(-60, 60), Math_Manager::CalRandIntFromTo(-80, 160), Math_Manager::CalRandIntFromTo(-60, 60));


	//	CEffect_DeathSmoke* pEffect = nullptr;
	//	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_SmokeBall", SCENE_STAGE, L"Layer_ESmokeBall", (CGameObject**)&pEffect)))
	//		_MSG_BOX("Fail Make_SmokeBall");
	//	pEffect->Set_Param(m_vPos + vRand, &vSmokeDir);
	//	//pEffect->Set_Type(1);

	//}



	//Safe_Release(pManagement);


}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Death * CEffect_Death::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Death*	pInst = new CEffect_Death(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Death::Clone_GameObject()
{
	CEffect_Death*	pInst = new CEffect_Death(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_Death Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Death::Free()
{

	CGameObject::Free();
}