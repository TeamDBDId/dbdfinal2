#include "stdafx.h"
#include "..\Headers\WraithEffectUI.h"
#include "Management.h"
#include "MeshTexture.h"
#include "Slasher.h"

_USING(Client)

CWraithEffectUI::CWraithEffectUI(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CWraithEffectUI::CWraithEffectUI(const CWraithEffectUI & rhs)
	: CGameObject(rhs)
{
}

HRESULT CWraithEffectUI::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CWraithEffectUI::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	return NOERROR;
}

_int CWraithEffectUI::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	Check_UseSkill();

	m_fTime += fTimeDelta;
	if (m_fTime > 1.f)
		m_fTime = 0.f;

	return _int();
}

_int CWraithEffectUI::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	Check_UseSkill();

	if (!m_bSkill)
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return _int();
}

void CWraithEffectUI::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	LPD3DXEFFECT pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();
	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(12);

	SetUp_ConstantTable(pEffect);
	pEffect->CommitChanges();

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CWraithEffectUI::Check_UseSkill()
{
	if (5 != exPlayerNumber)
		return;

	GET_INSTANCE_MANAGEMENT;

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front();
	if (nullptr == pSlasher)
	{
		Safe_Release(pManagement);
		return;
	}

	m_bSkill = pSlasher->Get_Stealth();

	Safe_Release(pManagement);
}

HRESULT CWraithEffectUI::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Shader_Default");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CWraithEffectUI::SetUp_ConstantTable(LPD3DXEFFECT pEffect)
{
	if (nullptr == m_pRendererCom)
		return E_FAIL;

	_matrix			matWorld;
	D3DXMatrixIdentity(&matWorld);

	matWorld._11 = g_iBackCX;
	matWorld._22 = g_iBackCY;

	matWorld._41 = /*(g_iBackCX * 0.5f) - (g_iBackCX >> 1);*/0.f;
	matWorld._42 = /*-(g_iBackCY * 0.5f) + (g_iBackCY >> 1);*/ 0.f;

	pEffect->SetMatrix("g_matWorld", &matWorld);

	_matrix			matView;
	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	_matrix			matProj;
	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetFloat("g_fTime", m_fTime);
	m_pRendererCom->SetUp_OnShaderFromTarget(pEffect, L"Target_BackBuffer", "g_DiffuseTexture");

	pEffect->SetTexture("g_DistortionTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"Water.tga")));
	pEffect->SetTexture("g_OffsetTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Noise01.tga")));
	pEffect->SetTexture("g_FlowTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Flowmap3.tga")));
	pEffect->SetTexture("g_MaskTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"Decal.tga")));
	pEffect->SetTexture("g_BorderTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"FlowMap01.tga")));
	pEffect->SetTexture("g_BorderOffsetTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"Water.tga")));

	return NOERROR;
}

CWraithEffectUI * CWraithEffectUI::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CWraithEffectUI*	pInstance = new CWraithEffectUI(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CWraithEffectUI Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CWraithEffectUI::Clone_GameObject()
{
	CWraithEffectUI*	pInstance = new CWraithEffectUI(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CWraithEffectUI Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CWraithEffectUI::Free()
{
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pShaderCom);

	CGameObject::Free();
}
