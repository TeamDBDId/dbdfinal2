#include "stdafx.h"
#include "..\Headers\Action_BeingKilledBySpider.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Camera_Camper.h"
#include "MeatHook.h"

_USING(Client)

CAction_BeingKilledBySpider::CAction_BeingKilledBySpider()
{
}

HRESULT CAction_BeingKilledBySpider::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	pCamper->SetCurCondition(CCamper::HOOKDEAD);
	pCamper->Set_State(AC::BeingKilledBySpiderIN);
	m_iState = AC::BeingKilledBySpiderIN;
	if(m_pHook != nullptr)
		m_pHook->SetState(CMeatHook::SpiderStabIN);
	Send_ServerData();



	

	return NOERROR;
}

_int CAction_BeingKilledBySpider::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;
	CManagement* pManagement = CManagement::GetInstance();
	CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0);
	if (pCamera == nullptr)
		return END_ACTION;
	pCamera->Set_MoriCamera(true);
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	if (pMeshCom == nullptr)
		return END_ACTION;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (pCamper == nullptr)
		return END_ACTION;
	if (m_iState == AC::BeingKilledBySpiderOut && pMeshCom->IsOverTime(0.3f))
	{
		return END_ACTION;
	}
	else if (m_iState == AC::BeingKilledBySpiderLoop && pMeshCom->IsOverTime(0.3f))
	{
		pCamper->Set_State(AC::BeingKilledBySpiderOut);
		m_iState = AC::BeingKilledBySpiderOut;
		if(m_pHook != nullptr)
			m_pHook->SetState(CMeatHook::SpiderStabOut);
	}
	else if (m_iState == AC::BeingKilledBySpiderIN && pMeshCom->IsOverTime(0.3f))
	{
		pCamper->Set_State(AC::BeingKilledBySpiderLoop);
		m_iState = AC::BeingKilledBySpiderLoop;
		if (m_pHook != nullptr)
			m_pHook->SetState(CMeatHook::SpiderStabLoop);

		((CCamper*)m_pGameObject)->SetCurCondition(CCamper::CONDITION::HOOKDEAD);
	}
	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_BeingKilledBySpider::End_Action()
{
	CManagement* pManagement = CManagement::GetInstance();
	CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0);
	if (pCamera == nullptr)
		return;
	m_bIsPlaying = false;
	pCamera->Set_MoriCamera(false);
	pCamera->Set_IsLockCamera(true);
	camper_data.InterationObject = m_pHook->GetID();
	camper_data.InterationObjAnimation = 101;
	((CCamper*)m_pGameObject)->SetCurCondition(CCamper::CONDITION::HOOKDEAD);
	CTransform* pTransform = (CTransform*)((CCamper*)m_pGameObject)->Get_ComponentPointer(L"Com_Transform");
	if (nullptr == pTransform)
	{
		m_pHook = nullptr;
		return;
	}
	pTransform->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(300000.f, 0.f, 300000.f));
	m_pHook = nullptr;
}

void CAction_BeingKilledBySpider::Send_ServerData()
{
	if (m_pHook != nullptr)
	{
		camper_data.InterationObject = m_pHook->GetID();
		camper_data.InterationObjAnimation = m_pHook->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_BeingKilledBySpider::Free()
{
	CAction::Free();
}
