#include "stdafx.h"
#include "..\Headers\CollisionBox.h"
#include "Management.h"
#include "Defines.h"
_USING(Client)

CCollisionBox::CCollisionBox(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CCollisionBox::CCollisionBox(const CCollisionBox & rhs)
	: CGameObject(rhs)
{
}

HRESULT CCollisionBox::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CCollisionBox::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	return NOERROR;
}

_int CCollisionBox::Update_GameObject(const _float & fTimeDelta)
{
	if (GET_INSTANCE(CLoadManager)->Get_Progress() != 0.f)
		return 0;
	if (m_isDead)
		return 1;
	return _int();
}

_int CCollisionBox::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (GET_INSTANCE(CLoadManager)->Get_Progress() != 0.f)
		return 0;
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 6000.f)
		return 0;

	if (true == m_pFrustumCom->Culling_Frustum(m_pTransformCom, m_Max + 20.f))
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;
	}

	return _int();
}

void CCollisionBox::Render_GameObject()
{
	m_pColliderCom->Render_Collider();
}

void CCollisionBox::Set_Base(const _tchar * Key, const _matrix & _matWorld, const _int & OtherOption)
{
	_matrix matWorld = _matWorld;
	m_pTransformCom->Set_Matrix(matWorld);


	if (Key == nullptr)
		return;

	m_Key = Key;


	m_fRad = 150.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
	//matLocalTransform._42 = matWorld._42;

	m_iOtherOption = OtherOption;


	if (0 == m_iOtherOption)
	{
		//For.Com_Collider
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		Add_Component(L"Com_Collider", m_pColliderCom);


		_vec3 vFront = -*m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
		D3DXVec3Normalize(&vFront, &vFront);

		m_vPosArr = new _vec3[2];
		m_vPosArr[DIR::FRONT] = vFront*1.2f;
		m_vPosArr[DIR::BACK] = -vFront;

		m_pColliderCom->Set_Dist(45.f, 1.f);

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_WINDOW, this);
	}
	else
	{
		m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_AABB, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		Add_Component(L"Com_Collider", m_pColliderCom);

		CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_EXITBOX, this);
	}
		
}

_matrix CCollisionBox::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CCollisionBox::Get_Key()
{
	return m_Key.c_str();
}

_int CCollisionBox::Get_OtherOption()
{
	return 0;
}

HRESULT CCollisionBox::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

CCollisionBox * CCollisionBox::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCollisionBox*	pInstance = new CCollisionBox(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCollisionBox Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCollisionBox::Clone_GameObject()
{
	CCollisionBox*	pInstance = new CCollisionBox(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCollisionBox Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CCollisionBox::Free()
{
	if (0 == m_iOtherOption)
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_WINDOW, this);
	else
		CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_EXITBOX, this);


	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
