#include "stdafx.h"
#include "Management.h"
#include "UI_Texture.h"
#include "UI_Ending.h"
#include "LoadManager.h"
#include "Camper.h"
#include "Math_Manager.h"
#include <string>
#include "UI_Perk.h"
#include "UI_GameState.h"
#include "UI_Interaction.h"
#include "Camera_Camper.h"
_USING(Client);


CUI_Ending::CUI_Ending(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Ending::CUI_Ending(const CUI_Ending & rhs)
	: CGameObject(rhs)
	, m_State(rhs.m_State)
{
}


HRESULT CUI_Ending::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_Ending::Ready_GameObject()
{
	m_iPlayerNum = exPlayerNumber;
	m_fDeltaTime = 0.f;

	for (_int i = 0; i < 4; i++)
		m_iCondition[i] = CCamper::CONDITION::END;
	m_iCondition[4] = 0;

	m_CurButton = None;
	m_OldButton = None;
	m_fTimer = 0.f;
	return NOERROR;
}

_int CUI_Ending::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	Update_AllScore();
	Update_OtherState();
	Update_State(fTimeDelta);
	Update_Score();
	Mouse_Check();
	Button_Check();
	Update_Observe();
	Update_Name(fTimeDelta);
	
	return _int();
}

_int CUI_Ending::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_Ending::Set_Ending_Start()
{
	GET_INSTANCE_MANAGEMENT;
	
	CUI_Texture* pTexture;

	_vec2 vPos = { g_iBackCX*0.207046f, g_iBackCY*0.354502f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ending_Start_Base_01.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(2.f);
	m_UITexture.insert({ L"Start_Base_01", pTexture });

	vPos = { g_iBackCX*0.0552811f, g_iBackCY*0.473014f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ending_Start_Base_02.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(2.f);
	m_UITexture.insert({ L"Start_Base_02", pTexture });

	vPos = { g_iBackCX*0.089f, g_iBackCY*0.307642f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"게임 결과"),_vec2(14.f, 20.f),D3DXCOLOR(1.f,1.f,1.f,1.f),1);
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.3f);
	_float ScaleX= pTexture->Get_Scale().x;
	m_UITexture.insert({ L"Start_ResultText", pTexture });


	vPos = { vPos.x - (ScaleX * 0.5f), g_iBackCY * 0.357642f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);

	if (m_iPlayerNum == 5)
	{
		_int iEscapeCount = 0;
		for (size_t i = 0; i < 4; i++)
		{
			if (server_data.Campers[m_iPlayerNum - 1].iCondition == CCamper::ESCAPE)
				iEscapeCount++;
		}
		if(iEscapeCount == 0)
			pTexture->Set_Font(wstring(L"무자비한 살인마"), _vec2(32.f, 36.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
		else if(iEscapeCount == 1|| iEscapeCount == 2)
			pTexture->Set_Font(wstring(L"잔혹한 살인마"), _vec2(32.f, 36.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
		else if (iEscapeCount == 4 || iEscapeCount == 3)
			pTexture->Set_Font(wstring(L"엔티티를 만족시키지 못했습니다."), _vec2(32.f, 36.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
	}
	else
	{
		_uint iCondition = server_data.Campers[m_iPlayerNum - 1].iCondition;
		if (iCondition == CCamper::ESCAPE)
			pTexture->Set_Font(wstring(L"탈출함"), _vec2(32.f, 36.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
		else
			pTexture->Set_Font(wstring(L"희생됨"), _vec2(32.f, 36.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 2);
	}
	vPos.x += pTexture->Get_Scale().x *0.5f;
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.3f);
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	pTexture->IsColor(true);
	m_UITexture.insert({ L"Start_Result", pTexture });
	Safe_Release(pManagement);
}

void CUI_Ending::Set_Ending_Point_Background()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;
	_vec2 vPos = { g_iBackCX*0.258886f, g_iBackCY*0.341831f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Ending_Base.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ L"Point_Base", pTexture });

	vPos = { g_iBackCX*0.5f, g_iBackCY*0.929863f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Ending_Bottom_Base.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.6666f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ L"Point_Bottom_Base", pTexture });
	
	vPos = { g_iBackCX * 0.07221f, g_iBackCY * 0.142433f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"점수판"), _vec2(20.f, 25.f), D3DXCOLOR(1.f,1.f,1.f,1.f),1);
	vPos.x = vPos.x + (pTexture->Get_Scale().x * 0.5f);
	pTexture->Set_Pos(vPos);
	pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	pTexture->IsColor(true);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ L"Point_MenuText_01", pTexture });

	vPos = { g_iBackCX * 0.07221f, g_iBackCY * 0.11f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"게임"), _vec2(15.f, 18.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	vPos.x = vPos.x + (pTexture->Get_Scale().x * 0.5f);
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ L"Point_MenuText_02", pTexture });

	Safe_Release(pManagement);
}

void CUI_Ending::Set_Ending_Panel()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;

	_vec2 vPos = { g_iBackCX * 0.260038f, g_iBackCY * 0.745206f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Ending_Point_RedBase.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_FadeIn(0.5f);

	if (m_iPlayerNum == 5)
		vPos.y -= 0.1f*g_iBackCY;
	pTexture->Set_Pos(vPos);
	m_UITexture.insert({ L"Point_Slasher_Base", pTexture });

	vPos = { g_iBackCX*0.27988f, g_iBackCY*0.209f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"UI_Ending_Horizon_Thick_Line.tga"));
	pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ L"Point_Thick_Line", pTexture });

	vPos = { g_iBackCX*0.261198f, g_iBackCY*0.233333f };

	for (_int i = 0; i < 4; i++)
	{
		if (i == m_iPlayerNum - 1)
			vPos.y += g_iBackCY*0.18f;
		else
			vPos.y += g_iBackCY*0.08f;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"UI_Ending_HorizonLine.tga"));
		pTexture->Set_Scale(_vec2(0.666667f, 0.666667f));
		pTexture->Set_Pos(vPos);
		pTexture->Set_FadeIn(0.5f);
		_tchar chrTexTag[256] = L"";
		swprintf_s(chrTexTag, L"Point_Thin_Line_0%d", i + 1);
		m_UITexture.insert({ wstring(chrTexTag), pTexture });
	}

	vPos = { g_iBackCX*0.0911385f, g_iBackCY* 0.217768f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"플레이어"), _vec2(10.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ wstring(L"Point PlayerText"), pTexture });

	vPos = { vPos.x - (pTexture->Get_Scale().x*0.5f) - 0.02f * g_iBackCX , g_iBackCY*0.193333f };

	for (_int i = 0; i < 5; i++)
	{
		_int iFont = 0;
		_vec2 vScale = { 15.f,19.f };
		if (i == m_iPlayerNum - 1)
		{
			vPos.y += g_iBackCY*0.13f;
			iFont = 2;
			vScale = { 20.f,25.f };
		}
		else
			vPos.y += g_iBackCY*0.08f;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		_tchar chrTexTag[256] = L"살인마";
		if (i != 4)
			swprintf_s(chrTexTag, L"생존자0%d", i + 1);
		pTexture->Set_Font(wstring(chrTexTag), vScale, D3DXCOLOR(1.f, 1.f, 1.f, 1.f), iFont);
		vScale = pTexture->Get_Scale();
		pTexture->Set_Pos(_vec2(vPos.x + (vScale.x*0.5f), vPos.y));
		pTexture->Set_FadeIn(0.5f);
		swprintf_s(chrTexTag, L"Point_PlayerName_0%d", i + 1);
		m_UITexture.insert({ wstring(chrTexTag), pTexture });

		if (iFont == 2)
			vPos.y += 0.05f*g_iBackCY;
		else
			pTexture->Set_Alpha(0.5f);
	}
	vPos = { g_iBackCX*0.44f, g_iBackCY* 0.217008f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"점수"), _vec2(10.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ wstring(L"Point_PlayerText"), pTexture });

	vPos.y = g_iBackCY*0.193333f;
	for (_int i = 0; i < 5; i++)
	{
		_int iFont = 0;
		_bool IsMe = false;
		_vec2 vScale = { 15.f,19.f };
		if (i == m_iPlayerNum - 1)
		{
			vPos.y += g_iBackCY*0.13f;
			iFont = 2;
			vScale = { 20.f,25.f };
		}
		else
			vPos.y += g_iBackCY*0.08f;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		pTexture->Set_Font(wstring(L"0"), vScale, D3DXCOLOR(1.f, 1.f, 1.f, 1.f), iFont);
		vScale = pTexture->Get_Scale();
		pTexture->Set_Pos(_vec2(vPos));
		pTexture->Set_FadeIn(0.5f);
		_tchar chrTexTag[256] = L"";
		swprintf_s(chrTexTag, L"Point_PlayerMainScore_0%d", i + 1);
		m_UITexture.insert({ wstring(chrTexTag), pTexture });

		if (iFont == 2)
			vPos.y += 0.05f*g_iBackCY;
		else
			pTexture->Set_Alpha(0.5f);
	}

	vPos = { g_iBackCX*0.46f, g_iBackCY* 0.217768f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"상태"), _vec2(10.f, 14.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(vPos);
	pTexture->Set_FadeIn(0.5f);
	m_UITexture.insert({ wstring(L"Point_StateText"), pTexture });
	
	vPos.y = g_iBackCY*0.193333f;
	for (_int i = 0; i < 5; i++)
	{
		_bool IsMe = false;
		_float fScale = 1.f;
		if (i == m_iPlayerNum - 1)
		{
			vPos.y += g_iBackCY*0.13f;
			IsMe = true;
			_vec2 vScale = { 20.f,25.f };
			fScale = 1.2f;
		}
		else
			vPos.y += g_iBackCY*0.08f;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		pTexture->Set_TexName(wstring(L"Ending_State_Slasher.tga"));
		pTexture->Set_Scale(_vec2(0.666667f, 0.666667f)*fScale);
		pTexture->Set_Pos(vPos);
		pTexture->Set_FadeIn(0.5f);
		_tchar chrTexTag[256] = L"";
		swprintf_s(chrTexTag, L"Point_PlayerState_0%d", i + 1);
		m_UITexture.insert({ wstring(chrTexTag), pTexture });

		if (IsMe)
			vPos.y += 0.05f*g_iBackCY;
		else
			pTexture->Set_Alpha(0.5f);
	}
	 
	vPos = _vec2(g_iBackCX* 0.188652f, g_iBackCY*0.242f);
	if (m_iPlayerNum == 1)
		vPos.y += g_iBackCY*0.13f;
	else
		vPos.y += g_iBackCY*(0.13f + (0.08f *(m_iPlayerNum - 1)));

	for (_int i = 0; i < 4; i++)
	{
		_bool IsMe = false;
		
		if(i != 0)
			vPos.x += g_iBackCX * 0.06f;
		wstring Icon;
		if (m_iPlayerNum == 5)
		{
			if (i == 0)
				Icon = L"Score_Icon_Brutality.tga";
			else if (i == 1)
				Icon = L"Score_Icon_Deviousness.tga";
			else if (i == 2)
				Icon = L"Score_Icon_Hunter.tga";
			else if (i == 3)
				Icon = L"Score_Icon_Sacrifice.tga";
		}
		else
		{
			if (i == 0)
				Icon = L"Score_Icon_Objective.tga";
			else if (i == 1)
				Icon = L"Score_Icon_Survival.tga";
			else if (i == 2)
				Icon = L"Score_Icon_Altruism.tga";
			else if (i == 3)
				Icon = L"Score_Icon_Boldness.tga";
		}


		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		pTexture->Set_Font(wstring(L"0"),_vec2(15.f,19.f),D3DXCOLOR(1.f,1.f,1.f,1.f),1);
		pTexture->Set_Pos(vPos);
		_tchar chrTexTag[256] = L"";
		swprintf_s(chrTexTag, L"Point_PlayerScore_0%d", i + 1);
		m_UITexture.insert({ wstring(chrTexTag), pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		pTexture->Set_TexName(Icon);
		pTexture->Set_Scale(_vec2(0.5f, 0.5f));
		pTexture->Set_Pos(_vec2(vPos.x-0.02f* g_iBackCX, vPos.y - 0.08f*g_iBackCY));
		swprintf_s(chrTexTag, L"Point_ScoreIcon%d", i + 1);
		m_UITexture.insert({ wstring(chrTexTag), pTexture });

		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		pTexture->Set_TexName(L"UI_Ending_VerticalLine.tga");
		pTexture->Set_Scale(_vec2(0.666667f, 0.6f));
		pTexture->Set_Pos(_vec2(vPos.x+0.009f*g_iBackCX, vPos.y -0.05f*g_iBackCY));
		swprintf_s(chrTexTag, L"Point_VerticalLine%d", i + 1);
		m_UITexture.insert({ wstring(chrTexTag), pTexture });
	}

	Safe_Release(pManagement);
}

void CUI_Ending::Set_Buttons()
{
	GET_INSTANCE_MANAGEMENT;
	CUI_Texture* pTexture = nullptr;
	
	_vec2 vPos = { 0.862086f, 0.93263f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.45f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Exit_Panel", pTexture });

	vPos = { 0.870642f, 0.93322f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Icon.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.4f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Exit_Icon", pTexture });

	vPos = { 0.92f, 0.937f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"나가기"), _vec2(15.f, 18.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Exit_Text", pTexture });
	
	vPos = { 0.947719f, 0.932579f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.4f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Exit_Arrow", pTexture });

	vPos = { 0.15f, 0.93263f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Panel.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.45f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Spectate_Panel", pTexture });

	vPos = { 0.15f, 0.93322f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Icon.tga"));
	pTexture->Set_Rotation(D3DXToRadian(180));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.666667f, 0.4f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Spectate_Icon", pTexture });

	vPos = { 0.0940354f, 0.937f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_Font(wstring(L"관전"), _vec2(15.f, 18.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Spectate_Text", pTexture });

	vPos = { 0.0745465f, 0.932579f };
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Ready_Arrow.tga"));
	pTexture->Set_Pos(_vec2(vPos.x* g_iBackCX, vPos.y * g_iBackCY));
	pTexture->Set_Scale(_vec2(0.8f, 0.4f));
	pTexture->Set_Alpha(0.5f);
	m_UITexture.insert({ L"Spectate_Arrow", pTexture });

	Safe_Release(pManagement);
}

CUI_Ending * CUI_Ending::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Ending*	pInstance = new CUI_Ending(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Ending Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Ending::Clone_GameObject()
{
	CUI_Ending*	pInstance = new CUI_Ending(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Ending Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


CUI_Texture * CUI_Ending::Find_UITexture(wstring UIName)
{
	auto& iter = m_UITexture.find(UIName);
	if (iter == m_UITexture.end())
		return nullptr;
	return iter->second;
}

void CUI_Ending::Delete_UI_Texture(wstring UIName)
{
	auto& iter = m_UITexture.find(wstring(UIName));
	m_UITexture.erase(iter);
}

void CUI_Ending::Set_FadeIn()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pTexture;

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	pTexture->Set_TexName(wstring(L"Gray.png"));
	pTexture->Set_Scale(_vec2((_float)g_iBackCX, (_float)g_iBackCY),false);
	pTexture->Set_Pos(_vec2(g_iBackCX*0.5f, g_iBackCY*0.5f));
	pTexture->Set_FadeIn(3.f);
	m_UITexture.insert({ L"Gray", pTexture });
	
	Safe_Release(pManagement);
}

void CUI_Ending::Delete_All()
{
	for (auto& iter : m_UITexture)
	{
		iter.second->SetDead();
	}
	m_UITexture.clear();
}

void CUI_Ending::Delete_NoneGray()
{
	for (auto& iter : m_UITexture)
	{
		if (iter.first != wstring(L"Gray"))
		{
			iter.second->SetDead();
			m_UITexture.erase(iter.first);
		}
	}
}

_bool CUI_Ending::IsWin()
{
	if (exPlayerNumber != 5)
	{
		if (m_iCondition[exPlayerNumber - 1] == CCamper::CONDITION::ESCAPE)
			return true;
		else
			return false;
	}
	else
	{
		_int Num = 0;
		for (int i = 0; i < 4; i++)
		{
			if (m_iCondition[i] == CCamper::CONDITION::ESCAPE)
				Num++;
			if (Num >= 2)
				return true;
		}
		return false;
	}
}

void CUI_Ending::Update_Score()
{
	if (m_State != Ending_Panel)
		return;

	if (m_fDeltaTime >= 6.f)
	{
		_float Progress = (m_fDeltaTime - 6.f) / 1.45f;
		if (Progress > 1.f)
			Progress = 1.f;

		_int CurUIScore = (_int)Math_Manager::ProgressToPersent(0.f, (_float)m_iScore[3], Progress);
		CUI_Texture* pTexture = Find_UITexture(wstring(L"Point_PlayerScore_04"));

		_float fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(15.f, 19.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));

		CurUIScore += (m_iScore[0] + m_iScore[1] + m_iScore[2]);
		wstring MainScore = L"Point_PlayerMainScore_0" + to_wstring(m_iPlayerNum);
		pTexture = Find_UITexture(wstring(MainScore));
		fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(20.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));
	}
	else if (m_fDeltaTime >= 4.5f)
	{
		_float Progress = (m_fDeltaTime - 4.5f) / 1.45f;
		if (Progress > 1.f)
			Progress = 1.f;

		_int CurUIScore = (_int)Math_Manager::ProgressToPersent(0.f, (_float)m_iScore[2], Progress);
		CUI_Texture* pTexture = Find_UITexture(wstring(L"Point_PlayerScore_03"));

		_float fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(15.f, 19.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));

		CurUIScore += (m_iScore[0] + m_iScore[1]);
		wstring MainScore = L"Point_PlayerMainScore_0" + to_wstring(m_iPlayerNum);
		pTexture = Find_UITexture(wstring(MainScore));
		fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(20.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));
	}
	else if (m_fDeltaTime >= 3.f)
	{
		_float Progress = (m_fDeltaTime - 3.f) / 1.45f;
		if (Progress > 1.f)
			Progress = 1.f;

		_int CurUIScore = (_int)Math_Manager::ProgressToPersent(0.f, (_float)m_iScore[1], Progress);
		CUI_Texture* pTexture = Find_UITexture(wstring(L"Point_PlayerScore_02"));
		
		_float fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(15.f, 19.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));

		CurUIScore += m_iScore[0];
		wstring MainScore = L"Point_PlayerMainScore_0" + to_wstring(m_iPlayerNum);
		pTexture = Find_UITexture(wstring(MainScore));
		fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(20.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));
	}
	else if (m_fDeltaTime >= 1.5f)
	{
		_float Progress = (m_fDeltaTime - 1.5f) / 1.45f;
		if (Progress > 1.f)
			Progress = 1.f;

		_int CurUIScore = (_int)Math_Manager::ProgressToPersent(0.f, (_float)m_iScore[0], Progress);
		CUI_Texture* pTexture = Find_UITexture(wstring(L"Point_PlayerScore_01"));
		
		_float fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(15.f, 19.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX- pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));
		
		wstring MainScore = L"Point_PlayerMainScore_0" + to_wstring(m_iPlayerNum);
		pTexture = Find_UITexture(wstring(MainScore));
		fScaleX = pTexture->Get_Scale().x;
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(20.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX- pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));
	}
}

void CUI_Ending::Update_State(const _float & fTimeDelta)
{
	m_fDeltaTime += fTimeDelta;

	if (m_State == Ending && m_fDeltaTime >= 4.f)
	{
		GET_INSTANCE_MANAGEMENT;
		((CUI_Interaction*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_UI_Interaction", 0))->Delete_AllTexture();
		((CUI_Perk*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_UI_Perk", 0))->Is_AllTextureRender(false);
		((CUI_GameState*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_UI_State", 0))->Set_Delete();
		pManagement->Get_GameObject(SCENE_STAGE, L"Layer_UI_Score", 0)->SetDead();
		pManagement->Get_GameObject(SCENE_STAGE, L"Layer_UI_Interaction", 0)->SetDead();
		GET_INSTANCE(CUIManager)->Set_UI(nullptr, nullptr);
		Safe_Release(pManagement);
		Set_FadeIn();
		m_State = Ending_Fade;
		m_fDeltaTime = 0.f;
	}
	else if (m_State == Ending_Fade && m_fDeltaTime >= 4.f)
	{
		if(IsWin())
			GET_INSTANCE(CSoundManager)->PlaySound(string("BGM"),string("Situation/mu_victory_01.ogg"),_vec3(),0,1.f);
		else
			GET_INSTANCE(CSoundManager)->PlaySound(string("BGM"), string("Situation/mu_defeated_01.ogg"), _vec3(), 0, 1.f);
		Set_Ending_Start();
		m_State = Ending_Start;
		m_fDeltaTime = 0.f;
	}
	else if (m_State == Ending_Start && m_fDeltaTime >= 5.f)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Logo/theentity_long_02.ogg"), _vec3(), 0, 1.f);
		Delete_NoneGray();
		Set_Ending_Point_Background();
		m_State = Ending_Background;
		m_fDeltaTime = 0.f;
	}
	else if (m_State == Ending_Background && m_fDeltaTime >= 2.f)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Logo/theentity_long_01.ogg"), _vec3(), 0, 1.f);
		
		if(exPlayerNumber == 5)
			GET_INSTANCE(CSoundManager)->PlaySound(string("BGM"), string("Situation/mu_killer_tallyscreen_01.ogg"), _vec3(), 0, 1.f);
		else
			GET_INSTANCE(CSoundManager)->PlaySound(string("BGM"),string("Situation/mu_survivor_tallyscreen_01.ogg"), _vec3(), 0, 1.f);

		Set_Ending_Panel();
		m_State = Ending_Panel;
		m_fDeltaTime = 0.f;
	}
	else if (m_State == Ending_Panel && m_fDeltaTime >= 7.5f)
	{
		GET_INSTANCE(CLoadManager)->Set_MouseVisible(true);
		Set_Buttons();
		m_State = Ending_Select;
		m_fDeltaTime = 0.f;
	}
}

void CUI_Ending::Update_OtherState()
{
	if (m_State != Ending_Panel &&  m_State != Ending_Select)
		return;

	for (int i = 0; i < 4; i++)
	{
		wstring TexTag;
		if (m_iCondition[i] != server_data.Campers[i].iCondition)
		{
			if (server_data.Campers[i].iCondition == CCamper::HEALTHY)
				TexTag = L"Ending_State_Healthy.tga";
			else if (server_data.Campers[i].iCondition == CCamper::HOOKDEAD ||
				server_data.Campers[i].iCondition == CCamper::BLOODDEAD)
				TexTag = L"Ending_State_Dead.tga";
			else if (server_data.Campers[i].iCondition == CCamper::INJURED)
				TexTag = L"Ending_State_Injured.tga";
			else if (server_data.Campers[i].iCondition == CCamper::ESCAPE)
				TexTag = L"Ending_State_Escape.tga";
			else
				continue;
			
			m_iCondition[i] = server_data.Campers[i].iCondition;

			wstring PlayerTag = L"Point_PlayerState_0" + to_wstring(i+1);
			CUI_Texture* pTexture = Find_UITexture(PlayerTag);
			pTexture->Set_TexName(TexTag);

			if (m_iCondition[i] != CCamper::ESCAPE && 
				m_iCondition[i] != CCamper::HOOKDEAD &&
				m_iCondition[i] != CCamper::BLOODDEAD)
				continue;

			if (i == m_iPlayerNum - 1)
				continue;

			PlayerTag = L"Point_PlayerMainScore_0" + to_wstring(i + 1);
			pTexture = Find_UITexture(PlayerTag);
			_float fScaleX = pTexture->Get_Scale().x;
			_int CurUIScore = server_data.Campers[i].Score[0] + server_data.Campers[i].Score[1] + server_data.Campers[i].Score[2] + server_data.Campers[i].Score[3];
			pTexture->Set_Font(to_wstring(CurUIScore), _vec2(20.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
			fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
			pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));
		}
	}
	if (m_iCondition[4] != 0)
		return;
	_int GameEndCount = 0;
	_int MaxPlayer = 0;
	for (_int i = 0; i < 4; i++)
	{
		if (m_iCondition[i] == CCamper::ESCAPE ||
			m_iCondition[i] == CCamper::HOOKDEAD ||
			m_iCondition[i] == CCamper::BLOODDEAD)
			GameEndCount++;
		if (server_data.Campers[i].bConnect == true)
			MaxPlayer++;
	}
	if (GameEndCount == MaxPlayer)
	{
		wstring PlayerTag = L"Point_PlayerMainScore_05";
		CUI_Texture* pTexture = Find_UITexture(PlayerTag);
		_float fScaleX = pTexture->Get_Scale().x;
		_int CurUIScore = server_data.Slasher.Score[0] + server_data.Slasher.Score[1] + server_data.Slasher.Score[2] + server_data.Slasher.Score[3];
		pTexture->Set_Font(to_wstring(CurUIScore), _vec2(20.f, 25.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		fScaleX = (fScaleX - pTexture->Get_Scale().x) * 0.5f;
		pTexture->Set_PosPlus(_vec2(fScaleX, 0.f));
	}
}

void CUI_Ending::Mouse_Check()
{
	CUI_Texture* pTexture = nullptr;
	pTexture = Find_UITexture(wstring(L"Spectate_Panel"));
	if (pTexture == nullptr)
		return;
	if (pTexture->IsTouch())
	{
		if (m_CurButton == Spectate)
			return;
		m_CurButton = Spectate;
		return;
	}
	pTexture = Find_UITexture(wstring(L"Exit_Panel"));
	if (pTexture == nullptr)
		return;
	if (pTexture->IsTouch())
	{
		if (m_CurButton == Exit)
			return;
		m_CurButton = Exit;
		return;
	}
	m_CurButton = None;
}

void CUI_Ending::Button_Check()
{
	if (m_State != Ending_Select)
		return;

	if (m_CurButton != m_OldButton)
	{
		if (m_OldButton == Spectate)
			Button_Spectate(UnTouch);
		else if (m_OldButton == Exit)
			Button_Exit(UnTouch);

		if (m_CurButton == Spectate)
			Button_Spectate(Touch);
		else if (m_CurButton == Exit)
			Button_Exit(Touch);

		m_OldButton = m_CurButton;
	}

	if (KEYMGR->MousePressing(0))
	{
		if (m_CurButton == Spectate)
			Button_Spectate(Click);
		else if (m_CurButton == Exit)
			Button_Exit(Click);
	}
}

void CUI_Ending::Update_Observe()
{
	if (m_State != Observing)
		return;
	_int ObserveNum = 0;
	if (KEYMGR->KeyDown(DIK_RIGHT))
		ObserveNum = Find_ObserveNum(true);
	else if (KEYMGR->KeyDown(DIK_LEFT))
		ObserveNum = Find_ObserveNum(false);
	else if(!server_data.Campers[exiObservPlayer - 1].bLive)
		ObserveNum = Find_ObserveNum(false);

	if (ObserveNum == exiObservPlayer)
		return;
	else if (ObserveNum == 0)
		Game_End();
	else
	{
		exiObservPlayer = ObserveNum;
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"Spec_Num");
		pTexture->Set_Font(wstring(L"생존자") + to_wstring(ObserveNum), _vec2(20.f, 26.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
	}
}

void CUI_Ending::Update_AllScore()
{
	if (m_iPlayerNum == 5)
		memcpy_s(m_iScore, sizeof(_int) * 4, slasher_data.Score, sizeof(_int) * 4);
	else
		memcpy_s(m_iScore, sizeof(_int) * 4, camper_data.Score, sizeof(_int) * 4);
}

void CUI_Ending::Update_Name(const _float & fTImeDelta)
{
	//if (m_State != Ending_Select)
	//	return;
	//m_fTimer += fTImeDelta;
	//if (m_fTimer >= 5.f)
	//{
	//	if (m_fTimerCheck == 0)
	//	{
	//		m_fTimerCheck++;
	//		GET_INSTANCE_MANAGEMENT;
	//		CUI_Texture* pTexture = nullptr;
	//		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	//		Safe_Release(pManagement);
	//		pTexture->Set_Font(wstring(L"Map, UI, 사운드  이 종 민"), _vec2(30.f, 37.f), 2);
	//		pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	//		pTexture->Set_FadeIn(5.f);
	//		pTexture->IsColor(true);
	//		pTexture->Set_Pos(_vec2(g_iBackCX* 0.75f, g_iBackCY * 0.20f));
	//	}
	//	else if (m_fTimerCheck == 1) 
	//	{
	//		m_fTimer = 0;
	//		m_fTimerCheck++;
	//		GET_INSTANCE_MANAGEMENT;
	//		CUI_Texture* pTexture = nullptr;
	//		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	//		Safe_Release(pManagement);
	//		pTexture->Set_Font(wstring(L"Map, UI, 사운드  이 종 민"), _vec2(30.f, 37.f), 1);
	//		pTexture->Set_Color(D3DXCOLOR(0.f, 1.f, 1.f, 1.f));
	//		pTexture->IsColor(true);
	//		pTexture->Set_Pos(_vec2(g_iBackCX* 0.75f, g_iBackCY * 0.40f));
	//	}
	//	else if (m_fTimerCheck == 2)
	//	{
	//		m_fTimer = 0;
	//		m_fTimerCheck++;
	//		GET_INSTANCE_MANAGEMENT;
	//		CUI_Texture* pTexture = nullptr;
	//		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	//		Safe_Release(pManagement);
	//		pTexture->Set_Font(wstring(L"Map, UI, 사운드  이 종 민"), _vec2(30.f, 37.f), 1);
	//		pTexture->Set_Color(D3DXCOLOR(1.f, 0.f, 1.f, 1.f));
	//		pTexture->IsColor(true);
	//		pTexture->Set_Pos(_vec2(g_iBackCX* 0.75f, g_iBackCY * 0.60f));
	//	}
	//	else if (m_fTimerCheck == 3)
	//	{
	//		m_fTimer = 0;
	//		m_fTimerCheck++;
	//		GET_INSTANCE_MANAGEMENT;
	//		CUI_Texture* pTexture = nullptr;
	//		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
	//		Safe_Release(pManagement);
	//		pTexture->Set_Font(wstring(L"Map, UI, 사운드  이 종 민"), _vec2(30.f, 37.f), 1);
	//		pTexture->Set_Color(D3DXCOLOR(1.f, 1.f, 0.f, 1.f));
	//		pTexture->IsColor(true);
	//		pTexture->Set_Pos(_vec2(g_iBackCX* 0.75f, g_iBackCY * 0.80f));
	//	}
	//}
}

void CUI_Ending::Button_Spectate(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"Spectate_Arrow");
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(L"Spectate_Text");
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(L"Spectate_Panel");
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(L"Spectate_Icon");
		pTexture->Set_Alpha(1.f);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"Spectate_Arrow");
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(L"Spectate_Text");
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(L"Spectate_Panel");
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(L"Spectate_Icon");
		pTexture->Set_Alpha(0.5f);
	}
	else if (State == Click)
	{
		if (exPlayerNumber == 5)
			return;
		_int PlayerNum = Find_ObserveNum(true);
		if (PlayerNum == 0)
			return;
		bObserver = true;
		exiObservPlayer = PlayerNum;
		Delete_All();
		GET_INSTANCE(CSoundManager)->Set_PlayerNum(PlayerNum);
		GET_INSTANCE_MANAGEMENT;
		((CUI_Perk*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_UI_Perk", 0))->Is_AllTextureRender(true);
		((CUI_GameState*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_UI_State", 0))->Ready_GameObject();

		CUI_Texture* pTexture = nullptr;
		pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Ending", (CGameObject**)&pTexture);
		pTexture->Set_Font(wstring(L"생존자") + to_wstring(PlayerNum), _vec2(20.f, 26.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		pTexture->Set_Pos(_vec2(0.5f * g_iBackCX, 0.95f * g_iBackCY));
		m_UITexture.insert({ L"Spec_Num",pTexture });
		CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
		if (nullptr == pCamera)
			return;

		pCamera->Set_IsLockCamera(false);

		Safe_Release(pManagement);
		m_State = Observing;
	}
}

void CUI_Ending::Button_Exit(ButtonState State)
{
	if (State == Touch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"Exit_Arrow");
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(L"Exit_Text");
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(L"Exit_Panel");
		pTexture->Set_Alpha(1.f);
		pTexture = Find_UITexture(L"Exit_Icon");
		pTexture->Set_Alpha(1.f);
	}
	else if (State == UnTouch)
	{
		CUI_Texture* pTexture = nullptr;
		pTexture = Find_UITexture(L"Exit_Arrow");
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(L"Exit_Text");
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(L"Exit_Panel");
		pTexture->Set_Alpha(0.5f);
		pTexture = Find_UITexture(L"Exit_Icon");
		pTexture->Set_Alpha(0.5f);
	}
	else if (State == Click)
	{

	}
}

_int CUI_Ending::Find_ObserveNum(_bool IsNext)
{
	_int ObservablePlayer = exiObservPlayer;
	
	for (int i = 0; i < 5; i++)
	{
		if (i == 4)
			break;
		if (IsNext)
			ObservablePlayer++;
		else
			ObservablePlayer--;

		if (ObservablePlayer < 1)
			ObservablePlayer = 4;
		else if(ObservablePlayer > 4)
			ObservablePlayer = 1;

		if (server_data.Campers[ObservablePlayer - 1].bConnect && server_data.Campers[ObservablePlayer - 1].bLive)
			return ObservablePlayer;
	}
	return 0;
}

void CUI_Ending::Game_End()
{
}

void CUI_Ending::Free()
{
	CGameObject::Free();
}
