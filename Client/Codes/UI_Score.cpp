#include "stdafx.h"
#include "Management.h"
#include "UI_Texture.h"
#include "UI_Score.h"
#include <string>
#include "Math_Manager.h"
#include "Camper.h"
#include "Slasher.h"
_USING(Client);


CUI_Score::CUI_Score(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Score::CUI_Score(const CUI_Score & rhs)
	: CGameObject(rhs)
{
}


HRESULT CUI_Score::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_Score::Ready_GameObject()
{
	m_PlayerNum = exPlayerNumber;
	m_fWaitingTime = 0.f;
	m_fBasementTime = 0.f;
	m_fBoldTime = 0.f;
	m_ProgressTime = 1.f;
	return NOERROR;
 }

_int CUI_Score::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	Camper_Position_Check(fTimeDelta);
	Score_Check();
	Update_Score(fTimeDelta);

	return _int();
}

_int CUI_Score::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_Score::Camper_Position_Check(const _float & fTimeDelta)
{
	if (!server_data.Slasher.bConnect || !server_data.Campers[exPlayerNumber - 1].bConnect)
		return;
	if (m_pPlayer == nullptr)
	{
		GET_INSTANCE_MANAGEMENT;
		if (m_PlayerNum == 5)
			m_pPlayer = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Slasher", SLASHER);
		else
			m_pPlayer = pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Camper", CAMPER + (m_PlayerNum - 1));
		Safe_Release(pManagement);
	}
	if (m_PlayerNum == 5)
		return;

	_bool IsBasement = false;
	_bool IsBold = false;

	_int CamperCondition = server_data.Campers[m_PlayerNum - 1].iCondition;
	if (server_data.Campers[m_PlayerNum - 1].vPos.y <= -100.f &&(CamperCondition == CCamper::INJURED || CamperCondition == CCamper::HEALTHY || CamperCondition == CCamper::SPECIAL || CamperCondition == CCamper::OBSTACLE))
		IsBasement = true;

	if (!IsBasement && m_fBasementTime != 0.f)
	{
		Add_Score(Boldness, wstring(L"지하실 시간"), _int(m_fBasementTime * 5.f));
		m_fBasementTime = 0.f;
	}
	else if (IsBasement)
		m_fBasementTime += fTimeDelta;

	_vec3 PlayerToSlaher = server_data.Slasher.vPos - server_data.Campers[m_PlayerNum - 1].vPos;
	_float Length = D3DXVec3Length(&PlayerToSlaher);
	
	if (Length <= 500.f &&(CamperCondition == CCamper::INJURED || CamperCondition == CCamper::HEALTHY || CamperCondition == CCamper::SPECIAL || CamperCondition == CCamper::OBSTACLE))
		IsBold = true;

	if (!IsBold && m_fBoldTime != 0.f)
	{
		Add_Score(Boldness, wstring(L"용감함"), _int(m_fBoldTime * 6.f));
		m_fBoldTime = 0.f;
	}
	else if (IsBold)
		m_fBoldTime += fTimeDelta;

}

void CUI_Score::Add_Score(const _uint & Type, const wstring& Text, const _int & Point)
{
	if ((m_PlayerNum == 5 && Type < Brutality) || (m_PlayerNum != 5 && Boldness < Type) || Point == 0)
		return;
	SCOREINFO Score;
	GET_INSTANCE_MANAGEMENT;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Point", (CGameObject**)&Score.pProgress_Base);
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Point", (CGameObject**)&Score.pProgress_Bar);
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Point", (CGameObject**)&Score.pTypeIcon);
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Point", (CGameObject**)&Score.pText);
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_Point", (CGameObject**)&Score.pScore);
	Safe_Release(pManagement);
	

	Score.pProgress_Base->Set_TexName(wstring(L"Score_Progress_Base.tga"),2);
	
	Score.pProgress_Bar->Set_TexName(wstring(L"Score_Progress_Bar.tga"),2);
	if (Type == Objective)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Objective.tga"));
	else if(Type == Survival)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Survival.tga"));
	else if (Type == Altruism)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Altruism.tga"));
	else if (Type == Altruism)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Objective.tga"));
	else if (Type == Boldness)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Boldness.tga"));
	else if (Type == Brutality)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Brutality.tga"));
	else if (Type == Deviousness)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Deviousness.tga"));
	else if (Type == Hunter)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Hunter.tga"));
	else if (Type == Sacrifice)
		Score.pTypeIcon->Set_TexName(wstring(L"Score_Icon_Sacrifice.tga"));

	Score.pTypeIcon->Set_Scale(_vec2(0.666667f*0.5f, 0.666667f*0.5f));

	_int CurScore = Point;
	if (m_PlayerNum == 5)
		CurScore += server_data.Slasher.Score[Type-4];
	else
		CurScore += server_data.Campers[m_PlayerNum-1].Score[Type];
	_bool isMax = false;
	if (CurScore >= 8000)
		CurScore = 8000;
	if (m_PlayerNum == 5)
		slasher_data.Score[Type-4] = CurScore;
	else
		camper_data.Score[Type] = CurScore;
	Score.pProgress_Bar->Set_Angle(_vec2(0.000001f, D3DXToRadian(CurScore/8000.f*360.f)));

	wstring wstrText = L"";
	if (isMax)
		wstrText = L"최대";
	else
	{
		wstrText = L"+";
		wstrText += to_wstring(Point);
	}
	
	Score.pScore->Set_Font(wstrText, _vec2(12.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f),1);
	Score.pScore->Set_Color(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
	Score.pScore->IsColor(true);

	Score.pText->Set_Font(wstring(Text), _vec2(14.f, 14.f),D3DXCOLOR(1.f,1.f,1.f,1.f),1);

	Score.pProgress_Bar->IsRendering(false);
	Score.pProgress_Base->IsRendering(false);
	Score.pScore->IsRendering(false);
	Score.pText->IsRendering(false);
	Score.pTypeIcon->IsRendering(false);
	
	m_WaitingScore.push(Score);
}

void CUI_Score::Score_Check()
{
	if (m_ProgressTime < 1.f)
		return;
	if (m_WaitingScore.size() >= 1)
	{
		SCOREINFO Score = m_WaitingScore.front();

		_vec2 vPos = { 0.040736f * g_iBackCX, 0.460568f * g_iBackCY };

		Score.pProgress_Base->Set_Pos(vPos);
		Score.pProgress_Bar->Set_Pos(vPos);
		Score.pTypeIcon->Set_Pos(vPos);

		vPos = { 0.070406f*g_iBackCX + Score.pScore->Get_Scale().x * 0.5f,  0.470421f*g_iBackCY };
		Score.pScore->Set_Pos(vPos);

		vPos = { 0.070406f*g_iBackCX + Score.pText->Get_Scale().x*0.5f,  0.4485263f*g_iBackCY };
		Score.pText->Set_Pos(vPos);

		Score.pProgress_Bar->IsRendering(true);
		Score.pProgress_Base->IsRendering(true);
		Score.pScore->IsRendering(true);
		Score.pText->IsRendering(true);
		Score.pTypeIcon->IsRendering(true);

		m_vecScore.push_back(Score);
		m_WaitingScore.pop();
		m_ProgressTime = 0.f; 
		m_fWaitingTime = 0.f;
	}
}

void CUI_Score::FadeOut_Score()
{
	_int iSize = m_vecScore.size();
	_float fProgress = m_fWaitingTime - 4.f;
	if (fProgress > 1.f)
		fProgress = 1.f;
	if (iSize >= 1)
	{
		_float fAlpha = Math_Manager::ProgressToPersent(1.f, 0.f, fProgress);
		m_vecScore[iSize - 1].pProgress_Bar->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pProgress_Base->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pScore->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pText->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pTypeIcon->Set_Alpha(fAlpha);
		if(fProgress == 1.f)
		{
			m_vecScore[iSize - 1].pProgress_Bar->SetDead();
			m_vecScore[iSize - 1].pProgress_Base->SetDead();
			m_vecScore[iSize - 1].pScore->SetDead();
			m_vecScore[iSize - 1].pText->SetDead();
			m_vecScore[iSize - 1].pTypeIcon->SetDead();
		}
	}
	if (iSize >= 2)
	{
		_float fAlpha = Math_Manager::ProgressToPersent(0.5f, 0.f, fProgress);
		m_vecScore[iSize - 2].pProgress_Bar->Set_Alpha(fAlpha);
		m_vecScore[iSize - 2].pProgress_Base->Set_Alpha(fAlpha);
		m_vecScore[iSize - 2].pScore->Set_Alpha(fAlpha);
		m_vecScore[iSize - 2].pText->Set_Alpha(fAlpha);
		m_vecScore[iSize - 2].pTypeIcon->Set_Alpha(fAlpha);
		if (fProgress == 1.f)
		{
			m_vecScore[iSize - 2].pProgress_Bar->SetDead();
			m_vecScore[iSize - 2].pProgress_Base->SetDead();
			m_vecScore[iSize - 2].pScore->SetDead();
			m_vecScore[iSize - 2].pText->SetDead();
			m_vecScore[iSize - 2].pTypeIcon->SetDead();
		}
	}
	if (iSize >= 3)
	{
		_float fAlpha = Math_Manager::ProgressToPersent(0.5f, 0.f, fProgress);
		m_vecScore[iSize - 3].pProgress_Bar->Set_Alpha(fAlpha);
		m_vecScore[iSize - 3].pProgress_Base->Set_Alpha(fAlpha);
		m_vecScore[iSize - 3].pScore->Set_Alpha(fAlpha);
		m_vecScore[iSize - 3].pText->Set_Alpha(fAlpha);
		m_vecScore[iSize - 3].pTypeIcon->Set_Alpha(fAlpha);
		if (fProgress == 1.f)
		{
			m_vecScore[iSize - 3].pProgress_Bar->SetDead();
			m_vecScore[iSize - 3].pProgress_Base->SetDead();
			m_vecScore[iSize - 3].pScore->SetDead();
			m_vecScore[iSize - 3].pText->SetDead();
			m_vecScore[iSize - 3].pTypeIcon->SetDead();
		}
	}
	if (iSize >= 4)
	{
		_float fAlpha = Math_Manager::ProgressToPersent(0.5f, 0.f, fProgress);
		m_vecScore[iSize - 4].pProgress_Bar->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pProgress_Base->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pScore->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pText->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pTypeIcon->Set_Alpha(fAlpha);		
		if (fProgress == 1.f)
		{
			m_vecScore[iSize - 4].pProgress_Bar->SetDead();
			m_vecScore[iSize - 4].pProgress_Base->SetDead();
			m_vecScore[iSize - 4].pScore->SetDead();
			m_vecScore[iSize - 4].pText->SetDead();
			m_vecScore[iSize - 4].pTypeIcon->SetDead();
		}
	}
	if (fProgress == 1.f)
		m_vecScore.clear();

}

void CUI_Score::Update_Score(const _float& fTimeDelta)
{
	_int iSize = m_vecScore.size();
	if (iSize == 0)
		return;
	if (m_ProgressTime >= 1.f)
	{
		m_fWaitingTime += fTimeDelta;
		if (m_fWaitingTime >= 4.f)
			FadeOut_Score();
		return;
	}
	else
		m_ProgressTime += fTimeDelta * 1.5f;
	if (m_ProgressTime > 1.f)
		m_ProgressTime = 1.f;
	m_fWaitingTime = 0.f;
	
	_vec2 vPosPlus = { 0.f, -1.f *0.08f * g_iBackCY * fTimeDelta };
	if (iSize >= 1)
	{
		_vec2 vCurPos = m_vecScore[iSize - 1].pProgress_Bar->Get_Pos();
		_vec2 vPos = Math_Manager::ProgressToPersent(_vec2(0.040736f,0.460568f), _vec2(0.040736f, 0.53895f), m_ProgressTime);
		_float fAlpha = Math_Manager::ProgressToPersent(0.f, 1.f, m_ProgressTime);
		vPos = _vec2(vPos.x*g_iBackCX, vPos.y*g_iBackCY);
		m_vecScore[iSize - 1].pProgress_Bar->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 1].pProgress_Bar->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pProgress_Base->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 1].pProgress_Base->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pScore->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 1].pScore->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pText->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 1].pText->Set_Alpha(fAlpha);
		m_vecScore[iSize - 1].pTypeIcon->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 1].pTypeIcon->Set_Alpha(fAlpha);
	}
	if (iSize >= 2)
	{
		_vec2 vCurPos = m_vecScore[iSize - 2].pProgress_Bar->Get_Pos();
		_vec2 vPos = Math_Manager::ProgressToPersent(_vec2(0.040736f, 0.53895f), _vec2(0.040736f, 0.61052f), m_ProgressTime);
		vPos = _vec2(vPos.x*g_iBackCX, vPos.y*g_iBackCY);
		_vec2 vScale = Math_Manager::ProgressToPersent(_vec2(0.666667f, 0.666667f), _vec2(0.5f,0.5f), m_ProgressTime);
		_vec2 vFontScale = Math_Manager::ProgressToPersent(_vec2(12.f, 12.f), _vec2(10.f, 10.f), m_ProgressTime);
		_float fFontStart = Math_Manager::ProgressToPersent(0.070406f, 0.058804f, m_ProgressTime);
		_float fAlpha = Math_Manager::ProgressToPersent(1.f, 0.3f, m_ProgressTime);
		m_vecScore[iSize - 2].pProgress_Bar->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 2].pProgress_Bar->Set_Scale(vScale);
		m_vecScore[iSize - 2].pProgress_Bar->Set_Alpha(fAlpha);

		m_vecScore[iSize - 2].pProgress_Base->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 2].pProgress_Base->Set_Scale(vScale);
		m_vecScore[iSize - 2].pProgress_Base->Set_Alpha(fAlpha);

		m_vecScore[iSize - 2].pScore->Set_FontScale(vFontScale);
		_int Length = m_vecScore[iSize - 2].pScore->Get_TexName().length();
		_vec2 TexPos = m_vecScore[iSize - 2].pScore->Get_Pos();
		m_vecScore[iSize - 2].pScore->Set_PosPlus(_vec2((fFontStart*g_iBackCX) + Length*vFontScale.x* 0.5f - TexPos.x,(vCurPos.y - vPos.y)));
		m_vecScore[iSize - 2].pScore->Set_Alpha(fAlpha);

		m_vecScore[iSize - 2].pText->Set_FontScale(vFontScale /12.f * 14.f);
		Length = m_vecScore[iSize - 2].pText->Get_TexName().length();
		TexPos = m_vecScore[iSize - 2].pText->Get_Pos();
		m_vecScore[iSize - 2].pText->Set_PosPlus(_vec2((fFontStart*g_iBackCX) + (vFontScale.x / 12.f * 7.f*Length) -TexPos.x, vCurPos.y - vPos.y));
		m_vecScore[iSize - 2].pText->Set_Alpha(fAlpha);

		m_vecScore[iSize - 2].pTypeIcon->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 2].pTypeIcon->Set_Scale(vScale*0.5f);
		m_vecScore[iSize - 2].pTypeIcon->Set_Alpha(fAlpha);
	}
	if (iSize >= 3)
	{
		_vec2 vCurPos = m_vecScore[iSize - 3].pProgress_Bar->Get_Pos();
		_vec2 vPos = Math_Manager::ProgressToPersent(_vec2(0.040736f, 0.61052f), _vec2(0.040736f, 0.678947f), m_ProgressTime);
		vPos = _vec2(vPos.x*g_iBackCX, vPos.y*g_iBackCY);
		m_vecScore[iSize - 3].pProgress_Bar->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 3].pProgress_Base->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 3].pScore->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 3].pText->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 3].pTypeIcon->Set_PosPlus(vCurPos - vPos);
	}
	if (iSize >= 4)
	{
		_vec2 vCurPos = m_vecScore[iSize - 4].pProgress_Bar->Get_Pos();
		_vec2 vPos = Math_Manager::ProgressToPersent(_vec2(0.040736f, 0.678947f), _vec2(0.040736f, 0.747374f), m_ProgressTime);
		vPos = _vec2(vPos.x*g_iBackCX, vPos.y*g_iBackCY);
		_float fAlpha= Math_Manager::ProgressToPersent(0.5f,0.f, m_ProgressTime);
		m_vecScore[iSize - 4].pProgress_Bar->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 4].pProgress_Bar->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pProgress_Base->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 4].pProgress_Base->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pScore->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 4].pScore->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pText->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 4].pText->Set_Alpha(fAlpha);
		m_vecScore[iSize - 4].pTypeIcon->Set_PosPlus(vCurPos - vPos);
		m_vecScore[iSize - 4].pTypeIcon->Set_Alpha(fAlpha);
		if (m_ProgressTime == 1.f)
		{
			m_vecScore[iSize - 4].pTypeIcon->SetDead();
			m_vecScore[iSize - 4].pText->SetDead();
			m_vecScore[iSize - 4].pScore->SetDead();
			m_vecScore[iSize - 4].pProgress_Base->SetDead();
			m_vecScore[iSize - 4].pProgress_Bar->SetDead();
			m_vecScore.erase(m_vecScore.begin());
		}
	}
}


CUI_Score * CUI_Score::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Score*	pInstance = new CUI_Score(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Score Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Score::Clone_GameObject()
{
	CUI_Score*	pInstance = new CUI_Score(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Score Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_Score::Free()
{
	CGameObject::Free();
}
