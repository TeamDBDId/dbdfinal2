#include "stdafx.h"
#include "..\Headers\Action_StunDrop.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client)

CAction_StunDrop::CAction_StunDrop()
{
}

HRESULT CAction_StunDrop::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	m_fIndex = 0.f;

	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->IsLockKey(true);
		pSlasher->Set_State(AS::Stun_Drop_In);
		m_iState = AS::Stun_Drop_In;
		m_fFaintTime = 0.f;
	}
	else
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_WiggleEnd); //내릴때
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None); //내릴때  
		pCamper->IsLockKey(true);
		pCamper->SetCarry(false);
		pCamper->Set_ProgressTime(0.f);
		server_data.Slasher.iCharacter == 0 ? pCamper->Set_State(AC::TT_Stun_Drop) : pCamper->Set_State(AC::WI_Stun_Drop);
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
	}

	m_bIsPlaying = true;

	return NOERROR;
}

_int CAction_StunDrop::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	m_fIndex += fTimeDelta * 30.f;
	m_fFaintTime += fTimeDelta;
	size_t iIndex = (size_t)m_fIndex;
	if (m_pGameObject->GetID() & CAMPER)
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		if (pMeshCom->IsOverTime(0.3f))
			pCamper->Set_State(AC::Injured_Idle);

		if (m_fIndex >= 33.f)
			return END_ACTION;

		if (m_vecCamperPos.size() <= iIndex)
			return END_ACTION;

		_vec3 vLocalPos = m_vecCamperPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}
	else
	{
		if (m_iState == AS::Stun_Drop_In && pMeshCom->IsOverTime(0.3f))
		{
			m_iState = AS::Stun_Drop_Out;
			((CSlasher*)m_pGameObject)->Set_State(AS::Stun_Drop_Out);
		}

		if (m_iState == AS::Stun_Drop_Out && pMeshCom->IsOverTime(0.3f))
			((CSlasher*)m_pGameObject)->Set_State(AS::Idle);

		if (m_fFaintTime >= 5.f)
			return END_ACTION;

	}

	return UPDATE_ACTION;
}

void CAction_StunDrop::End_Action()
{
	m_bIsPlaying = false;
	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_Carry(false);
		CCamper* pCarriedCamper = (CCamper*)pSlasher->Get_CarriedCamper();
		if(nullptr != pCarriedCamper)
			pCarriedCamper->SetColl(true);
		pSlasher->Set_CarriedCamper(nullptr);
		pSlasher->IsLockKey(false);
	
	}
	else
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->SetColl(true);
		pCamper->Set_State(AC::Injured_Idle);
		pCamper->SetCurCondition(CCamper::INJURED);
		pCamper->IsLockKey(false);
		pCamper->SetCarry(false);
		pCamper->Set_SlasherColl(true);
		pCamper->SetDyingEnergy();
		pCamper->Set_ProgressTime(0.f);
		pCamper->SetHeal(0.f);
	}

}

void CAction_StunDrop::Send_ServerData()
{
}

void CAction_StunDrop::SetVectorPos()
{
	m_vecCamperPos.reserve(33);

	m_vecCamperPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecCamperPos.push_back(_vec3(2.012f, 0.f, -0.228f));
	m_vecCamperPos.push_back(_vec3(7.472f, 0.f, -0.849f));
	m_vecCamperPos.push_back(_vec3(15.518f, 0.f, -1.763f));
	m_vecCamperPos.push_back(_vec3(25.289f, 0.f, -2.872f));
	m_vecCamperPos.push_back(_vec3(35.921f, 0.f, -4.08f));
	m_vecCamperPos.push_back(_vec3(46.554f, 0.f, -5.288f));
	m_vecCamperPos.push_back(_vec3(56.325f, 0.f, -6.398f));
	m_vecCamperPos.push_back(_vec3(64.371f, 0.f, -7.311f));
	m_vecCamperPos.push_back(_vec3(69.831f, 0.f, -7.932f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));
	m_vecCamperPos.push_back(_vec3(71.843f, 0.f, -8.16f));

	m_bIsInit = true;
}

void CAction_StunDrop::Free()
{
	CAction::Free();
}
