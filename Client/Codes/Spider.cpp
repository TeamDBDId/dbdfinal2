#include "stdafx.h"
#include "../Headers/Spider.h"
#include "Management.h"
#include "Light_Manager.h"
#include "MeshTexture.h"
#include "Camper.h"

_USING(Client)

CSpider::CSpider(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CSpider::CSpider(const CSpider & rhs)
	: CGameObject(rhs)
{

}


HRESULT CSpider::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CSpider::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_OldState = Invisible;
	m_CurState = Invisible;
	m_pMeshCom->Set_AnimationSet(Invisible);
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CSpider::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	m_fDeltaTime = fTimeDelta;
	//m_iCurAnimation = m_CurState;
	Camper_Check();
	ComunicateWithServer();
	State_Check();

	return _int();
}

_int CSpider::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 6000.f)
		return 0;

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_matrix matWorld = m_pTransformCom->Get_Matrix();
	vPosition.y += 35.f;
	matWorld._41 = vPosition.y;
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);

	//if (m_CurState == Invisible)
	//	return 0;
	if (!m_bCamper)
		return 0;

	if (m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, 400.f))
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
			return -1;
	}

	return _int();
}

void CSpider::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	m_pMeshCom->Play_Animation(m_fDeltaTime);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(25);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetInt("numBoneInf", 1);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CSpider::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CSpider::Render_Stemp()
{
}

void CSpider::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;


	//_vec3 vFront = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	//D3DXVec3Normalize(&vFront, &vFront);
	//vFront *= m_fRad;

	//m_vPosArr = new _vec3[1];
	//m_vPosArr[DIR::FRONT] = vFront;


	//CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_CHEST, this);
}

_matrix CSpider::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CSpider::Get_Key()
{
	return m_Key.c_str();
}

_int CSpider::Get_OtherOption()
{
	return m_iOtherOption;
}

void CSpider::Set_RotationY()
{
	m_pTransformCom->SetUp_Speed(10.f, 1.f);
	//m_pTransformCom->Rotation_Y(D3DXToRadian(180));
}

void CSpider::Camper_Check()
{
	_int checknum = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (!server_data.Campers[i].bConnect)
			continue;

		if (server_data.Campers[i].iCondition == CCamper::HOOKED
			|| server_data.Campers[i].iCondition == CCamper::HOOKDEAD)
		{
			_vec3 vPlaPos = server_data.Campers[i].vPos;
			_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

			_float fLength = D3DXVec3Length(&(vPlaPos - vMyPos));

			if (fLength < 800.f)
			{
				++checknum;
				if (server_data.Campers[i].fEnergy > 60.f)
					m_fDissolveTime = (server_data.Campers[i].fEnergy - 60.f) / 60.f;
				else
					m_fDissolveTime = 0.f;
			}
		}
	}
	if (0 == checknum)
		m_bCamper = false;
	else
		m_bCamper = true;
}

void CSpider::State_Check()
{
	if (m_CurState == m_OldState)
		return;
	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CSpider::ComunicateWithServer()
{
	m_CurState = (STATE)server_data.Game_Data.Spider[m_eObjectID];
}

HRESULT CSpider::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Dynamic
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Spider");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CSpider::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);

	pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"DissolveSpider.tga")));
	pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
	pEffect->SetFloat("g_fTimeAcc", m_fDissolveTime);

	return NOERROR;
}


CSpider * CSpider::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CSpider*	pInstance = new CSpider(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CSpider Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CSpider::Clone_GameObject()
{
	CSpider*	pInstance = new CSpider(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CSpider Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CSpider::Free()
{

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
