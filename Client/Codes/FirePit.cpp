#include "stdafx.h"
#include "FirePit.h"
#include "Management.h"
#include "Defines.h"
#include "CustomLight.h"
#include "Light_Manager.h"

_USING(Client)

CFirePit::CFirePit(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CFirePit::CFirePit(const CFirePit & rhs)
	: CGameObject(rhs)
{
}

HRESULT CFirePit::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CFirePit::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (nullptr != m_pMeshCom && m_Max < 0.001f)
	{
		_vec3 vMax = m_pMeshCom->GetMax();
		_vec3 vMin = m_pMeshCom->GetMin();
		_vec3 vResult = (vMax - vMin) * 0.51f;
		m_Max = max(vResult.x, max(vResult.y, vResult.z));
	}
	return NOERROR;
}

_int CFirePit::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	Init_Light();

	m_fBrTime += fTimeDelta;

	if (m_fBrTime > 0.1f)
	{
		m_fBright = ((rand() % 10) + 15) * 0.1f;
		m_fBrTime = 0.f;
	}

	if (nullptr != m_pCustomLight)
	{
		m_pCustomLight->Set_Diffuse(D3DXCOLOR(2.f * m_fBright, 1.64f * m_fBright, 0.74f * m_fBright, 1.f));
	}

	return _int();
}

_int CFirePit::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	//Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	//if (m_fCameraDistance > 6000.f)
	//	return 0;

	if(nullptr != m_pCustomLight)
		m_pCustomLight->SetRender(true);

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CFirePit::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);


	//m_pColliderCom->Render_Collider();
}

void CFirePit::Init_Light()
{
	if (m_LateInit)
		return;

	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	_vec3 vLocalPos = { 0.f, 20.f, 0.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_OBB, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider", m_pLightCollider);

	EFFMGR->Make_Effect(CEffectManager::E_Fire, m_pLightCollider->Get_Center(),1);

	_vec3 vCenter = m_pLightCollider->Get_Center();

	m_pCustomLight = nullptr;

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight)))
		return;

	m_pCustomLight->Add_Light();
	m_pCustomLight->AddRef();

	m_pCustomLight->Set_Position(vCenter);
	m_pCustomLight->Set_Range(500.f);
	m_pCustomLight->Set_Shaft(true);
	m_pCustomLight->SetRender(true);
	//Add_ShadowCubeGroup
	GET_INSTANCE_MANAGEMENT;

	m_pCustomLight->Add_ShadowCubeGroup(this);

	Safe_Release(pManagement);

	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return;

	pLight_Manager->AddRef();

	pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());
	Safe_Release(pLight_Manager);


	m_LateInit = true;
}

void CFirePit::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);
	m_iOtherOption = OtherOption;
	if (Key == nullptr)
		return;

	m_Key = Key;


	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_LOBBY, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_AABB, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);
}

_matrix CFirePit::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CFirePit::Get_Key()
{
	return m_Key.c_str();
}

_int CFirePit::Get_OtherOption()
{
	return m_iOtherOption;
}

HRESULT CFirePit::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_FirePit");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CFirePit::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	D3DXMatrixInverse(&matView, nullptr, &matView);
	pEffect->SetVector("g_vCamPosition", (_vec4*)&matView.m[3][0]);

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);


	return NOERROR;
}

CFirePit * CFirePit::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CFirePit*	pInstance = new CFirePit(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CFirePit Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CFirePit::Clone_GameObject()
{
	CFirePit*	pInstance = new CFirePit(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CFirePit Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CFirePit::Free()
{
	Safe_Release(m_pLightCollider);
	Safe_Release(m_pCustomLight);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
