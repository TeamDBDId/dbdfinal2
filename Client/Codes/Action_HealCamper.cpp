#include "stdafx.h"
#include "..\Headers\Action_HealCamper.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Action_BeingHeal.h"
#include "Item.h"

_USING(Client)

CAction_HealCamper::CAction_HealCamper()
{
}

HRESULT CAction_HealCamper::Ready_Action()
{
	if (m_fDelay >= 0.f)
		return NOERROR;

	if (m_bIsPlaying == true)
		return NOERROR;

	m_fEndDelay = 0.0f;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	pCamper->Set_State(AC::HealCamper);
	m_iState = AC::HealCamper;
	m_fOldHealCount = pCamper->GetHeal();
	//if (nullptr != m_pCamper)
	//{
	//	camper_data.InterationObjAnimation = (m_pCamper->GetID() - CAMPER) + 1;
	//	CTransform* pTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
	//	if (nullptr == pTransform)
	//		return FAILED_ACTION;

	//	_vec3 vMyPos = server_data.Campers[pCamper->GetID() - CAMPER].vPos;
	//	_vec3 vCamperPos = server_data.Campers[m_pCamper->GetID() - CAMPER].vPos;
	//	_vec3 vDir = vCamperPos - vMyPos;

	//	//pTransform->Go_ToTarget_ChangeDirection(&vDir, 1.f, true);
	//}
		
	//CAction_BeingHeal* pAction = (CAction_BeingHeal*)m_pCamper->Find_Action(L"Action_BeingHeal");
	//if (!pAction->m_bIsPlaying)
	//{
	//	//pAction->SetCamper(m_pGameObject);
	//	//m_pCamper->Set_Action(L"Action_BeingHeal", 100.f);
	//}
	m_fHealTime = 0.f;
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_HealCamper);
	GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_pCamper->GetHeal(), m_pCamper->GetMaxHeal());
	GET_INSTANCE(CUIManager)->Set_SkillCheckState(0);
	return NOERROR;
}

_int CAction_HealCamper::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay >= 0.f)
	{
		m_fDelay -= fTimeDelta;
		m_bIsPlaying = false;
		return FAILED_ACTION;
	}

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (pCamper->GetCurCondition() == CCamper::DYING || pCamper->GetOldCondition() == CCamper::DYING)
		return END_ACTION;
	if (nullptr != m_pCamper)
	{
		camper_data.SecondInterationObject = m_pCamper->GetID();
		camper_data.SecondInterationObjAnimation = BEING_HEAL;
		camper_data.InterationObject = m_pCamper->GetID();
		camper_data.InterationObjAnimation = (m_pCamper->GetID() - CAMPER) + 1;


		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		_vec3 vHPos = server_data.Campers[m_pCamper->GetID() - CAMPER].vPos;
		_vec3 vMyPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
		_vec3 vDir = vHPos - vMyPos;
		D3DXVec3Normalize(&vDir, &vDir);

		pTransform->Go_ToTarget_ChangeDirection(&vDir, fTimeDelta, true);
	}

	//else if(nullptr == m_pCamper)
	//	return END_ACTION;

	if (nullptr != pCamper->GetCurItem() && pCamper->GetCurItem()->Get_ItemType() == CItem::MEDIKIT)
	{
		if (KEYMGR->MousePressing(1))
		{
			if (!pCamper->GetCurItem()->Get_IsDead())
			{
				camper_data.bUseItem = true;
				_bool bOverDurability = false;

				bOverDurability = pCamper->GetCurItem()->Use_Item(fTimeDelta, nullptr, nullptr);
			}
		}
	}

	if (nullptr != m_pCamper)
	{
		_float fRange = D3DXVec3Length(&(server_data.Campers[m_pCamper->GetID() - CAMPER].vPos - server_data.Campers[exPlayerNumber - 1].vPos));
		if (fRange > 120.f)
			return END_ACTION;
	}

	if (m_fHealTime > 0.1f)
	{
		if (server_data.Campers[m_pCamper->GetID() - CAMPER].Packet == 3050)
		{
			camper_data.InterationObjAnimation = 0;
			camper_data.SecondInterationObject = 0;
			camper_data.SecondInterationObjAnimation = 0;
			return END_ACTION;
		}		
	}
	
	m_fEndDelay += fTimeDelta;
	m_fHealTime += fTimeDelta;
	GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_pCamper->GetHeal(), m_pCamper->GetMaxHeal());
	_int SkillCheck = GET_INSTANCE(CUIManager)->Get_SkillCheckState();
	if (SkillCheck == FAILED_SKILL_CHECK)
	{
		camper_data.Packet = FAILED_SKILL_CHECK;   // 실패
		m_iState = AC::HealCamperFail;
		((CCamper*)m_pGameObject)->Set_State(AC::HealCamperFail);
	}
	else if (SkillCheck == SUCCESS_SKILL_CHECK)
	{
		camper_data.Packet = SUCCESS_SKILL_CHECK; // 대성공
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Altruism, L"스킬 체크(훌륭함)", 150);
	}
	else if(SkillCheck == 12)
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Altruism, L"스킬 체크(좋음)", 50);

	if (m_iState == AC::HealCamperFail)
	{
		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.3f))
		{
			GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Boldness, wstring(L"부주의함"), 250);
			m_fOldHealCount = m_pCamper->GetHeal();
			m_iState = AC::HealCamper;
			((CCamper*)m_pGameObject)->Set_State(AC::HealCamper);
		}
	}
	//else
	//	if (m_fOldHealCount < m_pCamper->GetHeal())
	//	{
	//		m_fOldHealCount = m_pCamper->GetHeal();
	//		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Altruism, wstring(L"치료"), 200);
	//	}
			

	if (!KEYMGR->MousePressing(0) && !KEYMGR->MousePressing(1))
		return END_ACTION;

	//if (m_fEndDelay > 0.2f)
	//{
	//	if (m_pCamper->GetState() != AC::BeingHeal && m_pCamper->GetState() != AC::CrawlFT && m_pCamper->GetState() != AC::BeingHealFail)
	//		return END_ACTION;
	//}

	//if (m_pCamper->GetCurCondition() != CCamper::INJURED && m_pCamper->GetCurCondition() != CCamper::DYING)
	//	return END_ACTION;

	//if (m_pCamper->GetState() != AC::BeingHeal && m_pCamper->GetState() != AC::CrawlFT)
	//	return END_ACTION;

	return UPDATE_ACTION;
}

void CAction_HealCamper::End_Action()
{
	GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Survival, wstring(L"치료"), (_int)(400.f*(m_fHealTime / 30.f)));
	
	camper_data.InterationObjAnimation = 0;
	m_fDelay = 0.5f;
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	m_pCamper = nullptr;
	Send_ServerData();
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
}

void CAction_HealCamper::Send_ServerData()
{
	//if(m_pCamper != nullptr)
	//	camper_data.InterationObject = m_pCamper->GetID();
	//else
	//	camper_data.InterationObject = 0;
}

void CAction_HealCamper::SetCamper(CGameObject * pCamper)
{
	if(nullptr == m_pCamper)
		m_pCamper = (CCamper*)pCamper;
}

void CAction_HealCamper::Free()
{
	CAction::Free();
}
