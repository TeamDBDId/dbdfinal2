#include "stdafx.h"
#include "..\Headers\Action_HookBeing.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Item.h"
#include "MeatHook.h"
#include "Slasher.h"
#include "Camera_Camper.h"
#include "Action_SpiderReaction.h"
#include "Action_HookFree.h"

_USING(Client)

CAction_HookBeing::CAction_HookBeing()
{
}

HRESULT CAction_HookBeing::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	//if (m_fDelay > 0.f)
	//	return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (nullptr == m_pGameObject)
		return NOERROR;

	m_fHookBeingTime = 0.f;
	m_bIsPlaying = true;
	m_bIsRescued = false;
	m_bIsInit = false;
	pCamper->IsLockKey(true);
	m_iID_HealCamper = -1;
	m_fTime = 0.f;
	GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fHookBeingTime, 1.f);
	if (pCamper->GetCurCondition() != CCamper::HOOKED)
	{
		Send_ServerData();
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_HookFree);
		pCamper->Set_State(AC::HookBeingCamperIn);
		m_iState = AC::HookBeingCamperIn;
		pCamper->SetOldCondition(pCamper->GetCurCondition());
		pCamper->SetCurCondition(CCamper::SPECIAL);

		Send_ServerData();
	}
	else
	{
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_HookFreed);
		pCamper->Set_State(AC::HookBeingRescuedIn);
		m_iState = AC::HookBeingRescuedIn;
	}

	return NOERROR;
}

_int CAction_HookBeing::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	//m_fDelay -= fTimeDelta;

	//if (m_fDelay > 0.f)
	//	return eState;
	if (eState != UPDATE_ACTION)
		return eState;

	if (((CCamper*)m_pGameObject)->GetHookStage() == CCamper::Struggle)
	{
		m_fHookBeingTime += fTimeDelta * 1.05f;
		if (m_fHookBeingTime >= 0.90f)
		{
			((CCamper*)m_pGameObject)->Set_State(AC::Idle);
			m_bIsRescued = true;
			return END_ACTION;
		}
		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.25f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::HookBeingRescuredEnd);
			m_iState = AC::HookBeingRescuredEnd;
		}

		return UPDATE_ACTION;
	}

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	m_fHookBeingTime += fTimeDelta * 1.05f;

	GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fHookBeingTime, 1.f);
	if (m_iState == AC::HookBeingCamperIn)
	{
		if (!KEYMGR->MousePressing(0))
			return END_ACTION;

		if (pMeshCom->IsOverTime(0.25f))
		{
			Send_ServerData();
			((CCamper*)m_pGameObject)->Set_State(AC::HookBeingCamperEnd);
			m_iState = AC::HookBeingCamperEnd;
		}

		Send_ServerData();
	}
	else if (m_iState == AC::HookBeingCamperEnd)
	{
		if (!KEYMGR->MousePressing(0))
			return END_ACTION;

		if (pMeshCom->IsOverTime(0.25f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::Idle);
			Send_ServerData();
			GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Survival, wstring(L"갈고리 구출"), 1500);
			return END_ACTION;
		}

		Send_ServerData();
	}
	
	if (m_iState == AC::HookBeingRescuedIn)
	{
		m_fTime += fTimeDelta;
		m_iID_HealCamper = -1;

		for (int i = 0; i < 4; ++i)
		{
			if (exPlayerNumber == (i + 1))
				continue;

			if (!server_data.Campers[i].bConnect)
				continue;

			if (!server_data.Campers[i].bLive)
				continue;

			if (server_data.Campers[i].SecondInterationObject == ((exPlayerNumber + CAMPER) - 1))
			{
				m_iID_HealCamper = i;

			}		
		}

		if (m_iID_HealCamper != -1 && m_fTime <= 0.2f)
		{
			CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
			_vec3 vHPos = server_data.Campers[m_iID_HealCamper].vPos;
			_vec3 vMyPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
			_vec3 vDir = vHPos - vMyPos;
			D3DXVec3Normalize(&vDir, &vDir);

			pTransform->Go_ToTarget_ChangeDirection(&vDir, fTimeDelta, true);
		}

		if (m_fTime >= 0.3f && m_iID_HealCamper == -1)
		{
			GET_INSTANCE(CUIManager)->Set_ProgressPoint(0.f, 1.f);
			GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_HookStruggle);
			return END_ACTION;
		}
		

		if (pMeshCom->IsOverTime(0.25f))
		{
			((CCamper*)m_pGameObject)->Set_State(AC::HookBeingRescuredEnd);
			m_iState = AC::HookBeingRescuredEnd;
		}
		
	}
	else if (m_iState == AC::HookBeingRescuredEnd)
	{
		if (pMeshCom->IsOverTime(0.25f) || m_fHookBeingTime > 1.f)
		{
			((CCamper*)m_pGameObject)->Set_State(AC::Idle);
			m_bIsRescued = true;
			//if (m_iID_HealCamper != -1)
			//{
			//	if (server_data.Campers[m_iID_HealCamper].Perk & BORROWEDTIME)
			//	{
			//		((CCamper*)m_pGameObject)->Set_PerkTime(L"HOOK_BEINGBORROWED", 15.f);
			//	}
			//}

			return END_ACTION; 
		}
	}

	return UPDATE_ACTION;
}

void CAction_HookBeing::End_Action()
{
	//m_fTime = 0.0f;
	//m_fHookBeingTime = 0.0f;

	//m_fDelay = 0.2f;
	//m_iCountOfFrame = 0;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	if (m_iState == AC::HookBeingCamperEnd || m_iState == AC::HookBeingCamperIn)
	{
		if(!m_bIsRescued)
			m_bIsRescued = true;

		pCamper->SetCurCondition(pCamper->GetOldCondition());
		camper_data.InterationObjAnimation = 0;
		camper_data.SecondInterationObject = 0;
		m_bIsPlaying = false;
		return;
	}

	if (m_bIsRescued)
	{
		if (nullptr == pCamper)
			return;

		camper_data.SecondInterationObject = pCamper->GetID();
		camper_data.SecondInterationObjAnimation = CLEAR_PACKET;

		if (server_data.Campers[exPlayerNumber - 1].Perk & BREAKING)
		{
			if (nullptr != m_pMeatHook && pCamper->Get_PerkTime(L"BREAKING") <= 0.1f)
			{
				pCamper->Set_PerkTime(L"BREAKING", 40.f);
				camper_data.InterationObject = m_pMeatHook->GetID();
				camper_data.InterationObjAnimation = 101;

				GET_INSTANCE_MANAGEMENT;

				if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size() != 0)
				{
					CSlasher* pSlasher = (CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front();
					if (nullptr != pSlasher)
					{
						pSlasher->Set_Penetration(6.f);
						pSlasher->Set_PColor(_vec4(0.6f, 0.f, 0.f, 1.f));
					}
				}

				Safe_Release(pManagement);
			}
		}

		pCamper->SetCurCondition(CCamper::INJURED);			

		if (pCamper->GetEnergy() >= 60.f)
		{
			pCamper->SetHookStage(CCamper::Struggle);
			pCamper->SetEnergy(59.9f);
			pCamper->SetHookedEnergy();
		}
		else
		{
			pCamper->SetHookStage(CCamper::Sacrifice);
			pCamper->SetEnergy(0.f);
			pCamper->SetHookedEnergy();
		}
		m_pMeatHook = nullptr;
	}

	if(m_bIsPlaying && !m_bIsRescued && pCamper->GetCurCondition() == CCamper::HOOKED)
	{
		CAction_SpiderReaction* pAction = (CAction_SpiderReaction*)m_pGameObject->Find_Action(L"Action_SpiderReaction");
		pAction->SetFailedHookBeing(true);
		pCamper->Set_Action(L"Action_SpiderReaction", 150.f);
		m_bIsPlaying = false;
		m_pMeatHook = nullptr;
		return;
	}
	m_bIsPlaying = false;
}

void CAction_HookBeing::Send_ServerData()
{
	//camper_data.InterationObject = m_pHook
	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (pCamper->GetCurCondition() != CCamper::HOOKED && m_pCamper != nullptr)
	{
		camper_data.SecondInterationObject = m_pCamper->GetID();
		camper_data.SecondInterationObjAnimation = HOOK_BEING;
		camper_data.InterationObjAnimation = (m_pCamper->GetID() - CAMPER) + 1;
	}
}

//_bool CAction_HookBeing::Check_HealCamperState()
//{
//	if (server_data.Campers[m_iID_HealCamper].iState != AC::HookBeingCamperIn && server_data.Campers[m_iID_HealCamper].iState != AC::HookBeingCamperEnd)
//		return m_bIsRescued = false;
//
//	return true;
//}
//
//void CAction_HookBeing::Init_HealCamper()
//{
//	CManagement* pManagement = CManagement::GetInstance();
//	m_iID_HealCamper = -1;
//	for (_int i = 0; i < 5; ++i)
//	{
//   		if (server_data.Campers[i].iState == AC::HookBeingCamperIn || server_data.Campers[i].iState == AC::HookBeingCamperEnd)
//		{
//			_vec3 vOtherCamperPos = server_data.Campers[i].vPos;
//			_vec3 vMyPos = *((CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform"))->Get_StateInfo(CTransform::STATE_POSITION);
//
//			_float fDist = D3DXVec3Length(&(vMyPos - vOtherCamperPos));
//			if (fDist < 150.f)
//			{
//				m_iID_HealCamper = i;
//				m_bIsInit = true;
//				return;
//			}
//		}
//	}
//
//}

_bool CAction_HookBeing::Check_ResecueCamper()
{
	for (int i = 0; i < 4; ++i)
	{
		if (exPlayerNumber == (i + 1))
			continue;

		if (!server_data.Campers[i].bConnect)
			continue;
		
		if (!server_data.Campers[i].bLive)
			continue;

		if (server_data.Campers[i].InterationObjAnimation == exPlayerNumber)
			return true;
	}

	return false;
}

void CAction_HookBeing::Free()
{
	CAction::Free();
}
