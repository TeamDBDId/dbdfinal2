#include "stdafx.h"
#include "..\Headers\Totem.h"
#include "Management.h"
#include "Defines.h"
#include "MeshTexture.h"
#include "CustomLight.h"
#include "Light_Manager.h"

_USING(Client)

CTotem::CTotem(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CTotem::CTotem(const CTotem & rhs)
	: CGameObject(rhs)
{
}

HRESULT CTotem::Ready_Prototype()
{

	
	return NOERROR;
}

HRESULT CTotem::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 14.f;
	return NOERROR;
}

_int CTotem::Update_GameObject(const _float & fTimeDelta)
{
	Init_Light();
	if (m_isDead)
	{
		m_pCustomLight->SetRender(false);
		m_bDissolve = true;
	}
	if (m_bDissolve)
	{
		m_fDissolveTime += fTimeDelta;
	}
	if (m_fDissolveTime >= 2.5f)
		return DEAD_OBJ;
	Update_Sound();
	ComunicateWithServer();

	return _int();
}

_int CTotem::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	Compute_CameraDistance(&vPosition);

	if (m_fCameraDistance > m_Max* 50.f)
		return 0;

	_matrix		matLocal, matWorld;
	matWorld = m_pTransformCom->Get_Matrix();
	matLocal = m_pMeshCom->Get_LocalTransform();

	matWorld = matLocal * matWorld;
	vPosition = *(_vec3*)&matWorld.m[3][0];
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);

	if (m_iRuin == 0)
	{
		if (!m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, m_Max))
			return 0;
	}

	if (!m_bDissolve)
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

		if (exPlayerNumber == 5 && m_iRuin != 0)
			m_pRendererCom->Add_StencilGroup(this);
	}
	else
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
			return -1;
	}


	return _int();
}

void CTotem::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	_uint iPass = 0;
	if (!m_bDissolve)
		iPass = 0;
	else
		iPass = 8;

	if (8 != iPass && exPlayerNumber == 5 && m_iRuin != 0)
		iPass = 26;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(iPass);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);


	//m_pColliderCom->Render_Collider();
}

void CTotem::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();



	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();
}

void CTotem::Render_Stemp()
{
	LPD3DXEFFECT	pEffect = m_pRendererCom->GetCubeEffectHandle()->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(13);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(i);
		if (nullptr == pSubSet)
			return;

		m_pMeshCom->Render_Mesh(i);

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CTotem::Render_Stencil()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(27);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;
		pEffect->SetVector("g_DiffuseColor", &_vec4(0.8f, 0, 0, 0.8f));

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CTotem::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	m_fRad = 80.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Mesh_Totem");
	Add_Component(L"Com_Mesh", m_pMeshCom);

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if(FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		_MSG_BOX("111");
	m_pColliderCom->Set_CircleRad(80.f);
	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_TOTEM, this);

	_vec3 vMax = m_pMeshCom->GetMax();
	_vec3 vMin = m_pMeshCom->GetMin();
	_vec3 vResult = (vMax - vMin) * 0.52f;

	m_Max = max(vResult.x, max(vResult.y, vResult.z));
}

_matrix CTotem::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CTotem::Get_Key()
{
	return m_Key.c_str();
}

_int CTotem::Get_OtherOption()
{
	return 0;
}

void CTotem::Update_Sound()
{
	_float fRatio = m_fProgressTime / m_fMaxProgressTime;

	if (fRatio == 0.f)
		m_SoundCheck = 0;
	else if (fRatio >= 0.1f && m_SoundCheck == 0)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.2f && m_SoundCheck == 1)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.3f && m_SoundCheck == 2)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_03.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.4f && m_SoundCheck == 3)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_04.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.5f && m_SoundCheck == 4)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_05.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.6f && m_SoundCheck == 5)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_06.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.7f && m_SoundCheck == 6)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.8f && m_SoundCheck == 7)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (fRatio >= 0.9f && m_SoundCheck == 8)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_sabotage_bones_03.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (m_fDissolveTime >= 0.1f && m_SoundCheck == 9)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_fire_short_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (m_fDissolveTime >= 0.9f && m_SoundCheck == 9)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_fire_short_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}
	else if (m_fDissolveTime >= 1.8f && m_SoundCheck == 9)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Totem/totem_fire_short_03.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		m_SoundCheck++;
	}

}

void CTotem::Init_Light()
{
	if (m_LateInit)
		return;

	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 10.f, 10.f, 10.f);
	_vec3 vLocalPos = { 0.f, 120.f, 0.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_OBB, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider", m_pLightCollider);

	//D3DXVec3TransformCoord(&vLocalPos, &vLocalPos, m_pTransformCom->Get_Matrix_Pointer());

	_vec3 vCenter = m_pLightCollider->Get_Center();

	m_pCustomLight = nullptr;

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight)))
		return;

	m_pCustomLight->Add_Light();
	m_pCustomLight->AddRef();

	m_pCustomLight->Set_Position(vCenter);
	m_pCustomLight->Set_Type(D3DLIGHT_SPOT);

	m_pCustomLight->Set_Diffuse(D3DXCOLOR(3.6f, 1.2f, 1.2f, 1.f));
	m_pCustomLight->Set_Range(400.f);
	m_pCustomLight->Set_Theta(D3DXToRadian(43.5f));
	m_pCustomLight->Set_Phi(D3DXToRadian(53.5f));

	_vec3 vLightDir = { 0.f, -1.f, 0.f };
	m_pCustomLight->Set_Direction(vLightDir);

	//Add_ShadowCubeGroup
	GET_INSTANCE_MANAGEMENT;

	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static"))
	{
		m_pCustomLight->Add_ShadowCubeGroup(pObj);
	}
	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject"))
	{
		m_pCustomLight->Add_ShadowCubeGroup(pObj);
	}
	Safe_Release(pManagement);
	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return;

	pLight_Manager->AddRef();
	pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());
	m_pCustomLight->SetRender(false);
	m_LateInit = true;

	Safe_Release(pLight_Manager);
}

HRESULT CTotem::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CTotem::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	if (m_bDissolve)
	{
		pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"DissolveSpider.tga")));
		pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
		pEffect->SetFloat("g_fTimeAcc", (m_fDissolveTime / 2.5f));
	}

	return NOERROR;
}

void CTotem::ComunicateWithServer()
{
	_float fTotemTime = 0.0f;
	fTotemTime = server_data.Game_Data.Totem[m_eObjectID - TOTEM];

	m_iRuin = _int(fTotemTime / 100.f);
	if (m_iRuin != 0)
	{
		if (nullptr != m_pCustomLight)
			m_pCustomLight->SetRender(true);
		m_bCurse = true;
		fTotemTime -= m_iRuin * 100.f;
	}	
	else
	{
		if (nullptr != m_pCustomLight)
			m_pCustomLight->SetRender(false);
	}

	m_fProgressTime = fTotemTime;

	if (m_fProgressTime > m_fMaxProgressTime)
		m_isDead = true;
}

CTotem * CTotem::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTotem*	pInstance = new CTotem(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CTotem Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CTotem::Clone_GameObject()
{
	CTotem*	pInstance = new CTotem(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CTotem::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_TOTEM, this);

	Safe_Release(m_pLightCollider);
	Safe_Release(m_pCustomLight);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
