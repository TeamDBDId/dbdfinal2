#include "stdafx.h"
#include "..\Headers\AfterImage.h"
#include "Management.h"
#include "MeshTexture.h"

_USING(Client)

CAfterImage::CAfterImage(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CAfterImage::CAfterImage(const CAfterImage & rhs)
	: CGameObject(rhs)
{

}


// 원형객체 생성될 때 호출.
HRESULT CAfterImage::Ready_Prototype()
{
	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CAfterImage::Ready_GameObject()
{
	// 실제 사용되기위한 객체들 만의 추가적인 데이터를 셋.

	// 현재 객체에게 필요한 컴포넌트들을 복제해서 온다.
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pVB = new VTXTEX[124];

	m_pTransformCom->SetUp_Speed(1.0f, D3DXToRadian(90.0f));

	_matrix mat;
	D3DXMatrixIdentity(&mat);
	m_pTransformCom->Set_Matrix(mat);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.f, 0.f, 0.f));
	m_pTransformCom->Scaling(1.f, 1.f, 1.f);

	return NOERROR;
}

_int CAfterImage::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return DEAD_OBJ;
	
	if (m_pVB == nullptr)
		return 0;

	if (!m_bRender)
		return 0;

	m_iCount++;
	if (m_iCount >= 60)
		m_iCount = 0;

	if (m_iIndex >= 60)
	{
		for (int i = 1; i < m_iIndex - 1; i += 2)
		{
			m_pVB[i] = m_pVB[i + 2];
			m_pVB[i + 1] = m_pVB[i + 3];
		}
		m_iIndex -= 2;
	}

	return _int();
}

_int CAfterImage::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (!m_bRender)
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CAfterImage::Render_GameObject()
{
	if (!m_bRender)
		return;

	LPD3DXEFFECT pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	if (m_pVB == nullptr)
		return;

	if (m_iIndex < 4)
		return;

	for (int i = 0; i < m_iIndex; ++i)
	{
		if (i == 1)
			m_pVB[i].vTexUV = { 0.f, 0.f };
		else if ((i % 2) == 0)
			m_pVB[i].vTexUV = { 1.f / ((m_iIndex - 2) * 0.5f) * (i * 0.5f), 0.f };
		else if ((i % 2) == 1)
			m_pVB[i].vTexUV = { 1.f / ((m_iIndex - 2) * 0.5f) * ((i - 1) * 0.5f), 1.f };
	}

	pEffect->AddRef();

	if (FAILED(SetUp_ConstantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(13);

	//m_pGraphic_Device->SetStreamSource(0, *(LPDIRECT3DVERTEXBUFFER9*)&m_pVB, 0, sizeof(VTXTEX));

	D3DVERTEXELEMENT9 decl[] =
	{
		{ 0,  0, D3DDECLTYPE_FLOAT3,        D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,    0 },
		{ 0, 12, D3DDECLTYPE_FLOAT2,        D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,    0 },
		D3DDECL_END()
	};

	m_pGraphic_Device->CreateVertexDeclaration(decl, &m_pB);
	m_pGraphic_Device->SetVertexDeclaration(m_pB);
	//m_pGraphic_Device->SetFVF(D3DFVF_XYZ | D3DFVF_TEX1);
	m_pGraphic_Device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, m_iIndex - 2, m_pVB, sizeof(VTXTEX));

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CAfterImage::SetInfo(const _vec3 vPos1, const _vec3 vPos2)
{
	if (!m_bRender)
		return;

	m_pVB[m_iIndex].vPosition = vPos1;
	m_pVB[m_iIndex].vTexUV = _vec2(0.0f, 1.f);
	m_iIndex++;
	m_pVB[m_iIndex].vPosition = vPos2;
	m_pVB[m_iIndex].vTexUV = _vec2(1.f, 1.f);
	m_iIndex++;
}

void CAfterImage::ClearBuffer()
{
	Safe_Delete_Array(m_pVB);
	m_iIndex = 0;
	m_pVB = new VTXTEX[124];
}

HRESULT CAfterImage::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (nullptr == m_pTransformCom)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (nullptr == m_pRendererCom)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Shader_Default");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CAfterImage::SetUp_ConstantTable(LPD3DXEFFECT pEffect)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetTexture("g_DiffuseTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Volumelight.tga")));
	pEffect->SetTexture("g_DistortionTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Noise01.tga")));

	_float fRepeatX = (10 + (rand() % 100)) * 0.1f;
	_float fRepeatY = (10 + (rand() % 100)) * 0.1f;

	pEffect->SetFloat("g_fRandX", fRepeatX);
	pEffect->SetFloat("g_fRandY", fRepeatY);


	return NOERROR;
}

CAfterImage * CAfterImage::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CAfterImage*	pInstance = new CAfterImage(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CAfterImage Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CAfterImage::Clone_GameObject()
{
	CAfterImage*	pInstance = new CAfterImage(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CAfterImage Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CAfterImage::Free()
{
	if (nullptr != m_pVB)
		Safe_Delete_Array(m_pVB);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pShaderCom);

	CGameObject::Free();
}
