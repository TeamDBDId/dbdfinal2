#include "stdafx.h"
#include "..\Headers\Moon.h"
#include "Management.h"

_USING(Client)

CMoon::CMoon(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CMoon::CMoon(const CMoon & rhs)
	: CGameObject(rhs)
{

}

HRESULT CMoon::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CMoon::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(-5.f, 2000.f, 3000.f));

	return NOERROR;
}

_int CMoon::Update_GameObject(const _float & fTimeDelta)
{
	//_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	//_matrix matView, matProj, matVP;
	//m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	//m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matProj);
	//matVP = matView * matProj;

	//float viewX = vPos.x * matVP._11 + vPos.y * matVP._21 + vPos.z * matVP._31 + matVP._41;
	//float viewY = vPos.x * matVP._12 + vPos.y * matVP._22 + vPos.z * matVP._32 + matVP._42;
	//float viewZ = vPos.x * matVP._13 + vPos.y * matVP._23 + vPos.z * matVP._33 + matVP._43;

	//int screenX, screenY;
	//screenX = (int)((viewX / viewZ + 1.0f) * g_iBackCX * 0.5f);
	//screenY = (int)((-viewY / viewZ + 1.0f) * g_iBackCY * 0.5f);

	//m_vProjPos = { (_float)screenX - 0.5f , (_float)screenY - 0.5f, 1.f };

	//m_pRendererCom->SetScreenPos(m_vProjPos);

	return _int();
}

_int CMoon::LastUpdate_GameObject(const _float & fTimeDelta)
{
	//if (nullptr == m_pRendererCom)
	//	return -1;


	//_matrix		matView;
	//m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	//D3DXMatrixInverse(&matView, nullptr, &matView);

	//_vec3		vRight, vUp, vLook;

	//for (size_t i = 0; i < 2; ++i)
	//{
	//	vRight = *(_vec3*)&matView.m[0][0] * m_pTransformCom->Get_Scale().x;
	//	vUp = *(_vec3*)&matView.m[1][0] * m_pTransformCom->Get_Scale().y;
	//	vLook = *(_vec3*)&matView.m[2][0] * m_pTransformCom->Get_Scale().z;

	//	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	//	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	//	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
	//}

	//if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_PRIORITY, this)))
	//	return -1;

	return _int();
}

void CMoon::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	if (FAILED(SetUp_ConstantTable(pEffect, 0)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(0);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	if (FAILED(SetUp_ConstantTable(pEffect, 1)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(1);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CMoon::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
}

HRESULT CMoon::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//// For.Com_Shader
	//m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_LightCircle");
	//if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
	//	return E_FAIL;

	// For.Com_Buffer_RcTex
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	// For.Com_Texture
	m_pLightTexture = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Texture_Light");
	if (FAILED(Add_Component(L"Com_Texture0", m_pLightTexture)))
		return E_FAIL;

	// For.Com_Texture
	m_pTexCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Texture_Moon");
	if (FAILED(Add_Component(L"Com_Texture1", m_pTexCom)))
		return E_FAIL;

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CMoon::SetUp_ConstantTable(LPD3DXEFFECT pEffect, _uint i)
{
	_matrix	 matWVP, matWorld, matView, matProj;

	m_pTransformCom->Scaling(100.f, 100.f, 100.f);

	matWorld = m_pTransformCom->Get_Matrix();
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matWVP = matWorld * matView* matProj;

	pEffect->SetMatrix("g_matWVP", &matWVP);
	pEffect->SetVector("g_vLightDiffuse", &_vec4(1.f, 1.f, 1.f, 1.f));

	if(i == 0)
		m_pLightTexture->SetUp_OnShader(pEffect, "g_DiffuseTexture");
	else
		m_pTexCom->SetUp_OnShader(pEffect, "g_DiffuseTexture");

	return NOERROR;
}

CMoon * CMoon::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CMoon*	pInstance = new CMoon(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CMoon Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CMoon::Clone_GameObject()
{
	CMoon*	pInstance = new CMoon(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CMoon Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CMoon::Free()
{
	Safe_Release(m_pTexCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pLightTexture);
	CGameObject::Free();
}
