#include "stdafx.h"
#include "Hatch.h"
#include "Management.h"
#include "Light_Manager.h"
_USING(Client)

CHatch::CHatch(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CHatch::CHatch(const CHatch & rhs)
	: CGameObject(rhs)
{

}


HRESULT CHatch::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CHatch::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_CurState = IdleClose;
	m_pMeshCom->Set_AnimationSet(IdleClose);
	m_isRendering = false;
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CHatch::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	Hatch_Check(fTimeDelta);
	State_Check();
	Position_Check();
	ComunicateWithServer();
	m_pMeshCom->Set_AnimationSet((_uint)m_CurState);
	m_pMeshCom->Play_Animation(fTimeDelta);
	return _int();
}

_int CHatch::LastUpdate_GameObject(const _float & fTimeDelta)
{


	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;
	//if (!m_isRendering)
	//	return 0;
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 6000.f)
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;


	return _int();
}

void CHatch::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	//m_pColliderCom->Render_Collider();
	Safe_Release(pEffect);
}

void CHatch::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
}

void CHatch::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}


void CHatch::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;
	m_eObjectID = HATCH;


	m_Key = Key;



	/*
	_vec3 vBack = -*m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vBack, &vBack);
	vBack *= 120.f;
	*/
	//vPos += vBack * 120.f;



	m_fRad = 120.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
	matLocalTransform._43 = 120.f;

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);


	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_HATCH, this);

}

_matrix CHatch::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CHatch::Get_Key()
{
	return m_Key.c_str();
}

_int CHatch::Get_OtherOption()
{
	return m_iOtherOption;
}

void CHatch::State_Check()
{
	if (m_CurState == m_OldState)
		return;
	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CHatch::Hatch_Check(const _float& fTimeDelta)
{
	if (IdleOpen == m_CurState)
		return;

	_uint iCount = 0;

	for (int i = 0; i < 4; ++i)
	{
		if (server_data.Campers[i].bConnect && server_data.Campers[i].bLive)
			++iCount;
	}

	if (iCount == 1 && m_fTime <= 1.5f)
	{
		m_fTime += fTimeDelta;
		m_CurState = Open;
	}
	else if (iCount == 1 && m_fTime > 1.5f)
	{
		m_fTime = 5.f;
		m_CurState = IdleOpen;
		GET_INSTANCE(CSoundManager)->PlaySound(string("Hatch/Hatch.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1700.f, 1.f);
		//m_CurState = Open;
	}
	else
		m_CurState = IdleClose;
}

void CHatch::Position_Check()
{
	if (m_bPos)
		return;

	if (m_iNumber != 0)
		m_bPos = true;

	switch (m_iNumber)
	{
	case 1:
		vMyPos = { 4136.f, 15.f, 3547.f };
		break;
	case 2:
		vMyPos = { 7601.f, 15.f, 4079.f };
		break;
	case 3:
		vMyPos = { 9755.f, 15.f, 9876.f };
		break;
	case 4:
		vMyPos = { 4229.f, 15.f, 8170.f };
		break;
	case 5:
		vMyPos = { 479.f, 15.f, 7669.f };
		break;
	case 6:
		vMyPos = { 3005.f, 15.f, 6628.f };
		break;
	default:
		vMyPos = { 0.f,0.f,0.f };
		break;
	}

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vMyPos);

}

HRESULT CHatch::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Dynamic
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Hatch");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;


	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CHatch::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);
	pEffect->SetInt("numBoneInf", 1);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	return NOERROR;
}

void CHatch::ComunicateWithServer()
{
	m_iNumber = server_data.Game_Data.iHatch;
}

CHatch * CHatch::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CHatch*	pInstance = new CHatch(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CHatch Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CHatch::Clone_GameObject()
{
	CHatch*	pInstance = new CHatch(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CHatch Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CHatch::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_HATCH, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
