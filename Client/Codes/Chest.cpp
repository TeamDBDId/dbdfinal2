#include "stdafx.h"
#include "Chest.h"
#include "Management.h"
#include "Light_Manager.h"
#include "Item.h"
_USING(Client)

CChest::CChest(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CChest::CChest(const CChest & rhs)
	: CGameObject(rhs)
	,m_SoundList(rhs.m_SoundList)
{

}


HRESULT CChest::Ready_Prototype()
{
	SetUp_Sound();

	return NOERROR;
}

HRESULT CChest::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_CurState = Idle;
	m_pMeshCom->Set_AnimationSet(Idle);
	m_fSoundTimer = 0.f;
	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 15.f;
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CChest::Update_GameObject(const _float & fTimeDelta)
{
	//cout << m_iMapIndex << endl;
	
	if (m_isDead)
		return 1;

	m_fDeltaTime = fTimeDelta;
	//m_iCurAnimation = m_CurState;

	ComunicateWithServer();
	State_Check();

	if (m_fMaxProgressTime < m_fProgressTime)
		m_isBool = true;

	return _int();
}

_int CChest::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 6000.f)
		return 0;

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_matrix matWorld = m_pTransformCom->Get_Matrix();
	vPosition.y += 35.f;
	matWorld._41 = vPosition.y;
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);
	if (m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, 120.f))
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;
		if (FAILED(m_pRendererCom->Add_StemGroup(this)))
			return -1;
	}

	return _int();
}

void CChest::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	m_pMeshCom->Play_Animation(m_fDeltaTime);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);
			
			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	//m_pColliderCom->Render_Collider();
	Safe_Release(pEffect);
}

void CChest::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(1);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetMatrix("g_matVP", VP);
			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CChest::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CChest::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	
	_vec3 vFront = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vFront, &vFront);

	m_vPosArr = new _vec3[1];
	m_vPosArr[DIR::FRONT] = vFront;


	m_pColliderCom->Set_Dist(90.f,1.f);
	Set_Cloth();
	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_CHEST, this);
}

_matrix CChest::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CChest::Get_Key()
{
	return m_Key.c_str();
}

_int CChest::Get_OtherOption()
{
	return m_iOtherOption;
}

void CChest::State_Check()
{
	Update_Sound();
	if (m_CurState == m_OldState)
		return;
	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CChest::Set_Cloth()
{
	GET_INSTANCE_MANAGEMENT;
	CGameObject* pGameObject = nullptr;
	pManagement->Add_GameObjectToLayer(L"GameObject_StaticMap", SCENE_STAGE, L"Layer_Map_Static", &pGameObject);
	pGameObject->Set_Base(L"Mesh_ChestCloth", m_pTransformCom->Get_Matrix());
	Safe_Release(pManagement);
}

void CChest::ComunicateWithServer()
{
	//if (m_bInitItem)
	//	return;

	m_fProgressTime = server_data.Game_Data.ItemBox[m_eObjectID - CHEST];
	m_CurState = (CChest::STATE)server_data.Game_Data.ItemBoxAnim[m_eObjectID - CHEST];
	if (!m_bInitItem && server_data.Game_Data.ItemBox[m_eObjectID - CHEST] > 1000.f)
	{
		m_bInitItem = true;
		_int IndexType = _int((server_data.Game_Data.ItemBox[m_eObjectID - CHEST] - 1000.f));
		_int ItemIndex = _int(IndexType * 0.1f);
		_int ItemType = _int(IndexType % 10);

		GET_INSTANCE_MANAGEMENT;

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Item", SCENE_STAGE, L"Layer_Item")))
			return;

		CItem* pItem = (CItem*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Item").back();
		pItem->SetID(ITEM + ItemIndex);
		_int Durability = (rand() % 20) + 20;
		_vec3	vPosition = *(_vec3*)m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
		vPosition.y += 30.f;
		pItem->Ready_Item((CItem::ITEMTYPE)ItemType, (_float)Durability, &vPosition);

		Safe_Release(pManagement);
	}

	if (m_CurState != Idle && m_fProgressTime < 15.f)
	{
		_uint iCount = 0;
		for (int i = 0; i < 4; ++i)
		{
			if (i == (exPlayerNumber - 1))
				continue;
			if (server_data.Campers[i].InterationObject != m_eObjectID)
				++iCount;
		}

		if (iCount == 0)
			m_CurState = Idle;
	}
}

void CChest::SetUp_Sound()
{
	Add_SoundList(Open, 0.3f, "Chest/chest_open_01.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.08f,"Chest/chest_open_02.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.2f, "Chest/chest_open_03.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.3f, "Chest/chest_open_04.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.4f, "Chest/chest_open_05.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.5f, "Chest/chest_open_06.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.6f, "Chest/chest_open_07.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.7f, "Chest/chest_open_02.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.8f, "Chest/chest_open_02.mp3", 1700.f, 2.f, 0);
	Add_SoundList(IdleOpen, 0.9f, "Chest/chest_open_04.mp3", 1700.f, 2.f, 0);

	Add_SoundList(Closing, 48.7f/61.f, "Chest/chest_close_01.mp3", 1700.f, 2.f, 0);

}

void CChest::Update_Sound()
{
	_float Ratio;
	if (m_CurState == Closing || m_CurState == Open)
	{
		_float TimeAcc = (_float)m_pMeshCom->GetTimeAcc();
		_float Period = (_float)m_pMeshCom->GetPeriod(m_CurState);
		Ratio = TimeAcc / Period;
	}
	else
		Ratio = m_fProgressTime / m_fMaxProgressTime;

	if (m_CurState != m_OldState)
		m_iSoundCheck = 0;	//�ߺ�üũ
	else
	{
		m_fRatio = Ratio;
		_int SoundCount = 0;
		for (auto& iter : m_SoundList)
		{
			if (m_CurState == iter.m_iState)
			{
				if (iter.OtherOption != 0)
					continue;
				if (Ratio >= iter.Ratio && SoundCount == m_iSoundCheck)
				{
					GET_INSTANCE(CSoundManager)->PlaySound(iter.SoundName, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), iter.SoundDistance, iter.SoundValue);
					m_iSoundCheck++;
					return;
				}
				SoundCount++;
			}
		}
	}
}

void CChest::Add_SoundList(const _uint & m_iState, const _float Ratio, const char * SoundName, const _float & SoundDistance, const _float & SoundValue, const _int & OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundDistance = SoundDistance;
	Data.SoundValue = SoundValue;
	Data.OtherOption = OtherOption;

	m_SoundList.push_back(Data);
}

HRESULT CChest::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Dynamic
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Chest");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;


	m_fRad = 120.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);



	//For.Com_Collider
	/*m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_OBB, matWorld, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);*/

	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		_MSG_BOX("111");
	if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, L"Mesh_Chest")))
		_MSG_BOX("111");




	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CChest::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);

	return NOERROR;
}


CChest * CChest::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CChest*	pInstance = new CChest(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CChest Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CChest::Clone_GameObject()
{
	CChest*	pInstance = new CChest(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CChest Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CChest::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_CHEST, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
