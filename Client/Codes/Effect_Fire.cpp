#include "stdafx.h"
#include "Effect_Fire.h"
#include "Management.h"

#include "Effect_FireCore.h"
#include "Effect_FireSmoke.h"


#include "Math_Manager.h"
_USING(Client)

CEffect_Fire::CEffect_Fire(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_Fire::CEffect_Fire(const CEffect_Fire & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Fire::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Fire::Ready_GameObject()
{

	return NOERROR;
}

_int CEffect_Fire::Update_GameObject(const _float & _fTick)
{

	Compute_CameraDistance(&m_vPos);

	if (4000.f < m_fCameraDistance)
		return NOERROR;

	Make_Core(_fTick);
	Make_Smoke(_fTick);
	



	return _int();
}

_int CEffect_Fire::LastUpdate_GameObject(const _float & _fTick)
{



	return _int();
}

void CEffect_Fire::Render_GameObject()
{


}


void CEffect_Fire::Set_Param(const _vec3 & _vPos, const _uint & _iType)
{
	m_vPos = _vPos;
	m_iType = _iType;

	if (0 == m_iType)
	{
		COREDELAY = 0.3f;
		SMOKEDELAY = 0.7f;
	}
	else
	{
		COREDELAY = 0.15f;
		SMOKEDELAY = 0.35f;
	}

}

void CEffect_Fire::Make_Core(const _float& _fTick)
{
	m_fCoreDelay += _fTick;
	if (m_fCoreDelay<COREDELAY)
		return;
	m_fCoreDelay = 0.f;


	GET_INSTANCE_MANAGEMENT;

	CEffect_FireCore* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_FireCore", SCENE_STAGE, L"Layer_FireCore", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_FireCore");
	pEffect->Set_Param(m_vPos, &m_iType);


	Safe_Release(pManagement);

}

void CEffect_Fire::Make_Smoke(const _float& _fTick)
{
	m_fSmokeDelay += _fTick;
	if (m_fSmokeDelay<SMOKEDELAY)
		return;
	m_fSmokeDelay = 0.f;


	GET_INSTANCE_MANAGEMENT;


	CEffect_FireSmoke* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_FireSmoke", SCENE_STAGE, L"Layer_FireSmoke", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_FireSmoke");
	pEffect->Set_Param(m_vPos, &m_iType);
	

	Safe_Release(pManagement);
}


// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Fire * CEffect_Fire::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Fire*	pInst = new CEffect_Fire(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Fire::Clone_GameObject()
{
	CEffect_Fire*	pInst = new CEffect_Fire(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_Fire Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Fire::Free()
{

	CGameObject::Free();
}