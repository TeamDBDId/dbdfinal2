

// 컨스턴트 테이블
matrix		g_matWorld, g_matView, g_matProj;
vector		g_vColor;

int			g_iColumn;
int			g_iRow;


float		g_fAlpha;

float		g_fDot;

texture		g_Tex0;
sampler Tex0Sampler = sampler_state
{
	texture = g_Tex0;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};



struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};


struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	float2	vSubUV : TEXCOORD1;
};


VS_OUT VS_Default(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matrix matProj = g_matProj;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;


	return Out;
}

VS_OUT VS_SubUV(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV.x = (In.vTexUV.x + g_iRow)*g_fDot;
	Out.vSubUV.y = (In.vTexUV.y + g_iColumn)*g_fDot;
	return Out;
}


struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	float2	vSubUV : TEXCOORD1;
};


struct PS_OUT
{
	vector	vColor : COLOR;
};


PS_OUT PS_Blood_Default(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;//초기화

	vector	vDiffuseColor = tex2D(Tex0Sampler, In.vTexUV);
	Out.vColor = vector(vDiffuseColor.r, 0.f, 0.f, vDiffuseColor.r*g_fAlpha);
	return Out;
}


PS_OUT PS_Blood_SubUV(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;//초기화

	vector	vDiffuseColor = tex2D(Tex0Sampler, In.vSubUV);
	Out.vColor = vector(vDiffuseColor.r, 0.f, 0.f, vDiffuseColor.r*g_fAlpha);


	return Out;
}


technique	DefaultDevice
{

	pass Default//0
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		
		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_Blood_Default();
	}
	pass UV//1
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV();
		PixelShader = compile ps_3_0 PS_Blood_SubUV();
	}

	pass Ball//2
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV();
		PixelShader = compile ps_3_0 PS_Blood_SubUV();
	}
}

