

// 컨스턴트 테이블
matrix		g_matWorld, g_matView, g_matProj , g_matVInv;
vector		g_vColor;

int			g_iColumn;
int			g_iRow;

float		g_fTime;

float		g_fSize;
float		g_fSin;

float		g_fAlpha;

float		g_fDot;

texture		g_DepthTexture;
sampler DepthSampler = sampler_state
{
	texture = g_DepthTexture;	
};


texture		g_Tex0;
sampler Tex0Sampler = sampler_state
{
	texture = g_Tex0;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_Tex1;
sampler Tex1Sampler = sampler_state
{
	texture = g_Tex1;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_Tex2;
sampler Tex2Sampler = sampler_state
{
	texture = g_Tex2;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
texture		g_Tex3;
sampler Tex3Sampler = sampler_state
{
	texture = g_Tex3;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_Tex4;
sampler Tex4Sampler = sampler_state
{
	texture = g_Tex4;
};


struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_IN2
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;
	float3	vNormal : NORMAL;
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
	vector	vViewPos : TEXCOORD3;
};
struct VS_OUT2
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
	vector	vNormal : NORMAL;
};

VS_OUT VS_Default(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matrix matProj = g_matProj;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;


	return Out;
}

VS_OUT VS_SubUV8(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV.x = (In.vTexUV.x + g_iRow)*g_fDot;
	Out.vSubUV.y = (In.vTexUV.y + g_iColumn)*g_fDot;
	Out.vProjPos = Out.vPosition;
	Out.vViewPos = mul(vector(In.vPosition, 1.f), matWV);
	return Out;
}

VS_OUT VS_Flash(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV = float2(g_fAlpha, 0.5f);
	Out.vProjPos = Out.vPosition;

	return Out;
}


VS_OUT2 VS_Leaf(VS_IN2 In)
{
	VS_OUT2			Out = (VS_OUT2)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV.x = (In.vTexUV.x + g_iRow)*0.5f;
	Out.vSubUV.y = (In.vTexUV.y + g_iColumn)*0.5;
	Out.vProjPos = Out.vPosition;
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), g_matWorld));
	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
	vector	vViewPos : TEXCOORD3;
};
struct PS_IN2
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float2	vSubUV : TEXCOORD2;
	vector	vNormal : NORMAL;
};

struct PS_OUT
{
	vector	vColor : COLOR;
};
struct PS_OUT2
{
	vector	vColor : COLOR;
	vector	vNormal : COLOR1;
	vector	vDepth : COLOR2;
};


PS_OUT PS_Death(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV);
	//vector	vTex1Color = tex2D(Tex1Sampler, In.vTexUV);
	//vector	vTex2Color = tex2D(Tex2Sampler, float2(vTex1Color.r,0.5f));
	
	
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;	

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = float4(0.f,0.f,0.f, vTex0Color.a);
	Out.vColor.a = Out.vColor.a * saturate(fViewZ - In.vProjPos.w);


	/*if (0.6f < g_fTime)
	{
		vTex1Color.a = vTex1Color.r;
		vTex1Color = vTex1Color.r*vTex2Color;
		Out.vColor += vTex1Color;
	}*/
	if (1.5f < g_fTime)
	{
		Out.vColor.a *= (2.f - g_fTime)*2.f;
	}


	return Out;
}

PS_OUT PS_ExitBG(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector vTex0Color = tex2D(Tex0Sampler, In.vTexUV);

	Out.vColor = float4(0.f, 0.f, 0.f, vTex0Color.r);




	return Out;
}


PS_OUT PS_ExitDefault(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector vTex0Color = tex2D(Tex0Sampler, In.vTexUV);
	vector vTex1Color = tex2D(Tex1Sampler, In.vTexUV);


	Out.vColor = vTex1Color;
	Out.vColor.a = vTex1Color.r;




	if (0 != vTex0Color.r)
	{
		Out.vColor = vTex0Color;
	}

	Out.vColor.b *= 0.9f;



	float fX = In.vTexUV.x - 0.5f;
	float fY = In.vTexUV.y - 0.5f;
	float fPow = pow(fX, 2) + pow(fY, 2);
	if (0.25f*(1 - g_fSize) < fPow)
		Out.vColor.a = 0.f;


	return Out;
}


PS_OUT PS_FailBG(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector vTex0Color = tex2D(Tex0Sampler, In.vTexUV);

	Out.vColor = float4(0.f, 0.f, 0.f, vTex0Color.r*3.f);


	return Out;
}



PS_OUT PS_FailDefault(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector vTex0Color = tex2D(Tex0Sampler, In.vTexUV);
	vector vTex1Color = tex2D(Tex1Sampler, float2(vTex0Color.r*0.985f, 0.5f));
	vector vTex2Color = tex2D(Tex2Sampler, In.vTexUV);
	vector vTex3Color = tex2D(Tex3Sampler, In.vTexUV);


	vTex0Color -= g_fSize*0.5f;
	Out.vColor = saturate(vTex1Color - (vTex2Color - g_fSin)) - vTex3Color*0.3f;
	Out.vColor.a = vTex0Color.r*1.5f;



	return Out;
}


PS_OUT PS_FireCore(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vTexUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = vTex0Color;
	Out.vColor.a = Out.vColor.a * saturate(fViewZ - In.vProjPos.w);// *Out.vColor.r;


	return Out;
}


PS_OUT PS_FireSmoke(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;



	Out.vColor = float4(0.f, 0.f, 0.f, vTex0Color.a*3.f);
	Out.vColor.a = Out.vColor.a * saturate(fViewZ - In.vProjPos.w)*g_fAlpha;


	return Out;
}



PS_OUT PS_GeneSmoke(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = float4(1.f, 1.f, 1.f, vTex0Color.a*g_fAlpha);

	return Out;
}

PS_OUT PS_GeneFlash(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;


	vector	vTex0Color = tex2D(Tex0Sampler, In.vTexUV);
	vector	vTex1Color = tex2D(Tex1Sampler, In.vSubUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = float4(vTex1Color.rgb, vTex0Color.r*0.4f);

	return Out;
}


PS_OUT PS_JumpSmoke(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV);
	//float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	//float2	vUV;

	//vUV.x = vProjPos.x * 0.5f + 0.5f;
	//vUV.y = vProjPos.y * -0.5f + 0.5f;

	//vector	vDepthInfo = tex2D(DepthSampler, vUV);

	//float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = float4(1.f, 1.f, 1.f, vTex0Color.a*0.2f*g_fAlpha);
	//Out.vColor.a = Out.vColor.a * saturate(fViewZ - In.vProjPos.w)*;

	return Out;
}

PS_OUT2 PS_Leaf(VS_OUT2 In)
{
	PS_OUT2		Out = (PS_OUT2)0;

	Out.vColor = tex2D(Tex0Sampler, In.vSubUV);

	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);

	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.f, 0.f, 0.f);

	Out.vColor.r *= 0.5f;
	Out.vColor.g *= 0.6f;
	Out.vColor.b *= 0.5f;

	return Out;
}


PS_OUT PS_Mist(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV);

	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;
	//float2	vTexUV = In.vTexUV;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	Depth = vDepthInfo.g;// *30000.f;

	float3 ViewRay = In.vViewPos.xyz*(30000.f / In.vViewPos.z);
	float4 ViewPos = float4(ViewRay*Depth, 1.f);

	float4 ObjPos = mul(ViewPos, g_matVInv);




	Out.vColor = float4(1.f, 1.f, 1.f, vTex0Color.a);
	Out.vColor.a = Out.vColor.a * g_fAlpha;


	if (ObjPos.y < 70.f)
	{
		Out.vColor.a *= ObjPos.y / 70.f;
	}


	return Out;
}


PS_OUT PS_PlayerChange(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex0Color = tex2D(Tex0Sampler, In.vSubUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = float4(0.f, 0.f, 0.f, vTex0Color.a);
	Out.vColor.a = Out.vColor.a * saturate(fViewZ - In.vProjPos.w);

	return Out;
}

PS_OUT PS_Particle(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vTex4Color = tex2D(Tex4Sampler, In.vTexUV);
	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 30000.f;

	Out.vColor = vTex4Color;// float4(vTex0Color.r, vTex0Color.r, vTex0Color.r, vTex0Color.r*10.f);
	Out.vColor.a = Out.vColor.a * saturate(fViewZ - In.vProjPos.w)*vTex4Color.r;

	return Out;
}


PS_OUT2 PS_BGMesh(VS_OUT2 In)
{
	PS_OUT2		Out = (PS_OUT2)0;

	Out.vColor = tex2D(Tex0Sampler, In.vTexUV);

	Out.vColor = vector(Out.vColor.r, Out.vColor.r*0.97f, Out.vColor.r*0.5f,0.5f);

	return Out;
}




technique	DefaultDevice
{
	pass Death//0
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		
		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV8();
		PixelShader = compile ps_3_0 PS_Death();
	}
	pass ExitBG//1
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_ExitBG();
	}

	pass ExitDefault//2
	{
		ZEnable = false;
		ZWriteEnable = true;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_ExitDefault();
	}

	pass FailBG//3
	{
		ZEnable = false;
		ZWriteEnable = true;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_FailBG();
	}


	pass FailDefault//4
	{
		ZEnable = false;
		ZWriteEnable = true;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_FailDefault();
	}

	pass Fire_Core//5
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = ONE;
		DestBlend = ONE;
		blendop = add;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV8();
		PixelShader = compile ps_3_0 PS_FireCore();
	}

	pass Fire_Smoke//6
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_SubUV8();
		PixelShader = compile ps_3_0 PS_FireSmoke();
	}

	pass Gene_Smoke//7
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_SubUV8();
		PixelShader = compile ps_3_0 PS_GeneSmoke();
	}

	pass Gene_Flash//8
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_Flash();
		PixelShader = compile ps_3_0 PS_GeneFlash();
	}

	pass JumpSmoke//9
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV8();
		PixelShader = compile ps_3_0 PS_JumpSmoke();
	}

	pass Leaf//10
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = none;


		VertexShader = compile vs_3_0 VS_Leaf();
		PixelShader = compile ps_3_0 PS_Leaf();
	}

	pass Mist//11
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV8();
		PixelShader = compile ps_3_0 PS_Mist();
	}

	pass PlayerChange//12
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV8();
		PixelShader = compile ps_3_0 PS_PlayerChange();
	}

	pass Particle//13
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = none;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_Particle();
	}

	pass BGMesh//14
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_BGMesh();
	}
}

