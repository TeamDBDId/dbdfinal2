
matrix		g_matWVP;
texture		g_DiffuseTexture;
vector		g_LightColor = vector(1,1,1,1);

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;

};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	Out.vPosition = mul(vector(In.vPosition, 1.f), g_matWVP);
	Out.vTexUV = In.vTexUV;

	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;	

};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
};

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.vDiffuse.xyz = g_LightColor.xyz;
	Out.vDiffuse.a = tex2D(DiffuseSampler, In.vTexUV).a;

	return Out;
}

PS_OUT PS_OBJECT(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.vDiffuse = vector(0, 0, 0, 1);

	return Out;
}

technique	DefaultDevice
{
	pass Light
	{
		ZEnable = true;
		ZWriteEnable = true;

		//AlphaBlendEnable = true;
		//SrcBlend = one;
		//DestBlend = one;

		AlphaBlendEnable = true;
		SrcBlend = one;
		DestBlend = one;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}
	pass Objtect
	{
		ZEnable = true;
		ZWriteEnable = true;

		AlphaBlendEnable = true;

		SrcBlend = one;
		DestBlend = one;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}