int			numBoneInf = 4;
matrix		g_matWorld, g_matVP;
texture		g_DiffuseTexture;
texture		g_NormalTexture;
texture		g_CubeTexture;
texture		g_AOTexture;
texture		g_MetalicTexture;
texture		g_RoughnessTexture;

vector		g_DiffuseColor = vector(1, 0, 0, 1);
float		g_fTimeAcc;
float		g_fFloorHeight = 0.f;
float		g_Thicness;

bool		g_isFootPrint = true;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler NormalSampler = sampler_state
{
	texture = g_NormalTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler CubeSampler = sampler_state
{
	texture = g_CubeTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler MetalSampler = sampler_state
{
	texture = g_MetalicTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler RoughSampler = sampler_state
{
	texture = g_RoughnessTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

sampler AOSampler = sampler_state
{
	texture = g_AOTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_SkinTexture;
sampler SkinSampler = sampler_state
{
	texture = g_SkinTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_LightEmissive;
sampler LESampler = sampler_state
{
	texture = g_LightEmissive;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_DissolveTexture;
sampler DissolveSampler = sampler_state
{
	texture = g_DissolveTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_DissolveEffect;
sampler DisEffectSampler = sampler_state
{
	texture = g_DissolveEffect;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture		g_HairTexture;
sampler HairSampler = sampler_state
{
	texture = g_HairTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float3	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD;
	float3	vTangent : TANGENT;
};

struct VS_OUT_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	vector	vTangent : TANGENT;
};

VS_OUT_PHONG VS_MAIN_PHONG(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), g_matWorld));
	Out.vTangent = normalize(mul(vector(In.vTangent, 0.f), g_matWorld));
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	return Out;
}

struct VS_IN_SKIN
{
	float3	vPosition : POSITION;
	float3	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	float3	vTangent : TANGENT;
	float4	weights : BLENDWEIGHT0;
	int4	boneIndices : BLENDINDICES0;

};

float4x4 GetSkinMatrix(int idx)
{
	float4 uv = float4(((float)((idx % 32) * 4) + 0.5f) / 128.f, ((float)((idx / 32)) + 0.5f) / 128.f, 0.0f, 0.0f);

	float4x4 mat =
	{
		tex2Dlod(SkinSampler, uv),
		tex2Dlod(SkinSampler, uv + float4(1.0f / 128.f, 0, 0, 0)),
		tex2Dlod(SkinSampler, uv + float4(2.0f / 128.f, 0, 0, 0)),
		tex2Dlod(SkinSampler, uv + float4(3.0f / 128.f, 0, 0, 0))
	};

	return mat;
}

VS_OUT_PHONG VS_MAIN_SKIN(VS_IN_SKIN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vTexUV = In.vTexUV;

	float lastweight = 0.f;
	int n = numBoneInf - 1;
	In.vNormal = normalize(In.vNormal);

	for (int i = 0; i < n; ++i)
	{
		lastweight += In.weights[i];
		Out.vPosition += In.weights[i] * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vNormal += In.weights[i] * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vTangent += In.weights[i] * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[i]));
	}

	lastweight = 1.f - lastweight;
	Out.vPosition += lastweight * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vNormal += lastweight * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vTangent += lastweight * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[n]));

	Out.vPosition = mul(Out.vPosition, matWVP);
	Out.vNormal = normalize(Out.vNormal);
	Out.vNormal = normalize(mul(vector(Out.vNormal.xyz, 0.f), g_matWorld));
	Out.vTangent = normalize(Out.vTangent);
	Out.vTangent = normalize(mul(vector(Out.vTangent.xyz, 0.f), g_matWorld));
	Out.vProjPos = Out.vPosition;

	return Out;
}

struct VS_OUT_SKILL
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float	fWorldH : TEXCOORD2;
	vector	vTangent : TANGENT;
};

VS_OUT_SKILL VS_MAIN_SKINSKILL(VS_IN_SKIN In)
{
	VS_OUT_SKILL			Out = (VS_OUT_SKILL)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vTexUV = In.vTexUV;

	float lastweight = 0.f;
	int n = numBoneInf - 1;
	In.vNormal = normalize(In.vNormal);

	for (int i = 0; i < n; ++i)
	{
		lastweight += In.weights[i];
		Out.vPosition += In.weights[i] * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vNormal += In.weights[i] * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vTangent += In.weights[i] * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[i]));
	}

	lastweight = 1.f - lastweight;
	Out.vPosition += lastweight * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vNormal += lastweight * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vTangent += lastweight * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[n]));

	Out.fWorldH = mul(Out.vPosition, g_matWorld).y;

	Out.vPosition = mul(Out.vPosition, matWVP);
	Out.vNormal = normalize(Out.vNormal);
	Out.vNormal = normalize(mul(vector(Out.vNormal.xyz, 0.f), g_matWorld));
	Out.vTangent = normalize(Out.vTangent);
	Out.vTangent = normalize(mul(vector(Out.vTangent.xyz, 0.f), g_matWorld));
	Out.vProjPos = Out.vPosition;

	return Out;
}

VS_OUT_SKILL VS_MAIN_SKINSKILLSTATIC(VS_IN In)
{
	VS_OUT_SKILL			Out = (VS_OUT_SKILL)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), g_matWorld));
	Out.vTangent = normalize(mul(vector(In.vTangent, 0.f), g_matWorld));
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	Out.fWorldH = mul(In.vPosition, g_matWorld).y;

	Out.vPosition = mul(Out.vPosition, matWVP);
	Out.vProjPos = Out.vPosition;

	return Out;
}

struct PS_IN_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	vector	vTangent : TANGENT;
};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
	vector	vNormal : COLOR1;
	vector	vDepth : COLOR2;
	vector	vRMAO : COLOR3;
};

PS_OUT PS_MAIN_PHONG(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;


	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w /30000.f , g_isFootPrint, 0.f);
	//if (g_isFootPrint)
	//	Out.vDepth.a = 1.f;

		
	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.7f; 

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz = AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse.xyz *= AO;

	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN); 
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;

	Out.vRMAO.r = tex2D(RoughSampler, In.vTexUV) * 0.5f;
	Out.vRMAO.g = tex2D(MetalSampler, In.vTexUV);

	//Out.vRMAO.b = g_isFootPrint;
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	//Out.fFoot = g_isFootPrint;



	return Out;
}

PS_OUT PS_MAIN_PHONG_CROSS(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);
	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz = AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);
	Out.vDiffuse *= AO;

	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);
	Out.vRMAO.w = 1.f;

	//Out.vRMAO.b = g_isFootPrint;
	return Out;
}

PS_OUT PS_MAIN_EMI(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);
	float  LightEmissive = 0.f;
	LightEmissive = pow(tex2D(LESampler, In.vTexUV), 2.2).x * 1.5f;

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	if (LightEmissive > 0.1f)
		Color.xyz *= LightEmissive * 10.f;

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, LightEmissive);


	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;

	AO.xyz = max(6.10352e-5, AO.xyz);
	AO.xyz = AO.xyz > 0.04045 ? pow(AO.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : AO.xyz * (1.0 / 12.92);

	Out.vDiffuse *= AO;

	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;
	//

	Out.vRMAO.x = tex2D(RoughSampler, In.vTexUV);
	Out.vRMAO.y = tex2D(MetalSampler, In.vTexUV);
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	//Out.vRMAO.b = g_isFootPrint;
	return Out;
}

float3x3 Kx = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };
float3x3 Ky = { 1, 2, 1, 0, 0, 0, -1, -2, -1 };
vector pixelOffset = {1.f / 1280.f, 1.f / 720.f, 0.f, 0.f, };
PS_OUT PS_MAIN_STENCIL(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	float Lx = 0;
	float Ly = 0;

	for (int y = -1; y <= 1; ++y)
	{
		for (int x = -1; x <= 1; ++x)
		{
			float2 offset = float2(x, y) * pixelOffset;
			float3 tex = tex2D(DiffuseSampler, In.vTexUV + offset).rgb;
			float luminance = dot(tex, float3(0.3, 0.59, 0.11));

			Lx += luminance * Kx[y + 1][x + 1];
			Ly += luminance * Ky[y + 1][x + 1];
		}
	}
	
	//Out.vDiffuse = Color;

	// N : Normal V : 시선방향
	//float RimColor = smoothstep(0.9f, 1.f, 1.f - max(0, dot(N, V))) * 0.05f;
	float L = sqrt((Lx*Lx) + (Ly*Ly));
	Out.vDiffuse = float4(g_DiffuseColor.x + L.x, g_DiffuseColor.y, g_DiffuseColor.z, g_DiffuseColor.w);
	return Out;
}

struct PS_IN_SKILL
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	float	fWorldH : TEXCOORD2;
	vector	vTangent : TANGENT;
};

PS_OUT PS_MAIN_EXITDOORSTENCIL(PS_IN_SKILL In)
{
	PS_OUT		Out = (PS_OUT)0;

	if (In.fWorldH > 180.f)
		return Out;

	float Lx = 0;
	float Ly = 0;

	for (int y = -1; y <= 1; ++y)
	{
		for (int x = -1; x <= 1; ++x)
		{
			float2 offset = float2(x, y) * pixelOffset;
			float3 tex = tex2D(DiffuseSampler, In.vTexUV + offset).rgb;
			float luminance = dot(tex, float3(0.3, 0.59, 0.11));

			Lx += luminance * Kx[y + 1][x + 1];
			Ly += luminance * Ky[y + 1][x + 1];
		}
	}

	//Out.vDiffuse = Color;

	// N : Normal V : 시선방향
	//float RimColor = smoothstep(0.9f, 1.f, 1.f - max(0, dot(N, V))) * 0.05f;
	float L = sqrt((Lx*Lx) + (Ly*Ly));
	Out.vDiffuse = float4(g_DiffuseColor.x + L.x, g_DiffuseColor.y, g_DiffuseColor.z, g_DiffuseColor.w);
	return Out;
}

PS_OUT PS_MAIN_NOISE(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.vDiffuse = vector(0, 0, 0, 0);
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 1.5f, 0.f);


	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);

	return Out;
}

PS_OUT PS_MAIN_SKINEDMESH(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);
	Color.xyz *= 3.f;

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 1.f);

	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.3f + 0.3f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x = AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse.xyz *= AO.x;
	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;
	//

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;
	//Out.vRMAO.z = tex2D(AOSampler, In.vTexUV);

	return Out;
}

PS_OUT PS_MAIN_DISSOLVE(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	float DissolveTex = tex2D(DissolveSampler, In.vTexUV).g;
	vector DissolveEffect = tex2D(DisEffectSampler, In.vTexUV);
	float ClipAmount = DissolveTex - g_fTimeAcc;

	if (ClipAmount >= 0.f && ClipAmount < 0.2f)
	{
		Out.vDiffuse = DissolveEffect;
		Out.vDiffuse.xyz = max(6.10352e-5, Out.vDiffuse.xyz);
		Out.vDiffuse.xyz = Out.vDiffuse.xyz > 0.04045 ? pow(Out.vDiffuse.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Out.vDiffuse.xyz * (1.0 / 12.92);
		Out.vDiffuse.xyz *= 15.f;
		Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 1.5f);

		Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
		Out.vRMAO = vector(0, 0, 0, 1);
		//vector AO = tex2D(AOSampler, In.vTexUV);
		Out.vRMAO.x = /*AO.y*/0.f;
		Out.vRMAO.y = /*AO.z*/0.f;
		return Out;
	}

	if (ClipAmount < 0.0f)
		return Out;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);
	Color.xyz *= 3.f;

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);

	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x = AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse.xyz *= AO.x;

	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = normalize(mul(TBN, tangentNormal));
	Out.vNormal.w = 0;
	//

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;

	return Out;
}

PS_OUT PS_MAIN_DISSOLVEKNIFE(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector DissolveEffect = tex2D(DisEffectSampler, In.vTexUV);
	float ClipAmount = DissolveEffect.g - g_fTimeAcc;

	//if (ClipAmount >= 0.f && ClipAmount < 0.2f)
	//{
	//	Out.vDiffuse = DissolveEffect;
	//	Out.vDiffuse.xyz = max(6.10352e-5, Out.vDiffuse.xyz);
	//	Out.vDiffuse.xyz = Out.vDiffuse.xyz > 0.04045 ? pow(Out.vDiffuse.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Out.vDiffuse.xyz * (1.0 / 12.92);

	//	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.0f);
	//	Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	//	Out.vRMAO = vector(0, 0, 0, 1);
	//	//vector AO = tex2D(AOSampler, In.vTexUV);
	//	Out.vRMAO.x = /*AO.y*/0.f;
	//	Out.vRMAO.y = /*AO.z*/0.f;
	//	return Out;
	//}

	if (ClipAmount < 0.0f)
		return Out;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);
	Color.xyz *= 3.f;

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);

	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x = AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse.xyz *= AO.x;

	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = normalize(mul(TBN, tangentNormal));
	Out.vNormal.w = 0;
	//

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;

	return Out;
}

PS_OUT PS_MAIN_DISSOLVESKILL(PS_IN_SKILL In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector DissolveEffect = tex2D(DisEffectSampler, In.vTexUV * 10.f);
	float ClipAmount = ((In.fWorldH - g_fFloorHeight) / 180.f) - g_fTimeAcc;

	if (ClipAmount >= 0.f && ClipAmount < 0.8f)
	{
		Out.vDiffuse = DissolveEffect;
		Out.vDiffuse.xyz = max(6.10352e-5, Out.vDiffuse.xyz);
		Out.vDiffuse.xyz = Out.vDiffuse.xyz > 0.04045 ? pow(Out.vDiffuse.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Out.vDiffuse.xyz * (1.0 / 12.92);
		Out.vDiffuse *= 3.5f;
		Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 1.1f);
		//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
		Out.vRMAO = vector(0, 0, 0, 1);

		float4 final = Out.vDiffuse;
		float4 col;

		for (int x = -6; x <= 6; ++x)
		{
			float2 T = In.vTexUV;
			T.x += (2.f * x) / 1024.f;
			col = tex2D(DisEffectSampler, T * 10.f) * exp((-x*x) / 12.f);
			col.xyz = max(6.10352e-5, col.xyz);
			col.xyz = col.xyz > 0.04045 ? pow(col.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : col.xyz * (1.0 / 12.92);

			col.xyz *= 15.f;
			final.xyz = lerp(final, col, 0.5f).xyz;
		}
		for (int x = -3; x <= 3; ++x)
		{
			float2 T = In.vTexUV;
			T.y += (2.f * x) / 1024.f;
			col = tex2D(DisEffectSampler, T * 10.f) * exp((-x*x) / 6.f);
			col.xyz = max(6.10352e-5, col.xyz);
			col.xyz = col.xyz > 0.04045 ? pow(col.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : col.xyz * (1.0 / 12.92);

			col.xyz *= 15.f;
			final.xyz = lerp(final, col, 0.5f).xyz;
		}
		Out.vDiffuse = final;

		//
		float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
		tangentNormal = normalize(tangentNormal * 2 - 1);

		float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

		float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
		TBN = transpose(TBN);
		Out.vNormal.xyz = mul(TBN, tangentNormal);
		Out.vNormal.w = 0;
		//

		return Out;
	}

	if (ClipAmount >= 0.8f && ClipAmount < 0.81f)
	{
		vector vBorderAlpha = tex2D(DissolveSampler, In.vTexUV);
		Out.vDiffuse = DissolveEffect;
		Out.vDiffuse.xyz = max(6.10352e-5, Out.vDiffuse.xyz);
		Out.vDiffuse.xyz = Out.vDiffuse.xyz > 0.04045 ? pow(Out.vDiffuse.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Out.vDiffuse.xyz * (1.0 / 12.92);
		Out.vDiffuse *= 3.5f;
		Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 1.1f);
		//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
		Out.vRMAO = vector(0, 0, 0, 1);

		float4 final = Out.vDiffuse;
		float4 col;

		for (int x = -6; x <= 6; ++x)
		{
			float2 T = In.vTexUV;
			T.x += (2.f * x) / 1024.f;
			col = tex2D(DisEffectSampler, T * 10.f) * exp((-x*x) / 12.f);
			col.xyz = max(6.10352e-5, col.xyz);
			col.xyz = col.xyz > 0.04045 ? pow(col.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : col.xyz * (1.0 / 12.92);

			col.xyz *= 15.f;
			final.xyz = lerp(final, col, 0.5f).xyz;
		}
		for (int x = -3; x <= 3; ++x)
		{
			float2 T = In.vTexUV;
			T.y += (2.f * x) / 1024.f;
			col = tex2D(DisEffectSampler, T * 10.f) * exp((-x*x) / 6.f);
			col.xyz = max(6.10352e-5, col.xyz);
			col.xyz = col.xyz > 0.04045 ? pow(col.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : col.xyz * (1.0 / 12.92);

			col.xyz *= 15.f;
			final.xyz = lerp(final, col, 0.5f).xyz;
		}
		Out.vDiffuse = final;
		Out.vDiffuse.a = vBorderAlpha.r;

		//
		float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
		tangentNormal = normalize(tangentNormal * 2 - 1);

		float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

		float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
		TBN = transpose(TBN);
		Out.vNormal.xyz = mul(TBN, tangentNormal);
		Out.vNormal.w = 0;
		//

		return Out;
	}

	if (ClipAmount < 0.0f)
		return Out;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 0.f);

	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x = AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse.xyz *= AO.x;

	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;
	//

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;

	return Out;
}

PS_OUT PS_MAIN_DISTORB(PS_IN_SKILL In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = vector(0,0,0,0);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 5.0f, 0.f);
	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);

	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;
	//

	return Out;
}

struct PS_STEMP
{
	vector	vDiffuse : COLOR0;
};

PS_STEMP PS_OBJECT(PS_IN_PHONG In)
{
	PS_STEMP		Out = (PS_STEMP)0;

	Out.vDiffuse = vector(0, 0, 0, 1);



	return Out;
}

PS_STEMP PS_OBJECTKSIN(PS_IN_PHONG In)
{
	PS_STEMP		Out = (PS_STEMP)0;


	Out.vDiffuse = vector(0, 0, 0, 1);

	return Out;
}

struct VS_OUT_SHAPESKILL
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	vector	vTangent : TANGENT;
};

struct PS_IN_SHAPESKILL
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	vector	vTangent : TANGENT;
};

VS_OUT_SHAPESKILL VS_MAIN_SHAPESKIN(VS_IN_SKIN In)
{
	VS_OUT_SHAPESKILL		Out = (VS_OUT_SHAPESKILL)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vTexUV = In.vTexUV;

	float lastweight = 0.f;
	int n = numBoneInf - 1.f;
	In.vNormal = normalize(In.vNormal);

	for (int i = 0; i < n; ++i)
	{
		lastweight += In.weights[i];
		Out.vPosition += In.weights[i] * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vNormal += In.weights[i] * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vTangent += In.weights[i] * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[i]));
	}

	lastweight = 1.f - lastweight;
	Out.vPosition += lastweight * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vNormal += lastweight * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vTangent += lastweight * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[n]));

	Out.vPosition = mul(Out.vPosition, matWVP);
	//Out.vNormal = normalize(Out.vNormal);
	Out.vNormal = normalize(mul(vector(Out.vNormal.xyz, 0.f), g_matWorld));
	Out.vTangent = normalize(mul(vector(Out.vTangent.xyz, 0.f), g_matWorld));
	Out.vProjPos = Out.vPosition;

	return Out;
}

VS_OUT_SHAPESKILL VS_MAIN_SHAPESKINOUTLINE(VS_IN_SKIN In)
{
	VS_OUT_SHAPESKILL		Out = (VS_OUT_SHAPESKILL)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vTexUV = In.vTexUV;

	float lastweight = 0.f;
	int n = numBoneInf - 1.f;
	In.vNormal = normalize(In.vNormal);

	for (int i = 0; i < n; ++i)
	{
		lastweight += In.weights[i];
		Out.vPosition += In.weights[i] * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vNormal += In.weights[i] * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vTangent += In.weights[i] * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[i]));
	}

	lastweight = 1.f - lastweight;
	Out.vPosition += lastweight * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vNormal += lastweight * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vTangent += lastweight * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vPosition += Out.vNormal * 1.3f;

	Out.vPosition = mul(Out.vPosition, matWVP);
	//Out.vNormal = normalize(Out.vNormal);
	Out.vNormal = normalize(mul(vector(Out.vNormal.xyz, 0.f), g_matWorld));
	Out.vTangent = normalize(mul(vector(Out.vTangent.xyz, 0.f), g_matWorld));
	Out.vProjPos = Out.vPosition;

	return Out;
}

PS_OUT PS_MAIN_SHAPESKILL(PS_IN_SHAPESKILL In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.f, 1.f);
	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x = AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse *= AO.x;

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;

	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;
	//


	return Out;
}

PS_OUT PS_MAIN_SHAPESKILLOUTLINE(PS_IN_SHAPESKILL In)
{
	PS_OUT		Out = (PS_OUT)0;

	float vDecrColor = 1.f - g_fTimeAcc;
	vector Color = vector(1, vDecrColor, vDecrColor, 1);

	Color.xyz = Color.xyz;
	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 1.5f);
	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);
	Out.vRMAO = vector(0, 0, 0, 1);

	//
	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;
	//

	return Out;
}

PS_OUT PS_MAIN_SPIRITSKILL(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;
	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 1.f);

	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x = AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse *= AO.x;

	float Alpha = g_fTimeAcc;
	if (Alpha < 0)
	{
		Out.vDiffuse.a = 0;
		Out.vDepth = vector(0, 0, 0, 0);
		return Out;
	}
	Out.vDiffuse.a = Alpha;

	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;

	return Out;
}

VS_OUT_PHONG VS_MAIN_HAIR(VS_IN_SKIN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vTexUV = In.vTexUV;

	float lastweight = 0.f;
	int n = numBoneInf - 1;
	In.vNormal = normalize(In.vNormal);

	for (int i = 0; i < n; ++i)
	{
		lastweight += In.weights[i];
		Out.vPosition += In.weights[i] * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vNormal += In.weights[i] * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[i]));
		Out.vTangent += In.weights[i] * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[i]));
	}

	float3 HairInfo = tex2Dlod(HairSampler, float4(In.vTexUV, 0, 0)).rgb;

	lastweight = 1.f - lastweight;
	Out.vPosition += lastweight * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vNormal += lastweight * mul(vector(In.vNormal, 0.f), GetSkinMatrix(In.boneIndices[n]));
	Out.vTangent += lastweight * mul(vector(In.vTangent, 0.f), GetSkinMatrix(In.boneIndices[n]));

	Out.vPosition.x += pow(HairInfo.r, 2) * g_fTimeAcc * 10.f;
	Out.vPosition.z += pow(HairInfo.g, 2) * g_fTimeAcc * 10.f;

	Out.vPosition = mul(Out.vPosition, matWVP);
	Out.vNormal = normalize(Out.vNormal);
	Out.vNormal = normalize(mul(vector(Out.vNormal.xyz, 0.f), g_matWorld));
	Out.vTangent = normalize(Out.vTangent);
	Out.vTangent = normalize(mul(vector(Out.vTangent.xyz, 0.f), g_matWorld));
	Out.vProjPos = Out.vPosition;

	return Out;
}

PS_OUT PS_MAIN_HAIR(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector Color = vector(0,0,0,1);
	vector vHairMask = tex2D(DiffuseSampler, In.vTexUV);

	Out.vDiffuse = Color;
	if (vHairMask.r == 0)
	{
		Out.vDiffuse.a = 0;
		return Out;
	}

	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.0f, 1.f);

	Out.vRMAO = vector(0, 0, 0, 1);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.x = AO.x * 0.5f + 0.5f;

	AO.x = max(6.10352e-5, AO.x);
	AO.x = AO.x > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	Out.vDiffuse *= AO.x;

	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;

	Out.vRMAO.x = AO.y;
	Out.vRMAO.y = AO.z;

	return Out;
}
VS_OUT_PHONG VS_MAIN_TERRAIN(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWVP;

	matWVP = mul(g_matWorld, g_matVP);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), g_matWorld));
	Out.vTangent = normalize(mul(vector(1,0,0, 0.f), g_matWorld));
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	return Out;
}

PS_OUT PS_MAIN_TERRAIN(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	float2 vUV = In.vTexUV *	g_fTimeAcc;
	vector Color = tex2D(DiffuseSampler, vUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;

	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);

	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 1.f, 0.f);


	vector AO = tex2D(AOSampler, vUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;
	AO.xyz = max(6.10352e-5, AO.x);
	AO.xyz = AO.xyz > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	float vRough = tex2D(RoughSampler, vUV).r;
	Out.vDiffuse.xyz *= AO.xyz;

	Out.vRMAO = vector(vRough, 0, 0, 1);


	float3 tangentNormal = tex2D(NormalSampler, vUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;

	return Out;
}

PS_OUT PS_MAIN_REDCAMPER(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	float Lx = 0;
	float Ly = 0;

	for (int y = -1; y <= 1; ++y)
	{
		for (int x = -1; x <= 1; ++x)
		{
			float2 offset = float2(x, y) * pixelOffset;
			float3 tex = tex2D(DiffuseSampler, In.vTexUV + offset).rgb;
			float luminance = dot(tex, float3(0.3, 0.59, 0.11));

			Lx += luminance * Kx[y + 1][x + 1];
			Ly += luminance * Ky[y + 1][x + 1];
		}
	}

	//Out.vDiffuse = Color;

	// N : Normal V : 시선방향
	//float RimColor = smoothstep(0.9f, 1.f, 1.f - max(0, dot(N, V))) * 0.05f;
	float L = sqrt((Lx*Lx) + (Ly*Ly));
	Out.vDiffuse = float4(g_DiffuseColor.x + L.x, L.x * 0.3f, 0, g_DiffuseColor.w);
	return Out;
}

technique	DefaultDevice
{
	pass Phong // 0
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}

	pass CrossBuffer // 1
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x1f;

		CullMode = none;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG_CROSS();
	}

	pass SKIN // 2
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_SKINEDMESH();
	}

	pass EMI // 3
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_EMI();
	}

	pass SKINSTENCIL // 4
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		StencilEnable = true;
		StencilRef = 0x1;
		StencilMask = 0xffffffff;
		StencilWriteMask = 0xffffffff;
		StencilFunc = always;
		StencilZfail = KEEP;
		StencilFail = KEEP;
		StencilPass = REPLACE;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();	
		PixelShader = compile ps_3_0 PS_MAIN_SKINEDMESH();

		//ZWriteEnable = false;
	}

	pass SKINPHANTOM // 5
	{	
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = false;

		//AlphaTestEnable = true;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		//AlphaFunc = Greater;
		//AlphaRef = 0x0f;

		StencilEnable = true;		
		
		ZFUNC = GREATER;
		//STENCILMASK = 0x11;
		STENCILFUNC = EQUAL;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_STENCIL();

	//	STENCILPASS = KEEP;
		//StencilEnable = false;
	}

	pass Noise // 6
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_NOISE();
	}

	pass Dissolve // 7
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVE();
	}

	pass DissolveStatic // 8
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVE();
	}

	pass WEAPON // 9
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_SKINEDMESH();
	}

	pass SKILLDISOLVE // 10
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		//AlphaTestEnable = true;
		//AlphaFunc = Greater;
		//AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKINSKILL();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVESKILL();
	}

	pass SKILLDISOLVESTATIC // 11
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		//AlphaTestEnable = true;
		//AlphaFunc = Greater;
		//AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKINSKILLSTATIC();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVESKILL();
	}

	pass DISTORB // 12
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKINSKILLSTATIC();
		PixelShader = compile ps_3_0 PS_MAIN_DISTORB();
	}

	pass StempStatic // 13
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_OBJECT();
	}

	pass StempDynamic // 14
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_OBJECTKSIN();
	}

	pass ShapeSkill // 15
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SHAPESKIN();
		PixelShader = compile ps_3_0 PS_MAIN_SHAPESKILL();
	}

	pass ShapeSkillOutLine // 16
	{
		CullMode = cw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SHAPESKINOUTLINE();
		PixelShader = compile ps_3_0 PS_MAIN_SHAPESKILLOUTLINE();
	}

	pass EMISKIN // 17
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_EMI();
	}

	pass SKINNNONE // 18
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}

	pass SKINNNONE // 19
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaBlendEnable = true;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_SPIRITSKILL();
	}

	pass HAIR // 20
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_HAIR();
		PixelShader = compile ps_3_0 PS_MAIN_HAIR();
	}

	pass DissolveKnife // 21
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVEKNIFE();
	}	

	pass Terrain // 22
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_MAIN_TERRAIN();
		PixelShader = compile ps_3_0 PS_MAIN_TERRAIN();
	}

	pass SKINPHANTOM // 23
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = false;

		//AlphaTestEnable = true;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		//AlphaFunc = Greater;
		//AlphaRef = 0x0f;

		StencilEnable = true;

		ZFUNC = GREATER;
		//STENCILMASK = 0x11;
		STENCILFUNC = EQUAL;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_STENCIL();

		//	STENCILPASS = KEEP;
		//StencilEnable = false;
	}

	pass SKINPUIPHANTOM // 24
	{
		CullMode = ccw;
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_REDCAMPER();
	}

	pass Dissolve // 25
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN_DISSOLVEKNIFE();
	}

	pass STENCILMASKSTATIC // 26
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		StencilEnable = true;
		StencilRef = 0x1;
		StencilMask = 0xffffffff;
		StencilWriteMask = 0xffffffff;
		StencilFunc = always;
		StencilZfail = KEEP;
		StencilFail = KEEP;
		StencilPass = REPLACE;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}

	pass STENCILSTATIC // 27
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = false;

		//AlphaTestEnable = true;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		StencilEnable = true;

		ZFUNC = GREATER;
		STENCILFUNC = EQUAL;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_STENCIL();
	}

	pass EXITDOOR // 28
	{
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = false;

		//AlphaTestEnable = true;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		//AlphaFunc = Greater;
		//AlphaRef = 0x0f;

		StencilEnable = true;

		ZFUNC = GREATER;
		//STENCILMASK = 0x11;
		STENCILFUNC = EQUAL;

		VertexShader = compile vs_3_0 VS_MAIN_SKINSKILL();
		PixelShader = compile ps_3_0 PS_MAIN_EXITDOORSTENCIL();

		//	STENCILPASS = KEEP;
		//StencilEnable = false;
	}
}