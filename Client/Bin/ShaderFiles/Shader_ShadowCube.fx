matrix	g_matW;
matrix	g_matVP;
vector	g_vLightPos;

int		numBoneInf = 4;

texture		g_SkinTexture;
sampler SkinSampler = sampler_state
{
	texture = g_SkinTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
};

struct VS_OUT
{
	vector	vPosition : POSITION0;
	float3	vLight : TEXCOORD0;
};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	float4x4 matWVP;
	matWVP = mul(g_matW, g_matVP);

	vector vPos = mul(vector(In.vPosition, 1.f), g_matW);
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);

	Out.vLight = (g_vLightPos.xyz - vPos.xyz);

	return Out;
}

struct VS_IN_SKIN
{
	float3	vPosition : POSITION;
	float4	weights : BLENDWEIGHT0;
	int4	boneIndices : BLENDINDICES0;
};

float4x4 GetSkinMatrix(int idx)
{
	float4 uv = float4(((float)((idx % 32) * 4) + 0.5f) / 128.f, ((float)((idx / 32)) + 0.5f) / 128.f, 0.0f, 0.0f);

	float4x4 mat =
	{
		tex2Dlod(SkinSampler, uv),
		tex2Dlod(SkinSampler, uv + float4(1.0f / 128.f, 0, 0, 0)),
		tex2Dlod(SkinSampler, uv + float4(2.0f / 128.f, 0, 0, 0)),
		tex2Dlod(SkinSampler, uv + float4(3.0f / 128.f, 0, 0, 0))
	};

	return mat;
}

VS_OUT VS_MAIN_SKIN(VS_IN_SKIN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWVP = mul(g_matW, g_matVP);

	float lastweight = 0.f;
	int n = numBoneInf - 1.f;

	for (int i = 0; i < n; ++i)
	{
		lastweight += In.weights[i];
		Out.vPosition += In.weights[i] * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[i]));
	}

	lastweight = 1.f - lastweight;
	Out.vPosition += lastweight * mul(vector(In.vPosition, 1.f), GetSkinMatrix(In.boneIndices[n]));

	vector vPos = mul(vector(Out.vPosition.xyz, 1.f), g_matW);

	Out.vPosition = mul(Out.vPosition, matWVP);

	Out.vLight = (g_vLightPos.xyz - vPos.xyz);

	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION0;
	float3	vLight : TEXCOORD0;
};

struct PS_OUT
{
	vector		Tex : COLOR0;
};


PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	Out.Tex.rgba = length(In.vLight) + 11.f;

	return Out;
}

technique	Tech0
{
	pass Cube
	{
		ZEnable = true;
		ZWriteEnable = true;
		COLORWRITEENABLE = red;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();

		COLORWRITEENABLE = red | green | blue | alpha;
	}

	pass CubeSkin
	{
		ZEnable = true;
		ZWriteEnable = true;
		COLORWRITEENABLE = red;

		VertexShader = compile vs_3_0 VS_MAIN_SKIN();
		PixelShader = compile ps_3_0 PS_MAIN();

		COLORWRITEENABLE = red | green | blue | alpha;
	}
}