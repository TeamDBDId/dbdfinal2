#pragma once

matrix		g_matWorld, g_matView, g_matProj;

texture		g_DiffuseTexture;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float3	vTexUV : TEXCOORD;
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float3	vTexUV : TEXCOORD0;
	float	fFog : TEXCOORD1;
};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT		Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;

	Out.fFog = In.vPosition.y * 0.05f;

	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION;
	float3	vTexUV : TEXCOORD0;
	float	fFog : TEXCOORD1;
};

struct PS_OUT
{
	half4	vColor : COLOR0;
	half4	vNormal : COLOR1;
	vector	vDepth : COLOR2;
};

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT		Out;

	half4 vDiffuse = texCUBE(DiffuseSampler, In.vTexUV);
	vDiffuse.xyz *= 0.6f;
	vDiffuse.xyz = max(6.10352e-5, vDiffuse.xyz);
	vDiffuse.xyz > 0.04045 ? pow(vDiffuse.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vDiffuse.xyz * (1.0 / 12.92);

	//float fFog = 1.f / exp(pow(length(vLook) * 0.0004f, 2));
	vDiffuse = In.fFog * vDiffuse + (1.f - In.fFog) * half4(0.0323f, 0.0272f, 0.0252f, 1.f);

	Out.vColor = vDiffuse;

	Out.vNormal = half4(1, 1, 1, 1);
	Out.vDepth = vector(1, 5.f, 0.0f, 0);

	return Out;
}

technique	Default_Device
{
	pass Default_Rendering
	{
		CullMode = cw;
		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}	
}