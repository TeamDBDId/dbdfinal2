
matrix		g_matWorld, g_matView, g_matProj;
float		g_fTime;
texture		g_DiffuseTexture;
sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
texture		g_DistortionTexture;
sampler DistortionSampler = sampler_state
{
	texture = g_DistortionTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_OffsetTexture;
sampler OffsetSampler = sampler_state
{
	texture = g_OffsetTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_FlowTexture;
sampler FlowSampler = sampler_state
{
	texture = g_FlowTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_BorderTexture;
sampler BorderSampler = sampler_state
{
	texture = g_BorderTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
texture		g_BorderOffsetTexture;
sampler BorderOffsetSampler = sampler_state
{
	texture = g_BorderOffsetTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_MaskTexture;
sampler MaskSampler = sampler_state
{
	texture = g_MaskTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT_PHONG
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
};

VS_OUT_PHONG VS_MAIN_PHONG(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);

	Out.vTexUV = In.vTexUV;

	return Out;
}

struct PS_IN_PHONG
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector	vColor : COLOR0;
};

PS_OUT PS_MAIN_PHONG(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	float Mask = tex2D(MaskSampler, In.vTexUV).r;
	if (Mask > 0.9)
	{
		Out.vColor = vector(0, 0, 0, 0);
		return Out;
	}
	else if (Mask > 0.4f)
	{
		float fTime = g_fTime;
		float fTime2 = g_fTime - 0.5f;
		if (fTime2 < 0.0f)
			fTime2 += 1.f;
		else if (fTime2 > 1.0f)
			fTime2 -= 1.f;
		float2 flowmap = tex2D(BorderSampler, In.vTexUV).rg * 2.0f - 1.0f;
		float cycleOffset = tex2D(BorderOffsetSampler, In.vTexUV).b;

		float phase0 = cycleOffset * 0.5f + fTime;
		float phase1 = cycleOffset * 0.5f + fTime2 - 0.5f;

		float3 normalT0 = tex2D(DiffuseSampler, (In.vTexUV * 0.95f) + flowmap * phase0);
		float3 normalT1 = tex2D(DiffuseSampler, (In.vTexUV * 0.95f) + flowmap * phase1);

		float flowLerp = (abs(0.5f - g_fTime) / 0.5f);
		float3 offset = lerp(normalT0, normalT1, flowLerp);

		Out.vColor.xyz = offset;
		Out.vColor.a = 1;

		return Out;
	}

	//
	//float fTime = (g_fTime * 0.5f);
	//float2 vUV = saturate(In.vTexUV + g_fTime * 0.4f);
	//float2 vDis = tex2D(DistortionSampler, vUV).xy * 0.05f;
	//vector Color = tex2D(DiffuseSampler, saturate(In.vTexUV + vDis - 0.1f));

	//Color.xyz *= 0.8f;

	//Out.vColor = Color;

	float fTime = g_fTime;
	float fTime2 = g_fTime - 0.5f;
	if (fTime2 < 0.0f)
		fTime2 += 1.f;
	else if (fTime2 > 1.0f)
		fTime2 -= 1.f;
	float2 flowmap = tex2D(FlowSampler, In.vTexUV).rg * 2.0f - 1.0f;
	float cycleOffset = tex2D(OffsetSampler, In.vTexUV).r;

	float phase0 = cycleOffset * 0.5f + fTime;
	float phase1 = cycleOffset * 0.5f + fTime2 - 0.5f;

	float3 normalT0 = tex2D(DiffuseSampler, (In.vTexUV * 0.8f) + flowmap * phase0);
	float3 normalT1 = tex2D(DiffuseSampler, (In.vTexUV * 0.8f) + flowmap * phase1);

	float flowLerp = (abs(0.5f - g_fTime) / 0.5f);
	float3 offset = lerp(normalT0, normalT1, flowLerp);

	Out.vColor.xyz = offset;
	Out.vColor.a = 1;

	return Out;
}

technique	DefaultDevice
{
	pass Phong
	{			
		CullMode = ccw;
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}
}
