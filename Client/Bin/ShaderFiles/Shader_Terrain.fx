
matrix		g_matWorld, g_matView, g_matProj;
texture		g_DiffuseTexture;
float2		g_fUV;

texture		g_NormalTexture;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler NormalSampler = sampler_state
{
	texture = g_NormalTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture g_RoughnessTexture;

sampler RoughSampler = sampler_state
{
	texture = g_RoughnessTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

texture g_AOTexture;
sampler AOSampler = sampler_state
{
	texture = g_AOTexture;
	MinFilter = linear;
	MagFilter = linear;
	MipFilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float3	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;	
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD2;	
	vector	vTangent : TANGENT;
};

VS_OUT_PHONG VS_MAIN_PHONG(VS_IN In)
{
	VS_OUT_PHONG			Out = (VS_OUT_PHONG)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vNormal = normalize(mul(vector(In.vNormal, 0.f), g_matWorld));
	Out.vTangent = normalize(mul(vector(1,0,0, 0.f), g_matWorld));

	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;


	return Out;
}

struct PS_IN_PHONG
{
	vector	vPosition : POSITION;
	vector	vNormal : NORMAL;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD2;
	vector	vTangent : TANGENT;
};

struct PS_OUT
{
	vector	vDiffuse : COLOR0;
	vector	vNormal : COLOR1;
	vector	vDepth : COLOR2;
	vector	vRMAO : COLOR3;
};

PS_OUT PS_MAIN_PHONG(PS_IN_PHONG In)
{
	PS_OUT		Out = (PS_OUT)0;

	In.vTexUV = In.vTexUV *	g_fUV;
	vector Color = tex2D(DiffuseSampler, In.vTexUV);

	Color.xyz = max(6.10352e-5, Color.xyz);
	Color.xyz = Color.xyz > 0.04045 ? pow(Color.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : Color.xyz * (1.0 / 12.92);

	Out.vDiffuse = Color;

	//Out.vNormal = vector(In.vNormal.xyz * 0.5f + 0.5f, 0.f);

	Out.vDepth = vector(In.vProjPos.z / In.vProjPos.w, In.vProjPos.w / 30000.0f, 0.f, 0.f);
	vector AO = tex2D(AOSampler, In.vTexUV);
	AO.xyz = AO.xyz * 0.5f + 0.5f;
	AO.xyz = max(6.10352e-5, AO.x);
	AO.xyz = AO.xyz > 0.04045 ? pow(AO.x * (1.0 / 1.055) + 0.0521327, 2.4) : AO.x * (1.0 / 12.92);

	float vRough = tex2D(RoughSampler, In.vTexUV).r;
	Out.vDiffuse.xyz *= AO.xyz;

	Out.vRMAO = vector(vRough, 0, 0, 1);


	float3 tangentNormal = tex2D(NormalSampler, In.vTexUV).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1);

	float3 vBinormal = cross(In.vNormal.xyz, In.vTangent.xyz);

	float3x3 TBN = float3x3(normalize(In.vTangent.xyz), normalize(vBinormal), normalize(In.vNormal.xyz));
	TBN = transpose(TBN);
	Out.vNormal.xyz = mul(TBN, tangentNormal);
	Out.vNormal.w = 0;

	return Out;
}

technique	DefaultDevice
{
	pass Phong
	{			
		CullMode = ccw;
		ZEnable = true;
		ZWriteEnable = true;

		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0x0f;

		CullMode = ccw;

		VertexShader = compile vs_3_0 VS_MAIN_PHONG();
		PixelShader = compile ps_3_0 PS_MAIN_PHONG();
	}
}

