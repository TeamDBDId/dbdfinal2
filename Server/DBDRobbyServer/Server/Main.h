#pragma once


#define MAX_Client 5

// IOCP 관련 모드 설정
#define Recv_Mode 0
#define Send_Mode 1

#define No_Connected FALSE



typedef struct tagRobby //Robby
{
	int		Number = -1;
	int		character = 0;

	unsigned int		Packet = 0;

	char	IPAdress[16];
	bool	bConnect = false;
	bool	Ready = false;
}RobbyData;

typedef struct tagRobbyServerData
{
	RobbyData playerdat[5];
	int		  Number[5] = { 0,0,0,0,0 };
	int		  PickTime = 0;
	int		  WaittingTime = 0;
	bool	  GameStart = false;
	bool	  AllReady = false;
}RServer_Data;

//-------------------------------------------------------------------------------------
// IOCP 관련 소켓 정보 구조체
struct SOCKETINFO {
	OVERLAPPED overlapped;
	SOCKET sock;
	char buf[BUFSIZE + 1];
	bool Incoming_data;
	WSABUF wsabuf;
};

// 클라이언트 고유번호, 팀번호 구조체
typedef struct ClientData {
	int client_imei = -1;
	int Character = -1;
}ClientData;

// IOCP 관련 구조체
typedef struct {
	SOCKET hClntSock;
	SOCKADDR_IN clntAddr;
	int client_imei = 0;
} PER_HANDLE_DATA, *LPPER_HANDLE_DATA;

typedef struct {
	OVERLAPPED overlapped;
	char buffer[BUFSIZE + 1];
	bool Incoming_data;
	WSABUF wsaBuf;
} PER_IO_DATA, *LPPER_IO_DATA;
