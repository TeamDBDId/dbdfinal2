#pragma comment(lib, "ws2_32")
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include <winsock2.h>

#include "d3d9.h"
#include "d3dx9.h"

#include <stdlib.h>
#include <atlstr.h>
#include <Windows.h>
#include <process.h>
#include "resource.h"
#include "Define.h"
#include "Main.h"
#include <random>

int CountPlayerNum = 0;
bool StartTime = false;
int	slasherNum = -1;
bool client[5] = { false }; 
bool init_client[5] = { false }; 
char buf[MAX_PATH];

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND, UINT, UINT, DWORD); 

void Update_Game(int select);
void DisplayText(HWND hDlg, char *fmt, ...);
void DisplayEditText(HWND hDlg, char *fmt, ...);
void clrUser(int);
_int CalRandIntFromTo(const _int & _iFrom, const _int & _iTo);

SOCKET sock; // 소켓
HWND shareHwnd, hList, hList2, hTextBox[3];
HFONT hFont[3];
HWND hPlayer[12], hPlayerOff[12];
HWND Time_Button[6];
SYSTEM_INFO SystemInfo;

int CheckNumber[5] = {-1, -1, -1, -1, -1};

RServer_Data server_data;

DWORD WINAPI ProcessClient(LPVOID arg);
unsigned int __stdcall CompletionThread(LPVOID pComPort);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	ZeroMemory(&server_data, sizeof(RServer_Data));
	CreateThread(NULL, 0, ProcessClient, NULL, 0, NULL);

	SetTimer(shareHwnd, 1, 1024, TimerProc);

	DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DlgProc);

	closesocket(sock);
	WSACleanup();

	return 0;
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime) {   //함수 정의
	HDC hdc;
	hdc = GetDC(hWnd);
	int PlayerCheck = 0;
	int ConnectedPlayer = 0;
	bool GameStart;
	bool isSlasher = false;
	int CountCamper = 0;
	int ReadyPlayer = 0;

	for (int i = 0; i < 5; ++i)
	{
		if (server_data.playerdat[i].bConnect)
			PlayerCheck++;

		if (client[i])
			ConnectedPlayer++;

		if (server_data.playerdat[i].Ready)
			ReadyPlayer++;

		if (server_data.playerdat[i].Number == 5)
			isSlasher = true;
		
		else if(server_data.Number[i] < 5
			&& server_data.Number[i] > 0)
			CountCamper++;	
	}

	if (ConnectedPlayer == 0)
	{
		server_data.AllReady = false;
		server_data.WaittingTime = 6;
		server_data.GameStart = false;
		StartTime = false;
		for (int i = 0; i < 5; ++i)
		{
			CheckNumber[i] = -1;

			server_data.Number[i] = 0;
			server_data.playerdat[i].Number = 0;
			server_data.playerdat[i].Ready = false;
			server_data.playerdat[i].bConnect = false;
			strcpy(server_data.playerdat[i].IPAdress, "");
		}
	}

	if (ConnectedPlayer != 0 
		&&(PlayerCheck == ConnectedPlayer) 
		&& (ReadyPlayer == ConnectedPlayer)
		/*&& (CountCamper >= 1)*/ /*&& isSlasher*/ 
		&& !StartTime)
	{
		StartTime = true;
		server_data.AllReady = true;
		server_data.PickTime = 0;
		if(server_data.WaittingTime == 0)
			server_data.WaittingTime = 6;
	}

	if (server_data.WaittingTime > 0 && server_data.AllReady)
		server_data.WaittingTime--;

 	else if (server_data.AllReady && server_data.WaittingTime == 0 
		&& !server_data.GameStart)
	{
		server_data.GameStart = true;
	
	}
	
	CountPlayerNum = PlayerCheck;

	ReleaseDC(hWnd, hdc);
}


BOOL CALLBACK DlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	shareHwnd = hDlg;
	switch (uMsg) {
	case WM_INITDIALOG:
		//hProgress = GetDlgItem( hDlg, IDC_PROGRESS1 );
		hFont[0] = CreateFont(20, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, "돋음");
		hFont[1] = CreateFont(30, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, "돋음");
		hFont[2] = CreateFont(35, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, "돋음");

		hList = GetDlgItem(hDlg, IDC_LIST1);
		hList2 = GetDlgItem(hDlg, IDC_LIST2);
		hTextBox[0] = GetDlgItem(hDlg, IDC_EDIT1);
		hTextBox[1] = GetDlgItem(hDlg, IDC_EDIT2);

		SendMessage(hTextBox[0], WM_SETFONT, (WPARAM)hFont[0], (LPARAM)TRUE);
		SendMessage(hTextBox[1], WM_SETFONT, (WPARAM)hFont[1], (LPARAM)TRUE);
		SendMessage(hTextBox[2], WM_SETFONT, (WPARAM)hFont[2], (LPARAM)TRUE);

		DisplayText(hList, "%d Thread Created..!", SystemInfo.dwNumberOfProcessors * 2);
		SendMessage(hList, LB_ADDSTRING, 0, (LPARAM)"Server is Ready..!");
		char temp[32];
		for (int i = 0; i < MAX_CLIENT; i++)
		{
			_itoa(i+1, temp, 10);
			SendMessage(hList2, LB_ADDSTRING, 0, (LPARAM)temp);
		}
			

		return TRUE;


	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDC_LIST1:
			switch (HIWORD(wParam)) {
			case LBN_SELCHANGE:
				int listIndex = SendMessage(hList, LB_GETCURSEL, 0, 0);
				SendMessage(hList, LB_GETTEXT, listIndex, (LPARAM)buf);
				MessageBox(hList, buf, "RobbyServer", 0);
				break;
			}
			return TRUE;
		case IDC_BUTTON1:
		{
			int listIndex = SendMessage(hList, LB_GETCURSEL, 0, 0);
			client[listIndex] = false;
			return TRUE;
		}
		case IDCANCEL:
			EndDialog(hDlg, IDCANCEL);
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

DWORD WINAPI ProcessClient(LPVOID arg) {
	WSADATA wsaData;
	HANDLE hCompletionPort;
	SOCKADDR_IN servAddr;
	LPPER_IO_DATA PerIoData;
	LPPER_HANDLE_DATA PerHandleData;

	int RecvBytes;
	int i, Flags;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) /* Load Winsock 2.2 DLL */
		DisplayText(hList, "WSAStartup() error!");

	hCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);

	GetSystemInfo(&SystemInfo);
	for (i = 0; i < SystemInfo.dwNumberOfProcessors * 2; i++)
		_beginthreadex(NULL, 0, CompletionThread, (LPVOID)hCompletionPort, 0, NULL);

	bool flag = TRUE;
	sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(flag));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port = htons(SERVERPORT);
	bind(sock, (SOCKADDR*)&servAddr, sizeof(servAddr));
	listen(sock, 5);

	while (TRUE) {
		SOCKET hClntSock;
		SOCKADDR_IN clntAddr;
		int addrLen = sizeof(clntAddr);

		hClntSock = accept(sock, (SOCKADDR*)&clntAddr, &addrLen);

		DisplayText(hList, "클라이언트 접속: IP 주소=%s, 포트 번호=%d", inet_ntoa(clntAddr.sin_addr), ntohs(clntAddr.sin_port));

		PerHandleData = (LPPER_HANDLE_DATA)malloc(sizeof(PER_HANDLE_DATA));
		for (int i = 0; i < 5; ++i) {
			if (client[i] == false) {
				client[i] = true;

				SendMessage(hList2, LB_DELETESTRING, i, 0);
				SendMessage(hList2, LB_INSERTSTRING, i, (LPARAM)inet_ntoa(clntAddr.sin_addr));

				PerHandleData->client_imei = i;

				DisplayText(hList, "Client ID : %d\n", PerHandleData->client_imei);
				//DisplayText(hList, "Client Team : %d\n", (PerHandleData->client_imei + 1) % 2);
				break;
			}
		}

		//sprintf(buf, "Player %d : %s:%d [ Team : %d ]", PerHandleData->client_imei, inet_ntoa(clntAddr.sin_addr), ntohs(clntAddr.sin_port), PerHandleData->client_imei % 2);
		//SendMessage(/*hPlayer[PerHandleData->client_imei]*/hList2, LB_ADDSTRING, 0, (LPARAM)buf);

		PerHandleData->hClntSock = hClntSock;
		memcpy(&(PerHandleData->clntAddr), &clntAddr, addrLen);


		CreateIoCompletionPort((HANDLE)hClntSock, hCompletionPort, (DWORD)PerHandleData, 0);

		PerIoData = (LPPER_IO_DATA)malloc(sizeof(PER_IO_DATA));
		memset(&(PerIoData->overlapped), 0, sizeof(OVERLAPPED));
		PerIoData->wsaBuf.len = BUFSIZE;
		PerIoData->wsaBuf.buf = PerIoData->buffer;
		PerIoData->Incoming_data = Recv_Mode;

		Flags = 0;

		WSARecv(PerHandleData->hClntSock,&(PerIoData->wsaBuf),1,(LPDWORD)&RecvBytes,(LPDWORD)&Flags,
			&(PerIoData->overlapped), NULL);
		/*sprintf(buf, "%s", );
		SendMessage(hList2, LB_ADDSTRING, 0, (LPARAM)buf);*/
	}
	return 0;
}

unsigned int __stdcall CompletionThread(LPVOID pComPort) {
	HANDLE hCompletionPort = (HANDLE)pComPort;


	DWORD BytesTransferred;
	LPPER_HANDLE_DATA PerHandleData;
	LPPER_IO_DATA PerIoData;
	DWORD flags;

	RobbyData robby_data;

	int sendBytes = 0;

	while (1) {
		GetQueuedCompletionStatus(hCompletionPort,
			&BytesTransferred, 
			(LPDWORD)&PerHandleData,
			(LPOVERLAPPED*)&PerIoData,
			INFINITE
		);

		if (BytesTransferred == 0 || client[PerHandleData->client_imei] == false) 
		{							
			sprintf(buf, "%d", PerHandleData->client_imei+1);
			
			SendMessage(hList2, LB_DELETESTRING, PerHandleData->client_imei, 0);
			SendMessage(hList2, LB_INSERTSTRING, PerHandleData->client_imei, (LPARAM)buf);

			clrUser(PerHandleData->client_imei);
			StartTime = false;
			server_data.WaittingTime = 6;
			server_data.GameStart = false;
			server_data.AllReady = false;
			client[PerHandleData->client_imei] = false;
			init_client[PerHandleData->client_imei] = false;
			PerIoData->Incoming_data = Recv_Mode;
			CheckNumber[PerHandleData->client_imei] = -1;

			DisplayText(hList, "%d번 클라이언트가 강제 종료 or 퇴장 되었습니다..!", PerHandleData->client_imei);
			closesocket(PerHandleData->hClntSock);
			free(PerHandleData);
			free(PerIoData);
			continue;
		}
		
		if (PerIoData->Incoming_data == Recv_Mode) {
			PerIoData->wsaBuf.buf[BytesTransferred] = '\0';
			
			server_data.playerdat[PerHandleData->client_imei] = (RobbyData&)PerIoData->buffer;

			if (init_client[PerHandleData->client_imei] == false) {
				sprintf(buf, "Player %d : %d ", PerHandleData->client_imei, server_data.playerdat[PerHandleData->client_imei].character);
				SendMessage(hPlayer[PerHandleData->client_imei], WM_SETTEXT, 0, (LPARAM)buf);
				init_client[PerHandleData->client_imei] = true;
			}

			if (5 == server_data.playerdat[PerHandleData->client_imei].Number)
				slasherNum = PerHandleData->client_imei;

			if (client[PerHandleData->client_imei])
			{
				server_data.playerdat[PerHandleData->client_imei].bConnect = true;
				//if (server_data.playerdat[PerHandleData->client_imei].Number <= 0)
				//{
				//	if (CheckNumber[PerHandleData->client_imei] == -1)
				//	{
				//		CheckNumber[PerHandleData->client_imei] = PerHandleData->client_imei + 1;
				//		//server_data.Number[PerHandleData->client_imei] = PerHandleData->client_imei + 1;
				//		server_data.playerdat[PerHandleData->client_imei].Number = PerHandleData->client_imei + 1;
				//	}
				//	if(CheckNumber[PerHandleData->client_imei] > 0 && CheckNumber[PerHandleData->client_imei] < 6)
				//		server_data.Number[PerHandleData->client_imei] = CheckNumber[PerHandleData->client_imei];
				//}
				if (server_data.playerdat[PerHandleData->client_imei].Packet == LSLASHER)
				{
					CheckNumber[PerHandleData->client_imei] = 5;
					server_data.Number[PerHandleData->client_imei] = 5;
					server_data.playerdat[PerHandleData->client_imei].Packet = 0;
				}
				else if (server_data.playerdat[PerHandleData->client_imei].Packet >= LCAMPER1
					&& server_data.playerdat[PerHandleData->client_imei].Packet <= LCAMPER4)
				{
					int nNumber = server_data.playerdat[PerHandleData->client_imei].Packet - 1;
					CheckNumber[PerHandleData->client_imei] = nNumber;
					server_data.Number[PerHandleData->client_imei] = nNumber;
					server_data.playerdat[PerHandleData->client_imei].Packet = 0;
				}
				else if (server_data.playerdat[PerHandleData->client_imei].Packet == CLICKBACK)
				{
					CheckNumber[PerHandleData->client_imei] = 0;
					server_data.Number[PerHandleData->client_imei] = 0;
					server_data.playerdat[PerHandleData->client_imei].Packet = 0;
				}
				//server_data.playerdat[PerHandleData->client_imei].Number = PerHandleData->client_imei + 1;
			}
				
			else
			{
				clrUser(PerHandleData->client_imei);
				server_data.Number[PerHandleData->client_imei] = 0;
				server_data.playerdat[PerHandleData->client_imei].Packet = 0;
				server_data.playerdat[PerHandleData->client_imei].bConnect = false;
			
			}
			
			
			/*char tempt[32];
			SendMessage(hList2, LB_GETTEXT, PerHandleData->client_imei, (LPARAM)tempt);

			if (strcmp(tempt, server_data.player_imei[PerHandleData->client_imei].nickName))
			{
				sprintf(buf, "%s", server_data.Players[PerHandleData->client_imei].nickName);
				SendMessage(hList2, LB_DELETESTRING, PerHandleData->client_imei, 0);
				SendMessage(hList2, LB_INSERTSTRING, PerHandleData->client_imei, (LPARAM)buf);
			}*/
			//if (!client[PerHandleData->client_imei])
			//{
				//sprintf(buf, "%s", CharacterName[server_data.playerdat[PerHandleData->client_imei].character]);
				//SendMessage(hList2, LB_DELETESTRING, PerHandleData->client_imei, 0);
				//SendMessage(hList2, LB_INSERTSTRING, PerHandleData->client_imei, (LPARAM)buf);
			//}

			PerIoData->Incoming_data = Send_Mode;		
		}


		if (PerIoData->Incoming_data == Send_Mode) {
			PerIoData->wsaBuf.len = sizeof(RServer_Data);
			PerIoData->wsaBuf.buf = (char*)&server_data;
			PerIoData->Incoming_data = Recv_Mode;

			WSASend(PerHandleData->hClntSock, &(PerIoData->wsaBuf), 1, (LPDWORD)&sendBytes, 0, NULL, NULL);

		}

		memset(&(PerIoData->overlapped), 0, sizeof(OVERLAPPED));
		PerIoData->wsaBuf.len = BUFSIZE;
		PerIoData->wsaBuf.buf = PerIoData->buffer;

		flags = 0;
		WSARecv(PerHandleData->hClntSock, &(PerIoData->wsaBuf), 1, NULL, &flags, &(PerIoData->overlapped), NULL);

	}
	return 0;
}

void DisplayText(HWND hDlg, char *fmt, ...) {
	va_list arg;
	va_start(arg, fmt);

	char contentBuf[MAX_PATH];
	vsprintf(contentBuf, fmt, arg);

	SendMessage(hDlg, LB_ADDSTRING, 0, (LPARAM)contentBuf);
}

void DisplayEditText(HWND hDlg, char *fmt, ...) {
	va_list arg;
	va_start(arg, fmt);

	char contentBuf[MAX_PATH]; 
	vsprintf(contentBuf, fmt, arg);

	SendMessage(hDlg, WM_SETTEXT, 0, (LPARAM)contentBuf);
}

void clrUser(int client_imei) 
{
	ZeroMemory(&server_data.playerdat[client_imei], sizeof(RobbyData));
	server_data.playerdat[client_imei].Number = -1;
	CheckNumber[client_imei] = -1;
	client[client_imei] = false;
}

void Update_Game(int select) 
{

}

_int CalRandIntFromTo(const _int & _iFrom, const _int & _iTo)
{
	std::random_device rn;
	std::mt19937_64 rnd(rn());

	std::uniform_int_distribution<_int> range(_iFrom, _iTo);
	return range(rnd);
}