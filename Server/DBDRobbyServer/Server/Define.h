#pragma once



typedef unsigned char			_ubyte;
typedef signed char				_byte;

typedef unsigned short			_ushort;
typedef signed short			_short;

typedef unsigned int			_uint;
typedef signed int				_int;

typedef unsigned long			_ulong;
typedef signed long				_long;

typedef float					_float;
typedef double					_double;

typedef bool					_bool;

typedef wchar_t					_tchar;

typedef D3DXVECTOR2				_vec2;
typedef D3DXVECTOR3				_vec3;
typedef D3DXVECTOR4				_vec4;

typedef D3DXMATRIX				_matrix;

#define SERVERPORT 10004
#define BUFSIZE    10240

#define		MAX_CLIENT	5

#define		LSLASHER	1
#define		LCAMPER1	2
#define		LCAMPER2	3
#define		LCAMPER3	4
#define		LCAMPER4	5
#define		CLICKBACK	6