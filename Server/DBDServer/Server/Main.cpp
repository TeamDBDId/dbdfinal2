#pragma comment(lib, "ws2_32")
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include <winsock2.h>

#include "d3d9.h"
#include "d3dx9.h"

#include <stdlib.h>
#include <atlstr.h>
#include <Windows.h>
#include <process.h>
#include "resource.h"
#include "Define.h"
#include "Main.h"
#include "time.h"

float	fTime = 0.f;
bool	Init_Start = false;
char*	ipAddress[5];
int CountPlayerNum = 0;
bool client[5] = { false };
bool init_client[5] = { false };
int respawn_time[MAX_Client] = { 0 };
char buf[MAX_PATH];
int aTeam = 0, bTeam = 0;
int PlayerNum[5] = { -1,-1,-1,-1, -1 };
int tempNum[5] = { 0,0,0,0,0 };
float DesGenerator[7] = { 0.f, 0.f, 0.f,0.f,0.f,0.f,0.f };
bool ClearGenerator[7] = { false, false, false, false, false, false, false };
int sItem[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
bool bCurse[3] = { false, false, false };
float HookRegen[12] = { 0.f, 0.f, 0.f,0.f,0.f,0.f,0.f ,0.f, 0.f, 0.f,0.f,0.f };
float GeneratorFail[7] = { 0.f, 0.f, 0.f,0.f,0.f,0.f,0.f };
bool bCreateHatch = false;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND, UINT, UINT, DWORD);

float GetTime();
void Update_Game(int select);
void CalculateGernerator(int Gindex, int NumPlayer, int Packet = 0, int iUseToolBox = 0);
void TotemCheck(int Tindex, bool Player);
void HookBrokenCheck(int HookIndex);

void DisplayText(HWND hDlg, char *fmt, ...);
void DisplayEditText(HWND hDlg, char *fmt, ...);
void clrUser(int client_imei);
void LoadGameFile();
void Check_InteractionOfPlayer();


SOCKET sock; // 소켓
HWND shareHwnd, hList, hList2, hTextBox[3];
HFONT hFont[3];
HWND hPlayer[12], hPlayerOff[12];
HWND Time_Button[6];
SYSTEM_INFO SystemInfo;

Server_Data server_data;

DWORD WINAPI ProcessClient(LPVOID arg);
DWORD WINAPI GameLogic(LPVOID arg);
unsigned int __stdcall CompletionThread(LPVOID pComPort);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	srand((unsigned)time(NULL));

	QueryPerformanceCounter(&m_FrameTime);
	QueryPerformanceCounter(&m_FixTime);
	QueryPerformanceCounter(&m_LastTime);
	QueryPerformanceFrequency(&m_CpuTick);

	ZeroMemory(&server_data, sizeof(Server_Data));

	for (int i = 0; i < 7; ++i)
		server_data.Game_Data.GRP[i] = 0.00f;

	for (int i = 0; i < 9; ++i)
		server_data.Game_Data.ItemBoxAnim[i] = (int)3;

	for (int i = 0; i < 27; ++i)
		server_data.Game_Data.Locker[i] = 6;

	CreateThread(NULL, 0, ProcessClient, NULL, 0, NULL);
	CreateThread(NULL, 0, GameLogic, NULL, 0, NULL);

	SetTimer(shareHwnd, 1, 1024, TimerProc);

	DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DlgProc);

	closesocket(sock);
	WSACleanup();

	//for (int i = 0; i < 5; ++i)
	//	delete[] ipAddress[i];

	return 0;
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime) {   //함수 정의
	HDC hdc;
	hdc = GetDC(hWnd);
	bool GameStart = false;
	int playernum = -1;
	int PlayerCheck = 0;

	for (int i = 0; i < 4; ++i)
	{
		if (server_data.Campers[i].bConnect)
			PlayerCheck++;
	}
	if (server_data.Slasher.bConnect)
		PlayerCheck++;

	if (Init_Start && PlayerCheck == 0)
	{
		Init_Start = false;

		for (int i = 0; i < 7; ++i)
		{
			DesGenerator[i] = 0.0f;
			ClearGenerator[i] = false;
		}
		for (int i = 0; i < 10; ++i)
		{
			sItem[i] = 0;
		}
		for (int i = 0; i < 3; ++i)
		{
			bCurse[i] = false;
		}
		for (int i = 0; i < 5; ++i)
		{
			PlayerNum[i] = -1;
			tempNum[i] = 0;
			client[i] = false;
			init_client[i] = false;
		}

		ZeroMemory(&server_data.Game_Data, sizeof(GameData));
	}


	//if (!Init_Start && (CountPlayerNum != 0) && (PlayerCheck == CountPlayerNum))
	//{
	//	Init_Start = true;
	//	for (int i = 0; i < 5; ++i)
	//	{
	//		for (int j = 0; j < 5; ++j)
	//		{
	//			if(server_data)
	//		}
	//	}
	//}

	ReleaseDC(hWnd, hdc);
}


BOOL CALLBACK DlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	shareHwnd = hDlg;
	switch (uMsg) {
	case WM_INITDIALOG:
		//hProgress = GetDlgItem( hDlg, IDC_PROGRESS1 );
		hFont[0] = CreateFont(20, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, "돋음");
		hFont[1] = CreateFont(30, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, "돋음");
		hFont[2] = CreateFont(35, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, "돋음");

		hList = GetDlgItem(hDlg, IDC_LIST1);
		hList2 = GetDlgItem(hDlg, IDC_LIST2);
		hTextBox[0] = GetDlgItem(hDlg, IDC_EDIT1);
		hTextBox[1] = GetDlgItem(hDlg, IDC_EDIT2);

		SendMessage(hTextBox[0], WM_SETFONT, (WPARAM)hFont[0], (LPARAM)TRUE);
		SendMessage(hTextBox[1], WM_SETFONT, (WPARAM)hFont[1], (LPARAM)TRUE);
		SendMessage(hTextBox[2], WM_SETFONT, (WPARAM)hFont[2], (LPARAM)TRUE);

		DisplayText(hList, "%d Thread Created..!", SystemInfo.dwNumberOfProcessors * 2);
		SendMessage(hList, LB_ADDSTRING, 0, (LPARAM)"Server is Ready..!");
		char temp[32];
		for (int i = 0; i < 5; i++)
		{
			_itoa(i + 1, temp, 10);
			SendMessage(hList2, LB_ADDSTRING, 0, (LPARAM)temp);
		}


		return TRUE;


	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDC_LIST1:
			switch (HIWORD(wParam)) {
			case LBN_SELCHANGE:
				int listIndex = SendMessage(hList, LB_GETCURSEL, 0, 0);
				SendMessage(hList, LB_GETTEXT, (listIndex), (LPARAM)buf);
				MessageBox(hList, buf, "GameServer", 0);
				break;
			}
			return TRUE;
		case IDC_BUTTON1:
		{
			int listIndex = SendMessage(hList, LB_GETCURSEL, 0, 0);
			client[listIndex] = false;
			return TRUE;
		}
		case IDCANCEL:
			EndDialog(hDlg, IDCANCEL);
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

DWORD WINAPI ProcessClient(LPVOID arg) {
	WSADATA wsaData;
	HANDLE hCompletionPort;
	SOCKADDR_IN servAddr;
	LPPER_IO_DATA PerIoData;
	LPPER_HANDLE_DATA PerHandleData;
	ZeroMemory(&PerIoData, sizeof(LPPER_IO_DATA));

	int RecvBytes;
	int i, Flags;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) /* Load Winsock 2.2 DLL */
		DisplayText(hList, "WSAStartup() error!");

	hCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);

	GetSystemInfo(&SystemInfo);
	for (i = 0; i < SystemInfo.dwNumberOfProcessors * 2; i++)
		_beginthreadex(NULL, 0, CompletionThread, (LPVOID)hCompletionPort, 0, NULL);

	bool flag = TRUE;
	sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(flag));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port = htons(SERVERPORT);
	bind(sock, (SOCKADDR*)&servAddr, sizeof(servAddr));
	listen(sock, 12);

	while (TRUE) {
		SOCKET hClntSock;
		SOCKADDR_IN clntAddr;
		int addrLen = sizeof(clntAddr);

		hClntSock = accept(sock, (SOCKADDR*)&clntAddr, &addrLen);

		DisplayText(hList, "클라이언트 접속: IP 주소=%s, 포트 번호=%d", inet_ntoa(clntAddr.sin_addr), ntohs(clntAddr.sin_port));

		PerHandleData = (LPPER_HANDLE_DATA)malloc(sizeof(PER_HANDLE_DATA));

		//for (int i = 0; i < 5; ++i) {
		//	if (client[i] == false) {
		//		client[i] = true;
		//		/*if(!Init_Start)
		//			LoadGameFile();*/
		//		PerHandleData->client_imei = i;
		//	
		//		DisplayText(hList, "Client ID : %d\n", PerHandleData->client_imei);
		//		DisplayText(hList, "Client Team : %d\n", (PerHandleData->client_imei + 1) % 2);
		//		break;
		//	}
		//}

		//sprintf(buf, "Player %d : %s:%d [ Team : %d ]", PerHandleData->client_imei, inet_ntoa(clntAddr.sin_addr), ntohs(clntAddr.sin_port), PerHandleData->client_imei % 2);
		//SendMessage(/*hPlayer[PerHandleData->client_imei]*/hList2, LB_ADDSTRING, 0, (LPARAM)buf);

		PerHandleData->hClntSock = hClntSock;
		memcpy(&(PerHandleData->clntAddr), &clntAddr, addrLen);


		CreateIoCompletionPort((HANDLE)hClntSock, hCompletionPort, (DWORD)PerHandleData, 0);

		PerIoData = (LPPER_IO_DATA)malloc(sizeof(PER_IO_DATA));
		memset(&(PerIoData->overlapped), 0, sizeof(OVERLAPPED));
		PerIoData->wsaBuf.len = BUFSIZE;
		PerIoData->wsaBuf.buf = PerIoData->buffer;
		PerIoData->Incoming_data = Recv_Mode;

		Flags = 0;

		WSARecv(PerHandleData->hClntSock, &(PerIoData->wsaBuf), 1, (LPDWORD)&RecvBytes, (LPDWORD)&Flags,
			&(PerIoData->overlapped), NULL);
		/*sprintf(buf, "%s", );
		SendMessage(hList2, LB_ADDSTRING, 0, (LPARAM)buf);*/


	}
	return 0;
}

DWORD WINAPI GameLogic(LPVOID arg)
{
	LARGE_INTEGER   ltickPerSecond;
	LARGE_INTEGER   lstartTick, endTick;

	LARGE_INTEGER   lGtickPerSecond;
	LARGE_INTEGER   lGstartTick, lGendTick;

	float		fTime = 0.f;

	QueryPerformanceFrequency(&lGtickPerSecond);
	QueryPerformanceCounter(&lGstartTick);
	while (1)
	{
		QueryPerformanceCounter(&lGendTick);
		fTime = (double)(lGendTick.QuadPart - lGstartTick.QuadPart) / lGtickPerSecond.QuadPart;

		while (fTime >= 0.010000) {
			QueryPerformanceCounter(&lGstartTick);
			fTime = 0;
			Check_InteractionOfPlayer();
			GetTime();
		}
	}
	return 0;
}

unsigned int __stdcall CompletionThread(LPVOID pComPort) {
	HANDLE hCompletionPort = (HANDLE)pComPort;

	DWORD BytesTransferred;
	LPPER_HANDLE_DATA PerHandleData;
	LPPER_IO_DATA PerIoData;
	DWORD flags;
	CamperData camper_data;
	SlasherData	slasher_data;
	int sendBytes = 0, player_hp, Attacked_Player;
	int player_kill, player_death;
	bool player_live;
	int player_Dmg;


	while (1) {
		GetQueuedCompletionStatus(hCompletionPort,
			&BytesTransferred,
			(LPDWORD)&PerHandleData,
			(LPOVERLAPPED*)&PerIoData,
			INFINITE
		);
		int IndexNum;
		//= *(int*)(PerIoData->buffer) - 1;
		memcpy(&IndexNum, PerIoData->buffer + (sizeof(int)), sizeof(int));

		IndexNum -= 1;

		if (IndexNum < 0 || IndexNum > 5)
			continue;

		if (BytesTransferred != 0 && client[IndexNum] == false)
			client[IndexNum] = true;

		if (BytesTransferred == 0 || client[IndexNum] == false) {

			sprintf(buf, "%d", IndexNum + 1);

			SendMessage(hList2, LB_DELETESTRING, IndexNum, 0);
			SendMessage(hList2, LB_INSERTSTRING, IndexNum, (LPARAM)buf);


			clrUser(IndexNum);
			client[IndexNum] = false;
			init_client[IndexNum] = false;

			PerIoData->Incoming_data = Recv_Mode;


			DisplayText(hList, "%d번 클라이언트가 강제 종료 or 퇴장 되었습니다..!", IndexNum);
			closesocket(PerHandleData->hClntSock);
			free(PerHandleData);
			free(PerIoData);
			continue;
		}

		//if (PerIoData->number <= 0 || PerIoData->number > 5)
		//	continue;

		if (PerIoData->Incoming_data == Recv_Mode) {
			//PerIoData->wsaBuf.buf[BytesTransferred] = '\0';

			if (IndexNum <= 3)
			{
				server_data.Campers[IndexNum] = (CamperData&)PerIoData->buffer;

				if (init_client[IndexNum] == false) {
					sprintf(buf, "Player %d : %d [ Team : %d ]", IndexNum, server_data.Campers[IndexNum].Character, 0);
					SendMessage(hPlayer[IndexNum], WM_SETTEXT, 0, (LPARAM)buf);

					char temp[32];
					char tpNum[3];

					_itoa_s(IndexNum, tpNum, 10);

					strcpy(temp, "Camper");
					strcat(temp, tpNum);

						
					//strcpy(ipAddress[IndexNum], temp);
					SendMessage(hList2, LB_DELETESTRING, IndexNum, 0);
					SendMessage(hList2, LB_INSERTSTRING, IndexNum, (LPARAM)temp);

					init_client[IndexNum] = true;
				}
				if (client[IndexNum])
					server_data.Campers[IndexNum].bConnect = true;
				else
					server_data.Campers[IndexNum].bConnect = false;

				//server_data.Ca[IndexNum]
			}
			else
			{
				server_data.Slasher = (SlasherData&)PerIoData->buffer;

				if (init_client[IndexNum] == false) {
					sprintf(buf, "Player %d : %d [ Team : %d ]", IndexNum, server_data.Slasher.iCharacter, 0);
					SendMessage(hPlayer[IndexNum], WM_SETTEXT, 0, (LPARAM)buf);

					char temp[32];
					char tpNum[3];

					_itoa_s(IndexNum, tpNum, 10);

					strcpy(temp, "Slasher");
					strcat(temp, tpNum);

					SendMessage(hList2, LB_DELETESTRING, IndexNum, 0);
					SendMessage(hList2, LB_INSERTSTRING, IndexNum, (LPARAM)temp);

					init_client[IndexNum] = true;
				}

				if (client[4])
					server_data.Slasher.bConnect = true;
				else
					server_data.Slasher.bConnect = false;
			}

			char tempt[32];
			SendMessage(hList2, LB_GETTEXT, IndexNum, (LPARAM)tempt);

			// 캐릭터 네임 정하면 넣어주자.
			//if (strcmp(tempt, server_data.player_imei[PerHandleData->client_imei].nickName))
			//{
			//	sprintf(buf, "%s", server_data.Players[PerHandleData->client_imei].nickName);
			//	SendMessage(hList2, LB_DELETESTRING, PerHandleData->client_imei, 0);
			//	SendMessage(hList2, LB_INSERTSTRING, PerHandleData->client_imei, (LPARAM)buf);
			//}

			PerIoData->Incoming_data = Send_Mode;

		}

		if (PerIoData->Incoming_data == Send_Mode) {
			PerIoData->wsaBuf.len = sizeof(Server_Data);
			PerIoData->wsaBuf.buf = (char*)&server_data;
			PerIoData->Incoming_data = Recv_Mode;

			WSASend(PerHandleData->hClntSock, &(PerIoData->wsaBuf), 1, (LPDWORD)&sendBytes, 0, NULL, NULL);

		}

		memset(&(PerIoData->overlapped), 0, sizeof(OVERLAPPED));
		PerIoData->wsaBuf.len = BUFSIZE;
		PerIoData->wsaBuf.buf = PerIoData->buffer;

		flags = 0;
		WSARecv(PerHandleData->hClntSock, &(PerIoData->wsaBuf), 1, NULL, &flags, &(PerIoData->overlapped), NULL);

	}
	return 0;
}

void DisplayText(HWND hDlg, char *fmt, ...) {
	va_list arg;
	va_start(arg, fmt);

	char contentBuf[MAX_PATH];
	vsprintf(contentBuf, fmt, arg);

	SendMessage(hDlg, LB_ADDSTRING, 0, (LPARAM)contentBuf);
}

void DisplayEditText(HWND hDlg, char *fmt, ...) {
	va_list arg;
	va_start(arg, fmt);

	char contentBuf[MAX_PATH];
	vsprintf(contentBuf, fmt, arg);

	SendMessage(hDlg, WM_SETTEXT, 0, (LPARAM)contentBuf);
}

void clrUser(int client_imei)
{
	if (client_imei < 4)
	{
		for (int i = 0; i < 9; ++i)
		{
			if (int(server_data.Game_Data.Item[i] / 10) == (client_imei + 1))
				server_data.Game_Data.Item[i] = 0;
		}
		ZeroMemory(&server_data.Campers[client_imei], sizeof(CamperData));
	}
	else
		ZeroMemory(&server_data.Slasher, sizeof(SlasherData));

	PlayerNum[client_imei] = -1;
	tempNum[client_imei] = 0;

	server_data.Game_Data.iPlayerAction[client_imei] = 0;
}

float GetTime()
{
	QueryPerformanceCounter(&m_FrameTime);

	fTime = float(m_FrameTime.QuadPart - m_LastTime.QuadPart) / m_CpuTick.QuadPart;

	m_LastTime = m_FrameTime;

	if (m_FrameTime.QuadPart - m_FixTime.QuadPart >= m_CpuTick.QuadPart)
	{
		QueryPerformanceFrequency(&m_CpuTick);
		m_FixTime = m_FrameTime;
	}

	return fTime;
}

void Update_Game(int select)
{
	//switch (select)
	//{
	//case 0:
	//	aTeam = 0;
	//	bTeam = 0;
	//	for (int i = 0; i < MAX_Client; ++i)
	//	{
	//		if (((i + 1) % 2) == 1)
	//		{
	//			aTeam += server_data.Players[i].Kill;
	//			server_data.GameRound.Ateam = aTeam;
	//		}
	//		else
	//		{
	//			bTeam += server_data.Players[i].Kill;
	//			server_data.GameRound.Bteam = bTeam;
	//		}
	//	}
	//	if (aTeam > bTeam) {
	//		server_data.GameRound.Around++;
	//	}
	//	else if (aTeam < bTeam)
	//	{
	//		server_data.GameRound.Bround++;
	//	}
	//	break;

	//case 1:
	//	// kd 초기화
	//	for (int i = 0; i < MAX_Client; ++i)
	//	{
	//		server_data.Players[i].Kill = 0;
	//		server_data.Players[i].Death = 0;
	//	}
	//	Update_Game(0);
	//	break;
	//case 2:
	//	aTeam = 0;
	//	bTeam = 0;
	//	for (int i = 0; i < MAX_Client; ++i)
	//	{
	//		if (((i + 1) % 2) == 1)
	//		{
	//			aTeam += server_data.Players[i].Kill;
	//			server_data.GameRound.Ateam = aTeam;
	//		}
	//		else
	//		{
	//			bTeam += server_data.Players[i].Kill;
	//			server_data.GameRound.Bteam = bTeam;
	//		}
	//	}
	//	break;
	//case 3: // 점령
	//{
	//	aTeam = 0;
	//	bTeam = 0;
	//	for (int i = 0; i < MAX_Client; ++i)
	//	{
	//		if (server_data.Players[i].team)
	//		{
	//			if (server_data.Players[i].isColl)
	//				aTeam++;
	//		}
	//		else
	//		{
	//			if (server_data.Players[i].isColl)
	//				bTeam++;
	//		}
	//	}// 99 일때 다른팀 있으면 멈추게
	//	if ((aTeam > bTeam) && server_data.GameRound.Ateam <= 99)
	//	{
	//		server_data.GameRound.Ateam += (aTeam - bTeam) * 2;
	//		server_data.GameRound.GameTime++;
	//	}
	//	else if (aTeam != 0 && bTeam != 0 && aTeam == bTeam)
	//	{
	//		server_data.GameRound.GameTime++;
	//	}
	//		
	//	else if ((aTeam < bTeam) && server_data.GameRound.Bteam <= 99)
	//	{
	//		server_data.GameRound.Bteam += (bTeam - aTeam) * 2;
	//		server_data.GameRound.GameTime++;
	//	}
	//		
	//	else
	//	{
	//		if ((server_data.GameRound.Ateam) >= 99 && (aTeam >= 1))
	//		{
	//			server_data.GameRound.GameTime++;
	//			if (bTeam >= 1)
	//				server_data.GameRound.Ateam = 99;
	//			else
	//				server_data.GameRound.Ateam = 100;
	//		}
	//		else if ((server_data.GameRound.Bteam >= 99) && (bTeam >= 1))
	//		{
	//			server_data.GameRound.GameTime++;
	//			if (aTeam >= 1)
	//				server_data.GameRound.Bteam = 99;
	//			else
	//				server_data.GameRound.Bteam = 100;
	//		}
	//	}
	//	if (server_data.GameRound.Ateam >= 100)
	//	{
	//		server_data.GameRound.Ateam = 0;
	//		//server_data.
	//		server_data.GameRound.Around++;
	//		server_data.GameRound.GameTime = 180;
	//		server_data.GameRound.WaittingTime = 6;
	//		server_data.GameRound.GameStart = true;
	//		server_data.GameRound.RoundStart = false;
	//	}
	//	else if(server_data.GameRound.Bteam >= 100)
	//	{
	//		server_data.GameRound.Bteam = 0;
	//		server_data.GameRound.Bround++;
	//		server_data.GameRound.GameTime = 180;
	//		server_data.GameRound.WaittingTime = 6;
	//		server_data.GameRound.GameStart = true;
	//		server_data.GameRound.RoundStart = false;
	//	}
	//}
	//	break;
	//case 4:
	//{		
	//	int iKill = 0;

	//	for (int i = 0; i < MAX_Client; ++i)
	//	{
	//		if (!server_data.Players[i].bConnect)
	//			continue;
	//		if (server_data.Players[i].Kill > iKill)
	//		{
	//			iKill = server_data.Players[i].Kill;
	//			server_data.GameRound.Ateam = i;
	//		}
	//	}
	//}
	//	break;
	//default:
	//	break;
	//}
}

void LoadGameFile()
{
	//HANDLE hFile = CreateFile("../../Server.dat", GENERIC_WRITE, 0, 0, CREATE_ALWAYS,
	//	FILE_ATTRIBUTE_NORMAL, nullptr);

	//if (INVALID_HANDLE_VALUE == hFile)
	//{
	//	DisplayText(hList, "Road Game File Failed!\n");
	//	return;
	//}

	//DWORD dwByte = 0;

	//WriteFile(hFile, &CountPlayerNum, sizeof(int), &dwByte, nullptr);

	//for (int i = 0; i < 5; ++i)
	//{
	//	int Num = 0;
	//	int length = 1;
	//	ReadFile(hFile, &length, sizeof(int), &dwByte, nullptr);
	//	ReadFile(hFile, &ipAddress[i], sizeof(length), &dwByte, nullptr);
	//	ReadFile(hFile, &Num, sizeof(int), &dwByte, nullptr);
	//	tempNum[i] = Num;
	//}

	//CloseHandle(hFile);
	//DisplayText(hList, "Road Game File Success!\n");
}

void Check_InteractionOfPlayer()
{
	// Camper
	int GPacket[7] = { 0,0,0,0,0,0,0 };
	int Generator[7] = { 0,0,0,0,0,0,0 };
	bool Totem[5] = { false,false,false,false,false };
	bool ItemBox[10] = { false,false,false,false,false,false,false,false,false,false };
	int UseToolBox[7] = { 0,0,0,0,0,0,0 };
	int HealCamper[4] = { 0,0,0,0 };

	for (int i = 0; i < 4; ++i)
	{
		if (!server_data.Campers[i].bConnect)
			continue;

		//if (0 == server_data.Campers[i].InterationObject)
		//	continue;
		if (0 > server_data.Campers[i].InterationObject || server_data.Campers[i].InterationObject >= 0x200000)
			continue;

		if (0 > server_data.Campers[i].SecondInterationObject || 0x200000 <= server_data.Campers[i].SecondInterationObject)
			continue;

		if (server_data.Campers[i].SecondInterationObject & CAMPER)
		{
			int Index = server_data.Campers[i].SecondInterationObject - CAMPER;

			if (!(server_data.Game_Data.iPlayerAction[Index] & server_data.Campers[i].SecondInterationObjAnimation))
			{
				if (server_data.Campers[i].SecondInterationObjAnimation == HOOK_BEING && (server_data.Campers[i].Perk & BORROWEDTIME))
					server_data.Game_Data.iPlayerAction[Index] |= HOOK_BEINGBORROWED;
				else
					server_data.Game_Data.iPlayerAction[Index] |= server_data.Campers[i].SecondInterationObjAnimation;

				if (server_data.Campers[i].SecondInterationObjAnimation == CLEAR_PACKET)
				{
					server_data.Game_Data.iPlayerAction[Index] = 0;
				}
			}		
		}
		if (server_data.Campers[i].SecondInterationObject & SLASHER)
		{
			if(!(server_data.Game_Data.iPlayerAction[4] & server_data.Campers[i].SecondInterationObjAnimation))
				server_data.Game_Data.iPlayerAction[4] |= server_data.Campers[i].SecondInterationObjAnimation;
		}
		if (server_data.Campers[i].RecvPacket != 0)
		{
			server_data.Game_Data.iPlayerAction[i] ^= server_data.Campers[i].RecvPacket;
		}

		if (server_data.Campers[i].InterationObject & GENERATOR)
		{
			if (server_data.Campers[i].InterationObject < 0)
				server_data.Campers[i].InterationObject = 0;
			if (server_data.Campers[i].SecondInterationObject < 0)
				server_data.Campers[i].SecondInterationObject = 0;

			int Index = server_data.Campers[i].InterationObject - GENERATOR;
			++Generator[Index];
			if (server_data.Campers[i].Packet == GSKILLCHECKFAIL)
			{
				GPacket[Index] = GSKILLCHECKFAIL;
				server_data.Campers[i].Packet = 0;
			}
			else if (server_data.Campers[i].Packet == GSKILLCHECKGREATSUCCESS)
			{
				GPacket[Index] = GSKILLCHECKGREATSUCCESS;
				server_data.Campers[i].Packet = 0;
			}

			if (server_data.Campers[i].bUseItem
				&&	int(server_data.Campers[i].iItem % 10) == 2)
			{
				++UseToolBox[Index];
			}
		}

		else if (server_data.Campers[i].InterationObject & EXITDOOR)
		{
			int Index = server_data.Campers[i].InterationObject - EXITDOOR;
			server_data.Game_Data.ExitDoor[Index] += fTime;
		}

		else if (server_data.Campers[i].InterationObject & CHEST)
		{
			int Index = server_data.Campers[i].InterationObject - CHEST;

			if (server_data.Game_Data.ItemBox[Index] < 15.f)
			{
				if(server_data.Campers[i].iState != 64)
					server_data.Game_Data.ItemBox[Index] += fTime;
				server_data.Game_Data.ItemBoxAnim[Index] = server_data.Campers[i].InterationObjAnimation;
			}
			//else
			//	server_data.Game_Data.ItemBoxAnim[Index] = 2;
			else if (server_data.Game_Data.ItemBox[Index] >= 15.f && server_data.Game_Data.ItemBox[Index] < 20.f)
			{
				server_data.Game_Data.ItemBoxAnim[Index] = 2;
				for (int j = 0; j < 10; ++j)
				{
					if (0 == sItem[j])
					{
						sItem[j] = (rand() % 4) + 1;
						server_data.Game_Data.Item[j] = sItem[j];
						server_data.Game_Data.ItemBox[Index] = 1000.5f + (j * 10.f) + sItem[j];
						break;
					}
				}
			}

		}
		else if (server_data.Campers[i].InterationObject & ITEM)
		{
			int Index = server_data.Campers[i].InterationObject - ITEM;

			//if (int(server_data.Game_Data.Item[Index] / 10) == 0
			//	&& server_data.Campers[i].InterationObjAnimation == 1)
			//{
			//	int PlayerNum = (i + 1) * 10;
			//	int ItemNum = server_data.Game_Data.Item[Index] % 10;
			//	server_data.Game_Data.Item[Index] = PlayerNum + ItemNum;
			//}
			//if (int(server_data.Game_Data.Item[Index] / 10) != 0
			//	&& server_data.Campers[i].InterationObjAnimation == 2)
			//{
			//	int PlayerNum = (i + 1) * 10;
			//	int ItemNum = server_data.Game_Data.Item[Index] % 10;
			//	server_data.Game_Data.Item[Index] -= PlayerNum;
			//}
			//if (int(server_data.Game_Data.Item[Index] / 10) != 0
			//	&& server_data.Campers[i].InterationObjAnimation == 3)
			//{
			//	int PlayerNum = (i + 1) * 10;
			//	int ItemNum = server_data.Game_Data.Item[Index] % 10;
			//	server_data.Game_Data.Item[Index] = PlayerNum + ItemNum;
			//	for (int j = 0; j < 10; ++j)
			//	{
			//		if (j == Index)
			//			continue;

			//		if ((server_data.Game_Data.Item[j] / 10) == (i + 1))
			//		{
			//			server_data.Game_Data.Item[j] = ItemNum;
			//			break;
			//		}
			//	}
			//}
			if (server_data.Campers[i].InterationObjAnimation == 4)
			{
				server_data.Game_Data.Item[Index] = 0;
			}
		}
		else if (server_data.Campers[i].InterationObject & TOTEM)
		{
			int Index = server_data.Campers[i].InterationObject - TOTEM;

			Totem[Index] = true;
		}

		else if (server_data.Campers[i].InterationObject & CLOSET)
		{
			int Index = server_data.Campers[i].InterationObject - CLOSET;
			if (Index > 0 && Index < 100)
				server_data.Game_Data.Locker[Index] = server_data.Campers[i].InterationObjAnimation;
		}

		else if (server_data.Campers[i].InterationObject & HOOK)
		{
			int Index = server_data.Campers[i].InterationObject - HOOK;

			if (server_data.Campers[i].InterationObjAnimation == SABOTAGEING)
				server_data.Game_Data.HookBroken[Index] += fTime;
			else
				server_data.Game_Data.Hook[Index] = server_data.Campers[i].InterationObjAnimation;

			if (server_data.Campers[i].InterationObjAnimation == BROKENING)
			{
				HookRegen[Index] = HOOKREPAIRTIME;
				server_data.Game_Data.HookBroken[Index] = 0.f;
			}

			if (server_data.Game_Data.HookBroken[Index] >= SABOTAGETIME)
			{
				HookRegen[Index] = HOOKREPAIRTIME;
				server_data.Game_Data.HookBroken[Index] = 0.f;
			}
		}

		else if (server_data.Campers[i].InterationObject & PLANK)
		{
			int Index = server_data.Campers[i].InterationObject - PLANK;
			server_data.Game_Data.WoodBoard[Index] = (BYTE)server_data.Campers[i].InterationObjAnimation;
		}
		//else if (server_data.Campers[i].InterationObject & )
	}

	for (int i = 0; i < 5; ++i)
		TotemCheck(i, Totem[i]);

	// Slasher
	if (server_data.Slasher.SecondInterationObject & CAMPER)
	{
		int Index = server_data.Slasher.SecondInterationObject - CAMPER;

		if (!(server_data.Game_Data.iPlayerAction[Index] & server_data.Slasher.SecondInterationObjAnimation))
			server_data.Game_Data.iPlayerAction[Index] |= server_data.Slasher.SecondInterationObjAnimation;
	}
	if (server_data.Slasher.RecvPacket != 0)
	{
		server_data.Game_Data.iPlayerAction[4] ^= server_data.Slasher.RecvPacket;
	}

	if (server_data.Slasher.InterationObject & CLOSET)
	{
		int Index = server_data.Slasher.InterationObject - CLOSET;
		server_data.Game_Data.Locker[Index] = server_data.Slasher.InterationObjAnimation;
	}

	if (server_data.Slasher.InterationObject & PLANK)
	{
		int Index = server_data.Slasher.InterationObject - PLANK;
		server_data.Game_Data.WoodBoard[Index] = 2;
	}

	if (server_data.Slasher.InterationObject & GENERATOR)
	{
		int Index = server_data.Slasher.InterationObject - GENERATOR;
		DesGenerator[Index] = 320.f;
	}

	// CalGenerator
	for (int i = 0; i < 7; ++i)
		CalculateGernerator(i, Generator[i], GPacket[i], UseToolBox[i]);

	for (int i = 0; i < 12; ++i)
	{
		HookBrokenCheck(i);

		switch (server_data.Game_Data.Hook[i])
		{
		case SpiderStruggle2Sacrifice:
			server_data.Game_Data.Spider[i] = StruggleToSacrifice;
			break;
		case SpiderStabOut:
			server_data.Game_Data.Spider[i] = StabOut;
			break;
		case SpiderStabLoop:
			server_data.Game_Data.Spider[i] = StabLoop;
			break;
		case SpiderStabIN:
			server_data.Game_Data.Spider[i] = StabIN;
			break;
		case SpiderReaction_OUT:
			server_data.Game_Data.Spider[i] = Reaction_Out;
			break;
		case SpiderReaction_Loop:
			server_data.Game_Data.Spider[i] = Reaction_Loop;
			break;
		case SpiderReaction_IN:
			server_data.Game_Data.Spider[i] = Reaction_In;
			break;
		case Spider_Struggle:
			server_data.Game_Data.Spider[i] = Stuggle;
			break;
		default:
			server_data.Game_Data.Spider[i] = Invisible;
			break;
		}
	}

	if (!bCreateHatch)
	{
		_int count = 0;
		for (int i = 0; i < 7; ++i)
		{
			if (server_data.Game_Data.GRP[i] >= 79.9f)
				++count;
		}

		if (count >= 2)
		{
			bCreateHatch = true;
			server_data.Game_Data.iHatch = (rand() % 6) + 1;
		}		
	}
}

void CalculateGernerator(int Gindex, int NumPlayer, int Packet, int iUseToolBox)
{
	if (GeneratorFail[Gindex] > 0.01f)
	{
		GeneratorFail[Gindex] -= fTime;
		return;
	}
	else
		GeneratorFail[Gindex] = 0.00f;

	if (server_data.Game_Data.GRP[Gindex] > 80.f)
	{
		server_data.Game_Data.GRP[Gindex] = 80.15f;
		ClearGenerator[Gindex] = true;
		return;
	}

	if (server_data.Game_Data.GRP[Gindex] < -0.01f)
	{
		server_data.Game_Data.GRP[Gindex] = 0.0001f;
	}

	float	Progress = 0.f;
	float	fBonus = 0.f;

	switch (NumPlayer)
	{
	case 0:
		if (DesGenerator[Gindex] > 0.f)
		{
			if (server_data.Game_Data.GRP[Gindex] > 0.25f)
				Progress = -0.25f;
			DesGenerator[Gindex] -= fTime;
		}
		break;
	case 1:
		DesGenerator[Gindex] = 0.f;
		Progress = 1.f;
		if (iUseToolBox > 0)
			Progress *= 1.2f;
		break;
	case 2:
		DesGenerator[Gindex] = 0.f;
		Progress = 1.8f;
		if (iUseToolBox == 1)
			Progress += 0.9f * 0.2f;
		else if (iUseToolBox == 2)
			Progress *= 1.2f;
		break;
	case 3:
		DesGenerator[Gindex] = 0.f;
		Progress = 2.4f;
		if (iUseToolBox == 1)
			Progress += 0.8f * 0.2f;
		else if (iUseToolBox == 2)
			Progress += 1.6f * 0.2f;
		else if (iUseToolBox == 3)
			Progress *= 1.2f;
		break;
	case 4:
		DesGenerator[Gindex] = 0.f;
		Progress = 2.8f;
		if (iUseToolBox == 1)
			Progress += 0.7f * 0.2f;
		else if (iUseToolBox == 2)
			Progress += 1.4f * 0.2f;
		else if (iUseToolBox == 3)
			Progress += 2.1f * 0.2f;
		else if (iUseToolBox == 4)
			Progress *= 1.2f;
		break;
	default:

		break;
	}

	if (Packet == GSKILLCHECKFAIL)
	{
		server_data.Game_Data.GRP[Gindex] *= 0.9f;
		GeneratorFail[Gindex] = 2.f;
	}
	if (Packet == GSKILLCHECKGREATSUCCESS)
	{
		if (!(server_data.Game_Data.Curse & CRUIN))
			fBonus = server_data.Game_Data.GRP[Gindex] * 0.02f;
	}
	if(Packet == SUCCESS_CHECK)
	{
		if (server_data.Game_Data.Curse & CRUIN)
		{
			server_data.Game_Data.GRP[Gindex] *= 0.95f;
			GeneratorFail[Gindex] = 2.f;
		}		
	}

	server_data.Game_Data.GRP[Gindex] += (Progress * fTime) + fBonus;
}

void TotemCheck(int Tindex, bool Player)
{
	_float fTotem = 0.0f;

	_int Ruin = (server_data.Game_Data.Totem[Tindex] / 100.f);
	_float CurProgress = server_data.Game_Data.Totem[Tindex] - (Ruin * 100.f);

	if (Ruin == 0 && !bCurse[1] && (server_data.Slasher.Perk & CRUIN) && !(server_data.Game_Data.Curse & CRUIN))
	{
		_int irandom = (rand() % 10);
		if (irandom == 3)
		{
			bCurse[1] = true;
			Ruin |= CRUIN;
			server_data.Game_Data.Curse |= CRUIN;
		}
	}
	if (Ruin == 0 && !bCurse[0] && (server_data.Slasher.Perk & CSWALLOWEDHOPE) && !(server_data.Game_Data.Curse & CSWALLOWEDHOPE))
	{
		_int iGenerator = 0;
		for (int z = 0; z < 7; ++z)
		{
			if (ClearGenerator[z])
				++iGenerator;
		}

		if (iGenerator > 3)
		{
			_int irandom = (rand() % 10);
			if (irandom == 6)
			{
				bCurse[0] = true;
				Ruin |= CSWALLOWEDHOPE;
				server_data.Game_Data.Curse |= CSWALLOWEDHOPE;
			}
		}
	}

	if (Ruin == 0 && !bCurse[2] && (server_data.Slasher.Perk & INCANTATION) && !(server_data.Game_Data.Curse & CINCANTATION))
	{
		_int iGenerator = 0;
		for (int z = 0; z < 7; ++z)
		{
			if (ClearGenerator[z])
				++iGenerator;
		}

		if (iGenerator > 4)
		{
			_int irandom = (rand() % 10);
			if (irandom == 8)
			{
			bCurse[2] = true;
			Ruin |= CINCANTATION;
			server_data.Game_Data.Curse |= CINCANTATION;
			}
		}
	}

	if (CurProgress > 14.f)
	{
		CurProgress = 14.50f;
		if (Ruin != 0 && (server_data.Game_Data.Curse & Ruin))
			server_data.Game_Data.Curse ^= Ruin;
		
		return;
	}	

	if (!Player)
	{
		CurProgress = 0.05f;
	}	
	else
	{
		CurProgress += fTime;
	}
	
	server_data.Game_Data.Totem[Tindex] = (Ruin * 100.f) + CurProgress;
}

void HookBrokenCheck(int HookIndex)
{
	if ((BreakEnd == server_data.Game_Data.Hook[HookIndex]
		|| Broken_Idle == server_data.Game_Data.Hook[HookIndex])
		&& HookRegen[HookIndex] <= 0.01)
	{
		server_data.Game_Data.Hook[HookIndex] = Idle;
	}

	if (HookRegen[HookIndex] > 0.01f)
	{
		HookRegen[HookIndex] -= fTime;
		server_data.Game_Data.Hook[HookIndex] = BreakEnd;
	}
	else
		HookRegen[HookIndex] = 0.00f;
}