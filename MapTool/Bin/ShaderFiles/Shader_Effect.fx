

// 컨스턴트 테이블
matrix		g_matWorld, g_matView, g_matProj;
vector		g_vColor;

texture		g_DepthTexture;

sampler DepthSampler = sampler_state
{
	texture = g_DepthTexture;	
};



// 텍스쳐정보를 받아오기위한 전역변수.
texture		g_DiffuseTexture;

sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

// 로컬영역상에(정점버퍼에 있는 정점의 정보) 있는 정점의 정보를 받아온다.
struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
	vector	vProjPos : TEXCOORD1;

};


VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	// 정점 위치 * 월드 * 뷰 * 투영.
	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;

	return Out;
}


struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
};

struct PS_OUT
{
	vector	vColor : COLOR;
};



PS_OUT PS_MAIN(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;


	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;

	float2	vUV;

	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;	

	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fViewZ = vDepthInfo.g * 500.0f;

	Out.vColor = vDiffuseColor;	

	Out.vColor.a = Out.vColor.a * saturate(fViewZ - In.vProjPos.w);

	return Out;
}


// technique : 장치 지원여부에 따른 ㅅㅖ이더 선택을 가능하게 하기위해. 
technique	DefaultDevice
{
	pass Default_Rendering
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		
		//AlphaTestEnable = true;
		//AlphaFunc = Greater;
		//AlphaRef = 100;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}

