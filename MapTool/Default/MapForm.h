#pragma once


#include "GameObject.h"
#include "afxwin.h"
// CMapForm 폼 뷰입니다.

class CMapToolView;
class CMapForm : public CFormView
{
	DECLARE_DYNCREATE(CMapForm)
public:
	enum MOUSESTATE { STATE_Idle, STATE_Draw, STATE_Select };
	enum TYPESTATE {TYPE_MAP, TYPE_NAVI, TYPE_OBJECT};
protected:
	CMapForm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CMapForm();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAPFORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
private:
	_float Round(_float value, int pos);
	_vec3 Round(_vec3 value, int pos);
	void CheckMouseState();
	void KeyCheck();
	void SelectPoint();
	void AddObject();
	_matrix Compute_Matrix();
	void ResetPosition();
	void PlusPosition();
	void MinusPosition();
	void Set_ResetPoint();
	_matrix Compute_Matrix_OnlyPos();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedDraw();
	afx_msg void OnBnClickedIdle();
	afx_msg void OnBnClickedChange();
	afx_msg void OnBnClickedResetListBox();
	afx_msg void OnBnClickedTypeRadio();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnEnChangeEdits();
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedReset();
private:
	CMapToolView* m_pMapToolView = nullptr;
	_vec3 m_PickingPoint = { 0.f,0.f,0.f };
	CComboBox m_ComboStage;
	int m_TypeRadio = 0;
	
	CListBox m_pObjectList;

	MOUSESTATE m_MouseState = STATE_Idle;

	CEdit m_CMouseState;
	CGameObject* m_SelectObject = nullptr;
	_vec3 m_vPosition = { 0.f, 0.f, 0.f };
	_vec3 m_vRotation = { 0.f, 0.f, 0.f };
	_vec3 m_vScale = { 1.f, 1.f, 1.f };
	_int m_OtherOption = 0;
	_float m_fTemp = 0;
	_vec3 m_ResetPosition = { 0.f,0.f,0.f };
	BOOL m_IsChangeRot;
	BOOL m_IsTempPressing;
	BOOL m_IsTempMoveX = TRUE;
};


