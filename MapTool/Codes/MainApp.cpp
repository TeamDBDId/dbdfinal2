#include "stdafx.h"
#include "..\Headers\MainApp.h"
#include "Scene_Logo.h"
#include "Management.h"
#include "Camera_Debug.h"
#include "Input_Device.h"
#include "Defines.h"
#include "Scene_Stage.h"
#include "ToolManager.h"
#include "LoadManager.h"

// 객체가 지형위에 탈수 있게만드는작업. 
// 유아이
// 말로 : 절두체, 큐브의 충돌. 

_USING(Client)

CMainApp::CMainApp()
	: m_pManagement(CManagement::GetInstance())
{
	m_pManagement->AddRef();
}

HRESULT CMainApp::Ready_MainApp()
{
	if (FAILED(Ready_Default_Setting(CGraphic_Device::MODE_WIN, g_iBackCX, g_iBackCY)))
		return E_FAIL;

	if (FAILED(Ready_GaraHeightMap()))
		return E_FAIL;

	if (FAILED(Ready_GaraNavigation()))
		return E_FAIL;

	//if (FAILED(Ready_Render_State()))
	//	return E_FAIL;

	//if (FAILED(Ready_Sampler_State()))
	//	return E_FAIL;	

	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;

	if (FAILED(Ready_Start_Scene(SCENE_STAGE)))
		return E_FAIL;

	return NOERROR;
}

_int CMainApp::Update_MainApp(const _float & fTimeDelta)
{
	if (nullptr == m_pManagement)
		return -1;

	m_fTimeAcc += fTimeDelta;

	CInput_Device::GetInstance()->SetUp_InputState();

	KEYMGR->KeyMgr_Update();
	
	return m_pManagement->Update_Management(fTimeDelta);
}

void CMainApp::Render_MainApp()
{
	if (nullptr == m_pGraphic_Device)
		return;

	if (nullptr == m_pManagement)
		return;

	m_pGraphic_Device->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, D3DXCOLOR(0.f, 0.f, 1.f, 1.f), 1.f, 0);

	m_pGraphic_Device->BeginScene();

	// 객체들을 렌더링한다.
	if (nullptr != m_pRenderer)
		m_pRenderer->Render_RenderGroup();


	// 씬의 렌더를 호출한다.
	m_pManagement->Render_Management();

	m_pGraphic_Device->EndScene();

	m_pGraphic_Device->Present(nullptr, nullptr, 0, nullptr);

	++m_dwRenderCnt;

	if (m_fTimeAcc >= 1.f)
	{
		wsprintf(m_szFPS, L"FPS:%d", m_dwRenderCnt);
		m_dwRenderCnt = 0;
		m_fTimeAcc = 0.f;
	}

	SetWindowText(g_hWnd, m_szFPS);
}

HRESULT CMainApp::Ready_Default_Setting(CGraphic_Device::WINMODE eMode, const _uint& iSizeX, const _uint& iSizeY)
{
	if (FAILED(CGraphic_Device::GetInstance()->Ready_Graphic_Device(eMode, g_hWnd, iSizeX, iSizeY, &m_pGraphic_Device)))
		return E_FAIL;

	if (FAILED(CInput_Device::GetInstance()->Ready_Input_Device(g_hInst, AfxGetMainWnd()->m_hWnd)))
		return E_FAIL;

	if (FAILED(m_pManagement->Ready_Management(SCENE_END)))
		return E_FAIL;	

	GET_INSTANCE(CLoadManager)->Set_GraphicDevice(m_pGraphic_Device);

	return NOERROR;
}

HRESULT CMainApp::Ready_Prototype_GameObject()
{
	//// For.GameObject_Camera_Debug


	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"GameObject_Camera_Debug", CCamera_Debug::Create(m_pGraphic_Device))))
		return E_FAIL;

	return NOERROR;
}

HRESULT CMainApp::Ready_Prototype_Component()
{
	if (nullptr == m_pManagement)
		return E_FAIL;
	
	// For.Component_Transform
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Transform", CTransform::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Renderer
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Renderer", m_pRenderer = CRenderer::Create(m_pGraphic_Device))))
		return E_FAIL;
	m_pRenderer->AddRef();

	// For.Component_Buffer_TriCol
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Buffer_TriCol", CBuffer_TriCol::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Buffer_RcTex
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Buffer_RcTex", CBuffer_RcTex::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Shader_Default
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Default", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Default.fx"))))
		return E_FAIL;

	return NOERROR;
}

HRESULT CMainApp::Ready_Start_Scene(SCENEID eSceneID)
{
	if (nullptr == m_pManagement)
		return E_FAIL;

	CScene*			pScene = nullptr;

	switch (eSceneID)
	{
	case SCENE_LOGO:
		pScene = CScene_Logo::Create(m_pGraphic_Device);
		break;
	case SCENE_STAGE:
		pScene = CScene_Stage::Create(m_pGraphic_Device);
		break;
	}

	if (nullptr == pScene)
		return E_FAIL;

	if (FAILED(m_pManagement->SetUp_ScenePointer(pScene)))
		return E_FAIL;

	Safe_Release(pScene);

	return NOERROR;
}

HRESULT CMainApp::Ready_Render_State()
{
	if (nullptr == m_pGraphic_Device)
		return E_FAIL;

	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphic_Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

	return NOERROR;
}

HRESULT CMainApp::Ready_Sampler_State()
{
	m_pGraphic_Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
	m_pGraphic_Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);

	m_pGraphic_Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pGraphic_Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_pGraphic_Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

	return NOERROR;
}

HRESULT CMainApp::Ready_GaraHeightMap()
{
	LPDIRECT3DTEXTURE9		pHeightTexture = nullptr;

	if (FAILED(D3DXCreateTexture(m_pGraphic_Device, 129, 129, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &pHeightTexture)))
		return E_FAIL;

	D3DLOCKED_RECT			LockRect;

	pHeightTexture->LockRect(0, &LockRect, nullptr, 0);

	for (size_t i = 0; i < 129; ++i)
	{
		for (size_t j = 0; j < 129; j++)
		{
			size_t iIndex = i * 129 + j;

			if(i < 64)
				((_ulong*)LockRect.pBits)[iIndex] = D3DCOLOR_ARGB(255, 0, 0, 100);
			else
				((_ulong*)LockRect.pBits)[iIndex] = D3DCOLOR_ARGB(255, 0, 0, 0);
		}
	}



	pHeightTexture->UnlockRect(0);

	//D3DXSaveTextureToFile(L"../Bin/Sample.bmp", D3DXIFF_BMP, pHeightTexture, nullptr);

	Safe_Release(pHeightTexture);

	return NOERROR;
}

HRESULT CMainApp::Ready_GaraNavigation()
{
	HANDLE	hFile = 0;
	_ulong	dwByte = 0;

	hFile = CreateFile(L"../Bin/Data/Navi.dat", GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (0 == hFile)
		return E_FAIL;

	_uint			iNumCells = 4;

	WriteFile(hFile, &iNumCells, sizeof(_uint), &dwByte, nullptr);

	_vec3	vPoint[3];

	vPoint[0] = _vec3(0.0f, 0.0f, 5.0f);
	vPoint[1] = _vec3(5.0f, 0.0f, 0.0f);
	vPoint[2] = _vec3(0.0f, 0.0f, 0.0f);
	WriteFile(hFile, vPoint, sizeof(_vec3) * 3, &dwByte, nullptr);

	vPoint[0] = _vec3(0.0f, 0.0f, 10.0f);
	vPoint[1] = _vec3(5.0f, 0.0f, 5.0f);
	vPoint[2] = _vec3(0.0f, 0.0f, 5.0f);
	WriteFile(hFile, vPoint, sizeof(_vec3) * 3, &dwByte, nullptr);

	vPoint[0] = _vec3(0.0f, 0.0f, 5.0f);
	vPoint[1] = _vec3(5.0f, 0.0f, 5.0f);
	vPoint[2] = _vec3(5.0f, 0.0f, 0.0f);
	WriteFile(hFile, vPoint, sizeof(_vec3) * 3, &dwByte, nullptr);

	vPoint[0] = _vec3(5.0f, 0.0f, 5.0f);
	vPoint[1] = _vec3(10.0f, 0.0f, 0.0f);
	vPoint[2] = _vec3(5.0f, 0.0f, 0.0f);
	WriteFile(hFile, vPoint, sizeof(_vec3) * 3, &dwByte, nullptr);

	CloseHandle(hFile);

	return NOERROR;
}

CMainApp * CMainApp::Create()
{
	CMainApp*	pInstance = new CMainApp();

	if (FAILED(pInstance->Ready_MainApp()))
	{
		MessageBox(0, L"CMainApp Created Failed", L"System Error", MB_OK);

		if (nullptr != pInstance)
		{
			delete pInstance;
			pInstance = nullptr;
		}
	}

	return pInstance;
}

void CMainApp::Free()
{
	Safe_Release(m_pRenderer);
	Safe_Release(m_pManagement);
	Safe_Release(m_pGraphic_Device);
	_ulong dwRefCnt;
	if (dwRefCnt = CToolManager::GetInstance()->DestroyInstance())
		_MSG_BOX("CToolManager Release Failed");
	if (dwRefCnt = CLoadManager::GetInstance()->DestroyInstance())
		_MSG_BOX("CToolManager Release Failed");
	KEYMGR->DestroyInstance();

	CManagement::Release_Engine();	
}

