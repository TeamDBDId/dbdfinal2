#include "stdafx.h"
#include "..\Headers\Map_Static.h"
#include "Management.h"
#include "ToolManager.h"
#include "Defines.h"
_USING(Client)

CMap_Static::CMap_Static(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CMap_Static::CMap_Static(const CMap_Static & rhs)
	: CGameObject(rhs)
{
}

HRESULT CMap_Static::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CMap_Static::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_isInstancing = false;
	return NOERROR;
}

_int CMap_Static::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	OnTool();
	return _int();
}

_int CMap_Static::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance > 8000.f)
		return 0;

	//if (m_isInstancing)
	//	m_pRendererCom->Add_Instancing(m_pMeshCom, m_pTransformCom->Get_Matrix());
	//else
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

	return _int();
}

void CMap_Static::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);


	//	m_pColliderCom->Render_Collider();
}

void CMap_Static::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);
	m_iOtherOption = OtherOption;
	if (Key == nullptr)
		return;

	m_Key = Key;

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, Key);
	Add_Component(L"Com_Mesh", m_pMeshCom);

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_OBB, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);
}

_matrix CMap_Static::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CMap_Static::Get_Key()
{
	return m_Key.c_str();
}

_int CMap_Static::Get_OtherOption()
{
	return m_iOtherOption;
}

void CMap_Static::OnTool()
{
	if (nullptr != m_pPickingCom)
		m_pPickingCom->Update_Ray();
	_bool bIsPick = false;
	_vec4 vPickingPoint;
	bIsPick = m_pColliderCom->Picking_ToCollider(&vPickingPoint, m_pPickingCom);
	if (bIsPick)
		GET_INSTANCE(CToolManager)->AddPickingPosition(this, vPickingPoint);
}


HRESULT CMap_Static::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Picking
	m_pPickingCom = (CPicking*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Picking");
	if (FAILED(Add_Component(L"Com_Picking", m_pPickingCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CMap_Static::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	D3DXMatrixInverse(&matView, nullptr, &matView);
	pEffect->SetVector("g_vCamPosition", (_vec4*)&matView.m[3][0]);

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);

	if (9 == m_iOtherOption)
		pEffect->SetBool("g_isColl",true);
	else
		pEffect->SetBool("g_isColl", false);


	return NOERROR;
}

CMap_Static * CMap_Static::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CMap_Static*	pInstance = new CMap_Static(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CMap_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CMap_Static::Clone_GameObject()
{
	CMap_Static*	pInstance = new CMap_Static(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CMap_Static::Free()
{
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pPickingCom);
	CGameObject::Free();
}
