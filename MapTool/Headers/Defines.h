#pragma once

#include "Base.h"
#include "KeyManager.h"
// ����
inline _vec3 operator^(_vec3 u, _vec3 v)
{
	return _vec3(u.y*v.z - u.z*v.y, -u.x*v.z + u.z*v.x, u.x*v.y - u.y*v.x);
}
// ����
inline float operator*(_vec3 u, _vec3 v)
{
	return (u.x*v.x + u.y*v.y + u.z*v.z);
}

const unsigned int g_iBackCX = 1280;
const unsigned int g_iBackCY = 720;

enum SCENEID {SCENE_STATIC, SCENE_LOGO, SCENE_STAGE, SCENE_END};
	
extern HINSTANCE g_hInst;
extern HWND g_hWnd;

#define KEYMGR	CKeyManager::GetInstance()