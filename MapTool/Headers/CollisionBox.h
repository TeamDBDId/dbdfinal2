#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CCollider;
class CPicking;
class CFrustum;
_END


_BEGIN(Client)

class CCollisionBox : public CGameObject
{
private:
	explicit CCollisionBox(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCollisionBox(const CCollisionBox& rhs);
	virtual ~CCollisionBox() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();


public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
private:
	void OnTool();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CPicking*			m_pPickingCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	_float	m_Max = 0.f;
	wstring m_Key = L"";
private:
	HRESULT Ready_Component();
public:
	static CCollisionBox* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
