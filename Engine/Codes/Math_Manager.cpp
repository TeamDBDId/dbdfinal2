#include "Math_Manager.h"
#include <random>


Math_Manager::Math_Manager()
{
}



_float Math_Manager::ProgressToPersent(const _float & _iFrom, const _float & _iTo, const _float & _Persent)
{
	return _iFrom + (_Persent * (_iTo- _iFrom));
}

_vec3 Math_Manager::ProgressToPersent(const _vec3 & _iFrom, const _vec3 & _iTo, const _float & _Persent)
{
	return _vec3(ProgressToPersent(_iFrom.x, _iTo.x, _Persent), ProgressToPersent(_iFrom.y, _iTo.y, _Persent), ProgressToPersent(_iFrom.z, _iTo.z, _Persent));
}

_vec2 Math_Manager::ProgressToPersent(const _vec2 & _iFrom, const _vec2 & _iTo, const _float & _Persent)
{
	return _vec2(ProgressToPersent(_iFrom.x, _iTo.x, _Persent), ProgressToPersent(_iFrom.y, _iTo.y, _Persent));
}

_int Math_Manager::CalRandIntFromTo(const _int & _iFrom, const _int & _iTo)
{
	random_device rn;
	mt19937_64 rnd(rn());

	uniform_int_distribution<_int> range(_iFrom, _iTo);
	return range(rnd);
}

_float Math_Manager::CalRandFloatFromTo(const _float & _fFrom, const _float & _fTo)
{
	random_device rn;
	mt19937_64 rnd(rn());

	uniform_real_distribution <_float> fdis(_fFrom, _fTo);
	return fdis(rnd);
}


_bool Math_Manager::CalBoolPer(const _float & _fPer)
{
	return (rand() % 100 + 1) <= _fPer * 100.f;
}

_bool Math_Manager::IsRectIn(const _vec2 & MousePos, const _vec4 & Rect)
{
	if (MousePos.x >= Rect.x &&
		MousePos.x <= Rect.y &&
		MousePos.y >= Rect.z &&
		MousePos.y <= Rect.w)
		return true;
	return false;
}
