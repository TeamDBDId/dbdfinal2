#include "..\Headers\Instancing.h"
#include "Shader.h"
#include "GameObject.h"

CInstancing::CInstancing(LPDIRECT3DDEVICE9 pGraphic_Device)
	:m_pGraphic_Device(pGraphic_Device)
{
	m_pGraphic_Device->AddRef();
}

HRESULT CInstancing::Ready_Instrancing()
{
	if (FAILED(D3DXCreateEffectFromFile(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Instancing.fx", nullptr, nullptr, 0, nullptr, &m_pEffect, nullptr)))
		return E_FAIL;

	/*if (FAILED(m_pGraphic_Device->CreateVertexBuffer(sizeof(InstancingData) * 50, 0, 0, D3DPOOL_MANAGED, &m_pInstancingVB, nullptr)))
	return E_FAIL;*/
	m_pAttributeTable = new D3DXATTRIBUTERANGE[300];
	m_pGraphic_Device->CreateVertexDeclaration(g_VertexDecl, &m_pVertexDecl);


	return NOERROR;
}

void CInstancing::Add_Instancing(CMesh_Static * pMesh_Static, const _matrix & matWorld)
{
	if (pMesh_Static == nullptr)
		return;
	LPD3DXMESH Mesh = pMesh_Static->Get_Mesh();
	auto& iter = m_mapInstancing.find(Mesh);




	if (iter == m_mapInstancing.end())
	{
		vecMatrix vecmatrix;
		tagInstancing Instance;
		//Instance.vecMatrix.reserve(256); 
		Instance.vecMatrix.push_back(matWorld);
		Instance.Mesh_Static = pMesh_Static;
		m_mapInstancing.insert({ Mesh, Instance });
	}
	else
		iter->second.vecMatrix.push_back(matWorld);
}

void CInstancing::Rendering_Instancing()
{


	int iIndex = 0;
	_matrix matView, matProj, matVP;

	m_pGraphic_Device->SetVertexDeclaration(m_pVertexDecl);
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);
	matVP = matView * matProj;
	m_pEffect->SetMatrix("g_matVP", &matVP);


	//cout << m_mapInstancing.size() << endl;
	for (auto& iter : m_mapInstancing)
	{

		LPDIRECT3DVERTEXBUFFER9		pVB = nullptr;
		LPDIRECT3DINDEXBUFFER9		pIB = nullptr;

		iter.first->GetVertexBuffer(&pVB);
		iter.first->GetIndexBuffer(&pIB);

		m_pGraphic_Device->CreateVertexBuffer(sizeof(InstancingData) *iter.second.vecMatrix.size(), 0, 0, D3DPOOL_MANAGED, &m_pInstancingVB, nullptr);
		Setup_Instancing(iter.second.vecMatrix);//Instance DataBuffer에 데이터를 넣는다.
		m_pGraphic_Device->SetStreamSource(0, pVB, 0, D3DXGetDeclVertexSize(g_INDEXDATA, 0));
		m_pGraphic_Device->SetStreamSourceFreq(0, D3DSTREAMSOURCE_INDEXEDDATA | iter.second.vecMatrix.size());

		m_pGraphic_Device->SetStreamSource(1, m_pInstancingVB, 0, D3DXGetDeclVertexSize(g_INSTANCEDATA, 1));
		m_pGraphic_Device->SetStreamSourceFreq(1, D3DSTREAMSOURCE_INSTANCEDATA | 1ul);

		m_pGraphic_Device->SetIndices(pIB);
		m_pEffect->Begin(nullptr, 0);
		

		_ulong dwNumMaterials = iter.second.Mesh_Static->Get_NumMaterials();
		iter.first->GetAttributeTable(m_pAttributeTable, nullptr);

		_uint		iPassIndex = 0;

		for (_ulong i = 0; i<dwNumMaterials; ++i)
		{
			const SUBSETDESC* pSubSet = iter.second.Mesh_Static->Get_SubSetDesc(i);
		
			if (!strcmp(pSubSet->Material.pTextureFilename, "T_PlantAtlas01_D.tga") ||
				!strcmp(pSubSet->Material.pTextureFilename, "T_treeLeaf01_D.tga") ||
				!strcmp(pSubSet->Material.pTextureFilename, "T_PlantAtlasAlpha_BC.tga")||
				!strcmp(pSubSet->Material.pTextureFilename, "T_PlantAtlasAlpha01_D.tga") ||
				!strcmp(pSubSet->Material.pTextureFilename, "T_PlantAtlasAlpha01_D.tga")||
				!strcmp(pSubSet->Material.pTextureFilename, "T_PlantAtlas01_D.tga"))
					iPassIndex = 1;
			else
				iPassIndex = 0;
			m_pEffect->BeginPass(iPassIndex);

			if (!strcmp(pSubSet->Material.pTextureFilename, "T_Corn01_D.tga.tga") ||
				!strcmp(pSubSet->Material.pTextureFilename, "T_PlantAtlasAlpha_BC.tga"))
				m_pEffect->SetBool("g_isFootPrint", false);

			m_pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
			m_pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
			m_pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
			//m_pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
			//m_pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);
			m_pEffect->CommitChanges();

			m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, m_pAttributeTable[i].VertexStart, m_pAttributeTable[i].VertexCount, m_pAttributeTable[i].FaceStart * 3, m_pAttributeTable[i].FaceCount);

			m_pEffect->EndPass();
		}
		
		m_pEffect->End();

		m_pGraphic_Device->SetStreamSourceFreq(0, 1);
		m_pGraphic_Device->SetStreamSourceFreq(1, 1);

		iIndex++;
		iter.second.vecMatrix.clear();
		*iter.second.Mesh_Static->Get_RenderNum() = 0;
		Safe_Release(m_pInstancingVB);
		
	}
	m_mapInstancing.clear();
}

void CInstancing::Rendering_StempInstancing()
{
	int iIndex = 0;
	_matrix matView, matProj, matVP;

	m_pGraphic_Device->SetVertexDeclaration(m_pVertexDecl);
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);
	matVP = matView * matProj;
	m_pEffect->SetMatrix("g_matVP", &matVP);


	//cout << m_mapInstancing.size() << endl;
	for (auto& iter : m_mapInstancing)
	{

		LPDIRECT3DVERTEXBUFFER9		pVB = nullptr;
		LPDIRECT3DINDEXBUFFER9		pIB = nullptr;

		iter.first->GetVertexBuffer(&pVB);
		iter.first->GetIndexBuffer(&pIB);

		m_pGraphic_Device->CreateVertexBuffer(sizeof(InstancingData) *iter.second.vecMatrix.size(), 0, 0, D3DPOOL_MANAGED, &m_pInstancingVB, nullptr);
		Setup_Instancing(iter.second.vecMatrix);//Instance DataBuffer에 데이터를 넣는다.
		m_pGraphic_Device->SetStreamSource(0, pVB, 0, D3DXGetDeclVertexSize(g_INDEXDATA, 0));
		m_pGraphic_Device->SetStreamSourceFreq(0, D3DSTREAMSOURCE_INDEXEDDATA | iter.second.vecMatrix.size());

		m_pGraphic_Device->SetStreamSource(1, m_pInstancingVB, 0, D3DXGetDeclVertexSize(g_INSTANCEDATA, 1));
		m_pGraphic_Device->SetStreamSourceFreq(1, D3DSTREAMSOURCE_INSTANCEDATA | 1ul);

		m_pGraphic_Device->SetIndices(pIB);
		m_pEffect->Begin(nullptr, 0);
		m_pEffect->BeginPass(2);

		_ulong dwNumMaterials = iter.second.Mesh_Static->Get_NumMaterials();
		iter.first->GetAttributeTable(m_pAttributeTable, nullptr);

		for (_ulong i = 0; i<dwNumMaterials; ++i)
		{
			const SUBSETDESC* pSubSet = iter.second.Mesh_Static->Get_SubSetDesc(i);

			m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, m_pAttributeTable[i].VertexStart, m_pAttributeTable[i].VertexCount, m_pAttributeTable[i].FaceStart * 3, m_pAttributeTable[i].FaceCount);
		}
		m_pEffect->EndPass();
		m_pEffect->End();

		m_pGraphic_Device->SetStreamSourceFreq(0, 1);
		m_pGraphic_Device->SetStreamSourceFreq(1, 1);

		iIndex++;
		Safe_Release(m_pInstancingVB);
	}
}

void CInstancing::Setup_Instancing(vector<_matrix> vecMatrix)
{
	InstancingData* pVerts;
	if (m_pInstancingVB == nullptr)
		return;
	m_pInstancingVB->Lock(0, 0, (void**)&pVerts, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE);

	for (_uint i = 0; i < vecMatrix.size(); i++)
	{
		memcpy(&(pVerts + i)->vRight, &vecMatrix[i].m[0][0], sizeof(_vec3));
		memcpy(&(pVerts + i)->vUp, &vecMatrix[i].m[1][0], sizeof(_vec3));
		memcpy(&(pVerts + i)->vLook, &vecMatrix[i].m[2][0], sizeof(_vec3));
		memcpy(&(pVerts + i)->vPosition, &vecMatrix[i].m[3][0], sizeof(_vec3));
	}
	m_pInstancingVB->Unlock();
}




CInstancing * CInstancing::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CInstancing*	pInstance = new CInstancing(pGraphic_Device);

	if (FAILED(pInstance->Ready_Instrancing()))
	{
		MessageBox(0, L"CInstancing Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CInstancing::Free()
{
	Safe_Delete_Array(m_pAttributeTable);
	Safe_Release(m_pGraphic_Device);
	Safe_Release(m_pInstancingVB);
	Safe_Release(m_pEffect);
	Safe_Release(m_pVertexDecl);
	m_mapInstancing.clear();
}
