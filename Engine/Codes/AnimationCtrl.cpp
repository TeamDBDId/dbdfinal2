#include "..\Headers\AnimationCtrl.h"

CAnimationCtrl::CAnimationCtrl()
{
}

HRESULT CAnimationCtrl::Ready_Animation(LPD3DXANIMATIONCONTROLLER pAniCtrl)
{
	m_pAniCtrl = pAniCtrl;
	m_pAniCtrl->AddRef();

	return NOERROR;
}

HRESULT CAnimationCtrl::Clone_Animation(const CAnimationCtrl & rhs)
{
	if (nullptr == &rhs)
		return NOERROR;

	if (FAILED(rhs.m_pAniCtrl->CloneAnimationController(rhs.m_pAniCtrl->GetMaxNumAnimationOutputs(), rhs.m_pAniCtrl->GetMaxNumAnimationSets(), rhs.m_pAniCtrl->GetMaxNumTracks(), rhs.m_pAniCtrl->GetMaxNumEvents(), &m_pAniCtrl)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CAnimationCtrl::Set_AnimationSet(const _uint & iAnimationID, _bool IsInterrupt)
{
	if (m_iOldAnimationIdx == iAnimationID)
		return NOERROR;

	LPD3DXANIMATIONSET		pAniSet = nullptr;

	m_pAniCtrl->GetAnimationSet(iAnimationID, &pAniSet);

	if (nullptr == pAniSet)
		return E_FAIL;

	if (m_bIsInterrupt)
	{
		m_iConnectAnimationIdx = iAnimationID;
		return NOERROR;
	}
	else
	{
		m_bIsInterrupt = IsInterrupt;

		m_Period = pAniSet->GetPeriod();

		m_iNewTrack = m_iCurrentTrack == 0 ? 1 : 0;

		if (FAILED(m_pAniCtrl->SetTrackAnimationSet(m_iNewTrack, pAniSet)))
			return E_FAIL;

		Safe_Release(pAniSet);

		m_pAniCtrl->UnkeyAllTrackEvents(m_iCurrentTrack);
		m_pAniCtrl->UnkeyAllTrackEvents(m_iNewTrack);

		m_pAniCtrl->KeyTrackEnable(m_iCurrentTrack, FALSE, m_TimeAcc + 0.25);
		m_pAniCtrl->KeyTrackSpeed(m_iCurrentTrack, 1.f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);
		m_pAniCtrl->KeyTrackWeight(m_iCurrentTrack, 0.1f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);

		m_pAniCtrl->SetTrackEnable(m_iNewTrack, TRUE);
		m_pAniCtrl->KeyTrackSpeed(m_iNewTrack, 1.f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);
		m_pAniCtrl->KeyTrackWeight(m_iNewTrack, 0.9f, m_TimeAcc, 0.25, D3DXTRANSITION_LINEAR);

		m_pAniCtrl->SetTrackPosition(m_iNewTrack, 0.0);
		m_pAniCtrl->ResetTime();
		m_TimeAcc = 0.0;

		m_iCurrentTrack = m_iNewTrack;
		m_iOldAnimationIdx = iAnimationID;
	}

	return NOERROR;
}

HRESULT CAnimationCtrl::Set_NoBleningAnimationSet(const _uint & iAnimationID)
{
 	if (m_iOldAnimationIdx == iAnimationID)
		return NOERROR;

	LPD3DXANIMATIONSET		pAniSet = nullptr;

	m_pAniCtrl->GetAnimationSet(iAnimationID, &pAniSet);

	if (nullptr == pAniSet)
		return E_FAIL;

	m_Period = pAniSet->GetPeriod();

	if (FAILED(m_pAniCtrl->SetTrackAnimationSet(m_iCurrentTrack, pAniSet)))
		return E_FAIL;

	Safe_Release(pAniSet);

	//m_pAniCtrl->UnkeyAllTrackEvents(m_iCurrentTrack);
	//m_pAniCtrl->SetTrackEnable(m_iCurrentTrack, TRUE);

	m_pAniCtrl->SetTrackPosition(m_iCurrentTrack, 0.0);
	m_pAniCtrl->ResetTime();
	m_TimeAcc = 0.0;
	m_iOldAnimationIdx = iAnimationID;

	return NOERROR;
}

void CAnimationCtrl::Play_Animation(const _float & fTimeDelta)
{
	m_pAniCtrl->AdvanceTime(fTimeDelta, nullptr);

	m_TimeAcc += fTimeDelta;
}

void CAnimationCtrl::Setup_AnimationTime(const _uint& Track,const _float & fFixingTime)
{
	m_pAniCtrl->SetTrackPosition(Track, 0.0);
	m_pAniCtrl->AdvanceTime(fFixingTime, nullptr);
}

void CAnimationCtrl::Stop_Animation()
{
	LPD3DXANIMATIONSET		pAniSet = nullptr;

	m_pAniCtrl->UnkeyAllTrackEvents(m_iOldAnimationIdx);
	m_pAniCtrl->SetTrackEnable(m_iOldAnimationIdx, FALSE);
	m_pAniCtrl->SetTrackPosition(m_iOldAnimationIdx, 0.0);
	m_iOldAnimationIdx = 0;
	m_pAniCtrl->ResetTime();
	m_TimeAcc = 0.0;

}

_bool CAnimationCtrl::IsOverTime(const _float & fCorrectValue)
{
	if (m_TimeAcc - (m_Period - fCorrectValue) > 0.001f)
		return true;

	return false;
}

_double CAnimationCtrl::GetPeriod()
{
	LPD3DXANIMATIONSET		pAniSet = nullptr;

	m_pAniCtrl->GetAnimationSet(m_iCurrentTrack, &pAniSet);
	
	return pAniSet->GetPeriod();
}

_double CAnimationCtrl::GetPeriod(_uint AniNum)
{
	LPD3DXANIMATIONSET		pAniSet = nullptr;

	m_pAniCtrl->GetAnimationSet(AniNum, &pAniSet);

	return pAniSet->GetPeriod();
}

_double CAnimationCtrl::GetTimeAcc()
{
	return m_pAniCtrl->GetTime();
}

HRESULT CAnimationCtrl::Set_SpecifyAnimationSet(ANIMSET tagAnimSet)
{
	LPD3DXANIMATIONSET		pOffsetAniSet = nullptr;
	LPD3DXANIMATIONSET		pBlendAniSet = nullptr;

	m_pAniCtrl->GetAnimationSet(tagAnimSet.iOffsetAnimationID, &pOffsetAniSet);
	m_pAniCtrl->GetAnimationSet(tagAnimSet.iBlendAnimationID, &pBlendAniSet);
	if (nullptr == pOffsetAniSet || nullptr == pBlendAniSet)
		return E_FAIL;

	_uint iOffsetTrack = 0;
	_uint iBlendTrack = 1;

	if (FAILED(m_pAniCtrl->SetTrackAnimationSet(iOffsetTrack, pOffsetAniSet)))
		return E_FAIL;
	if (FAILED(m_pAniCtrl->SetTrackAnimationSet(iBlendTrack, pBlendAniSet)))
		return E_FAIL;

	Safe_Release(pOffsetAniSet);
	Safe_Release(pBlendAniSet);

	m_pAniCtrl->UnkeyAllTrackEvents(iOffsetTrack);
	m_pAniCtrl->UnkeyAllTrackEvents(iBlendTrack);

	m_pAniCtrl->SetTrackEnable(iOffsetTrack, TRUE);
	m_pAniCtrl->KeyTrackSpeed(iOffsetTrack, 1.f, m_TimeAcc, 0.00001, D3DXTRANSITION_LINEAR);
	m_pAniCtrl->KeyTrackWeight(iOffsetTrack, tagAnimSet.fOffsetWeight, m_TimeAcc, 0.00001, D3DXTRANSITION_LINEAR);

	m_pAniCtrl->SetTrackEnable(iBlendTrack, TRUE);
	m_pAniCtrl->KeyTrackSpeed(iBlendTrack, 1.f, m_TimeAcc, 0.00001, D3DXTRANSITION_LINEAR);
	m_pAniCtrl->KeyTrackWeight(iBlendTrack, tagAnimSet.fBlendWeight, m_TimeAcc, 0.00001, D3DXTRANSITION_LINEAR);

	//m_pAniCtrl->SetTrackPosition(iOffsetTrack, m_TimeAcc);
	//m_pAniCtrl->SetTrackPosition(iBlendTrack, m_TimeAcc);
	//m_pAniCtrl->ResetTime();
	//m_TimeAcc = 0.0;

	m_iCurrentTrack = iBlendTrack;
	m_iOldAnimationIdx = tagAnimSet.iOffsetAnimationID;
	
	return NOERROR;
}

CAnimationCtrl * CAnimationCtrl::Create(LPD3DXANIMATIONCONTROLLER pAniCtrl)
{
	CAnimationCtrl*	pInstance = new CAnimationCtrl();

	if (FAILED(pInstance->Ready_Animation(pAniCtrl)))
	{
		MessageBox(0, L"pAniCtrl Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}

CAnimationCtrl * CAnimationCtrl::Clone()
{
	CAnimationCtrl*	pInstance = new CAnimationCtrl();

	if (FAILED(pInstance->Clone_Animation(*this)))
	{
		MessageBox(0, L"pAniCtrl Cloned Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}

void CAnimationCtrl::Free()
{

	Safe_Release(m_pAniCtrl);
}
