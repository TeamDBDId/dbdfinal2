#include "..\Headers\QuadTree.h"
#include "Frustum.h"

CQuadTree::CQuadTree(LPDIRECT3DDEVICE9 pGraphic_Device)
	: m_pGraphic_Device(pGraphic_Device)
{
	m_pGraphic_Device->AddRef();
}

HRESULT CQuadTree::Ready_QuadTree(const _vec3* pVerticesPos, const _uint& iNumVerticeX, const _uint& iNumVerticeZ)
{
	m_pVerticesPos = pVerticesPos;

	m_iCorner[CORNER_LT] = iNumVerticeX * iNumVerticeZ - iNumVerticeX;
	m_iCorner[CORNER_RT] = iNumVerticeX * iNumVerticeZ - 1;
	m_iCorner[CORNER_RB] = iNumVerticeX - 1;
	m_iCorner[CORNER_LB] = 0;

	m_iCenter = (m_iCorner[CORNER_LB] + m_iCorner[CORNER_RT]) >> 1;

	m_fRadius = D3DXVec3Length(&(m_pVerticesPos[m_iCorner[CORNER_LT]] - m_pVerticesPos[m_iCenter]));

	SetUp_ChildNode();

	SetUp_Neighbor();

	return NOERROR;
}

HRESULT CQuadTree::Ready_QuadTree(const _vec3 * pVerticesPos, const _uint & iLT, const _uint & iRT, const _uint & iRB, const _uint & iLB)
{
	m_pVerticesPos = pVerticesPos;

	m_iCorner[CORNER_LT] = iLT;
	m_iCorner[CORNER_RT] = iRT;
	m_iCorner[CORNER_RB] = iRB;
	m_iCorner[CORNER_LB] = iLB;

	m_iCenter = (m_iCorner[CORNER_LB] + m_iCorner[CORNER_RT]) >> 1;

	m_fRadius = D3DXVec3Length(&(m_pVerticesPos[m_iCorner[CORNER_LT]] - m_pVerticesPos[m_iCenter]));

	return NOERROR;
}

void CQuadTree::Culling_ToQuadTree(CFrustum * pFrustum, D3DXPLANE* pLocalPlane, POLYGON32* pVertexIndex, _uint& iNumPolygons)
{
	// 최하위 자식 쿼드트리일때만. 
	// if (nullptr == m_pChild[CHILD_LT])

	// 그냥 그려야할 상황이라면. 
	if(nullptr == m_pChild[CHILD_LT] ||
		 true == Check_LOD())
	{
		

		_bool		isIn[CORNER_END] = { false };

		isIn[CORNER_LT] = pFrustum->isIn_Frustum(pLocalPlane, &m_pVerticesPos[m_iCorner[CORNER_LT]], 0.f);
		isIn[CORNER_RT] = pFrustum->isIn_Frustum(pLocalPlane, &m_pVerticesPos[m_iCorner[CORNER_RT]], 0.f);
		isIn[CORNER_RB] = pFrustum->isIn_Frustum(pLocalPlane, &m_pVerticesPos[m_iCorner[CORNER_RB]], 0.f);
		isIn[CORNER_LB] = pFrustum->isIn_Frustum(pLocalPlane, &m_pVerticesPos[m_iCorner[CORNER_LB]], 0.f);


		_bool		isLOD[NEIGHBOR_END] = { true, true, true, true };

		for (size_t i = 0; i < NEIGHBOR_END; i++)
		{
			if (nullptr != m_pNeighbor[i])
				isLOD[i] = m_pNeighbor[i]->Check_LOD();
		}

		if (true == isLOD[NEIGHBOR_LEFT] &&
			true == isLOD[NEIGHBOR_TOP] &&
			true == isLOD[NEIGHBOR_RIGHT] &&
			true == isLOD[NEIGHBOR_BOTTOM])
		{
			if (true == isIn[CORNER_LT] ||
				true == isIn[CORNER_RT] ||
				true == isIn[CORNER_RB])
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LT];
				pVertexIndex[iNumPolygons]._1 = m_iCorner[CORNER_RT];
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_RB];
				++iNumPolygons;
			}

			if (true == isIn[CORNER_LT] ||
				true == isIn[CORNER_RB] ||
				true == isIn[CORNER_LB])
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LT];
				pVertexIndex[iNumPolygons]._1 = m_iCorner[CORNER_RB];
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_LB];
				++iNumPolygons;
			}
			return;
		}

		_uint		iLC, iTC, iRC, iBC;

		iLC = (m_iCorner[CORNER_LB] + m_iCorner[CORNER_LT]) >> 1;
		iTC = (m_iCorner[CORNER_RT] + m_iCorner[CORNER_LT]) >> 1;
		iRC = (m_iCorner[CORNER_RB] + m_iCorner[CORNER_RT]) >> 1;
		iBC = (m_iCorner[CORNER_LB] + m_iCorner[CORNER_RB]) >> 1;

		// 왼쩌ㅗㄱ 이우이 한번더 분열되서 그려지는거야!
		if (true == isIn[CORNER_LT] ||
			true == isIn[CORNER_RB] ||
			true == isIn[CORNER_LB])
		{
			if (false == isLOD[NEIGHBOR_LEFT])
			{
				
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LT];
				pVertexIndex[iNumPolygons]._1 = m_iCenter;
				pVertexIndex[iNumPolygons]._2 = iLC;
				++iNumPolygons;

				pVertexIndex[iNumPolygons]._0 = iLC;
				pVertexIndex[iNumPolygons]._1 = m_iCenter;
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_LB];
				++iNumPolygons;
				
			}
			else
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LT];
				pVertexIndex[iNumPolygons]._1 = m_iCenter;
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_LB];
				++iNumPolygons;
			}

			if (false == isLOD[NEIGHBOR_BOTTOM])
			{
				
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LB];
				pVertexIndex[iNumPolygons]._1 = m_iCenter;
				pVertexIndex[iNumPolygons]._2 = iBC;
				++iNumPolygons;

				pVertexIndex[iNumPolygons]._0 = iBC;
				pVertexIndex[iNumPolygons]._1 = m_iCenter;
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_RB];
				++iNumPolygons;
				
			}
			else
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LB];
				pVertexIndex[iNumPolygons]._1 = m_iCenter;
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_RB];
				++iNumPolygons;
			}
		}
		
		// 왼쩌ㅗㄱ 이우이 한번더 분열되서 그려지는거야!
		if (true == isIn[CORNER_LT] ||
			true == isIn[CORNER_RT] ||
			true == isIn[CORNER_RB])
		{
			if (false == isLOD[NEIGHBOR_TOP])
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LT];
				pVertexIndex[iNumPolygons]._1 = iTC;
				pVertexIndex[iNumPolygons]._2 = m_iCenter;
				++iNumPolygons;

				pVertexIndex[iNumPolygons]._0 = m_iCenter;
				pVertexIndex[iNumPolygons]._1 = iTC;
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_RT];
				++iNumPolygons;
			}
			else
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_LT];
				pVertexIndex[iNumPolygons]._1 = m_iCorner[CORNER_RT];
				pVertexIndex[iNumPolygons]._2 = m_iCenter;
				++iNumPolygons;
			}

			if (false == isLOD[NEIGHBOR_RIGHT])
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_RT];
				pVertexIndex[iNumPolygons]._1 = iRC;
				pVertexIndex[iNumPolygons]._2 = m_iCenter;
				++iNumPolygons;

				pVertexIndex[iNumPolygons]._0 = m_iCenter;
				pVertexIndex[iNumPolygons]._1 = iRC;
				pVertexIndex[iNumPolygons]._2 = m_iCorner[CORNER_RB];
				++iNumPolygons;

			}
			else
			{
				pVertexIndex[iNumPolygons]._0 = m_iCorner[CORNER_RT];
				pVertexIndex[iNumPolygons]._1 = m_iCorner[CORNER_RB];
				pVertexIndex[iNumPolygons]._2 = m_iCenter;
				++iNumPolygons;
			}
		}	

		return;
	}

	if (true == pFrustum->isIn_Frustum(pLocalPlane, &m_pVerticesPos[m_iCenter], m_fRadius))
	{
		for (size_t i = 0; i < CHILD_END; i++)
		{
			m_pChild[i]->Culling_ToQuadTree(pFrustum, pLocalPlane, pVertexIndex, iNumPolygons);
		}
	}
}

_bool CQuadTree::Check_LOD()
{
	_matrix			matView;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3			vCamPosition;
	memcpy(&vCamPosition, &matView.m[3][0], sizeof(_vec3));

	_float		fDistance = D3DXVec3Length(&(vCamPosition - m_pVerticesPos[m_iCenter]));
	_float		fWidth = D3DXVec3Length(&(m_pVerticesPos[m_iCorner[CORNER_RT]] - m_pVerticesPos[m_iCorner[CORNER_LT]]));

	if (fDistance * 0.1f > fWidth)
		return true;

	return _bool(false);
}

HRESULT CQuadTree::SetUp_ChildNode()
{
	if (1 == m_iCorner[CORNER_RT] - m_iCorner[CORNER_LT])
		return NOERROR;

	_uint		iLC, iTC, iRC, iBC;

	iLC = (m_iCorner[CORNER_LB] + m_iCorner[CORNER_LT]) >> 1;
	iTC = (m_iCorner[CORNER_RT] + m_iCorner[CORNER_LT]) >> 1;
	iRC = (m_iCorner[CORNER_RB] + m_iCorner[CORNER_RT]) >> 1;
	iBC = (m_iCorner[CORNER_LB] + m_iCorner[CORNER_RB]) >> 1;
	
	m_pChild[CHILD_LT] = CQuadTree::Create(m_pGraphic_Device, m_pVerticesPos, m_iCorner[CORNER_LT], iTC, m_iCenter, iLC);
	m_pChild[CHILD_RT] = CQuadTree::Create(m_pGraphic_Device, m_pVerticesPos, iTC, m_iCorner[CORNER_RT], iRC, m_iCenter);
	m_pChild[CHILD_RB] = CQuadTree::Create(m_pGraphic_Device, m_pVerticesPos, m_iCenter, iRC, m_iCorner[CORNER_RB], iBC);
	m_pChild[CHILD_LB] = CQuadTree::Create(m_pGraphic_Device, m_pVerticesPos, iLC, m_iCenter, iBC, m_iCorner[CORNER_LB]);	

	for (size_t i = 0; i < CHILD_END; i++)
	{
		m_pChild[i]->SetUp_ChildNode();
	}

	return NOERROR;
}

// 가장 큰 쿼드트리.
HRESULT CQuadTree::SetUp_Neighbor()
{
	if (nullptr == m_pChild[CHILD_LT])
		return NOERROR;

	m_pChild[CHILD_LT]->m_pNeighbor[NEIGHBOR_RIGHT] = m_pChild[CHILD_RT];
	m_pChild[CHILD_LT]->m_pNeighbor[NEIGHBOR_BOTTOM] = m_pChild[CHILD_LB];	
	if (nullptr != m_pNeighbor[NEIGHBOR_LEFT])
		m_pChild[CHILD_LT]->m_pNeighbor[NEIGHBOR_LEFT] = m_pNeighbor[NEIGHBOR_LEFT]->m_pChild[CHILD_RT];
	if (nullptr != m_pNeighbor[NEIGHBOR_TOP])
		m_pChild[CHILD_LT]->m_pNeighbor[NEIGHBOR_TOP] = m_pNeighbor[NEIGHBOR_TOP]->m_pChild[CHILD_LB];

	m_pChild[CHILD_RT]->m_pNeighbor[NEIGHBOR_LEFT] = m_pChild[CHILD_LT];
	m_pChild[CHILD_RT]->m_pNeighbor[NEIGHBOR_BOTTOM] = m_pChild[CHILD_RB];
	if (nullptr != m_pNeighbor[NEIGHBOR_RIGHT])
		m_pChild[CHILD_RT]->m_pNeighbor[NEIGHBOR_RIGHT] = m_pNeighbor[NEIGHBOR_RIGHT]->m_pChild[CHILD_LT];
	if (nullptr != m_pNeighbor[NEIGHBOR_TOP])
		m_pChild[CHILD_RT]->m_pNeighbor[NEIGHBOR_TOP] = m_pNeighbor[NEIGHBOR_TOP]->m_pChild[CHILD_RB];


	m_pChild[CHILD_RB]->m_pNeighbor[NEIGHBOR_LEFT] = m_pChild[CHILD_LB];
	m_pChild[CHILD_RB]->m_pNeighbor[NEIGHBOR_TOP] = m_pChild[CHILD_RT];
	if (nullptr != m_pNeighbor[NEIGHBOR_RIGHT])
		m_pChild[CHILD_RB]->m_pNeighbor[NEIGHBOR_RIGHT] = m_pNeighbor[NEIGHBOR_RIGHT]->m_pChild[CHILD_LB];
	if (nullptr != m_pNeighbor[NEIGHBOR_BOTTOM])
		m_pChild[CHILD_RB]->m_pNeighbor[NEIGHBOR_BOTTOM] = m_pNeighbor[NEIGHBOR_BOTTOM]->m_pChild[CHILD_RT];

	m_pChild[CHILD_LB]->m_pNeighbor[NEIGHBOR_RIGHT] = m_pChild[CHILD_RB];
	m_pChild[CHILD_LB]->m_pNeighbor[NEIGHBOR_TOP] = m_pChild[CHILD_LT];
	if (nullptr != m_pNeighbor[NEIGHBOR_LEFT])
		m_pChild[CHILD_LB]->m_pNeighbor[NEIGHBOR_LEFT] = m_pNeighbor[NEIGHBOR_LEFT]->m_pChild[CHILD_RB];
	if (nullptr != m_pNeighbor[NEIGHBOR_BOTTOM])
		m_pChild[CHILD_LB]->m_pNeighbor[NEIGHBOR_BOTTOM] = m_pNeighbor[NEIGHBOR_BOTTOM]->m_pChild[CHILD_LT];

	for (size_t i = 0; i < CHILD_END; i++)
	{
		if (nullptr != m_pChild[i])
			m_pChild[i]->SetUp_Neighbor();
	}

	return NOERROR;
}

CQuadTree * CQuadTree::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _vec3* pVerticesPos, const _uint& iNumVerticeX, const _uint& iNumVerticeZ)
{
	CQuadTree*	pInstance = new CQuadTree(pGraphic_Device);

	if (FAILED(pInstance->Ready_QuadTree(pVerticesPos, iNumVerticeX, iNumVerticeZ)))
	{
		MessageBox(0, L"CQuadTree Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CQuadTree * CQuadTree::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _vec3 * pVerticesPos, const _uint & iLT, const _uint & iRT, const _uint & iRB, const _uint & iLB)
{
	CQuadTree*	pInstance = new CQuadTree(pGraphic_Device);

	if (FAILED(pInstance->Ready_QuadTree(pVerticesPos, iLT, iRT, iRB, iLB)))
	{
		MessageBox(0, L"CQuadTree ChildNode Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CQuadTree::Free()
{
	for (size_t i = 0; i < CHILD_END; i++)	
		Safe_Release(m_pChild[i]);


	Safe_Release(m_pGraphic_Device);
}
