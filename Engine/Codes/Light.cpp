#include "..\Headers\Light.h"
#include "GameObject.h"
#include "Shader.h"
#include "Transform.h"
#include "Management.h"

CLight::CLight(LPDIRECT3DDEVICE9 pGraphic_Device)
	: m_pGraphic_Device(pGraphic_Device)
	
{
	m_pGraphic_Device->AddRef();
	
}

_vec3 CLight::GetProjPos()
{
	CalProjPos();

	return vProjPos;
}

HRESULT CLight::Ready_Light(const D3DLIGHT9 & LightInfo)
{
	// For.Shader_Cube
	
	m_LightInfo = LightInfo;

	//if (FAILED(D3DXCreateCubeTexture(m_pGraphic_Device, 512, 1, D3DUSAGE_RENDERTARGET, D3DFMT_R32F, D3DPOOL_DEFAULT, &m_pShadowTex)))
	//{
	//	if (FAILED(D3DXCreateCubeTexture(m_pGraphic_Device, 512, 1, 0, D3DFMT_R32F, D3DPOOL_DEFAULT, &m_pShadowTex)))
	//		return E_FAIL;
	//}

	if (FAILED(D3DXCreateCubeTexture(m_pGraphic_Device, 512, 1, D3DUSAGE_RENDERTARGET, D3DFMT_R32F, D3DPOOL_DEFAULT, &m_pTxCbm)))
	{
		if (FAILED(D3DXCreateCubeTexture(m_pGraphic_Device, 512, 1, 0, D3DFMT_R32F, D3DPOOL_DEFAULT, &m_pTxCbm)))
			return E_FAIL;
	}

	D3DVIEWPORT9		ViewPort;
	m_pGraphic_Device->GetViewport(&ViewPort);

	if (FAILED(m_pGraphic_Device->CreateVertexBuffer(sizeof(VTXVIEWPORT) * 4, 0, D3DFVF_XYZRHW | D3DFVF_TEX1, D3DPOOL_MANAGED, &m_pVB, nullptr)))
		return E_FAIL;

	VTXVIEWPORT*	pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec4(0.f - 0.5f, 0.0f - 0.5f, 0.0f, 1.f);
	pVertices[0].vTexUV = _vec2(0.0f, 0.f);

	pVertices[1].vPosition = _vec4(ViewPort.Width - 0.5f, 0.0f - 0.5f, 0.0f, 1.f);
	pVertices[1].vTexUV = _vec2(1.0f, 0.f);

	pVertices[2].vPosition = _vec4(ViewPort.Width - 0.5f, ViewPort.Height - 0.5f, 0.0f, 1.f);
	pVertices[2].vTexUV = _vec2(1.0f, 1.f);

	pVertices[3].vPosition = _vec4(0.0f - 0.5f, ViewPort.Height - 0.5f, 0.0f, 1.f);
	pVertices[3].vTexUV = _vec2(0.0f, 1.f);

	m_pVB->Unlock();

	if (FAILED(m_pGraphic_Device->CreateIndexBuffer(sizeof(POLYGON16) * 2, 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIB, nullptr)))
		return E_FAIL;
	POLYGON16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 0;
	pIndices[0]._1 = 1;
	pIndices[0]._2 = 2;

	pIndices[1]._0 = 0;
	pIndices[1]._1 = 2;
	pIndices[1]._2 = 3;

	m_pIB->Unlock();

	
	m_pFrustumCom = (CFrustum*)CManagement::GetInstance()->Clone_Component(2, L"Component_Frustum");

	return NOERROR;
}

void CLight::Render_Light(LPD3DXEFFECT pEffect)
{
	if (m_LightInfo.Type != D3DLIGHT_DIRECTIONAL)
	{
		if (!m_bIsRender)
		{
			m_bShaft = false;
			return;
		}		

		_matrix			matInverse;
		_matrix			matWorld;
		D3DXMatrixIdentity(&matWorld);
		memcpy(&matWorld.m[3], &m_LightInfo.Position, sizeof(_vec3));
		D3DXMatrixInverse(&matInverse, nullptr, &m_matWorld);
		
		if (!m_pFrustumCom->Culling_Frustum((_vec3*)&m_LightInfo.Position, matInverse, (m_LightInfo.Range + 50.f)))
		{
			m_bShaft = false;
			return;
		}	

		_matrix matView;
		m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
		D3DXMatrixInverse(&matView, nullptr, &matView);
		
		_vec3 vCamPos;
		memcpy(&vCamPos, &matView.m[3], sizeof(_vec3));
		m_fCameraDist = D3DXVec3Length(&(vCamPos - *(_vec3*)&m_LightInfo.Position));
		if (m_fCameraDist > m_LightInfo.Range * 30.f)
		{
			m_bShaft = false;
			return;
		}		
	}

	if (!m_bIsRender)
		return;

	m_iCount++;

	_int		iPassIdx = 0;

	if (D3DLIGHT_DIRECTIONAL == m_LightInfo.Type)
	{
		pEffect->SetVector("g_vLightDir", &_vec4(m_LightInfo.Direction, 0.f));
		iPassIdx = 0;
	}

	else if (D3DLIGHT_POINT == m_LightInfo.Type)
	{
		pEffect->SetVector("g_vLightPos", &_vec4(m_LightInfo.Position, 1.f));
		pEffect->SetFloat("g_fRange", m_LightInfo.Range);
		pEffect->SetBool("g_UseShadow", m_bUseShadowTex);

		if (m_bUseShadowTex)
		{
			pEffect->SetTexture("g_ShadowCubeTex", m_pTxCbm);	
		}
		m_bShaft = true;
		
		if (m_bIsShaft)
			m_bShaft = false;

		iPassIdx = 1;
	}

	else if (D3DLIGHT_SPOT == m_LightInfo.Type)
	{
		pEffect->SetVector("g_vLightPos", &_vec4(m_LightInfo.Position, 1.f));
		pEffect->SetFloat("g_fRange", m_LightInfo.Range);
		pEffect->SetFloat("g_fLightCutOff", m_LightInfo.Theta);
		pEffect->SetFloat("g_fLightOutCutOff", m_LightInfo.Phi);
		pEffect->SetVector("g_vLightDir", &_vec4(m_LightInfo.Direction, 0.f));

		pEffect->SetBool("g_UseShadow", m_bUseShadowTex);
		//pEffect->SetBool("g_UseDynamicShadow", m_bUseShadowTex);
		if (m_bUseShadowTex)
		{
			pEffect->SetTexture("g_ShadowCubeTex", m_pTxCbm);
			
		}
		//if (m_bUseDynamicShadowTex)
		//{
		//	pEffect->SetTexture("g_ShadowTex", m_pShadowTex);		
		//}

		m_bShaft = true;
		if (m_bIsShaft)
			m_bShaft = false;

		iPassIdx = 2;
	}
	
	pEffect->SetVector("g_vLightDiffuse", (_vec4*)&m_LightInfo.Diffuse);
	pEffect->SetVector("g_vLightAmbient", (_vec4*)&m_LightInfo.Ambient);
	pEffect->SetVector("g_vLightSpecular", (_vec4*)&m_LightInfo.Specular);

	_matrix			matView, matProj;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	D3DXMatrixInverse(&matView, nullptr, &matView);
	D3DXMatrixInverse(&matProj, nullptr, &matProj);

	pEffect->SetVector("g_vCamPosition", (_vec4*)&matView.m[3][0]);

	pEffect->SetMatrix("g_matViewInv", &matView);
	pEffect->SetMatrix("g_matProjInv", &matProj);

	pEffect->CommitChanges();

	pEffect->BeginPass(iPassIdx);

	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, sizeof(VTXVIEWPORT));
	m_pGraphic_Device->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

	pEffect->EndPass();

	if (m_iCount >= 60)
		m_iCount = 0;
}

void CLight::Render_ShadowMap()
{
	_matrix mtViwCur;

	_matrix mtViw[6];

	_matrix mtPrj;

	D3DXMatrixPerspectiveFovLH(&mtPrj, D3DX_PI / 2.0f, 1.0f, 5.f, 30000.0f);

	for (int i = 0; i < 6; ++i)
	{
		if (3 == i)
			continue;

		m_pTxCbm->GetCubeMapSurface((D3DCUBEMAP_FACES)(i), 0, &pSrf[i]);

		m_pGraphic_Device->GetRenderTarget(0, &pOld);
		if (SUCCEEDED(m_pGraphic_Device->SetRenderTarget(0, pSrf[i])))
		{
			m_pGraphic_Device->Clear(NULL, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1.0f, NULL);
		}

		T_SetupCubeViewMatrix(&mtViw[i], (D3DCUBEMAP_FACES)(i));

		_matrix matVP = mtViw[i] * mtPrj;

		LPD3DXEFFECT pEffect = m_pShader_Cube->Get_EffectHandle();

		//pEffect->Begin(nullptr, 0);
		//pEffect->BeginPass(0);

		for (auto& pGameObject : ObjList)
		{
			if (nullptr != pGameObject)
			{
				//CTransform* pTrans = (CTransform*)pGameObject->Get_ComponentPointer(L"Com_Transform");
				//_vec3 vPos = *pTrans->Get_StateInfo(CTransform::STATE_POSITION);
				//_vec3 vMyPos = m_LightInfo.Position;

				//_float length = D3DXVec3Length(&(vPos - vMyPos));
				//if (length <= (m_LightInfo.Range + 500.f))
					pGameObject->Render_ShadowCubeMap(&matVP, pEffect, *(_vec4*)&m_LightInfo.Position);
			}
		}

		//pEffect->EndPass();
		//pEffect->End();
		//m_pTxCbm->GetCubeMapSurface((D3DCUBEMAP_FACES)(i), 0, &pSrf[i]);
		//m_pGraphic_Device->StretchRect(, NULL, pSrf[i], NULL, D3DTEXF_LINEAR);

		m_pGraphic_Device->SetRenderTarget(0, pOld);

		Safe_Release(pOld);
		Safe_Release(pSrf[i]);

	}
	ObjList.clear();
	m_bUseShadowTex = true;

}

void CLight::Render_DynamicShadowMap()
{
	//if (!m_bUseDynamicShadowTex)
	//	return;

	//_matrix mtViwCur;

	//_matrix mtViw[6];

	//_matrix mtPrj;

	//D3DXMatrixPerspectiveFovLH(&mtPrj, D3DX_PI / 2.0f, 1.0f, 50.f, 30000.0f);

	//for (int i = 0; i < 6; ++i)
	//{
	//	m_pShadowTex->GetCubeMapSurface((D3DCUBEMAP_FACES)(i), 0, &pSrf[i]);

	//	m_pGraphic_Device->GetRenderTarget(0, &pOld);
	//	if (SUCCEEDED(m_pGraphic_Device->SetRenderTarget(0, pSrf[m_iCount % 6])))
	//	{
	//		m_pGraphic_Device->Clear(NULL, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1.0f, NULL);
	//	}

	//	T_SetupCubeViewMatrix(&mtViw[i], (D3DCUBEMAP_FACES)(i));

	//	_matrix matVP = mtViw[i] * mtPrj;

	//	LPD3DXEFFECT pEffect = m_pShader_Cube->Get_EffectHandle();

	//	//pEffect->Begin(nullptr, 0);
	//	//pEffect->BeginPass(0);

	//	for (auto& pGameObject : DynamicShadowList)
	//	{
	//		if (nullptr != pGameObject)
	//		{
	//			pGameObject->Render_ShadowMap(&matVP, pEffect, *(_vec4*)&m_LightInfo.Position);
	//		}
	//	}

	//	//pEffect->EndPass();
	//	//pEffect->End();

	//	m_pGraphic_Device->SetRenderTarget(0, pOld);

	//	Safe_Release(pOld);
	//	Safe_Release(pSrf[i]);
	//}
	//if (GetKeyState(VK_RETURN) < 0)
	//{
	//	D3DXSaveTextureToFile(L"../Bin/123.dds", D3DXIFF_DDS, m_pShadowTex, nullptr);
	//}

	//DynamicShadowList.clear();
}

void CLight::CalProjPos()
{
	//D3DVIEWPORT9 viewpt;
	//m_pGraphic_Device->GetViewport(&viewpt);
	//_matrix matView, matProj;
	//m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	//m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matProj);

	//if (m_LightInfo.Type == D3DLIGHT_POINT)
	//{
	//	D3DXVec3Project(&vProjPos, (_vec3*)&m_LightInfo.Position, &viewpt, &matProj, &matView, &m_matWorld);
	//}
}

_vec4 CLight::CalShaftVec()
{
	m_LightInfo.Position;

	D3DVIEWPORT9 viewpt;
	m_pGraphic_Device->GetViewport(&viewpt);
	_matrix matView, matProj, matOut;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	D3DVIEWPORT9 pViewPort;
	m_pGraphic_Device->GetViewport(&pViewPort);

	matOut = matView * matProj;
	_vec3 vOut;
	D3DXVec3TransformCoord(&vOut, (_vec3*)&m_LightInfo.Position, &matOut);
	
	vOut.x = ((vOut.x + 1) * pViewPort.Width / 2) / pViewPort.Width;
	vOut.y = ((-vOut.y + 1) * pViewPort.Height / 2) / pViewPort.Height;

	_float Dist = 1.f - (m_fCameraDist / 2000.f);
	_float Range = m_LightInfo.Range * 0.001f;

	_vec4 vReturn = { vOut.x, vOut.y, Dist, Range };
	return vReturn;
}

void CLight::T_SetupCubeViewMatrix(_matrix * pmtViw, DWORD dwFace)
{
	_vec3 vcEye = /*_vec3(0.f, 0.f, 0.f);*/ m_LightInfo.Position;

	_vec3 vcLook;

	_vec3 vcUp;


	switch (dwFace)

	{

	case D3DCUBEMAP_FACE_POSITIVE_X:

		//vcLook = _vec3(m_LightInfo.Position.x + 1.0f, m_LightInfo.Position.y, m_LightInfo.Position.z);

		//vcUp = _vec3(m_LightInfo.Position.x, m_LightInfo.Position.y + 1.0f, m_LightInfo.Position.z);

		vcLook = _vec3(1.f, 0.f, 0.f);
		vcUp = _vec3(0.f, 1.f, 0.f);
		break;

	case D3DCUBEMAP_FACE_NEGATIVE_X:

		//vcLook = _vec3(m_LightInfo.Position.x - 1.0f, m_LightInfo.Position.y, m_LightInfo.Position.z);

		//vcUp = _vec3(m_LightInfo.Position.x, m_LightInfo.Position.y + 1.0f, m_LightInfo.Position.z);

		vcLook = _vec3(-1.f, 0.f, 0.f);
		vcUp = _vec3(0.f, 1.f, 0.f);
		break;

	case D3DCUBEMAP_FACE_POSITIVE_Y:

		//vcLook = _vec3(m_LightInfo.Position.x, m_LightInfo.Position.y + 1.0f, m_LightInfo.Position.z);
		
		//vcUp = _vec3(m_LightInfo.Position.x , m_LightInfo.Position.y , m_LightInfo.Position.z - 1.0f);

		vcLook = _vec3(0.f, 1.f, 0.f);
		vcUp = _vec3(0.f, 0.f, -1.f);
		break;

	case D3DCUBEMAP_FACE_NEGATIVE_Y:

		//vcLook = _vec3(m_LightInfo.Position.x, m_LightInfo.Position.y - 1.0f, m_LightInfo.Position.z);

		//vcUp = _vec3(m_LightInfo.Position.x , m_LightInfo.Position.y , m_LightInfo.Position.z + 1.0f);

		vcLook = _vec3(0.f, -1.f, 0.f);
		vcUp = _vec3(0.f, 0.f, -1.f);
		break;

	case D3DCUBEMAP_FACE_POSITIVE_Z:

		//vcLook = _vec3(m_LightInfo.Position.x, m_LightInfo.Position.y, m_LightInfo.Position.z + 1.0f);

		//vcUp = _vec3(m_LightInfo.Position.x , m_LightInfo.Position.y + 1.0f, m_LightInfo.Position.z);

		vcLook = _vec3(0.f, 0.f, 1.f);
		vcUp = _vec3(0.f, 1.f, 0.f);
		break;

	case D3DCUBEMAP_FACE_NEGATIVE_Z:

		//vcLook = _vec3(m_LightInfo.Position.x, m_LightInfo.Position.y, m_LightInfo.Position.z - 1.0f);

		//vcUp = _vec3(m_LightInfo.Position.x , m_LightInfo.Position.y + 1.0f, m_LightInfo.Position.z );

		vcLook = _vec3(0.f, 0.f, -1.f);
		vcUp = _vec3(0.f, 1.f, 0.f);
		break;

	}
	vcLook += vcEye;
	D3DXMatrixLookAtLH(pmtViw, &vcEye, &vcLook, &vcUp);
}

CLight * CLight::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9 & LightInfo)
{
	CLight*	pInstance = new CLight(pGraphic_Device);

	if (FAILED(pInstance->Ready_Light(LightInfo)))
	{
		MessageBox(0, L"CLight Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CLight::Free()
{
	//Safe_Release(m_pShadowTex);
	Safe_Release(m_pFrustumCom);
	//Safe_Release(m_pShader_Cube);
	Safe_Release(m_pTxCbm);
	Safe_Release(m_pVB);
	Safe_Release(m_pIB);
	Safe_Release(m_pGraphic_Device);
}
