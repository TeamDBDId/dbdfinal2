#include "..\Headers\HierarchyLoader.h"
#include "MeshTexture.h"
CHierarchyLoader::CHierarchyLoader(LPDIRECT3DDEVICE9 pGraphic_Device)
	: m_pGraphic_Device(pGraphic_Device)
{
	m_pGraphic_Device->AddRef();
}

HRESULT CHierarchyLoader::Ready_HierarchyLoader(const _tchar * pFilePath)
{
	m_pFilePath = pFilePath;

	return NOERROR;
}

STDMETHODIMP CHierarchyLoader::CreateFrame(LPCSTR Name, LPD3DXFRAME * ppNewFrame)
{
	D3DXFRAME_DERIVED*		pFrame = new D3DXFRAME_DERIVED;
	ZeroMemory(pFrame, sizeof(D3DXFRAME_DERIVED));

	Allocate_Name(Name, &pFrame->Name);

	pFrame->CombinedTransformationMatrix = *D3DXMatrixIdentity(&pFrame->TransformationMatrix);

	*ppNewFrame = pFrame;

	return NOERROR;
}


// 로드한 메시를 보관하기위한 공간을 할당한다.
STDMETHODIMP CHierarchyLoader::CreateMeshContainer(LPCSTR Name
	, CONST D3DXMESHDATA * pMeshData
	, CONST D3DXMATERIAL * pMaterials
	, CONST D3DXEFFECTINSTANCE * pEffectInstances
	, DWORD NumMaterials
	, CONST DWORD * pAdjacency
	, LPD3DXSKININFO pSkinInfo, LPD3DXMESHCONTAINER * ppNewMeshContainer)
{
	D3DXMESHCONTAINER_DERIVED*		pMeshContainer = new D3DXMESHCONTAINER_DERIVED;
	ZeroMemory(pMeshContainer, sizeof(D3DXMESHCONTAINER_DERIVED));

	Allocate_Name(Name, &pMeshContainer->Name);



	// 실제 사용되는 메시의 타입을 보관한다.
	pMeshContainer->MeshData.Type = pMeshData->Type;

	// 실제 사용할 메시를 저장한다.
	LPD3DXMESH	pMesh = pMeshData->pMesh;
	pMesh->AddRef();

	// 인접폴리곤인덱스를 보관한다.
	_ulong		dwNumPolygons = pMesh->GetNumFaces();

	pMeshContainer->pAdjacency = new _ulong[dwNumPolygons * 3];
	memcpy(pMeshContainer->pAdjacency, pAdjacency, sizeof(_ulong) * dwNumPolygons * 3);

	_ulong dwFVF = 0;
	dwFVF |= D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX0 | D3DFVF_XYZB4 | D3DFVF_LASTBETA_D3DCOLOR;
	if (FAILED(pMesh->CloneMeshFVF(pMesh->GetOptions(), dwFVF, m_pGraphic_Device, &pMeshContainer->MeshData.pMesh)))
		return E_FAIL;

	//_ulong dwMeshFVF = pMesh->GetFVF();

	//if (dwMeshFVF & D3DFVF_NORMAL)
	//{
	//	if (FAILED(pMesh->CloneMeshFVF(pMesh->GetOptions(), dwMeshFVF, m_pGraphic_Device, &pMeshContainer->MeshData.pMesh)))
	//		return E_FAIL;
	//}
	//else
	//{
	//	if (FAILED(pMesh->CloneMeshFVF(pMesh->GetOptions(), dwMeshFVF | D3DFVF_NORMAL, m_pGraphic_Device, &pMeshContainer->MeshData.pMesh)))
	//		return E_FAIL;

	//	if (FAILED(D3DXComputeNormals(pMeshContainer->MeshData.pMesh, pMeshContainer->pAdjacency)))
	//		return E_FAIL;
	//}

	Safe_Release(pMesh);

	// 탄젠트를 추가한다.

	// 서브셋의 갯수를 저장한다.
	pMeshContainer->NumMaterials = NumMaterials == 0 ? 1 : NumMaterials;

	pMeshContainer->pMaterials = new D3DXMATERIAL[pMeshContainer->NumMaterials];

	// 서브셋 정보를 저장한다.
	if (0 == NumMaterials)
	{
		pMeshContainer->pMaterials[0].MatD3D.Diffuse = D3DXCOLOR(0.f, 0.f, 0.f, 1.f);
		pMeshContainer->pMaterials[0].MatD3D.Ambient = D3DXCOLOR(0.f, 0.f, 0.f, 1.f);
		pMeshContainer->pMaterials[0].MatD3D.Specular = D3DXCOLOR(0.f, 0.f, 0.f, 1.f);
		pMeshContainer->pMaterials[0].MatD3D.Emissive = D3DXCOLOR(0.f, 0.f, 0.f, 1.f);
		pMeshContainer->pMaterials[0].MatD3D.Power = 0.f;
		pMeshContainer->pMaterials[0].pTextureFilename = nullptr;
	}
	else
		memcpy(pMeshContainer->pMaterials, pMaterials, sizeof(D3DXMATERIAL) * pMeshContainer->NumMaterials);

	pMeshContainer->pSubSetDesc = new SUBSETDESC[pMeshContainer->NumMaterials];
	ZeroMemory(pMeshContainer->pSubSetDesc, sizeof(SUBSETDESC) * pMeshContainer->NumMaterials);

	CMeshTexture* pMeshTexture = GET_INSTANCE(CMeshTexture);
	if (pMeshTexture == nullptr)
		return E_FAIL;
	pMeshTexture->AddRef();

	for (_ulong i = 0; i < pMeshContainer->NumMaterials; ++i)
	{
		pMeshContainer->pSubSetDesc[i].Material = pMeshContainer->pMaterials[i];

		_tchar			szFullPath[MAX_PATH] = L"";
		_tchar			szFileName[MAX_PATH] = L"";

		MultiByteToWideChar(CP_ACP, 0, pMeshContainer->pMaterials[i].pTextureFilename, strlen(pMeshContainer->pMaterials[i].pTextureFilename), szFileName, MAX_PATH);

		lstrcpy(szFullPath, m_pFilePath);

		_tchar		szTmp[MAX_PATH] = L"";
		_tchar		szTmp2[MAX_PATH] = L"";
		lstrcpy(szTmp, szFullPath);
		lstrcat(szTmp, szFileName);
		if (FAILED(pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szFileName, &pMeshContainer->pSubSetDesc[i].MeshTexture.pDiffuseTexture)))
			return E_FAIL;

		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"N")))
		{
			lstrcat(szTmp, szTmp2);
			if (FAILED(pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pNormalTexture)))
				pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"T_FlatNormal_01.tga", &pMeshContainer->pSubSetDesc[i].MeshTexture.pNormalTexture);
		}
		else
			pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"T_FlatNormal_01.tga", &pMeshContainer->pSubSetDesc[i].MeshTexture.pNormalTexture);
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"ORM")))
		{
			lstrcat(szTmp, szTmp2);
			if (FAILED(pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture)))
				pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"WhiteAO.png", &pMeshContainer->pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture);
		}
		else
		{
			lstrcpy(szTmp, szFullPath);
			lstrcpy(szTmp2, szFileName);
			if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"CAO")))
			{
				lstrcat(szTmp, szTmp2);
				if (FAILED(pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture)))
					pMeshTexture->AddTexture(m_pGraphic_Device, L"../Bin/Resources/Meshes/Map/Stage1/Crate/", L"WhiteAO.png", &pMeshContainer->pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture);
			}
		}

		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"R")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pRoughnessTexture);
		}
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"H")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pHightTexture);
		}
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"M")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pMetallicTexture);
		}
		// Extends
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"E1")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pExtendsTexture1);
		}
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"E2")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pExtendsTexture2);
		}
		lstrcpy(szTmp, szFullPath);
		lstrcpy(szTmp2, szFileName);
		if (SUCCEEDED(Change_TextureFileName(szTmp2, L"BC", L"E3")))
		{
			lstrcat(szTmp, szTmp2);
			pMeshTexture->AddTexture(m_pGraphic_Device, m_pFilePath, szTmp2, &pMeshContainer->pSubSetDesc[i].MeshTexture.pExtendsTexture3);
		}
	}
	Safe_Release(pMeshTexture);

	// 피부에 대한 정보를 저장한다.
	pMeshContainer->pSkinInfo = pSkinInfo;
	pMeshContainer->pSkinInfo->AddRef();

	DWORD	maxVertInfluences = 0;
	DWORD	numBoneComboEntries = 0;

	if (FAILED(pSkinInfo->ConvertToIndexedBlendedMesh(pMeshData->pMesh,
		D3DXMESH_MANAGED,
		pMeshContainer->pSkinInfo->GetNumBones(), NULL, NULL, NULL, NULL, &maxVertInfluences,
		&numBoneComboEntries, &pMeshContainer->pBoneCombinationBuf, &pMeshContainer->MeshData.pMesh)))
		return E_FAIL;

	pMeshContainer->dwNumFrames = pMeshContainer->pSkinInfo->GetNumBones();

	pMeshContainer->pOffsetMatrices = new D3DXMATRIX[pMeshContainer->dwNumFrames];

	for (_ulong i = 0; i < pMeshContainer->dwNumFrames; ++i)
		pMeshContainer->pOffsetMatrices[i] = *pMeshContainer->pSkinInfo->GetBoneOffsetMatrix(i);

	pMeshContainer->ppCombinedTransformationMatrices = new D3DXMATRIX*[pMeshContainer->dwNumFrames];
	pMeshContainer->pRenderingMatrices = new D3DXMATRIX[pMeshContainer->dwNumFrames];



	D3DVERTEXELEMENT9			Element[MAX_FVF_DECL_SIZE];
	ZeroMemory(&Element, sizeof(D3DVERTEXELEMENT9));

	pMeshContainer->MeshData.pMesh->GetDeclaration(Element);

	//pMesh->GetDeclaration(Element);

	//if (FAILED(pMeshContainer->MeshData.pMesh->CloneMesh(pMeshContainer->MeshData.pMesh->GetOptions(), Element, m_pGraphic_Device, &pMeshContainer->pOriginalMesh)))
	//	return E_FAIL;

	void*		pVertices = nullptr;
	pMeshContainer->MeshData.pMesh->LockVertexBuffer(0, &pVertices);

	_uint		iVertexSize = D3DXGetDeclVertexSize(Element, 0);

	if (FAILED(D3DXComputeBoundingBox((_vec3*)pVertices, pMeshContainer->MeshData.pMesh->GetNumVertices(), iVertexSize, &pMeshContainer->vMin, &pMeshContainer->vMax)))
		return E_FAIL;

	pMeshContainer->MeshData.pMesh->UnlockVertexBuffer();

	//float3	vPosition : POSITION;
	//float3	vNormal : NORMAL;
	//float2	vTexUV : TEXCOORD0;
	//float4	weights : BLENDWEIGHT0;
	//int4	boneIndices : BLENDINDICES0;

	const D3DVERTEXELEMENT9 verDECL[] =
	{
		{ 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
		{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0 },
		{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
		{ 0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 },
		{ 0, 44, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 },
		{ 0, 60, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },	
		D3DDECL_END()
	};

	if (FAILED(pMeshContainer->MeshData.pMesh->CloneMesh(pMeshContainer->MeshData.pMesh->GetOptions(), verDECL, m_pGraphic_Device, &pMeshContainer->MeshData.pMesh)))
		return E_FAIL;

	if (FAILED(D3DXComputeTangent(pMeshContainer->MeshData.pMesh, 0, 0, 0, 0, (_ulong*)pMeshContainer->pAdjacency)))
		return E_FAIL;

	*ppNewMeshContainer = pMeshContainer;

	return NOERROR;
}


STDMETHODIMP CHierarchyLoader::DestroyFrame(LPD3DXFRAME pFrameToFree)
{
	Safe_Delete_Array(pFrameToFree->Name);


	if (nullptr != pFrameToFree->pFrameSibling)
		DestroyFrame(pFrameToFree->pFrameSibling);

	if (nullptr != pFrameToFree->pFrameFirstChild)
		DestroyFrame(pFrameToFree->pFrameFirstChild);

	Safe_Delete(pFrameToFree);

	return NOERROR;
}

STDMETHODIMP CHierarchyLoader::DestroyMeshContainer(LPD3DXMESHCONTAINER pMeshContainerToFree)
{
	D3DXMESHCONTAINER_DERIVED*		pMeshContainer = (D3DXMESHCONTAINER_DERIVED*)pMeshContainerToFree;

	for (size_t i = 0; i < pMeshContainer->NumMaterials; ++i)
	{
		pMeshContainer->pSubSetDesc[i].MeshTexture.pDiffuseTexture = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pNormalTexture = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pSpecularTexture = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pAmbientOcclusionTexture = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pHightTexture = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pMetallicTexture = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pRoughnessTexture = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pExtendsTexture1 = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pExtendsTexture2 = nullptr;
		pMeshContainer->pSubSetDesc[i].MeshTexture.pExtendsTexture3 = nullptr;
	}

	Safe_Delete_Array(pMeshContainer->Name);
	Safe_Release(pMeshContainer->MeshData.pMesh);
	Safe_Delete_Array(pMeshContainer->pMaterials);
	Safe_Delete_Array(pMeshContainer->pAdjacency);
	Safe_Release(pMeshContainer->pSkinInfo);
	Safe_Release(pMeshContainer->pOriginalMesh);
	Safe_Delete_Array(pMeshContainer->pSubSetDesc);


	Safe_Delete_Array(pMeshContainer->ppCombinedTransformationMatrices);
	Safe_Delete_Array(pMeshContainer->pOffsetMatrices);
	Safe_Delete_Array(pMeshContainer->pRenderingMatrices);



	Safe_Delete(pMeshContainer);

	return NOERROR;
}

_ulong CHierarchyLoader::AddRef()
{
	return ++m_dwRefCnt;
}

_ulong CHierarchyLoader::Release()
{
	if (0 == m_dwRefCnt)
	{
		Free();

		delete this;

		return 0;
	}

	else
		return m_dwRefCnt--;
}

HRESULT CHierarchyLoader::Allocate_Name(const char * pSourName, char ** ppDestName)
{
	if (nullptr == pSourName)
		return E_FAIL;

	size_t iLength = strlen(pSourName);

	*ppDestName = new char[iLength + 1];
	ZeroMemory(*ppDestName, sizeof(char) * iLength + 1);

	strcpy(*ppDestName, pSourName);

	return NOERROR;
}

HRESULT CHierarchyLoader::Change_TextureFileName(_tchar * pFileName, const _tchar * pSour, const _tchar * pDest)
{
	wstring FileName = pFileName;
	size_t Num = FileName.rfind(pSour);
	if (Num == string::npos)
		return E_FAIL;
	FileName.replace(Num, (sizeof(pDest) / sizeof(TCHAR)), pDest);
	lstrcpy(pFileName, FileName.c_str());
	return NOERROR;
}

CHierarchyLoader * CHierarchyLoader::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar * pFilePath)
{
	CHierarchyLoader*	pInstance = new CHierarchyLoader(pGraphic_Device);

	if (FAILED(pInstance->Ready_HierarchyLoader(pFilePath)))
	{
		MessageBox(0, L"CHierarchyLoader Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CHierarchyLoader::Free()
{
	Safe_Release(m_pGraphic_Device);
}
