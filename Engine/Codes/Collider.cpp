#include "..\Headers\Collider.h"
#include "Shader.h"
#include "Picking.h"

#include "Management.h"
#include "Mesh_Static.h"

#include "Transform.h"

CCollider::CCollider(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{

}

CCollider::CCollider(const CCollider& rhs)
	: CComponent(rhs)
	, m_eType(rhs.m_eType)
//	, m_pShader(rhs.m_pShader)
	, m_isColl(rhs.m_isColl)
	, m_pOBB(rhs.m_pOBB)
{
	//m_pShader->AddRef();

	rhs.m_pCollider->CloneMeshFVF(rhs.m_pCollider->GetOptions(), rhs.m_pCollider->GetFVF(), rhs.m_pGraphic_Device, &m_pCollider);
}

const _vec3 CCollider::Get_Center()
{
	_vec3 vCenter;

	_matrix		matTransform, matWorld;

	if (nullptr == m_ColliderInfo.pBoneMatrix)
		matTransform = *m_ColliderInfo.pWorldMatrix;
	else
		matTransform = *m_ColliderInfo.pBoneMatrix * *m_ColliderInfo.pWorldMatrix;

	if (nullptr != m_ColliderInfo.pParentMatrix)
		matTransform = matTransform * *m_ColliderInfo.pParentMatrix;

	matWorld = m_ColliderInfo.matLocalTransform * matTransform;

	memcpy(&vCenter, &matWorld.m[3][0], sizeof(_vec3));

	return vCenter;
}

HRESULT CCollider::Ready_Collider(TYPE eType)
{
	HRESULT		hr = 0;

	m_eType = eType;

	switch (eType)
	{
	case TYPE_BOX:
		hr = Ready_BoundingBox();
		break;
	case TYPE_SPHERE:
		hr = Ready_BoundingSphere();
		break;
	}

	if (FAILED(hr))
		return E_FAIL;

	/*m_pShader = CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Collider.fx");
	if (nullptr == m_pShader)
		return E_FAIL;*/

	return NOERROR;
}

HRESULT CCollider::Ready_Clone_Collider(void * pArg)
{
	// 로컬영역내에서의 변환.
	COLLIDERINFO*			pColliderInfo = (COLLIDERINFO*)pArg;

	m_ColliderInfo = *pColliderInfo;

	m_eBoxType = pColliderInfo->eBoxType;



	void*			pVertices = nullptr;

	_uint			iVertexSize = D3DXGetFVFVertexSize(m_pCollider->GetFVF());

	m_pCollider->LockVertexBuffer(0, &pVertices);

	_ulong			dwNumVertices = m_pCollider->GetNumVertices();

	for (size_t i = 0; i < dwNumVertices; ++i)
	{
		D3DXVec3TransformCoord((_vec3*)((_byte*)pVertices + (i * iVertexSize)), (_vec3*)((_byte*)pVertices + (i * iVertexSize)), &pColliderInfo->matLocalTransform);
	}

	// 로컬영역상의 민, 맥스포인트. 
	D3DXComputeBoundingBox((_vec3*)pVertices, dwNumVertices, iVertexSize, &m_vMin, &m_vMax);

	m_pCollider->UnlockVertexBuffer();



	if (BOXTYPE_OBB == m_eBoxType)
	{
		m_pOBB = new OBB;
		ZeroMemory(m_pOBB, sizeof(OBB));

		m_pOBB->vPoint[0] = _vec3(m_vMin.x, m_vMax.y, m_vMin.z);
		m_pOBB->vPoint[1] = _vec3(m_vMax.x, m_vMax.y, m_vMin.z);
		m_pOBB->vPoint[2] = _vec3(m_vMax.x, m_vMin.y, m_vMin.z);
		m_pOBB->vPoint[3] = _vec3(m_vMin.x, m_vMin.y, m_vMin.z);

		m_pOBB->vPoint[4] = _vec3(m_vMin.x, m_vMax.y, m_vMax.z);
		m_pOBB->vPoint[5] = _vec3(m_vMax.x, m_vMax.y, m_vMax.z);
		m_pOBB->vPoint[6] = _vec3(m_vMax.x, m_vMin.y, m_vMax.z);
		m_pOBB->vPoint[7] = _vec3(m_vMin.x, m_vMin.y, m_vMax.z);
	}








	return NOERROR;
}

HRESULT CCollider::Ready_Clone_ColliderSphere(void * _pArg)
{
	// 로컬영역내에서의 변환.
	COLLIDERINFO*			pColliderInfo = (COLLIDERINFO*)_pArg;

	m_ColliderInfo = *pColliderInfo;

	_float fMax = max(max(m_ColliderInfo.matLocalTransform._11, m_ColliderInfo.matLocalTransform._22), m_ColliderInfo.matLocalTransform._33);
	m_ColliderInfo.matLocalTransform._11 = m_ColliderInfo.matLocalTransform._22 = m_ColliderInfo.matLocalTransform._33 = fMax;

	void*			pVtx = nullptr;

	_uint			iVtxSize = D3DXGetFVFVertexSize(m_pCollider->GetFVF());

	m_pCollider->LockVertexBuffer(0, &pVtx);

	_ulong			dwNumVtx = m_pCollider->GetNumVertices();

	for (size_t i = 0; i < dwNumVtx; ++i)
	{
		D3DXVec3TransformCoord((_vec3*)((_byte*)pVtx + (i * iVtxSize)), (_vec3*)((_byte*)pVtx + (i * iVtxSize)), &m_ColliderInfo.matLocalTransform);
	}

	D3DXComputeBoundingSphere((_vec3*)pVtx, dwNumVtx, iVtxSize, &m_vCenter, &m_fRad);



	m_pCollider->UnlockVertexBuffer();


	return NOERROR;
}



HRESULT CCollider::Ready_HullMesh(const _uint& _iScene, const _tchar* _pHull)
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	_tchar hullTag[256];
	lstrcpy(hullTag, _pHull);
	lstrcat(hullTag, L"_HULL");


	m_pHull = (CMesh_Static*)pManagement->Clone_Component(_iScene, hullTag);
	if (nullptr == m_pHull)
		return E_FAIL;

	m_isHullLoad = true;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CCollider::Ready_NaviMesh(const _uint & _iScene, const _tchar * _pHull)
{

	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	_tchar hullTag[256];
	lstrcpy(hullTag, _pHull);
	lstrcat(hullTag, L"_NAVI");


	m_pNavi = (CMesh_Static*)pManagement->Clone_Component(_iScene, hullTag);
	if (nullptr == m_pNavi)
		return E_FAIL;

	m_isNaviLoad = true;

	Safe_Release(pManagement);
	return NOERROR;
}


HRESULT CCollider::Ready_RealMesh(CMesh_Static * _pRealMesh)
{
	
	m_pReal = (CMesh_Static*)_pRealMesh;
	m_pReal->AddRef();

	return NOERROR;
}


_bool CCollider::Collision_Sphere(const CCollider * _pTargetCollider)
{
	if (nullptr == _pTargetCollider)
		return false;

	_matrix		mat = Compute_WorldTransform();
	_matrix		matTarget = _pTargetCollider->Compute_WorldTransform();

	_vec3		vCenter;
	_vec3		vTargetCenter = _pTargetCollider->m_vCenter;


	D3DXVec3TransformCoord(&vCenter, &m_vCenter, &mat);
	D3DXVec3TransformCoord(&vTargetCenter, &vTargetCenter, &matTarget);



	_float fDist = D3DXVec3Length(&(vCenter - vTargetCenter));
	_float fSumRad = D3DXVec3Length((_vec3*)&mat.m[0][0])*m_fRad
		+ D3DXVec3Length((_vec3*)&matTarget.m[0][0])*_pTargetCollider->m_fRad;

	if (fDist < fSumRad)
	{
		return m_isColl = true;
	}
	else
		return m_isColl = false;
}

_bool CCollider::Collision_AABB(const CCollider * _pTargetCollider, _vec3 * _pGap)
{
	// 콜라이더들이 가지고 있는 로컬영역상의 min, max를 각 콜라이더의 월드스페이스변환을 위한 행렬. 

	_matrix		sourMatrix = _pTargetCollider->Compute_WorldTransform();
	_matrix		destMatrix = Compute_WorldTransform();

	_vec3		vPlayerPos = _vec3(0.f,0.f,0.f);
	_vec3		vSourMin, vSourMax;
	_vec3		vDestMin, vDestMax;

	D3DXVec3TransformCoord(&vDestMin, &m_vMin, &destMatrix);
	D3DXVec3TransformCoord(&vDestMax, &m_vMax, &destMatrix);


	D3DXVec3TransformCoord(&vPlayerPos, &vPlayerPos, &sourMatrix);

	m_isColl = false;



	if ((vPlayerPos.x < vDestMin.x) || (vDestMax.x < vPlayerPos.x))
		return false;
	
	if ((vPlayerPos.z < vDestMin.z) || (vDestMax.z < vPlayerPos.z))
		return false;



	_float 		fGap;

	_float		fBoxZ = (vDestMax.z + vDestMin.z)*0.5f;



	if (vPlayerPos.z < fBoxZ)//먼곳
	{
		fGap = vDestMin.z - vPlayerPos.z;
	}
	else//가까운곳
	{
		fGap = vDestMax.z - vPlayerPos.z;
	}



	m_isColl = true;

	_pGap->z = fGap;




	return _bool(true);
}




_bool CCollider::Collision_LAY(const CTransform* _pTrans, _vec3* _pGap, const _bool& _isReal,const _bool& _isHouse)
{
	LPD3DXMESH pMesh = nullptr;
	if (false == _isReal)
		pMesh = m_pHull->Get_Mesh();
	else
		pMesh = m_pReal->Get_Mesh();


	_vec3	vRayPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION);
	vRayPos.y += 80.f;

	_vec3	vRayDir;
	vRayDir = *_pTrans->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vRayDir, &vRayDir);
	

	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);

	_bool coll = false;
	_matrix matRot;
	_uint iCnt = 0;

	D3DXMatrixRotationY(&matRot, D3DXToRadian(15.f));
	for (size_t i = 0; i < 24; ++i)
	{
		_float		fDist = 0.f;
		BOOL		isColl = FALSE;
		DWORD		dwFaceIdx = 0;
		DWORD		iFaceCnt = 0; 

		D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, &dwFaceIdx, nullptr, nullptr, &fDist, nullptr, &iFaceCnt);

	
		if ((FALSE == isColl)||(60.f<=fDist))
		{
			D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matRot);
			continue;
		}

		
		

		_vec3 vDir;
		D3DXVec3TransformNormal(&vDir, &vRayDir, &matWorld);
		D3DXVec3Normalize(&vDir, &vDir);

		_vec3 vNormal = -Cal_Normal(pMesh, dwFaceIdx);
		D3DXVec3TransformNormal(&vNormal, &vNormal, &matWorld);
		vDir = -vNormal*D3DXVec3Dot(&vDir, &vNormal);
		D3DXVec3Normalize(&vDir,&vDir);
		fDist = 60.f - fDist;
		*_pGap += vDir*fDist;
		iCnt++;

		coll = true;

		D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matRot);




		if (1 == iFaceCnt % 2)
		{
			if (false == _isHouse)//집이아니면
			{
				*_pGap = -vDir*fDist*1.001f;
			}
			else//집이면
			{
				*_pGap = vDir*fDist*1.001f;
			}
			_pGap->y = 0.f;
			return true;
		}


	}

	if (false == coll)
		return false;

	if (0 == iCnt)
		return false;

	//*_pGap = _vec3(0.f,0.f,0.f);
	*_pGap /= (_float)iCnt;
	_pGap->y = 0.f;

	return true;
}

//_bool CCollider::Collision_LAY(const CTransform* _pTrans, _vec3* _pGap, const _bool& _isReal, const _bool& _isHouse)
//{
//	LPD3DXMESH pMesh = nullptr;
//	if (false == _isReal)
//		pMesh = m_pHull->Get_Mesh();
//	else
//		pMesh = m_pReal->Get_Mesh();
//
//
//	_vec3	vRayPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION);
//	vRayPos.y += 80.f;
//
//	_vec3	vRayDir;
//	vRayDir = *_pTrans->Get_StateInfo(CTransform::STATE_LOOK);
//	D3DXVec3Normalize(&vRayDir, &vRayDir);
//
//
//	_matrix matWorld = Compute_WorldTransformForMesh();
//	_matrix matWorldInv;
//	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);
//
//	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
//	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);
//
//	_bool coll = false;
//	_matrix matRot;
//	_uint iCnt = 0;
//
//	_float	fMaxDist = 0.f;
//	_vec3	vMaxDir = _vec3(0.f, 0.f, 0.f);
//
//	D3DXMatrixRotationY(&matRot, D3DXToRadian(15.f));
//	for (size_t i = 0; i < 24; ++i)
//	{
//		_float		fDist = 0.f;
//		BOOL		isColl = FALSE;
//		DWORD		dwFaceIdx = 0;
//		DWORD		iFaceCnt = 0;
//
//		D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, &dwFaceIdx, nullptr, nullptr, &fDist, nullptr, &iFaceCnt);
//
//
//		if ((FALSE == isColl) || (60.f <= fDist))
//		{
//			D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matRot);
//			continue;
//		}
//
//		coll = true;
//
//
//		_vec3 vDir;
//		D3DXVec3TransformNormal(&vDir, &vRayDir, &matWorld);
//		D3DXVec3Normalize(&vDir, &vDir);
//
//		_vec3 vNormal = -Cal_Normal(pMesh, dwFaceIdx);
//		D3DXVec3TransformNormal(&vNormal, &vNormal, &matWorld);
//		vDir = -vNormal*D3DXVec3Dot(&vDir, &vNormal);
//		D3DXVec3Normalize(&vDir, &vDir);
//		fDist = 60.f - fDist;
//		
//		if (fMaxDist < fDist)
//		{
//			fMaxDist = fDist;
//			vMaxDir = vDir;
//		}
//			
//
//
//		if (1 == iFaceCnt % 2)
//		{
//			if (false == _isHouse)//집이아니면
//			{
//				*_pGap = -vDir*fDist*1.001f;
//			}
//			else//집이면
//			{
//				*_pGap = vDir*fDist*1.001f;
//			}
//			_pGap->y = 0.f;
//			return true;
//		}
//		
//
//		
//		D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matRot);
//	}
//
//
//
//	*_pGap = vMaxDir*fMaxDist;
//
//	
//
//	if (false == coll)
//		return false;
//
//	_pGap->y = 0.f;
//
//	return true;
//}
_bool CCollider::Collision_CameraLAY(const CTransform * _pTrans, const  _vec3* _pCamPos, const _bool & _isReal, _float* _pDist)
{
	LPD3DXMESH pMesh = nullptr;
	if (false == _isReal)
		pMesh = m_pHull->Get_Mesh();
	else
		pMesh = m_pReal->Get_Mesh();


	_vec3	vRayPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION);
	vRayPos.y += 140.f;

	_vec3	vRayDir;
	vRayDir = *_pCamPos-vRayPos;
	D3DXVec3Normalize(&vRayDir, &vRayDir);


	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);

	_bool coll = false;
	 

	_float		fDist = 0.f;
	BOOL		isColl = FALSE;
	DWORD		iFaceCnt = 0;
	/*DWORD		dwFaceIdx = 0;*/

	D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, nullptr, nullptr, nullptr, &fDist, nullptr, &iFaceCnt);

	if (*_pDist < fDist)
		return false;

	if ((FALSE == isColl) || (200.f <= fDist)||1== iFaceCnt%2)
		return false;

	*_pDist = fDist;

	//_vec3 vPos = vRayPos+vRayDir*fDist;
	//D3DXVec3TransformCoord(&vPos, &vPos, &matWorld);
	//*_pGap = vPos;
	return true;
}

_bool CCollider::Collision_CameraLAY_Sphere(_float* _pDist)
{
	LPD3DXMESH pMesh = nullptr;

	_matrix matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);
	
	_vec3	vRayPos = *(_vec3*)&matView.m[3];
	_vec3	vRayDir = *(_vec3*)&matView.m[2];

	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);

	_bool coll = false;

	_float		fDist = 0.f;
	BOOL		isColl = FALSE;
	DWORD		iFaceCnt = 0;

	D3DXIntersect(m_pCollider, &vRayPos, &vRayDir, &isColl, nullptr, nullptr, nullptr, &fDist, nullptr, &iFaceCnt);

	if (FALSE == isColl)
		return false;

	*_pDist = fDist;

	return true;
}

_bool CCollider::Collision_WeaponLAY(CCollider* pTargetCollider, CTransform * _pTrans, const _bool & _isReal)
{
	LPD3DXMESH pMesh = nullptr;
	if (false == _isReal)
		pMesh = m_pHull->Get_Mesh();
	else
		pMesh = m_pReal->Get_Mesh();


	_matrix matWorldxParent = _pTrans->Get_WorldxParentMatrix();

	_vec3	vRayPos = pTargetCollider->Get_Center() + *(_vec3*)&(matWorldxParent._21)*20.f;

	_vec3	vRayDir = *(_vec3*)&(matWorldxParent._11);
	D3DXVec3Normalize(&vRayDir, &vRayDir);

	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);
	
	_float		fDist = 0.f;
	BOOL		isColl = FALSE;
	DWORD		dwFaceIdx = 0;

	D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, &dwFaceIdx, nullptr, nullptr, &fDist, nullptr, nullptr);

	if (FALSE == isColl||10.f < fDist)
		return false;

	return true;

}

_bool CCollider::Collision_HullMeshLAY(CTransform * _pTrans,const _vec3& vRay, _float* _fDist, const _bool & _isReal)
{
	LPD3DXMESH pMesh = nullptr;

	if (false == _isReal)
		pMesh = m_pHull->Get_Mesh();
	else
		pMesh = m_pReal->Get_Mesh();



	_vec3	vRayPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION);
	vRayPos.y += 60.f;

	_matrix matWorldxParent = _pTrans->Get_Matrix();

	_vec3	vRayDir = vRay;
	D3DXVec3Normalize(&vRayDir, &vRayDir);

	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);

	_float		fDist = 0.f;
	BOOL		isColl = FALSE;
	DWORD		dwFaceIdx = 0;

	D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, &dwFaceIdx, nullptr, nullptr, &fDist, nullptr, nullptr);


	if (FALSE == isColl)
		return false;

	if (*_fDist < fDist)
		return false;

	*_fDist = fDist;

	return true;
}

_bool CCollider::Collision_LAY_Terr(CTransform * _pTrans, _float* _fDist)
{
	_vec3	vRayDir = _vec3(0.f, -1.f, 0.f);
	_vec3	vRayPos = *_pTrans->Get_StateInfo(CTransform::STATE_POSITION);

	vRayPos.y += 50.f;

	_matrix matWorld = Compute_WorldTransformForMesh();
	_matrix matWorldInv;
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorld);

	D3DXVec3TransformCoord(&vRayPos, &vRayPos, &matWorldInv);
	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorldInv);

	_float fDist = 0.f;

	LPD3DXMESH pMesh = nullptr;

	pMesh = m_pNavi->Get_Mesh();

	BOOL		isColl = FALSE;

	DWORD dwFaceIdx = 0;

	D3DXIntersect(pMesh, &vRayPos, &vRayDir, &isColl, &dwFaceIdx, nullptr, nullptr, &fDist, nullptr, nullptr);
	
	if (false == isColl)
		return false;

	if (60.f < fDist)
		return false;

	//Cal_Dist(pMesh, dwFaceIdx, vRayPos, &fDist);


	D3DXVec3TransformNormal(&vRayDir, &vRayDir, &matWorld);
	D3DXVec3Normalize(&vRayDir, &vRayDir);

	fDist = fDist - 50.f;

	if (20.f < fDist)
		return false;

	if(fDist<*_fDist)
		*_fDist = fDist;

	return true;
}

_bool CCollider::Collision_Circle(CCollider * _pTargetCollider, _vec3 * _pGap )
{
	_matrix		mat = Compute_WorldTransform();
	_matrix		matTarget = _pTargetCollider->Compute_WorldTransform();

	_vec3		vCenter;// = (m_vMax + m_vMin)*0.5f;
	_vec3		vTargetCenter = _pTargetCollider->m_vCenter;
	//_float		fBulletRad = 3.f;

	D3DXVec3TransformCoord(&vCenter, &m_vCenter, &mat);
	D3DXVec3TransformCoord(&vTargetCenter, &vTargetCenter, &matTarget);

	vCenter.y = 0.f;
	vTargetCenter.y = 0.f;

	//_float		fBulletRad = (_pTargetCollider->m_vMax.x - _pTargetCollider->m_vMin.x)*0.5f;


	m_isColl = false;

	_vec3 vDir = vCenter - vTargetCenter;
	_float fDist = D3DXVec3Length(&vDir) - m_fCircleRad;// *1.2f;

	if (0.f<fDist)
		return false;
	else
	{
		D3DXVec3Normalize(&vDir, &vDir);
		vDir *= fDist;
		vDir.y = 0.f;
		if(nullptr!= _pGap)
			*_pGap = vDir;
		return m_isColl = true;
	}
}


_bool CCollider::Picking_ToCollider(_vec4 * pOut, const CPicking * pPickingCom)
{
	_vec3	vRayPivot = pPickingCom->Get_MouseRayPivot();
	_vec3	vRay = pPickingCom->Get_MouseRay();

	_matrix	matWorldInv = Compute_WorldTransform();
	D3DXMatrixInverse(&matWorldInv, nullptr, &matWorldInv);

	// 로컬영역상의 피봇과 레이를 구한다.
	D3DXVec3TransformCoord(&vRayPivot, &vRayPivot, &matWorldInv);
	D3DXVec3TransformNormal(&vRay, &vRay, &matWorldInv);


	_float		fU, fV, fDist;

	BOOL		isPick = FALSE;

	D3DXIntersect(m_pCollider, &vRayPivot, &vRay, &isPick, nullptr, &fU, &fV, &fDist, nullptr, nullptr);
	if (isPick)
	{
		_vec3 Pos = pPickingCom->Get_MouseRayPivot() + (pPickingCom->Get_MouseRay()* fDist);
		*pOut = _vec4(Pos.x, Pos.y, Pos.z, fDist);
	}

	return _bool(isPick != 0);
}

void CCollider::Render_Collider()
{
	//if (!m_fRender)
	//	return;
	/*if (nullptr == m_pShader)
		return;

	LPD3DXEFFECT pEffect = m_pShader->Get_EffectHandle();

	if (nullptr == pEffect)
		return;

	_matrix		matTransform = Compute_WorldTransform();

	pEffect->SetMatrix("g_matWorld", &matTransform);

	_matrix		matView, matProj;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);

	_vec4		vColor;

	vColor = m_isColl == true ? _vec4(1.f, 0.f, 0.f, 1.f) : _vec4(0.f, 1.f, 0.f, 1.f);

	pEffect->SetVector("g_vColor", &vColor);

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(0);

	m_pCollider->DrawSubset(0);

	pEffect->EndPass();
	pEffect->End();*/
}

_vec3 CCollider::Cal_Normal(const LPD3DXMESH & pMesh, const DWORD & dwNumFaces)
{
	LPDIRECT3DVERTEXBUFFER9 pVB;
	LPDIRECT3DINDEXBUFFER9 pIB;

	pMesh->GetVertexBuffer(&pVB);
	pMesh->GetIndexBuffer(&pIB);

	WORD* pIndices;
	VTXNORTEXTAN* pVertices;

	pIB->Lock(0, 0, (void**)&pIndices, 0);
	pVB->Lock(0, 0, (void**)&pVertices, 0);

	D3DXVECTOR3 v0 = pVertices[pIndices[3 * dwNumFaces + 0]].vPosition;
	D3DXVECTOR3 v1 = pVertices[pIndices[3 * dwNumFaces + 1]].vPosition;
	D3DXVECTOR3 v2 = pVertices[pIndices[3 * dwNumFaces + 2]].vPosition;

	D3DXVECTOR3 u = v1 - v0;
	D3DXVECTOR3 v = v2 - v0;

	D3DXVECTOR3 vOut;

	D3DXVec3Cross(&vOut, &u, &v);
	D3DXVec3Normalize(&vOut, &vOut);

	pVB->Unlock();
	pIB->Unlock();
	Safe_Release(pVB);
	Safe_Release(pIB);

	return vOut;
}
//
//void CCollider::Cal_Dist(const LPD3DXMESH & pMesh, const DWORD & dwNumFaces, const _vec3 & _vPos, _float * _fDist)
//{
//	LPDIRECT3DVERTEXBUFFER9 pVB;
//	LPDIRECT3DINDEXBUFFER9 pIB;
//
//	pMesh->GetVertexBuffer(&pVB);
//	pMesh->GetIndexBuffer(&pIB);
//
//	WORD* pIndices;
//	VTXNORTEXTAN* pVertices;
//
//	pIB->Lock(0, 0, (void**)&pIndices, 0);
//	pVB->Lock(0, 0, (void**)&pVertices, 0);
//
//	D3DXVECTOR3 v0 = pVertices[pIndices[3 * dwNumFaces + 0]].vPosition;
//	D3DXVECTOR3 v1 = pVertices[pIndices[3 * dwNumFaces + 1]].vPosition;
//	D3DXVECTOR3 v2 = pVertices[pIndices[3 * dwNumFaces + 2]].vPosition;
//
//	D3DXIntersectTri(&v0, &v1, &v2,&_vPos,&_vec3(0.f,-1.f,0.f),nullptr,nullptr, _fDist);
//
//
//	pVB->Unlock();
//	pIB->Unlock();
//	Safe_Release(pVB);
//	Safe_Release(pIB);
//
//
//}

void CCollider::Set_Dist(const _float & _fDist, const _float & _fSlsher)
{
	m_fDist = _fDist;
	m_fSlasher = _fSlsher;
}

HRESULT CCollider::Ready_BoundingBox()
{
	if (FAILED(D3DXCreateBox(m_pGraphic_Device, 1.f, 1.f, 1.f, &m_pCollider, &m_pAdjacency)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCollider::Ready_BoundingSphere()
{
	if (FAILED(D3DXCreateSphere(m_pGraphic_Device, 0.5f, 10, 10, &m_pCollider, &m_pAdjacency)))
		return E_FAIL;

	return NOERROR;
}

_matrix CCollider::Remove_Rotation(_matrix matTransform) const
{
	_vec3			vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	vRight *= D3DXVec3Length((_vec3*)&matTransform.m[0][0]);
	vUp *= D3DXVec3Length((_vec3*)&matTransform.m[1][0]);
	vLook *= D3DXVec3Length((_vec3*)&matTransform.m[2][0]);

	memcpy(&matTransform.m[0][0], &vRight, sizeof(_vec3));
	memcpy(&matTransform.m[1][0], &vUp, sizeof(_vec3));
	memcpy(&matTransform.m[2][0], &vLook, sizeof(_vec3));

	return _matrix(matTransform);
}

_matrix CCollider::Compute_WorldTransform() const
{
	_matrix		matTransform;

	if (nullptr == m_ColliderInfo.pBoneMatrix)
		matTransform = *m_ColliderInfo.pWorldMatrix;
	else
		matTransform = *m_ColliderInfo.pBoneMatrix * *m_ColliderInfo.pWorldMatrix;

	if (nullptr != m_ColliderInfo.pParentMatrix)
		matTransform = matTransform * *m_ColliderInfo.pParentMatrix;

	if (BOXTYPE_AABB == m_ColliderInfo.eBoxType)
		matTransform = Remove_Rotation(matTransform);

	return _matrix(matTransform);
}

_matrix CCollider::Compute_WorldTransformForMesh() const
{
	_matrix		matTransform;

	if (nullptr == m_ColliderInfo.pBoneMatrix)
		matTransform = *m_ColliderInfo.pWorldMatrix;
	else
		matTransform = *m_ColliderInfo.pBoneMatrix * *m_ColliderInfo.pWorldMatrix;

	if (nullptr != m_ColliderInfo.pParentMatrix)
		matTransform = matTransform * *m_ColliderInfo.pParentMatrix;

	return _matrix(matTransform);
}

void CCollider::Compute_AlignAxis(OBB * pOBB)
{
	pOBB->vAlignAxis[0] = pOBB->vPoint[2] - pOBB->vPoint[3];
	pOBB->vAlignAxis[1] = pOBB->vPoint[0] - pOBB->vPoint[3];
	pOBB->vAlignAxis[2] = pOBB->vPoint[7] - pOBB->vPoint[3];

	for (size_t i = 0; i < 3; ++i)
	{
		D3DXVec3Normalize(&pOBB->vAlignAxis[i], &pOBB->vAlignAxis[i]);
	}
}

void CCollider::Compute_ProjAxis(OBB * pOBB)
{
	pOBB->vProjAxis[0] = (pOBB->vPoint[5] + pOBB->vPoint[2]) * 0.5f - pOBB->vCenter;
	pOBB->vProjAxis[1] = (pOBB->vPoint[5] + pOBB->vPoint[0]) * 0.5f - pOBB->vCenter;
	pOBB->vProjAxis[2] = (pOBB->vPoint[5] + pOBB->vPoint[7]) * 0.5f - pOBB->vCenter;
}


CCollider * CCollider::Create(LPDIRECT3DDEVICE9 pGraphic_Device, TYPE eType)
{
	CCollider*	pInstance = new CCollider(pGraphic_Device);

	if (FAILED(pInstance->Ready_Collider(eType)))
	{
		MessageBox(0, L"CCollider Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CCollider::Clone_Component(void* pArg)
{
	CCollider*	pCollider = new CCollider(*this);

	if (m_eType == TYPE_BOX)
	{
		if (FAILED(pCollider->Ready_Clone_Collider(pArg)))
		{
			_MSG_BOX("Collider Cloned Failed");
			Safe_Release(pCollider);
		}
	}
	else if (m_eType == TYPE_SPHERE)
	{
		if (FAILED(pCollider->Ready_Clone_ColliderSphere(pArg)))
		{
			_MSG_BOX("Collider Cloned Failed");
			Safe_Release(pCollider);
		}
	}
	return pCollider;
}


void CCollider::Free()
{
	Safe_Delete_Array(m_pOBB);
	//Safe_Release(m_pShader);
	Safe_Release(m_pCollider);

	//if (true == m_isHullLoad)
		Safe_Release(m_pHull);

	//if (true == m_isNaviLoad)
		Safe_Release(m_pNavi);

	Safe_Release(m_pReal);

	CComponent::Free();
}
