#include "..\Headers\Light_Manager.h"
#include "Light.h"
#include "Shader.h"

_IMPLEMENT_SINGLETON(CLight_Manager)

CLight_Manager::CLight_Manager()
{

}

D3DLIGHT9 * CLight_Manager::Get_LightInfo(const _uint & iIndex)
{
	if (m_LightList.size() <= iIndex)
		return nullptr;

	auto	iter = m_LightList.begin();

	for (size_t i = 0; i < iIndex; ++i)
		++iter;	

	return (*iter)->Get_LightInfo();
}

_vec3 CLight_Manager::GetProjPos(const _uint & iIndex)
{
	if (m_LightList.size() <= iIndex)
		return _vec3(0,0,0);

	auto	iter = m_LightList.begin();

	for (size_t i = 0; i < iIndex; ++i)
		++iter;

	return (*iter)->GetProjPos();
}

HRESULT CLight_Manager::Ready_LightManager(LPDIRECT3DDEVICE9 pGraphicDev)
{
	m_pGraphicDev = pGraphicDev;
	m_pGraphicDev->AddRef();

	if (FAILED(m_pGraphicDev->CreateTexture(64, 1, 1, 0, D3DFMT_A32B32G32R32F, D3DPOOL_MANAGED, &m_pLightInfoTex, nullptr)))
		return E_FAIL;

	return NOERROR;
}

HRESULT CLight_Manager::Add_LightInfo(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9 & LightInfo)
{
	CLight*			pLight = CLight::Create(pGraphic_Device, LightInfo);
	if (nullptr == pLight)
		return E_FAIL;

	if(nullptr != m_pShader_ShadowCube)
		pLight->Set_Shader(m_pShader_ShadowCube);

	m_LightList.push_back(pLight);

	return NOERROR;
}

CLight * CLight_Manager::Add_LightInfo2(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9 & LightInfo)
{
	CLight*			pLight = CLight::Create(pGraphic_Device, LightInfo);
	if (nullptr == pLight)
		return nullptr;

	if (nullptr != m_pShader_ShadowCube)
		pLight->Set_Shader(m_pShader_ShadowCube);

	m_LightList.push_back(pLight);

	return pLight;
}

void CLight_Manager::Render_Light(LPD3DXEFFECT pEffect)
{
	for (auto& pLight : m_LightList)
		pLight->Render_Light(pEffect);
	//if(m_LightList.size()> 0)
	//	m_LightList.front()->Render_Light(pEffect);
}

void CLight_Manager::Ready_ShadowMap(CLight * pLight)
{
	for (auto Light : m_LightList)
		if (pLight == Light)
			Light->Render_ShadowMap();
}

void CLight_Manager::Cal_ShaftList()
{
	_float CameraDist[3];
	ZeroMemory(&m_LightInfo, sizeof(m_LightInfo));
	int i = 0;
	for (auto& pLight : m_LightList)
	{
		_float fDist = pLight->Get_CamDist();
		pLight->SetStemp(false);
		if (fDist > 2000.f)
			continue;

		if (!pLight->IsRender() || !pLight->IsShaft())
			continue;

		_vec4 vLightInfo = pLight->CalShaftVec();
		if (vLightInfo.x <= 0.f || vLightInfo.x >= 1.f
			|| vLightInfo.y <= 0.f || vLightInfo.y >= 1.f)
			continue;

		CameraDist[i] = fDist;
		pLight->SetStemp(true);
		m_LightInfo[i] = vLightInfo;
		++i;
		if (i > 2)
			break;
	}
	
	if (CameraDist[0] > CameraDist[i - 1])
	{
		_float temp = CameraDist[0];
		CameraDist[0] = CameraDist[i - 1];
		CameraDist[i - 1] = temp;
	}

	D3DLOCKED_RECT lock_Rect = { 0, };
	if (FAILED(m_pLightInfoTex->LockRect(0, &lock_Rect, NULL, 0)))
		return;

	memcpy(lock_Rect.pBits, &m_LightInfo, sizeof(_vec4) * i);

	m_pLightInfoTex->UnlockRect(0);

	m_iSize = i;
}

void CLight_Manager::Delete_AllLight()
{
	for (auto& iter : m_LightList)
		Safe_Release(iter);
	m_LightList.clear();
}

LPDIRECT3DTEXTURE9 CLight_Manager::Get_LightInfoTexture()
{
	if (nullptr == m_pLightInfoTex)
		return nullptr;

	Cal_ShaftList();

	return m_pLightInfoTex;
}

//void CLight_Manager::Render_DynamicShadow()
//{
//	for (auto Light : m_LightList)
//		Light->Render_DynamicShadowMap();
//}

void CLight_Manager::Free()
{
	Safe_Release(m_pLightInfoTex);
	Safe_Release(m_pShader_ShadowCube);

	for (auto& pLight : m_LightList)
		Safe_Release(pLight);
	m_LightList.clear();

	Safe_Release(m_pGraphicDev);
}
