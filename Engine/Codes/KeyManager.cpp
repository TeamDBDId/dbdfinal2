#include "..\Headers\KeyManager.h"
#include "Management.h"
#include "Input_Device.h"

_USING(Engine)

_IMPLEMENT_SINGLETON(CKeyManager)

CKeyManager::CKeyManager()
{
	for (int i = 1; i < 238; ++i)
	{
		if (i == 128)
			continue;
		map_dwKey[i][KEY_DOWN] = false;
		map_dwKey[i][KEY_PRESSING] = false;
		map_dwKey[i][KEY_UP] = false;
	}
	for (int i = 0; i < 4; ++i)
	{
		map_dwMouse[i][KEY_DOWN] = false;
		map_dwMouse[i][KEY_PRESSING] = false;
		map_dwMouse[i][KEY_UP] = false;
	}
}

void CKeyManager::KeyMgr_Update()
{
	if (map_dwKey.empty())
		return;

	m_bKeyInput = false;

	for (auto& map : map_dwKey)
	{
		map.second[KEY_PRESSING] = false;

		if (CInput_Device::GetInstance()->Get_DIKeyState(map.first) & 0x80)
		{
			m_bKeyInput = true;
			map.second[KEY_PRESSING] = true;
		}
	}
	for (auto& map : map_dwMouse)
	{
		map.second[KEY_PRESSING] = false;

		if (CInput_Device::GetInstance()->Get_DIMouseState(map.first))
		{
			m_bKeyInput = true;
			map.second[KEY_PRESSING] = true;
		}
	}
}

_bool CKeyManager::KeyDown(_uint In)
{
	if (!(map_dwKey[In][KEY_DOWN]) && (map_dwKey[In][KEY_PRESSING]))
	{
		map_dwKey[In][KEY_DOWN] = true;
		return true;
	}
	else if (!(map_dwKey[In][KEY_PRESSING]) && (map_dwKey[In][KEY_DOWN]))
	{
		map_dwKey[In][KEY_DOWN] = false;
		return false;
	}

	return false;
}

_bool CKeyManager::KeyUp(_uint In)
{
	if (map_dwKey[In][KEY_PRESSING])
	{
		map_dwKey[In][KEY_UP] = true;
		return false;
	}
	else if (map_dwKey[In][KEY_UP])
	{
		map_dwKey[In][KEY_UP] = false;
		return true;
	}

	return false;
}

_bool CKeyManager::KeyPressing(_uint In)
{
	if (map_dwKey[In][KEY_PRESSING])
		return true;

	return false;
}

_bool CKeyManager::MouseDown(_uint In)
{
	if (!(map_dwMouse[In][KEY_DOWN]) && (map_dwMouse[In][KEY_PRESSING]))
	{
		map_dwMouse[In][KEY_DOWN] = true;
		return true;
	}
	else if (!(map_dwMouse[In][KEY_PRESSING]) && (map_dwMouse[In][KEY_DOWN]))
	{
		map_dwMouse[In][KEY_DOWN] = false;
		return false;
	}

	return false;
}

_bool CKeyManager::MouseUp(_uint In)
{
	if (map_dwMouse[In][KEY_PRESSING])
	{
		map_dwMouse[In][KEY_UP] = true;
		return false;
	}
	else if (map_dwMouse[In][KEY_UP])
	{
		map_dwMouse[In][KEY_UP] = false;
		return true;
	}

	return false;
}

_bool CKeyManager::MousePressing(_uint In)
{
	if (map_dwMouse[In][KEY_PRESSING])
		return true;

	return false;
}

void CKeyManager::Free()
{
	map_dwKey.clear();
	map_dwMouse.clear();
}
