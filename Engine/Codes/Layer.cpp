#include "..\Headers\Layer.h"

CLayer::CLayer()
{
}

CComponent * CLayer::Get_ComponentPointer(const _tchar * pComponentTag, const _uint & iIndex)
{
	if (m_ObjectList.size() <= iIndex)
		return nullptr;

	auto	iter = m_ObjectList.begin();

	for (size_t i = 0; i < iIndex; ++i)
		++iter;

	return (*iter)->Get_ComponentPointer(pComponentTag);
}

HRESULT CLayer::Add_Object(CGameObject * pGameObject)
{
	if (nullptr == pGameObject)
		return E_FAIL;

	m_ObjectList.push_back(pGameObject);

	return NOERROR;
}

HRESULT CLayer::Ready_Layer()
{
	return NOERROR;
}

_int CLayer::Update_Object(const _float & fTimeDelta)
{
	_int ReturnCheck;
	OBJECTLIST::iterator iter;
	for (iter = m_ObjectList.begin(); iter != m_ObjectList.end();)
	{
		if (nullptr != *iter)
		{
			ReturnCheck = (*iter)->Update_GameObject(fTimeDelta);

			if (ReturnCheck == 1)
			{
				Safe_Release(*iter);
				iter = m_ObjectList.erase(iter);
			}
			else if (ReturnCheck & 0x80000000)
				return -1;
			else
				iter++;
		}
	}

	return _int();
}

_int CLayer::LastUpdate_Object(const _float & fTimeDelta)
{
	_int ReturnCheck;
	OBJECTLIST::iterator iter;
	for (iter = m_ObjectList.begin(); iter != m_ObjectList.end();)
	{
		if (nullptr != *iter)
		{
			ReturnCheck = (*iter)->LastUpdate_GameObject(fTimeDelta);

			if (ReturnCheck == 1)
			{
				Safe_Release(*iter);
				iter = m_ObjectList.erase(iter);
			}
			else if (ReturnCheck & 0x80000000)
				return -1;
			else
				iter++;
		}
	}

	return _int();
}

list<CGameObject*> & CLayer::GetGameObjectList()
{
	// TODO: 여기에 반환 구문을 삽입합니다.
	if (m_ObjectList.size() == 0)
	{
		/*_MSG_BOX("NOLIST");*/
		return m_ObjectList;
	}

	return m_ObjectList;
}

CGameObject * CLayer::Get_GameObject(const _uint & iIndex)
{
	if (m_ObjectList.size() <= iIndex)
		return nullptr;

	auto	iter = m_ObjectList.begin();

	for (size_t i = 0; i < iIndex; ++i)
		++iter;

	return (*iter);
}

CLayer * CLayer::Create()
{
	CLayer*	pInstance = new CLayer();

	if (FAILED(pInstance->Ready_Layer()))
	{
		MessageBox(0, L"CLayer Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}

void CLayer::Free()
{
	for (auto& pGameObject : m_ObjectList)
	{
		Safe_Release(pGameObject);
	}
	m_ObjectList.clear();
}
