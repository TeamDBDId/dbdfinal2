#pragma once

#include "Coll_Manager.h"

#include "GameObject.h"
#include "Collider.h"
#include "Transform.h"

#include "KeyManager.h"

_USING(Engine)
_IMPLEMENT_SINGLETON(CColl_Manager)

CColl_Manager::CColl_Manager()
{
}


HRESULT CColl_Manager::Add_CollGroup(eCollGroup _eGroup, CGameObject * _pObj, _bool* _bool)
{

	COL* col = new COL;

	col->pObj = _pObj;
	col->pCollider = _pObj->Get_ComponentPointer(L"Com_Collider");


	_uint iIdx = 0;
	if (C_PLAYER == _eGroup || C_WOOD == _eGroup || C_EXIT == _eGroup || C_WEAPON == _eGroup||C_CAMERA== _eGroup||C_HATCH== _eGroup)
		iIdx = 0;
	else
		iIdx = _pObj->Get_PosIdx();






	if (MAXIDX <= iIdx)
		_MSG_BOX("CColl_Manager Idx Fail");


	m_CollList[iIdx][_eGroup].push_back(col);


	Ready_Player(_eGroup);
	Ready_Weapon(_eGroup);
	Ready_Camera(_eGroup);

	return NOERROR;
}



HRESULT CColl_Manager::Erase_Obj(eCollGroup _eGroup, CGameObject * _pObj)
{

	_uint iIdx = 0;
	if (C_PLAYER == _eGroup || C_WOOD == _eGroup || C_EXIT == _eGroup || C_WEAPON == _eGroup|| C_CAMERA== _eGroup)
		iIdx = 0;
	else
		iIdx = _pObj->Get_PosIdx();


	for (auto& iter = m_CollList[iIdx][_eGroup].begin(); iter != m_CollList[iIdx][_eGroup].end();)
	{
		if (*iter != nullptr)
		{

			if ((*iter)->pObj == _pObj)
			{
				Safe_Delete(*iter);
				iter = m_CollList[iIdx][_eGroup].erase(iter);
				return NOERROR;
			}
			else
				iter++;

		}
	}

	if (m_pPlayerObj == _pObj)
	{
		m_pPlayerIdx = nullptr;
		m_pPlayerObj = nullptr;
		m_pPlayerCollider = nullptr;
		m_pPlayerTrans = nullptr;
	}


	return NOERROR;
}

HRESULT CColl_Manager::Coll(const _float& _fTime)
{
	if (m_pPlayerObj == nullptr)
		return NOERROR;
	Coll_Player(_fTime);
	Coll_Weapon();
	Coll_Camera();

	Coll_Look_Camper();
	Coll_Look_Slasher();


	return NOERROR;
}




void CColl_Manager::Ready_Player(eCollGroup _eGroup)
{

	if (C_PLAYER != _eGroup)
		return;


	if (1 != m_CollList[0][C_PLAYER].size())
		return;


	COL* pPlayer = m_CollList[0][C_PLAYER][0];
	m_pPlayerObj = (CGameObject*)pPlayer->pObj;
	m_pPlayerCollider = (CCollider*)pPlayer->pCollider;
	m_pPlayerTrans = (CTransform*)m_pPlayerObj->Get_ComponentPointer(L"Com_Transform");
	m_pPlayerIdx = m_pPlayerObj->Get_pPosIdx();

}

void CColl_Manager::Ready_Weapon(eCollGroup _eGroup)
{

	if (C_WEAPON != _eGroup)
		return;


	if (1 != m_CollList[0][C_WEAPON].size())
		return;


	COL* pWeapon = m_CollList[0][C_WEAPON][0];
	m_pWeaponObj = (CGameObject*)pWeapon->pObj;
	m_pWeaponCollider = (CCollider*)pWeapon->pCollider;
	m_pWeaponTrans = (CTransform*)m_pWeaponObj->Get_ComponentPointer(L"Com_Transform");

}

void CColl_Manager::Ready_Camera(eCollGroup _eGroup)
{
	if (C_CAMERA != _eGroup)
		return;

	if (1 != m_CollList[0][C_CAMERA].size())
		return;

	COL* pCamera = m_CollList[0][C_CAMERA][0];
	m_pCameraObj = (CGameObject*)pCamera->pObj;
	m_pCameraCollider = (CCollider*)pCamera->pCollider;

}

void CColl_Manager::Coll_Player(const _float & _fTime)
{
	if (m_pPlayerObj == nullptr)
		return;

	if (false == m_pPlayerObj->Get_IsColl())
		return;

	Coll_Player_Player();
	Coll_Player_Wood();
	Coll_Player_ExitBuilding();
	Coll_Player_LAY_IdxCheck();
	Coll_Player_HOUSEHUT_IdxCheck(_fTime);
	Coll_Player_Interation_IdxCheck();

	Coll_Player_Bush_IdxCheck();
	Coll_Player_Grass();
	
	
}

void CColl_Manager::Coll_Weapon()
{
	if (m_pWeaponObj == nullptr)
		return;

	if (m_pPlayerObj->GetID() & 0x100)
		return;

	if (0 == m_CollList[0][C_WEAPON].size())
		return;


	if (false == m_pWeaponObj->Get_IsColl())
		return;


	Coll_Weapon_Player();
	Coll_Weapon_LAY_IdxCheck();
	Coll_Weapon_Wood();
	Coll_Weapon_ExitBuilding();
	Coll_Weapon_HOUSEHUT_IdxCheck();
	Coll_Weapon_Interation_IdxCheck();

}

void CColl_Manager::Coll_Camera()
{
	if (nullptr == m_pCameraObj)
		return;

	if (m_pPlayerObj->GetID() & 0x200)
		return;

	if (false == m_pCameraObj->Get_IsColl())
		return;

	m_fDist = 10000.f;

	Coll_Camera_LAY_IdxCheck();
	Coll_Camera_Wood();
	Coll_Camera_ExitBuilding();
	Coll_Camera_HOUSEHUT_IdxCheck();
	Coll_Camera_Interation_IdxCheck();

	if (10000.f != m_fDist)
		m_pCameraObj->Do_Dist(m_fDist);
}

void CColl_Manager::Coll_Look_Camper()
{

	if (m_pPlayerObj->GetID() & 0x200)
		return;

	if (false == m_pPlayerObj->Get_IsAnger())
	{
		m_pPlayerObj->Set_IsLook(false);
		return;
	}

	//_float	fMinDist = 2100.f;

	for (auto& pSlasher : m_CollList[0][C_PLAYER])
	{
		CGameObject* pSlasherObj = (CGameObject*)pSlasher->pObj;

		if (pSlasherObj->GetID() & 0x100)
			continue;

	

		CTransform* pSlasherTrans = (CTransform*)pSlasherObj->Get_ComponentPointer(L"Com_Transform");
		
		const _vec3* vCamperPos = m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
		const _vec3* vSlasherPos = pSlasherTrans->Get_StateInfo(CTransform::STATE_POSITION);

		_vec3 vDir = *vCamperPos - *vSlasherPos;

		_float fDist = D3DXVec3Length(&vDir);

		if (2000.f<fDist)
			continue;

		const _vec3* pSlasherLook = pSlasherTrans->Get_StateInfo(CTransform::STATE_LOOK);
		D3DXVec3Normalize(&vDir, &vDir);



		_float fAngle = acosf(D3DXVec3Dot(pSlasherLook, &vDir));

		if (D3DXToRadian(30.f)<fAngle)
			continue;


		_float fDist1 = 2100.f;
		_float fDist2 = 2100.f;
		_float fDist3 = 2100.f;

		Coll_Look_House_IdxCheck(*pSlasherObj->Get_pPosIdx(), *m_pPlayerIdx, vDir, &fDist1);
		Coll_Look_Idx(*m_pPlayerIdx, vDir, &fDist2);
		Coll_Look_Idx(*pSlasherObj->Get_pPosIdx(), vDir, &fDist3);

		if (min(min(fDist1, fDist2), fDist3) < fDist)//시야에없음
		{
			m_pPlayerObj->Set_IsLook(false);
			continue;
		}

		m_pPlayerObj->Set_IsLook(true);
		return;
	}

}

void CColl_Manager::Coll_Look_Slasher()
{
	if (m_pPlayerObj->GetID() & 0x100)
		return;

	//_float	fMinDist = 2100.f;

	for (auto& pCamper : m_CollList[0][C_PLAYER])
	{
		CGameObject* pCamperObj = (CGameObject*)pCamper->pObj;

		if (m_pPlayerObj == pCamperObj)//나랑,나랑은 충돌안함
			continue;

		CTransform* pCamperTrans = (CTransform*)pCamperObj->Get_ComponentPointer(L"Com_Transform");

		const _vec3* vSlasherPos = m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
		const _vec3* vCamperPos = pCamperTrans->Get_StateInfo(CTransform::STATE_POSITION);

		_vec3 vDir = *vCamperPos - *vSlasherPos;

		_float fDist = D3DXVec3Length(&vDir);

		if (2000.f<fDist)
			continue;

		const _vec3* pSlasherLook = m_pPlayerTrans->Get_StateInfo(CTransform::STATE_LOOK);
		D3DXVec3Normalize(&vDir, &vDir);



		_float fAngle = acosf(D3DXVec3Dot(pSlasherLook, &vDir));

		if (D3DXToRadian(30.f)<fAngle)
			continue;


		_float fDist1 = 2100.f;
		_float fDist2 = 2100.f;
		_float fDist3 = 2100.f;

		Coll_Look_House_IdxCheck(*m_pPlayerIdx, *pCamperObj->Get_pPosIdx(), vDir, &fDist1);
		Coll_Look_Idx(*m_pPlayerIdx,vDir, &fDist2);
		Coll_Look_Idx(*pCamperObj->Get_pPosIdx(), vDir, &fDist3);

		if (min(min(fDist1, fDist2), fDist3) < fDist)//시야에없음
		{
			m_pPlayerObj->Set_IsLook(false);
			continue;
		}
		if (false == pCamperObj->Get_IsAnger())
		{
			m_pPlayerObj->Set_IsLook(false);
			continue;
		}


		m_pPlayerObj->Set_IsLook(true);
		return;
		


	}


}

void CColl_Manager::Coll_Look_Idx(const _uint& _iIdx,const _vec3& _vDir,_float* _fDist)
{


	_uint iIdx = _iIdx - 15;

	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iIdx&&iIdx < 196)//유효한인덱스일경우
			{
				Coll_Look_Obj(C_CLOSET, iIdx, _vDir, _fDist);
				Coll_Look_Obj(C_GENER, iIdx, _vDir, _fDist);
				Coll_Look_Obj(C_CIRCLE, iIdx, _vDir, _fDist);
				Coll_Look_Obj(C_LAY, iIdx, _vDir, _fDist);
				Coll_Look_Obj(C_WOOD, 0, _vDir, _fDist);
			}
			iIdx++;
		}
		iIdx += 11;
	}



}

void CColl_Manager::Coll_Look_Obj(const eCollGroup & _eCollGroup, const _uint & _iIdx, const _vec3& _vDir, _float* _fDist)
{
	if (m_CollList[_iIdx][_eCollGroup].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;


	for (auto& pObj : m_CollList[_iIdx][_eCollGroup])
	{
		((CCollider*)pObj->pCollider)->Collision_HullMeshLAY(m_pPlayerTrans, _vDir, _fDist);// 충돌안했다 즉 응시
	}

}
void CColl_Manager::Coll_Look_House_IdxCheck(const _uint & _iIdx1, const _uint & _iIdx2, const _vec3 & _vDir, _float* _fDist)
{


	if ((78 <= _iIdx1&&_iIdx1 <= 81) || (92 <= _iIdx1&&_iIdx1 <= 95) ||
		(106 <= _iIdx1&&_iIdx1 <= 109) || (121 <= _iIdx1&&_iIdx1 <= 123)
		|| (78 <= _iIdx2&&_iIdx2 <= 81) || (92 <= _iIdx2&&_iIdx2 <= 95) ||
		(106 <= _iIdx2&&_iIdx2 <= 109) || (121 <= _iIdx2&&_iIdx2 <= 123))
	{
		_bool isColl = false;


		Coll_Look_House(C_HOUSE, 94, _vDir, _fDist);
	}


}

void CColl_Manager::Coll_Look_House(const eCollGroup & _eGroup, const _uint & _iIdx, const _vec3& _vDir, _float* _fDist)
{
	COL* pObj = m_CollList[_iIdx][_eGroup][0];

	((CCollider*)pObj->pCollider)->Collision_HullMeshLAY(m_pPlayerTrans, _vDir, _fDist);

}




void CColl_Manager::Coll_Player_LAY_IdxCheck()
{
	if (m_pPlayerObj == nullptr)
		return;

	_uint iObjIdx = *m_pPlayerIdx - 15;


	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Player_LAY(iObjIdx);
				Coll_Player_Circle(iObjIdx);

				Coll_Player_ExitBox(iObjIdx);

			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}


}

void CColl_Manager::Coll_Player_LAY(const _uint& _iIdx)
{
	if (m_pPlayerObj == nullptr)
		return;

	if (m_CollList[_iIdx][C_LAY].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;



	for (auto& pObj : m_CollList[_iIdx][C_LAY])
	{
		if (((CCollider*)pObj->pCollider)->Collision_Sphere(m_pPlayerCollider))
		{

			_vec3 vGap = _vec3(0.f, 0.f, 0.f);

			if (((CCollider*)pObj->pCollider)->Collision_LAY(m_pPlayerTrans, &vGap,false))
				m_pPlayerTrans->Move_V3(vGap);
		}
	}


}

void CColl_Manager::Coll_Player_Circle(const _uint& _iIdx)
{
	if (m_pPlayerObj == nullptr)
		return;

	if (m_CollList[_iIdx][C_CIRCLE].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;

	for (auto& pObj : m_CollList[_iIdx][C_CIRCLE])
	{
		_vec3 vGap = _vec3(0.f, 0.f, 0.f);


		if (((CCollider*)pObj->pCollider)->Collision_Circle(m_pPlayerCollider, &vGap))
			m_pPlayerTrans->Move_V3(vGap);


	}




}
void CColl_Manager::Coll_Player_Bush_IdxCheck()
{
	if (m_pPlayerObj == nullptr)
		return;

	_uint iIdx = *m_pPlayerIdx - 15;

	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iIdx&&iIdx < 196)//유효한인덱스일경우
			{
				Coll_Player_Bush(iIdx);
				Coll_Player_Bush(iIdx);
			}
			iIdx++;
		}
		iIdx += 11;
	}


	
}



void CColl_Manager::Coll_Player_Bush(const _uint& _iIdx)
{
	if (m_pPlayerObj == nullptr)
		return;

	if (m_CollList[_iIdx][C_BUSH].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;

	for (auto& pObj : m_CollList[_iIdx][C_BUSH])
	{
		_vec3 vGap = _vec3(0.f, 0.f, 0.f);


		for (auto& pCamper : m_CollList[0][C_PLAYER])
		{
			CCollider* pCamperCollider = (CCollider*)pCamper->pCollider;

			if (((CCollider*)pObj->pCollider)->Collision_Circle(pCamperCollider, &vGap))
			{
				if(pCamperCollider==m_pPlayerCollider)
					m_pPlayerObj->Set_IStep(1);
				

				CGameObject* pGrassObj = (CGameObject*)pObj->pObj;
				CTransform* pGrassTrans = (CTransform*)pGrassObj->Get_ComponentPointer(L"Com_Transform");
				vGap *= 0.007f;
				vGap.z *= -1.f;
				pGrassTrans->SetUp_RotateXYZ(&vGap.z, &vGap.y, &vGap.x);
				continue;
			}
		}


	
			
	}


}
void CColl_Manager::Coll_Player_Grass()
{
	if (m_pPlayerObj == nullptr)
		return;

	if (nullptr == m_pPlayerIdx)
		return;
	if ( *m_pPlayerIdx<0)
		return;
	if (195<*m_pPlayerIdx)
		return;

	if (m_CollList[*m_pPlayerIdx][C_GRASS].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;

	for (auto& pObj : m_CollList[*m_pPlayerIdx][C_GRASS])
	{

		if (((CCollider*)pObj->pCollider)->Collision_Circle(m_pPlayerCollider))
		{
			m_pPlayerObj->Set_IStep(1);
			return;
		}
	}
	return;
}
//
//void CColl_Manager::Coll_Player_Tree0(const _uint & _iIdx)
//{
//
//	COL* pPlayer = m_CollList[0][C_PLAYER][0];
//	if (m_CollList[_iIdx][C_TREE0].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
//		return;
//	for (auto& pObj : m_CollList[_iIdx][C_TREE0])
//	{
//		_vec3 vGap = _vec3(0.f, 0.f, 0.f);
//
//		if (((CCollider*)pObj->pCollider)->Collision_Player(((CCollider*)pPlayer->pCollider), &vGap, 110.f))
//		{
//			CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");
//			pPlayerTrans->Move_V3(vGap);
//		}
//	}
//
//}
//
//void CColl_Manager::Coll_Player_Tree2(const _uint & _iIdx)
//{
//	COL* pPlayer = m_CollList[0][C_PLAYER][0];
//
//	if (m_CollList[_iIdx][C_TREE2].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
//		return;
//	for (auto& pObj : m_CollList[_iIdx][C_TREE2])
//	{
//		_vec3 vGap = _vec3(0.f, 0.f, 0.f);
//
//		if (((CCollider*)pObj->pCollider)->Collision_Player(((CCollider*)pPlayer->pCollider), &vGap))
//		{
//			CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");
//			pPlayerTrans->Move_V3(vGap);
//		}
//	}
//}

void CColl_Manager::Coll_Player_HOUSEHUT_IdxCheck(const _float& _fTime)
{
	if (m_pPlayerObj == nullptr)
		return;

	_float fTime = _fTime;
	if (0.017f < fTime)
		fTime = 0.017f;

	_float fDist = 100.f;


	//큰집
	if ((78 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 81) || (92 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 95) ||
		(106 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 109) || (121 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 123))
	{
		Coll_Player_HOUSEHUT(fTime, C_HOUSE, 94, &fDist);
		Coll_Player_HOUSEHUT(fTime, C_TERRAIN, 108, &fDist);

		//if (m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION)->y < 0)
		Coll_Player_HOUSEHUT(fTime, C_UNDER, 94, &fDist);


		if (fDist < 90.f)
		{
			_vec3 vPos = *m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION) + _vec3(0.f, -1.f, 0.f)* fDist;
			m_pPlayerTrans->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
		}
		return;
	}

	//작은집
	else if ((155 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 156) || (169 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 170))
		Coll_Player_HOUSEHUT(fTime, C_HUT, 155, &fDist);


	//나가는문1
	else if (69 == *m_pPlayerIdx || 70 == *m_pPlayerIdx || 83 == *m_pPlayerIdx || 84 == *m_pPlayerIdx)
	{
		Coll_Player_ExitDoor(83);
		Coll_Player_HOUSEHUT(fTime, C_HUT, 84, &fDist);
	}


	//나가는문2
	else if (139 == *m_pPlayerIdx || 140 == *m_pPlayerIdx || 153 == *m_pPlayerIdx || 154 == *m_pPlayerIdx)
	{
		Coll_Player_ExitDoor(154);
		Coll_Player_HOUSEHUT(fTime, C_HUT, 140, &fDist);
	}


	if (fDist < 90.f)
	{
		_vec3 vPos = *m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION) + _vec3(0.f, -1.f, 0.f)* fDist;
		if (vPos.y < 0.f)
			vPos.y = 0.f;
		m_pPlayerTrans->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}
	
}


void CColl_Manager::Coll_Player_HOUSEHUT(const _float& _fTime, const eCollGroup& _eGroup, const _uint& _iIdx,_float* _pDist)
{
	if (m_pPlayerObj == nullptr)
		return;


	COL* pObj = m_CollList[_iIdx][_eGroup][0];


	_vec3 vGap = _vec3(0.f, 0.f, 0.f);
	if (C_HOUSE == _eGroup || C_UNDER == _eGroup)
		((CCollider*)pObj->pCollider)->Collision_LAY(m_pPlayerTrans, &vGap, false,true);
	else if (C_HUT == _eGroup)
	{
		_vec3 vPos = *m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
		if (vPos.y <= 0.f)
		{
			vPos.y = 0.f;

			m_pPlayerTrans->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
			m_pPlayerTrans->Set_Acc(0.f);
		}
	}


	if (((CCollider*)pObj->pCollider)->Collision_LAY_Terr(m_pPlayerTrans, _pDist))
	{
		m_pPlayerTrans->Set_Acc(0.f);

		if(C_TERRAIN !=_eGroup)
			m_pPlayerObj->Set_IStep(2);
	}
	else
	{
		if(C_HUT!= _eGroup)
		m_pPlayerTrans->Move_Gravity(_fTime);
	}
		

	m_pPlayerObj->Set_IsBool(m_pPlayerTrans->Get_fAcc() < -100.f);
	m_pPlayerTrans->Move_V3(vGap);

}

void CColl_Manager::Coll_Player_Wood()
{
	if (m_pPlayerObj == nullptr)
		return;

	if (nullptr == m_pPlayerCollider)
		return;

	COL* pWood = m_CollList[0][C_WOOD][0];
	_vec3 vGap = _vec3(0.f, 0.f, 0.f);

	if (((CCollider*)pWood->pCollider)->Collision_Sphere(m_pPlayerCollider))
	{
		if (((CCollider*)pWood->pCollider)->Collision_LAY(m_pPlayerTrans, &vGap, false))
			m_pPlayerTrans->Move_V3(vGap);
	}


}

void CColl_Manager::Coll_Player_ExitBuilding()
{
	if (m_pPlayerObj == nullptr)
		return;

	for (auto& pObj : m_CollList[0][C_EXIT])
	{

		_vec3 vGap = _vec3(0.f, 0.f, 0.f);


		if (((CCollider*)pObj->pCollider)->Collision_LAY(m_pPlayerTrans, &vGap, true,true))
			m_pPlayerTrans->Move_V3(vGap);


	}


}

void CColl_Manager::Coll_Player_ExitDoor(const _uint& _iIdx)
{
	if (m_pPlayerObj == nullptr)
		return;

	COL* pObj = m_CollList[_iIdx][C_EXITDOOR][0];


	if (false == ((CGameObject*)pObj->pObj)->Get_IsColl())
		return;



	_vec3 vGap = _vec3(0.f, 0.f, 0.f);


	if (((CCollider*)pObj->pCollider)->Collision_LAY(m_pPlayerTrans, &vGap, false))
		m_pPlayerTrans->Move_V3(vGap);

}

void CColl_Manager::Coll_Player_Player()
{
	if (m_pPlayerObj == nullptr)
		return;

	if (0 == m_CollList[0][C_PLAYER].size()) //해당인덱스의 오브젝트가 없으면 리턴
		return;


	//COL* pPlayer = m_CollList[0][C_PLAYER][0];	//나

	if (false == m_pPlayerObj->Get_IsColl())
		return;


	for (auto& pOther : m_CollList[0][C_PLAYER])//다른플레이어들
	{	

		CGameObject* pOtherObj = (CGameObject*)pOther->pObj;

		if (m_pPlayerObj == pOtherObj)//나랑,나랑은 충돌안함
			continue;

		if (false == pOtherObj->Get_IsColl())
			continue;

		CCollider* pOtherCollider = (CCollider*)pOther->pCollider;

		if (pOtherCollider->Collision_Sphere(m_pPlayerCollider))
		{
			CTransform* pOtherTrans = (CTransform*)pOtherObj->Get_ComponentPointer(L"Com_Transform");

			const _vec3& vPlayerPos = *m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
			const _vec3& vOtherPos = *pOtherTrans->Get_StateInfo(CTransform::STATE_POSITION);

			_vec3 vLook = vPlayerPos - vOtherPos;
			D3DXVec3Normalize(&vLook, &vLook);

			_vec3 vPos = vOtherPos + vLook*60.f;

			m_pPlayerObj->Do_Coll_Player(pOtherObj, vPos);

			_vec3 vGap = _vec3(0.f, 0.f, 0.f);
			if (pOtherCollider->Collision_Circle(m_pPlayerCollider, &vGap))
			{
				if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_D))
				{
					m_pPlayerTrans->Move_V3(vGap);
				}
			}
		}



	}

}



void CColl_Manager::Coll_Player_Interation_IdxCheck()
{
	if (m_pPlayerObj == nullptr)
		return;

	_uint iObjIdx = *m_pPlayerIdx - 15;


	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Player_Interation(iObjIdx, C_CHEST);
				Coll_Player_Interation(iObjIdx, C_HOOK);
				Coll_Player_Interation(iObjIdx, C_CLOSET);

				Coll_Player_Interation(iObjIdx, C_GENER);
				Coll_Player_Interation(iObjIdx, C_WINDOW);
				Coll_Player_Interation(iObjIdx, C_EXITDOOR);

				Coll_Player_Interation(0, C_HATCH);
				Coll_Player_Interation(iObjIdx, C_TOTEM);

				Coll_Player_Interation(iObjIdx, C_PLANK);
				//추후 오브젝트

			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}

}

void CColl_Manager::Coll_Player_Interation(const _uint& _iIdx, const eCollGroup& _eCollGroup)
{

	/*COL* pPlayer = m_CollList[0][C_PLAYER][0];
	CGameObject* pPlayerObj = (CGameObject*)pPlayer->pObj;
	CCollider* pPlayerCollider = (CCollider*)pPlayer->pCollider;
	CTransform* pPlayerTrans = (CTransform*)((CGameObject*)pPlayer->pObj)->Get_ComponentPointer(L"Com_Transform");*/

	if (m_pPlayerObj == nullptr)
		return;


	if (m_CollList[_iIdx][_eCollGroup].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;




	for (auto& pObj : m_CollList[_iIdx][_eCollGroup])
	{

		CGameObject* pInterObj = (CGameObject*)pObj->pObj;
		CCollider* pInterCollider = (CCollider*)pObj->pCollider;
		CTransform* pInterTrans = (CTransform*)pInterObj->Get_ComponentPointer(L"Com_Transform");


		if (false == pInterObj->Get_IsColl())
			return;

		_vec3 vGap = _vec3(0.f, 0.f, 0.f);
		if (C_HOOK == _eCollGroup)
		{
			pInterCollider->Collision_LAY(m_pPlayerTrans, &vGap, false);
			m_pPlayerTrans->Move_V3(vGap);
		}
		else if (C_PLANK == _eCollGroup)
		{
			if (true == pInterObj->Get_IsBool())//넘어졌다면
				pInterCollider->Collision_LAY(m_pPlayerTrans, &vGap, true);
			else//안넘어졌으면
				pInterCollider->Collision_LAY(m_pPlayerTrans, &vGap, false);
			m_pPlayerTrans->Move_V3(vGap);
		}


		

		if (pInterCollider->Collision_Sphere(m_pPlayerCollider))
		{
			if (false == pInterObj->Get_IsColl())
				continue;


			if (C_TOTEM == _eCollGroup)
			{
				if (pInterCollider->Collision_Circle(m_pPlayerCollider, &vGap))
					m_pPlayerTrans->Move_V3(vGap);
			}
			else if (C_WINDOW != _eCollGroup&&C_HATCH != _eCollGroup&&C_PLANK != _eCollGroup&&C_HOOK != _eCollGroup&&C_EXITDOOR!= _eCollGroup)
			{
				pInterCollider->Collision_LAY(m_pPlayerTrans, &vGap, false);
				m_pPlayerTrans->Move_V3(vGap);
			}
			

			_float fDist = pInterCollider->Get_Dist();
			if (m_pPlayerObj->GetID() & 0x200)
				fDist *= pInterCollider->Get_Slasher();



			const _vec3& vPlayerPos = *m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
			const _vec3& vInterPos = pInterCollider->Get_Center();


			///////////////////////////////////////////////////////////////


			_vec3 vPos;
			_float minLength = 10000.f;
			switch (_eCollGroup)
			{
			case C_TOTEM:
			case C_HATCH:
				vPos = vInterPos;
				m_pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_CHEST:
				if (false == pInterObj->Get_IsBool())
				{
				vPos = vInterPos + pInterObj->Get_InterPos(0)*fDist;
				m_pPlayerObj->Do_Coll(pInterObj, vPos);
				}
				break;
			case C_CLOSET:
			case C_HOOK:
			case C_EXITDOOR:
				vPos = vInterPos + pInterObj->Get_InterPos(0)*fDist;
				m_pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_GENER:
				if (false == pInterObj->Get_IsBool())
				{
					for (size_t i = 0; i < 4; ++i)
					{
						_vec3 vLength = vPlayerPos - (vInterPos + pInterObj->Get_InterPos(i));
						if (D3DXVec3Length(&vLength) < minLength)
						{
							minLength = D3DXVec3Length(&vLength);
							vPos = vInterPos + pInterObj->Get_InterPos(i)*fDist;
						}

					}

					m_pPlayerObj->Do_Coll(pInterObj, vPos);
				}
				
				break;
			case C_WINDOW:

				for (size_t i = 0; i < 2; ++i)
				{
					if (!KEYMGR->KeyPressing(DIK_LSHIFT))//쉬프트를 안누르면
						fDist *= 1.3f;

					_vec3 vLength = vPlayerPos - (vInterPos + pInterObj->Get_InterPos(i));
					if (D3DXVec3Length(&vLength) < minLength)
					{
						minLength = D3DXVec3Length(&vLength);
						vPos = vInterPos + pInterObj->Get_InterPos(i)*fDist;
					}

				}
				m_pPlayerObj->Do_Coll(pInterObj, vPos);
				break;
			case C_PLANK:
				//if()
				_bool isColl = false;

				if (KEYMGR->KeyPressing(DIK_LSHIFT))//쉬프트를 누르고있을때는
				{
					if (true == pInterObj->Get_IsBool())//넘어졌다면
						isColl = true;
					else//안넘어졌다면
					{
						_vec3 vLook = *m_pPlayerTrans->Get_StateInfo(CTransform::STATE_LOOK);
						D3DXVec3Normalize(&vLook, &vLook);

						_vec3 vDir = ((CCollider*)pObj->pCollider)->Get_Center() - *m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);
						D3DXVec3Normalize(&vDir, &vDir);

						if (D3DXVec3Dot(&vLook, &vDir)<0)//뒤에있을때만
							isColl = true;
					}
				}
				else//안누르고 있을때는  
					isColl = true;

				if (true == isColl)
				{
					for (size_t i = 0; i < 2; ++i)
					{
						_vec3 vLength = vPlayerPos - (vInterPos + pInterObj->Get_InterPos(i));
						if (D3DXVec3Length(&vLength) < minLength)
						{
							minLength = D3DXVec3Length(&vLength);
							vPos = vInterPos + pInterObj->Get_InterPos(i)*fDist;
						}

					}
					m_pPlayerObj->Do_Coll(pInterObj, vPos);
				}

				break;
			}




		}
	}



}

void CColl_Manager::Coll_Player_ExitBox(const _uint& _iIdx)
{
	if (m_pPlayerObj == nullptr)
		return;

	if (m_CollList[_iIdx][C_EXITBOX].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;

	for (auto& pObj : m_CollList[_iIdx][C_EXITBOX])
	{
		_vec3 vGap = _vec3(0.f, 0.f, 0.f);

		if (((CCollider*)pObj->pCollider)->Collision_AABB(m_pPlayerCollider, &vGap))
		{

			if (m_pPlayerObj->GetID() & 0x100)//캠퍼
			{
				if (0!=m_pPlayerObj->Get_IFree())
					return;


				if(0.f<vGap.z)
					m_pPlayerObj->Set_IFree(1);
				else
					m_pPlayerObj->Set_IFree(2);
				
			}
			else//슬래셔
			{
				m_pPlayerTrans->Move_V3(vGap);
			}



		}

	}


}

void CColl_Manager::Coll_Weapon_Player()
{

	COL* pPlayer = m_CollList[0][C_PLAYER][0];



	for (auto& pCamper : m_CollList[0][C_PLAYER])//모든플래이어를돌면서
	{
		if (pPlayer == pCamper)
			continue;


		if (m_pWeaponCollider->Collision_Sphere(((CCollider*)pCamper->pCollider)))
		{

			CGameObject* pCamperObj = (CGameObject*)pCamper->pObj;
			pCamperObj->Do_Coll(m_pWeaponObj);
			m_pWeaponObj->Do_Coll(pCamperObj);
		}


	}


}



void CColl_Manager::Coll_Weapon_LAY_IdxCheck()
{

	_uint iObjIdx = *m_pPlayerIdx - 15;



	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Weapon_LAY(iObjIdx);
				Coll_Weapon_Circle(iObjIdx);
			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}

}

void CColl_Manager::Coll_Weapon_LAY(const _uint & _iIdx)
{

	if (m_CollList[_iIdx][C_LAY].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;



	for (auto& pObj : m_CollList[_iIdx][C_LAY])
	{
		if (((CCollider*)pObj->pCollider)->Collision_Sphere(m_pPlayerCollider))
		{
			if (((CCollider*)pObj->pCollider)->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
				m_pWeaponObj->Do_Coll((CGameObject*)pObj->pObj);
		}
	}

}

void CColl_Manager::Coll_Weapon_Circle(const _uint & _iIdx)
{

	if (m_CollList[_iIdx][C_CIRCLE].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;

	for (auto& pObj : m_CollList[_iIdx][C_CIRCLE])
	{

		/*if (((CCollider*)pObj->pCollider)->Collision_Sphere(m_pPlayerCollider))
		{*/
			if (((CCollider*)pObj->pCollider)->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
				m_pWeaponObj->Do_Coll((CGameObject*)pObj->pObj);
		//}


	}

}



void CColl_Manager::Coll_Weapon_Interation_IdxCheck()
{

	_uint iObjIdx = *m_pPlayerIdx - 15;





	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Weapon_Interation(iObjIdx, C_HOOK);
				Coll_Weapon_Interation(iObjIdx, C_CLOSET);

				Coll_Weapon_Interation(iObjIdx, C_GENER);

				Coll_Weapon_Interation(iObjIdx, C_PLANK);
				//추후 오브젝트

			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}
}

void CColl_Manager::Coll_Weapon_Interation(const _uint& _iIdx, const eCollGroup& _eCollGroup)
{


	if (m_CollList[_iIdx][_eCollGroup].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;




	for (auto& pObj : m_CollList[_iIdx][_eCollGroup])
	{



		CGameObject* pInterObj = (CGameObject*)pObj->pObj;
		CCollider* pInterCollider = (CCollider*)pObj->pCollider;
		CTransform* pInterTrans = (CTransform*)pInterObj->Get_ComponentPointer(L"Com_Transform");

		if (false == pInterObj->Get_IsColl())
			continue;

	
	
		if (C_HOOK == _eCollGroup)
		{
			if (pInterCollider->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
				m_pWeaponObj->Do_Coll(pInterObj);
		}
		else if (C_PLANK == _eCollGroup)
		{
			if (true == pInterObj->Get_IsBool())//넘어졌다면
			{
				if (pInterCollider->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans, true))
					m_pWeaponObj->Do_Coll(pInterObj);
			}
			else//안넘어졌으면
			{
				if (pInterCollider->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
					m_pWeaponObj->Do_Coll(pInterObj);
			}

		}
		else
		{

			if (pInterCollider->Collision_Sphere(m_pPlayerCollider))
			{

				if (pInterCollider->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
					m_pWeaponObj->Do_Coll(pInterObj);

			}


		}


	}




}

void CColl_Manager::Coll_Weapon_Wood()
{

	COL* pWood = m_CollList[0][C_WOOD][0];
	CGameObject* pWoodObj = (CGameObject*)pWood->pObj;
	CCollider*	pWoodCollider = (CCollider*)pWood->pCollider;
	

	if (pWoodCollider->Collision_Sphere(m_pWeaponCollider))
	{
		if (pWoodCollider->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
			m_pWeaponObj->Do_Coll(pWoodObj);
	}
}
///////
void CColl_Manager::Coll_Weapon_ExitBuilding()
{
	for (auto& pExit : m_CollList[0][C_EXIT])
	{
		if (((CCollider*)pExit->pCollider)->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans,true))
			m_pWeaponObj->Do_Coll((CGameObject*)pExit->pObj);
	}
}

void CColl_Manager::Coll_Weapon_ExitDoor(const _uint & _iIdx)
{

	COL* pExitDoor = m_CollList[_iIdx][C_EXITDOOR][0];
	CGameObject* pExitDoorObj = (CGameObject*)pExitDoor->pObj;

	if (false == pExitDoorObj->Get_IsColl())
		return;



	_vec3 vGap = _vec3(0.f, 0.f, 0.f);


	if (((CCollider*)pExitDoor->pCollider)->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
		m_pWeaponObj->Do_Coll(pExitDoorObj);

}



void CColl_Manager::Coll_Weapon_HOUSEHUT_IdxCheck()
{


	//큰집
	if ((78 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 81) ||	(92 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 95) ||
		(106 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 109) || (121 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 123))
	{
		Coll_Weapon_HOUSEHUT(94,C_HOUSE);

		if (m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION)->y < 0)
			Coll_Weapon_HOUSEHUT(94,C_UNDER);
	}


	//나가는문1
	else if (69 == *m_pPlayerIdx || 70 == *m_pPlayerIdx || 83 == *m_pPlayerIdx || 84 == *m_pPlayerIdx)
	{
		Coll_Weapon_ExitDoor(83);
	}


	//나가는문2
	else if (139 == *m_pPlayerIdx || 140 == *m_pPlayerIdx || 153 == *m_pPlayerIdx || 154 == *m_pPlayerIdx)
	{
		Coll_Weapon_ExitDoor(154);
	}
}

void CColl_Manager::Coll_Weapon_HOUSEHUT(const _uint & _iIdx, const eCollGroup & _eGroup)
{

	COL* pObj = m_CollList[_iIdx][_eGroup][0];

	
	if (((CCollider*)pObj->pCollider)->Collision_WeaponLAY(m_pWeaponCollider, m_pWeaponTrans))
		m_pWeaponObj->Do_Coll((CGameObject*)pObj->pObj);

}

void CColl_Manager::Coll_Camera_LAY_IdxCheck()
{

	_uint iObjIdx = *m_pPlayerIdx - 15;


	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Camera_LAY(iObjIdx);
				Coll_Camera_Circle(iObjIdx);
			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}

}

void CColl_Manager::Coll_Camera_LAY(const _uint & _iIdx)
{

	if (m_CollList[_iIdx][C_LAY].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;

	const _vec3* pCamPos = m_pCameraCollider->Get_CamPos();

	for (auto& pObj : m_CollList[_iIdx][C_LAY])
	{
		CGameObject* pGameObj = (CGameObject*)pObj->pObj;



		((CCollider*)pObj->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, false, &m_fDist);
	}

}

void CColl_Manager::Coll_Camera_Circle(const _uint & _iIdx)
{

	if (m_CollList[_iIdx][C_CIRCLE].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;


	const _vec3* pCamPos = m_pCameraCollider->Get_CamPos();

	for (auto& pObj : m_CollList[_iIdx][C_CIRCLE])
	{

		((CCollider*)pObj->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, false, &m_fDist);

	}

}

void CColl_Manager::Coll_Camera_Wood()
{

	COL* pWood = m_CollList[0][C_WOOD][0];


	const _vec3* pCamPos = m_pCameraCollider->Get_CamPos();

	if (((CCollider*)pWood->pCollider)->Collision_Sphere(m_pPlayerCollider))
	{
		((CCollider*)pWood->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, false, &m_fDist);
	}

}

void CColl_Manager::Coll_Camera_ExitBuilding()
{

	for (auto& pObj : m_CollList[0][C_EXIT])
	{

		const _vec3* pCamPos = m_pCameraCollider->Get_CamPos();

		((CCollider*)pObj->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, true, &m_fDist);


	}

}

void CColl_Manager::Coll_Camera_HOUSEHUT_IdxCheck()
{

	//큰집
	if ((78 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 81) || (92 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 95) ||
		(106 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 109) || (121 <= *m_pPlayerIdx&&*m_pPlayerIdx <= 123))
	{
		const _vec3* _vPos = m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION);

		if(!((5610.f<_vPos->x&&_vPos->x<5730.f)&&(7580.f<_vPos->z&&_vPos->z<7880.f)&&(_vPos->y<200.f)))
			Coll_Camera_HOUSEHUT(C_HOUSE, 94);

		Coll_Camera_HOUSEHUT(C_UNDER, 94);

		//if (m_pPlayerTrans->Get_StateInfo(CTransform::STATE_POSITION)->y < 0.f)
			
	}

}

void CColl_Manager::Coll_Camera_HOUSEHUT(const eCollGroup & _eGroup, const _uint & _iIdx)
{

	COL* pObj = m_CollList[_iIdx][_eGroup][0];


	const _vec3* pCamPos = m_pCameraCollider->Get_CamPos();

	((CCollider*)pObj->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, false, &m_fDist);


}

void CColl_Manager::Coll_Camera_Interation_IdxCheck()
{

	_uint iObjIdx = *m_pPlayerIdx - 15;


	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < 3; ++j)
		{

			if (0 <= iObjIdx&&iObjIdx < 196)//유효한인덱스일경우
			{
				Coll_Camera_Interation(iObjIdx, C_HOOK);
				Coll_Camera_Interation(iObjIdx, C_CLOSET);
				Coll_Camera_Interation(iObjIdx, C_GENER);

				//Coll_Camera_Interation(iObjIdx, C_PLANK);
				//추후 오브젝트

			}
			iObjIdx++;
		}
		iObjIdx += 11;
	}
	
}

void CColl_Manager::Coll_Camera_Interation(const _uint& _iIdx, const eCollGroup & _eCollGroup)
{

	if (m_CollList[_iIdx][_eCollGroup].size() == 0) //해당인덱스의 오브젝트가 없으면 리턴
		return;


	const _vec3* pCamPos = m_pCameraCollider->Get_CamPos();

	for (auto& pObj : m_CollList[_iIdx][_eCollGroup])
	{
		if (C_PLANK == _eCollGroup)
		{
			if (true == ((CGameObject*)pObj->pObj)->Get_IsBool())//넘어졌다면
				((CCollider*)pObj->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, true, &m_fDist);
			else
				((CCollider*)pObj->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, false, &m_fDist);

		}
		else
			((CCollider*)pObj->pCollider)->Collision_CameraLAY(m_pPlayerTrans, pCamPos, false, &m_fDist);
	}

}





void CColl_Manager::Free()
{

	for (size_t i = 0; i < MAXIDX; i++)
	{

		for (size_t j = 0; j < C_END; j++)
		{
			if (m_CollList[i][j].size() <= 0)
				continue;

			for (auto& pCol : m_CollList[i][j])
			{
				Safe_Delete(pCol);
			}

			m_CollList[i][j].clear();
		}

	}

}

