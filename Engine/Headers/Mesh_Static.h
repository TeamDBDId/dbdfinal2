#pragma once

#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CMesh_Static final : public CComponent
{
private:
	explicit CMesh_Static(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMesh_Static(const CMesh_Static& rhs);
	virtual ~CMesh_Static() = default;
public:
	_matrix Get_LocalTransform() const;

	const SUBSETDESC* Get_SubSetDesc(const _uint& iAttributeID) const {
		return &m_pSubSetDesc[iAttributeID]; }

	const _ulong Get_NumMaterials() const {
		return m_dwNumMaterials; } 
	
	const LPD3DXMESH Get_Mesh() { return m_pMesh; }
	_vec3	GetMin() { if (nullptr != &m_vMin) return m_vMin; else return _vec3(FLT_MAX, FLT_MAX, FLT_MAX); }
	_vec3	GetMax() { if (nullptr != &m_vMax)return m_vMax; else return _vec3(FLT_MAX, FLT_MAX, FLT_MAX); }
public:
	HRESULT Ready_Mesh_Static(const _tchar* pFilePath, const _tchar* pFileName);
	void Render_Mesh(_uint iAttributeID);

public:
	_int* Get_RenderNum() { return m_pRenderNum; }
private:
	LPD3DXMESH								m_pMesh = nullptr;
	LPD3DXBUFFER							m_pAdjacency = nullptr; // 정수배열
	LPD3DXBUFFER							m_pMaterials = nullptr; // D3DXMATERIAL배열
	_ulong									m_dwNumMaterials = 0;
	SUBSETDESC*								m_pSubSetDesc = nullptr;
	_int*									m_pRenderNum = nullptr;
private:
	_vec3				m_vMin, m_vMax;
private:
	HRESULT Change_TextureFileName(_tchar* fFileName, const _tchar* pSour, const _tchar* pDest);
public:
	static CMesh_Static* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar* pFilePath, const _tchar* pFileName);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};

_END