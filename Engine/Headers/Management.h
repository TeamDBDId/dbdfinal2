#pragma once

// 1. 엔진프로젝트 관리에 해당하는 역활을 수행한다.(엔진에서 사용된 매니져클래스들의 릴리즈)
// 2. 씬을 보관하고 관리한다.(CSceneMgr)
// 2-1. 현재 게임에 보여주는 씬을 보관하고 있는다.
// 3. 엔진에서 할당된 매니져객체들의 해제를 담당한다.

#include "Base.h"
#include "Object_manager.h"
#include "Component_Manager.h"

_BEGIN(Engine)

class CScene;
class CComponent;
class CGameObject;
class _ENGINE_DLL CManagement final : public CBase
{
	_DECLARE_SINGLETON(CManagement)
private:
	explicit CManagement();
	virtual ~CManagement() = default;
public:
	CScene*		Get_CurScene() { return m_pScene; }
	CComponent* Get_ComponentPointer(const _uint& iSceneID, const _tchar* pLayerTag, const _tchar* pComponentTag, const _uint& iIndex = 0);
	list<CGameObject*>& Get_ObjectList(const _uint& iSceneID, const _tchar* pLayerTag);
	CGameObject* Get_GameObject(const _uint& iSceneID, const _tchar* pLayerTag, const _uint& iIndex);
public:
	HRESULT	Remove_Prototype_GameObject(const _tchar* pGameObjectTag);
	HRESULT	Remove_GameObjectFromLayer(const _uint& iSceneID, const _tchar* pLayerTag);
	HRESULT Remove_Prototype_Component(const _uint& iSceneID, const _tchar* pComponentTag);
public:
	HRESULT Ready_Management(const _uint& iNumScene);
	HRESULT Add_Prototype_GameObject(const _tchar* pGameObjectTag, CGameObject* pGameObject);
	HRESULT Add_Prototype_Component(const _uint& iSceneID, const _tchar* pComponentTag, CComponent* pComponent);
	HRESULT Add_GameObjectToLayer(const _tchar* pProtoTag, const _uint& iSceneID, const _tchar* pLayerTag, CGameObject** ppCloneObject = nullptr);
	CComponent* Clone_Component(const _uint& iSceneID, const _tchar* pComponentTag, void* pArg = nullptr);
	HRESULT SetUp_ScenePointer(CScene* pNewScenePointer);
	_int	Update_Management(const _float& fTimeDelta);
	void	Render_Management();
	HRESULT Clear_Layers(const _uint& iSceneID);
	static void	Release_Engine();

	map<const _tchar*, CLayer*>*& Get_mapLayers();
	map<const _tchar*, CComponent*>*& Get_mapComPrototype();
	map<const _tchar*, CGameObject*>& Get_mapPrototype();
	list<CGameObject*>* Get_ObjList(const _uint & iSceneID, const _tchar * pLayerTag);
	CGameObject*		Get_GameObjectFromObjectID(const _uint & iSceneID, const _tchar * pLayerTag,const _uint& ObjectID);
private:
	// 현재 화면에 보여주는 씬이다.
	CScene*				m_pScene = nullptr;
	CObject_Manager*	m_pObject_Manager = nullptr;
	CComponent_Manager*	m_pComponent_Manager = nullptr;
protected:
	virtual void Free();
};


_END