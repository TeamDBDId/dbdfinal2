#pragma once
#include "Engine_Defines.h"

_BEGIN(Engine)
class _ENGINE_DLL Math_Manager
{
public:
	Math_Manager();
	~Math_Manager() = default;

public:
	static _float ProgressToPersent(const _float& _iFrom, const _float& _iTo, const _float& _Persent);
	static _vec3 ProgressToPersent(const _vec3& _iFrom, const _vec3& _iTo, const _float& _Persent);
	static _vec2 ProgressToPersent(const _vec2& _iFrom, const _vec2& _iTo, const _float& _Persent);
	static _int CalRandIntFromTo(const _int& _iFrom, const _int& _iTo);
	static _float CalRandFloatFromTo(const _float& _fFrom, const _float& _fTo);

	static _bool CalBoolPer(const _float& _fPer);
	static _bool IsRectIn(const _vec2& MousePos, const _vec4& Rect);
};


_END