#pragma once

#include "Base.h"

_BEGIN(Engine)

class _ENGINE_DLL CAnimationCtrl final : public CBase
{
public:

private:
	explicit CAnimationCtrl();
	virtual ~CAnimationCtrl() = default;
public:
	HRESULT Ready_Animation(LPD3DXANIMATIONCONTROLLER pAniCtrl);
	HRESULT Clone_Animation(const CAnimationCtrl& rhs);
	HRESULT Set_AnimationSet(const _uint& iAnimationID, _bool IsInterrupt = false); // 특정 애니메이션 동작을 준비시킨다.
	HRESULT Set_NoBleningAnimationSet(const _uint& iAnimationID);
	void Play_Animation(const _float& fTimeDelta); // 애니메이션을 재생시키낟.
	void  Setup_AnimationTime(const _uint& Track, const _float& fFixingTime); // 시간에 맞는 타임을 에니매이션을 동작시킴.
	void Stop_Animation();
	_bool IsOverTime(const _float& fCorrectValue);
	_uint Get_CurAnimation() { return m_iOldAnimationIdx; };
	_double GetPeriod();// { return m_Period; }
	_double GetPeriod(_uint AniNum);
	_double GetTimeAcc();// { return m_TimeAcc; }
	void SetTimeAcc(_double TimeAcc) { m_TimeAcc = TimeAcc; }
	_bool IsInturrupt() { return m_bIsInterrupt; }
	void SetInterrupt(_bool IsInturrupt) { m_bIsInterrupt = IsInturrupt; }
	HRESULT Set_SpecifyAnimationSet(ANIMSET tagAnimSet);
private:
	LPD3DXANIMATIONCONTROLLER		m_pAniCtrl = nullptr;
	_uint							m_iCurrentTrack = 0;
	_uint							m_iNewTrack = 1;
	_double							m_TimeAcc = 0.0;
	_uint							m_iCurAnimationIdx = 0;
	_uint							m_iOldAnimationIdx = 0;
	_int							m_iPlayType = 0;
	_double							m_Period = 0.0;
	_bool							m_bIsInterrupt = false;
	_uint							m_iConnectAnimationIdx = 0;
public:
	static CAnimationCtrl* Create(LPD3DXANIMATIONCONTROLLER pAniCtrl);
	CAnimationCtrl* Clone();
protected:
	virtual void Free();
};

_END