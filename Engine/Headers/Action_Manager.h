#pragma once

#include "Base.h"
#include "GameObject.h"

_BEGIN(Engine)

class CAction;
class CGameObject;
class _ENGINE_DLL CAction_Manager : public CBase
{
	_DECLARE_SINGLETON(CAction_Manager)
public:
	explicit CAction_Manager();
	virtual ~CAction_Manager() =default;
public:
	HRESULT Add_Action(const _tchar* pActionTag, CAction* pAction);
	HRESULT Set_Action(CGameObject* pGameObejct, const _tchar* pActionTag, const float& fTimeOfDuration);
	HRESULT Remove_Action(const _tchar* pActionTag);
	CAction* Find_Action(const _tchar* pActionTag);
	_bool Update_Actions(const _float& fTimeDelta);
	HRESULT Assert_EndAllAction();
	_bool	IsPlaying_Action(_tchar* pActionTag);
private:
	_tchar	m_pCurActionTag[100] = L"";
	map < const _tchar*, CAction*>	m_mapActions;
	typedef map<const _tchar*, CAction*> MAPACTIONS;
protected:
	virtual void Free();
};

_END