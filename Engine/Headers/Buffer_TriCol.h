#pragma once

// 삼각형의 형태를 띄는 정점들.
// 정점 자체에 색을 부여하고 있다.
#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_TriCol final : public CVIBuffer
{
private:
	explicit CBuffer_TriCol(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_TriCol(const CBuffer_TriCol& rhs);
	virtual ~CBuffer_TriCol() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();

public:
	static CBuffer_TriCol* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};

_END