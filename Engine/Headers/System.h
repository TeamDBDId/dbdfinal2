#pragma once

#include "Base.h"

_BEGIN(Engine)

class CTimer_Manager;
class CFrame_Manager;
class _ENGINE_DLL CSystem final : public CBase
{
	_DECLARE_SINGLETON(CSystem)
private:
	explicit CSystem();
	virtual ~CSystem() = default;
public: // For.Timer
	HRESULT Add_Timer(const _tchar* pTimerTag);
	_float Get_TimeDelta(const _tchar* pTimerTag);
public: // For.Frame
	HRESULT Add_Frame(const _tchar* pFrameTag, const _float& fCallCnt);
	_bool Permit_Call(const _tchar* pFrameTag, const _float& fTimeDelta);
private:
	CTimer_Manager*			m_pTimer_Manager = nullptr;
	CFrame_Manager*			m_pFrame_Manager = nullptr;
protected:
	virtual void Free();
};

_END