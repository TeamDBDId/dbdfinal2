#pragma once

#include "Base.h"

_BEGIN(Engine)

class CFrustum;
class CQuadTree final : public CBase
{
public:
	enum NEIGHBOR {NEIGHBOR_LEFT, NEIGHBOR_TOP, NEIGHBOR_RIGHT, NEIGHBOR_BOTTOM, NEIGHBOR_END};
	enum CHILD {CHILD_LT, CHILD_RT, CHILD_RB, CHILD_LB, CHILD_END};
	enum CORNER {CORNER_LT, CORNER_RT, CORNER_RB, CORNER_LB, CORNER_END};	
private:
	explicit CQuadTree(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CQuadTree() = default;
public:
	HRESULT Ready_QuadTree(const _vec3* pVerticesPos, const _uint& iNumVerticeX, const _uint& iNumVerticeZ);
	HRESULT Ready_QuadTree(const _vec3* pVerticesPos, const _uint& iLT, const _uint& iRT, const _uint& iRB, const _uint& iLB);
	void Culling_ToQuadTree(CFrustum* pFrustum, D3DXPLANE* pLocalPlane, POLYGON32* pVertexIndex, _uint& iNumPolygons);
	_bool Check_LOD();
private:
	LPDIRECT3DDEVICE9	m_pGraphic_Device = nullptr;
private:
	CQuadTree*			m_pNeighbor[NEIGHBOR_END] = { nullptr };
	CQuadTree*			m_pChild[CHILD_END] = { nullptr };
	_uint				m_iCorner[CORNER_END] = { 0 }; // 현재 쿼드트리(노드)가 대표하는 사각형 지형의 네개 꼭지점. 
	_uint				m_iCenter = 0;
	_float				m_fRadius = 0.f;
	const _vec3*		m_pVerticesPos = nullptr;
private:
	HRESULT SetUp_ChildNode();
	HRESULT SetUp_Neighbor();
public:
	static CQuadTree* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _vec3* pVerticesPos, const _uint& iNumVerticeX, const _uint& iNumVerticeZ);	
	static CQuadTree* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _vec3* pVerticesPos, const _uint& iLT, const _uint& iRT, const _uint& iRB, const _uint& iLB);
protected:
	virtual void Free();
};

_END