#pragma once

// 내 게임내에서 사용되는 뷰스페이스 변환에 대한 처리를 수행한다.
// 투영행렬의 곱변환까지 수행한다.

// 뷰, 투영행렬을 보관하고 있을꺼야.

#include "GameObject.h"
#include "Transform.h"
#include "Frustum.h"
#include "Input_Device.h"

_BEGIN(Engine)

class _ENGINE_DLL CCamera abstract : public CGameObject
{
protected:
	explicit CCamera(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamera(const CCamera& rhs);
	virtual ~CCamera() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
public:
	HRESULT SetUp_CameraProjDesc(const CAMERADESC& CameraDesc, const PROJDESC& ProjDesc);
	void Invalidate_ViewProjMatrix();
protected:
	CInput_Device*	m_pInput_Device = nullptr;
protected:
	//_matrix			m_matWorld;

	CTransform*		m_pTransform = nullptr;
	_matrix			m_matView;
	CAMERADESC		m_CameraDesc;
protected:
	_matrix			m_matProj;
	PROJDESC		m_ProjDesc;
private:
	HRESULT SetUp_ViewProjMatrices();
public:
	virtual CGameObject* Clone_GameObject() = 0;
protected:
	virtual void Free();
	
};

_END