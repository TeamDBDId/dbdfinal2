#pragma once

// 클라이언트에서 사용할 객체들의 부모가 되는 클래스다.

// 원형패턴(프로토타입디자인패턴)을 통해서 생성된다.

// 1원형객체를 게임 초기화시에 미리 생성.

// 2실제 인게임안에서 동적으로 생성할 시, 원형객체를 복사해서 만든다.

#include "Base.h"

_BEGIN(Engine)
class CTransform;
class CComponent;
class CAction;
class _ENGINE_DLL CGameObject abstract : public CBase
{
protected:
	explicit CGameObject(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CGameObject(const CGameObject& rhs);
	virtual ~CGameObject() = default;
public:
	_float Get_CameraDistance() const {
		return m_fCameraDistance;
 }
_uint	GetUIDist() { return m_UIDist; }
public:
	CComponent* Get_ComponentPointer(const _tchar* pComponentTag);
public:
	virtual HRESULT			Ready_GameObject();
	virtual _int			Update_GameObject(const _float& fTimeDelta);
	virtual _int			LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void			Render_GameObject();
	virtual void			Render_Occluder() {}
	virtual void			Render_Stencil() {}
	virtual void			Render_Stemp() {}
	virtual void			Render_CubeMap(_matrix* View, _matrix* Proj) {}
	virtual void			Render_ShadowCubeMap(_matrix* VP, LPD3DXEFFECT pEffect, _vec4 vLightPos) {}
	virtual void			Render_ShadowMap(_matrix* VP, LPD3DXEFFECT pEffect, _vec4 vLightPos) {}
	virtual void			Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0) { return; }
	virtual _matrix			Get_Matrix() { return _matrix(); }
	virtual _int			Get_OtherOption() { return _int(); }
	virtual const _tchar*	Get_Key() { return nullptr; }

	const _uint&			Get_IFree() { return m_iFree; }
public:
	HRESULT Set_Action(const _tchar* pActionTag, const float& fTimeDuration);
	CAction* Find_Action(const _tchar* pActionTag);
	_bool Update_Actions(const _float& fTimeDelta);
	HRESULT Assert_EndAllAction(const _tchar* pActionTag = nullptr);
public:
	void					SetDead() { m_isDead = true; }
	void					SetID(_uint ID) { m_eObjectID += ID; }
	void					Set_ProgressTime(_float pProgressTime) { m_fProgressTime = pProgressTime; }
	void					Set_MapIndex(const _uint iIndex) { m_iMapIndex = iIndex; }
	void					Set_Penetration(_bool pet) { m_bPenetration = pet; }
	void					Set_IsLook(const _bool& _isLook) { m_isLook = _isLook; }
	void					Set_IStep(const _uint& _iStep) { m_iStep = _iStep; }
	void					Set_IFree(const _uint& _iFree) { m_iFree = _iFree; }
public:
	_uint					GetID() { return m_eObjectID; }
	const _uint&			Get_PosIdx() { return m_iMapIndex; }
	_uint*					Get_pPosIdx() { return &m_iMapIndex; }
	const _uint&			Get_CollState() { return m_iCollState; }
	const _bool&			Get_IsDead() { return m_isDead; }
	const _bool&			Get_IsColl() {return m_isColl;}
	const _uint&			Get_CurAnimation() { return m_iCurAnimation; }
	const _float&			Get_ProgressTime() { return m_fProgressTime; }
	const _float&			Get_MaxProgressTime() { return m_fMaxProgressTime; }
	inline void				Set_IsBool(const _bool& _isFall) { m_isBool = _isFall; }
	inline const _bool&		Get_IsBool() { return m_isBool; }
	_bool					Get_Penetration() {	return m_bPenetration; }
	_bool					Get_IsAnger() { return m_isAnger; }

public:
	virtual _int Do_Coll(CGameObject* _pObj,const _vec3& _vPos=_vec3(0.f,0.f,0.f));
	virtual _int Do_Coll_Player(CGameObject* _pObj, const _vec3& _vPos = _vec3(0.f, 0.f, 0.f));
	virtual _int Do_Dist(const _float& _fDist);
	const _vec3&			Get_InterPos(const _uint& _iDir);
protected:
	LPDIRECT3DDEVICE9		m_pGraphic_Device = nullptr;
protected:
	_bool	m_bPenetration = false;
	_uint	m_iCurAnimation = 0;
	_uint	m_iMapIndex = 0;
	_uint	m_iCollState = 0;
	_uint	m_eObjectID = 0;
	_bool	m_isDead = false;
	_float	m_fProgressTime = 0.f;
	_float	m_fMaxProgressTime = 0.f;
	_vec3*	m_vPosArr = nullptr;
	_bool	m_isColl = true;
	_bool	m_isBool = false;
	_bool	m_isCameraOut = false;
	_bool	m_isLook = false;
	_uint	m_iStep = 0;
	_bool	m_isAnger = false;
	_uint	m_iFree = 0;
	LPDIRECT3DTEXTURE9	m_pSkinTextures = nullptr;
	map<const _tchar*, CAction*>			m_mapActions;
	typedef map<const _tchar*, CAction*> MAPACTION;
protected: // 현재 객체가 가진 컴포넌트들의 탐색을 용이하게 하기 위해.
	map<const _tchar*, CComponent*>			m_mapComponent;
	typedef map<const _tchar*, CComponent*>	MAPCOMPONENT;
protected:
	_float								m_fCameraDistance = 0.0f;
	_bool								bIsDead = false;
	_uint								m_UIDist = 0;
	_float								m_fRad = 0.f;
protected:
	HRESULT Add_Component(const _tchar* pComponentTag, CComponent* pComponent);
	CComponent* Find_Component(const _tchar* pComponentTag);
	HRESULT Compute_CameraDistance(const _vec3* pWorldPos);
public:
	virtual CGameObject* Clone_GameObject() = 0;
protected:
	virtual void Free();	
};

_END