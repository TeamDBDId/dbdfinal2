#pragma once

#include "Base.h"


_BEGIN(Engine)
class CGameObject;
class CCollider;
class CTransform;
class _ENGINE_DLL CColl_Manager final : public CBase
{
	_DECLARE_SINGLETON(CColl_Manager)

public:

	enum eCollGroup {C_PLAYER,C_CAMERA,C_CIRCLE,C_AABB,C_LAY,C_WOOD,C_CHEST,C_HOOK,C_GENER,C_CLOSET,C_WINDOW,C_EXITDOOR,C_HOUSE,C_HUT,C_TERRAIN,C_UNDER,C_PLANK,C_HATCH,C_TOTEM,C_EXIT,C_WEAPON,C_GRASS,C_BUSH,C_EXITBOX,C_END	};
private:
	explicit CColl_Manager();
	virtual ~CColl_Manager() = default;

public:
	HRESULT Add_CollGroup(eCollGroup _eGroup, CGameObject* _pObj, _bool* _bool = nullptr);
	HRESULT	Erase_Obj(eCollGroup _eGroup, CGameObject* _pObj);
	HRESULT Coll(const _float& _fTime);
	
private:
	void Ready_Player(eCollGroup _eGroup);
	void Ready_Weapon(eCollGroup _eGroup);
	void Ready_Camera(eCollGroup _eGroup);
	void Coll_Player(const _float& _fTime);
	void Coll_Weapon();
	void Coll_Camera();
	void Coll_Look_Camper();
	void Coll_Look_Slasher();
private:
	void Coll_Player_Player();

	void Coll_Player_LAY_IdxCheck();
	void Coll_Player_LAY(const _uint& _iIdx);

	void Coll_Player_Circle(const _uint& _iIdx);

	void Coll_Player_Bush_IdxCheck();
	void Coll_Player_Bush(const _uint& _iIdx);
	void Coll_Player_Grass();

	void Coll_Player_HOUSEHUT_IdxCheck(const _float& _fTime);
	void Coll_Player_HOUSEHUT(const _float& _fTime,const eCollGroup& _eGroup, const _uint& _iIdx, _float* _pDist);

	void Coll_Player_Wood();
	void Coll_Player_ExitBuilding();
	void Coll_Player_ExitDoor(const _uint& _iIdx);
	
	void Coll_Player_Interation_IdxCheck();
	void Coll_Player_Interation(const _uint& _iIdx, const eCollGroup& _eCollGroup);

	void Coll_Player_ExitBox(const _uint& _iIdx);

	void Coll_Weapon_Player();

	void Coll_Weapon_Interation_IdxCheck();
	void Coll_Weapon_Interation(const _uint& _iIdx, const eCollGroup& _eCollGroup);

	void Coll_Weapon_Wood();
	void Coll_Weapon_ExitBuilding();
	void Coll_Weapon_ExitDoor(const _uint& _iIdx);

	void Coll_Weapon_LAY_IdxCheck();
	void Coll_Weapon_LAY(const _uint& _iIdx);
	void Coll_Weapon_Circle(const _uint& _iIdx);

	void Coll_Weapon_HOUSEHUT_IdxCheck();
	void Coll_Weapon_HOUSEHUT(const _uint& _iIdx,const eCollGroup& _eGroup);


	void Coll_Camera_LAY_IdxCheck();
	void Coll_Camera_LAY(const _uint& _iIdx);
	void Coll_Camera_Circle(const _uint& _iIdx);

	void Coll_Camera_Wood();
	void Coll_Camera_ExitBuilding();

	void Coll_Camera_HOUSEHUT_IdxCheck();
	void Coll_Camera_HOUSEHUT(const eCollGroup& _eGroup, const _uint& _iIdx);

	void Coll_Camera_Interation_IdxCheck();
	void Coll_Camera_Interation(const _uint& _iIdx, const eCollGroup& _eCollGroup);

	void Coll_Look_Idx(const _uint& _iIdx,const _vec3& _vDir, _float* _fDist);
	void Coll_Look_Obj(const eCollGroup& _eGroup, const _uint& _iIdx, const _vec3& _vDir, _float* _fDist);
	void Coll_Look_House_IdxCheck(const _uint& _iIdx1, const _uint & _iIdx2,const _vec3& _vDir, _float* _fDist);
	void Coll_Look_House(const eCollGroup& _eGroup, const _uint& _iIdx, const _vec3& _vDir, _float* _fDist);
	
private:
	const _uint		MAXIDX = 196;
	vector<COL*>	m_CollList[196][C_END];

	_uint*				m_pPlayerIdx = nullptr;
	CGameObject*		m_pPlayerObj = nullptr;
	CCollider*			m_pPlayerCollider = nullptr;
	CTransform*			m_pPlayerTrans = nullptr;


	CGameObject*		m_pWeaponObj = nullptr;
	CCollider*			m_pWeaponCollider = nullptr;
	CTransform*			m_pWeaponTrans = nullptr;

	CGameObject*		m_pCameraObj = nullptr;
	CCollider*			m_pCameraCollider = nullptr;

	_vec3				m_vPos = _vec3(0.f,0.f,0.f);
	_float				m_fDist = 0.f;
protected:
	virtual void Free(void);
};

_END