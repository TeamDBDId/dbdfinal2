#pragma once

#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CTexture final : public CComponent
{
public:
	enum TYPE {TYPE_GENERAL, TYPE_CUBE, TYPE_END};

private:
	explicit CTexture(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CTexture(const CTexture& rhs);
	virtual ~CTexture() = default;
public:
	HRESULT Ready_Texture(TYPE eType, const _tchar* pFilePath, const _uint& iNumTexture);
	HRESULT SetUp_OnGraphicDev(const _uint& iIndex = 0);
	HRESULT SetUp_OnShader(LPD3DXEFFECT pEffect, const char* pConstantName, const _uint& iIndex = 0);

	_vec2* Get_TexRect(const _uint& iIndex);
private:	
	vector<IDirect3DBaseTexture9*>			m_vecTexture; // 텍스쳐를 여러장 로드하여 저장한다.
	typedef vector<IDirect3DBaseTexture9*>	VECTEXTURE;
	vector<_vec2>			m_vecTextureRect;
public:
	static CTexture* Create(LPDIRECT3DDEVICE9 pGraphic_Device, TYPE eType, const _tchar* pFilePath, const _uint& iNumTexture = 1);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};

_END