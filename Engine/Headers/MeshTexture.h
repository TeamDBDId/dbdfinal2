#pragma once
#include "Base.h"

_BEGIN(Engine)

class _ENGINE_DLL CMeshTexture final : public CBase
{

	_DECLARE_SINGLETON(CMeshTexture)
private:
	explicit CMeshTexture();
	virtual ~CMeshTexture() = default;
public:
	HRESULT AddTextureEX(LPDIRECT3DDEVICE9 pGraphic_Deivce, LPCWSTR szFilePath, LPCWSTR szFileName);
	HRESULT AddTexture(LPDIRECT3DDEVICE9 pGraphic_Deivce,LPCWSTR szFilePath, LPCWSTR szFileName, LPDIRECT3DTEXTURE9* pTexture);
	LPDIRECT3DTEXTURE9 Find_Texture(wstring FileName);
	_vec2* Find_TextureSize(wstring FileName);
	_bool Find_ReplacedName(wstring* FileName);
	void Add_ReplacedName(const _tchar* Name, const _tchar* ReplacedName);
	void DeleteTexture(const _tchar* FileName);
private:
	map<wstring, LPDIRECT3DTEXTURE9>	m_mapTexture;
	map<wstring, _vec2>					m_mapTextureSize;
	map<wstring, wstring>				m_mapReplacedName;
	virtual void Free() override;

};

_END