#pragma once

// 렌더링해야할 객체들을 모아서 관리한다.(왜? 3d에서 객체들을 그리는 순서가 중요하다)
// 모을때도 그리는 순서대로 모은다.순서대로 그린다.

#include "Component.h"

_BEGIN(Engine)

class CShader;
class CTarget_Manager;
class CGameObject;
class CInstancing;
class CMesh_Static;
class _ENGINE_DLL CRenderer final : public CComponent
{
public:
	enum RENDERGROUP { RENDER_PRIORITY, RENDER_NONEALPHA, RENDER_ALPHA, RENDER_UI, RENDER_END};
private:
	explicit CRenderer(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CRenderer() = default;	
public:
	void Set_TimeDelta(_float fTime) { m_fTimeDelta = fTime; }
	HRESULT Ready_Renderer();
	HRESULT Add_RenderGroup(RENDERGROUP eGroup, CGameObject* pGameObject);
	HRESULT Add_CubeGroup(CGameObject* pGameObject);
	HRESULT Add_StemGroup(CGameObject* pGameObject);
	HRESULT Add_StencilGroup(CGameObject* pGameObject);
	HRESULT Render_RenderGroup();
	CShader* GetCubeEffectHandle() { return m_pShader_Back; }
	void	Add_Instancing(CMesh_Static* pMeshCom, const _matrix& matWorld);
	void	SetScreenPos(_vec3 scr) { m_vScrPos = scr; }
	HRESULT SetUp_OnShaderFromTarget(LPD3DXEFFECT pEffect, _tchar* TargetName, const char* pConstantName);
	LPDIRECT3DTEXTURE9	Get_Texture(_tchar* pTargetTag);
	LPDIRECT3DTEXTURE9	Get_SkinTex() { return m_pSkinTex; }
	void Set_ShapeSkill(_bool bSkill) { m_bShapeSkill = bSkill; }
	void Set_MaskingTexture(LPDIRECT3DTEXTURE9 pTex) { m_pMaskingTexture = pTex; }
	void Set_FogTexture(LPDIRECT3DTEXTURE9 pTex) { m_pFogTexture = pTex; }
	void Set_Flashed(_bool bFalshed) { m_bFlashed = bFalshed; m_iFlashedCondition = 1; }
	void Set_Brightness(_float gBri) { fgE = gBri; }
	_float Get_Brightness() { return fgE; }
	void Set_RGB(_vec3 vRGB) { g_R = vRGB.x; g_G = vRGB.y; g_B = vRGB.z; }
	_vec3 Get_RGB() { return _vec3(g_R, g_G, g_B); }
	void Set_FogColor(_float ffog) { fFogUV = ffog; }
	//void Set_LutTexture(LPDIRECT3DVOLUMETEXTURE9 pTex) { m_pLutTex = pTex; }
private:
	CTarget_Manager*			m_pTarget_Manager = nullptr;
	CShader*					m_pShader_LightAcc = nullptr;
	CShader*					m_pShader_Blend = nullptr;
	CShader*					m_pShader_Back = nullptr;
private:
	list<CGameObject*>			m_RenderList[RENDER_END];
	typedef list<CGameObject*>	OBJECTLIST;
private:	// Cube
	_float						m_fTimeDelta = 0.f;
	list<CGameObject*>			m_StemmapList = list<CGameObject*>();
	list<CGameObject*>			m_CubemapList = list<CGameObject*>();
	list<CGameObject*>			m_StencilList = list<CGameObject*>();
	LPDIRECT3DSURFACE9			pSrf = nullptr;
	_int						m_iCount = 0;
	LPD3DXRenderToEnvMap		m_pRndEnv = nullptr;
	LPDIRECT3DCUBETEXTURE9		m_pTxCbm = nullptr;
	LPDIRECT3DTEXTURE9			m_pMaskingTexture = nullptr;
	LPDIRECT3DTEXTURE9			m_pFogTexture = nullptr;
	LPDIRECT3DTEXTURE9			m_pSkinTex = nullptr;
	LPDIRECT3DTEXTURE9			m_pRandomTexture = nullptr;
	LPDIRECT3DVOLUMETEXTURE9	m_pLutTex = nullptr;
	//LPDIRECT3DTEXTURE9	m_pLutTex = nullptr;
	_float						fFogUV = 0.17f;
	_float						fgW = 11.5f;
	_float						fgE = 45.f;

	_float						decay = 0.98f;
	_float						density = 0.96f;
	_float						exposour = 0.013f;
	_float						weight = 0.56f;

	_float						g_R = 0.5f;
	_float						g_G = 0.5f;
	_float						g_B = 0.5f;
	_bool						m_bSharp = false;

	_bool						m_bFlashed = false;
	_uint						m_iFlashedCondition = 0;
	_float						m_fFlashedTime = 0.f;
	_float						m_fFlashedCoolTime = 0.f;

	_bool						m_bssao = true;
	_vec4						m_ssaop = _vec4(1,1,1,1);
private:	// MotionBlur
	_matrix						m_matPreView = _matrix();
	CInstancing*				m_pInstancing = nullptr;
	_vec3						m_vScrPos;
	_bool						m_bShapeSkill = false;
private:
	LPDIRECT3DVERTEXBUFFER9		m_pVB = nullptr;
	LPDIRECT3DINDEXBUFFER9		m_pIB = nullptr;
private:
	void Render_Priority();
	void Render_NoneAlpha();
	void Render_Alpha();
	void Render_UI();
private:
	void Render_LightStem();
	void Render_Deferred();
	void Render_LightAcc();
	void Render_Blend();
private:
	void T_SetupCubeViewMatrix(_matrix* pmtViw, DWORD dwFace);
	void Render_CubeMap(_matrix* View, _matrix* Proj);
	void Render_EnvMap();
	void Render_Stencil();
	void Flashed();
public: 
	static CRenderer* Create(LPDIRECT3DDEVICE9 pGraphic_Device); // 원형객체 생성을 위한.
	virtual CComponent* Clone_Component(void* pArg = nullptr); // 복제될 객체를 생성하기 위한.
protected:
	virtual void Free();
};

_END