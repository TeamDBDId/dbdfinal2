#pragma once

// 컴포넌트로서 사용될 클래스들의 부모클래스다.

#include "Base.h"

_BEGIN(Engine)

class _ENGINE_DLL CComponent abstract : public CBase
{
protected:
	explicit CComponent(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CComponent(const CComponent& rhs);
	virtual ~CComponent() = default;
public:
	HRESULT Ready_Component();
protected:
	LPDIRECT3DDEVICE9			m_pGraphic_Device = nullptr;
	_bool						m_isClone = false;
public:
	virtual CComponent* Clone_Component(void* pArg = nullptr) = 0;
protected:
	virtual void Free();
};

_END