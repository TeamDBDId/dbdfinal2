#pragma once

#pragma warning (disable : 4251)

#pragma comment(lib, "ws2_32")
#define _WINSOCK_DEPRECATED_NO_WARNINGS
//#define _CRT_SECURE_NO_WARNINGS

#include <winsock2.h>

#include "d3d9.h"
#include "d3dx9.h"

#define DIRECTINPUT_VERSION 0x0800
#include "dinput.h"

#include <list>
#include <queue>
#include <vector>
#include <map>
#include <algorithm>
#include <time.h> 
#include <iostream>
#include <process.h>

// For.CRTDBG
//#ifdef _DEBUG
//#ifndef _TOOL
//#define _CRTDBG_MAP_ALLOC 
//#include <stdlib.h> 
//#include <crtdbg.h> 
//
//#ifndef DBG_NEW 
//#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ ) 
//#define malloc(s) _malloc_dbg(s,_NORMAL_BLOCK , __FILE__ , __LINE__ ) 
//#define new DBG_NEW 
//#endif 
//#endif
//#endif  // _DEBUG  

#include "Engine_Macro.h"
#include "Engine_Function.h"
#include "Engine_Typedef.h"
#include "Engine_functor.h"
#include "Engine_Struct.h"
#include "Engine_Extern.h"

using namespace std;
using namespace Engine;

