
vector			g_vLightDir;

vector			g_vLightPos;
float			g_fRange;

vector			g_vLightDiffuse;
vector			g_vLightAmbient;
vector			g_vLightSpecular;

vector			g_vMtrlDiffuse = vector(1.f, 1.f, 1.f, 1.f);
vector			g_vMtrlSpecular = vector(1.f, 1.f, 1.f, 1.f);
vector			g_vMtrlAmbient = vector(0.4f, 0.4f, 0.4f, 1.f);
float			g_fPower = 30.f;

vector			g_vCamPosition;

matrix			g_matProjInv;
matrix			g_matViewInv;
matrix			g_matPreViewProj;

texture			g_NormalTexture;
float			g_fLightCutOff;
float			g_fLightOutCutOff;
bool			g_UseShadow = false;
bool			g_UseDynamicShadow = false;

sampler	NormalSampler = sampler_state
{
	texture = g_NormalTexture;
};

texture			g_DepthTexture;

sampler	DepthSampler = sampler_state
{
	texture = g_DepthTexture;
};

texture			g_RMAOTexture;

sampler	RMAOSampler = sampler_state
{
	texture = g_RMAOTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_TexCube;

sampler CubeSampler = sampler_state
{
	texture = g_TexCube;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture g_ShadowCubeTex;

sampler ShadowCubeSampler = sampler_state
{
	texture = g_ShadowCubeTex;

	MipFilter = NONE;
	MinFilter = NONE;
	MagFilter = NONE;
	AddressU = wrap;
	AddressV = wrap;
};

texture g_ShadowTex;

sampler ShadowSampler = sampler_state
{
	texture = g_ShadowTex;

	MipFilter = NONE;
	MinFilter = NONE;
	MagFilter = NONE;
	AddressU = wrap;
	AddressV = wrap;
};

texture g_StemTexture;
sampler StemSampler = sampler_state
{
	texture = g_StemTexture;

	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct PS_IN
{
	vector		vPosition : POSITION;
	float2		vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector		vShade : COLOR0;
	vector		vSpecular : COLOR1;
	vector		vRef : COLOR2;
	vector		vEtc : COLOR3;
};

bool IsShadowing(float3 WorldPos)
{
	float3 LightDir = (g_vLightPos.xyz - WorldPos);
	float Dist = length(LightDir);
	LightDir = LightDir.xyz / Dist;

	float ShadowDepth = texCUBE(ShadowCubeSampler, float4(-(LightDir.xyz), 0.0f)).x;
	if (ShadowDepth < 15.f)
		return false;
	//float ShadowDepth2 = 0.000f;
	//float FinalDepth = 0.f;
	//if (g_UseDynamicShadow)
	//{
	//	ShadowDepth2 = texCUBE(ShadowSampler, float4(-(LightDir.xyz), 0.0f)).x;

	//	if (ShadowDepth2 >= 1.f)
	//	{
	//		if(ShadowDepth < ShadowDepth2)
	//			FinalDepth = ShadowDepth;
	//		else
	//			FinalDepth = ShadowDepth2;
	//	}
	//	else
	//		FinalDepth = ShadowDepth;
	//}
	//else
	//{
	//	FinalDepth = ShadowDepth;
	//}

	if (Dist > ShadowDepth)
		return true;
	else
		return false;
}

float3 SrgbToLinear(float3 srgb)
{
	float3 lin;
	lin = max(6.10352e-5, srgb);
	return lin > 0.04045 ? pow(lin * (1.0 / 1.055) + 0.0521327, 2.4) : lin * (1.0 / 12.92);
}

float OrenNayarLighting(float3 N, float3 L, float3 V, float roughness)
{
	float LdotN = dot(L, N);
	float VdotN = dot(V, N);
	float result = saturate(LdotN);
	float soft_rim = saturate(1 - VdotN / 2);
	float fakey = pow(1 - result*soft_rim, 2);
	float fakey_magic = 0.62;

	fakey = fakey_magic - fakey*fakey_magic;
	float Rt = lerp(result, fakey, roughness);
	if (Rt < 0.15f)
		Rt = 0.15f;

	return Rt;
}

float SpecularBRDF(float3 N, float3 L, float3 V, float3 H, float roughness, float metalic)
{
	float NV = dot(N, V);
	float NH = dot(N, H);
	float VH = dot(V, H);
	float NL = dot(N, L);
	float LH = dot(L, H);

	// Beckmann분포함수
	const float m = roughness; // 거친정도
	float NH2 = NH*NH;
	float D = exp(-(1 - NH2) / (NH2*m*m)) / (4 * m*m*NH2*NH2);

	// 기하감쇠율
	float G = min(1, min(2 * NH*NV / VH, 2 * NH*NL / VH));

	// 프리넬
	float3 refindex = { 0.85f, 1.0f, 1.0f };
	float F = lerp(pow(1 - NV, 5), 1, refindex);
	//vector ks;
	// 금속의 색
	//if (metalic > 0.01f)
	vector	ks = vector(2.0f*0.486f * metalic, 2.0f*0.433f * metalic, 2.0f*0.185f * metalic, 1.0f);

	return ks * max(0, F*D*G / NV);
}

PS_OUT PS_MAIN_DIRECTIONAL(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector		vNormalInfo = tex2D(NormalSampler, In.vTexUV);
	vector		vDepthInfo = tex2D(DepthSampler, In.vTexUV);
	vector		vRMAOInfo = tex2D(RMAOSampler, In.vTexUV);

	vector		vNormal = vector(vNormalInfo.xyz * 2.f - 1.f, 0.f);

	vector		vLook;

	vector		vPosition; 
	vector		vVPosition;
	vector		vVNormal;

	vPosition.x = In.vTexUV.x * 2.f - 1.f;
	vPosition.y = In.vTexUV.y * -2.f + 1.f;
	vPosition.z = vDepthInfo.r;
	vPosition.w = 1.f;

	if (vDepthInfo.g > 2.f)
		vDepthInfo.g = 1.f;

	Out.vSpecular.x = vDepthInfo.g;

	vector vCurViewPortPos = vPosition;

	// 투영행렬까지 곱해놓은 상태의 위치를 구하자.(* 투영행렬)
	float		fViewZ = vDepthInfo.g * 30000.0f;

	vPosition = vPosition * fViewZ;

	// 뷰스페이스 영역상의 위치를 구한다.
	vPosition = mul(vPosition, g_matProjInv);
	vVPosition = vPosition;

	vVNormal = vNormal * fViewZ;
	vVNormal = normalize(mul(vVNormal, g_matProjInv));
	// 월드 스페이스 영역상의 위치를 구한다.
	vPosition = mul(vPosition, g_matViewInv);

	vLook = vPosition - g_vCamPosition;

	////Fog
	//float fFog = pow(saturate((10000.f - length(vLook)) / (10000.f - 5.f)), 6.2f);

	float fFog = 1.f / exp(pow(length(vLook) * 0.0005f, 2));

	if (vPosition.y > 400.f)
		fFog = saturate(fFog + (vPosition.y) / 26000.f);

	if (vDepthInfo.w > 1.f)
	{
		fFog = 1.f;
	}
		
	//// ++이전 위치
	vector vPreViewPortPos = mul(vPosition, g_matPreViewProj);
	vPreViewPortPos /= vPreViewPortPos.w;

	float2 Speed = -(vCurViewPortPos - vPreViewPortPos) * 0.04f;

	if (dot(Speed.xy, Speed.xy) < 0.0001f)
		Speed = float2(0.f, 0.f);

	Out.vSpecular.y = fFog;
	Out.vSpecular.z = Speed.x;
	Out.vSpecular.w = Speed.y;

	float3 L = -g_vLightDir.xyz;				// 광원벡터
	float3 N = normalize(vNormal);				// 법선벡터
	float3 V = -normalize(vLook);			// 시선벡터
	float3 H = normalize(L + V);			// 하프벡터

	float3 E = -normalize(vVPosition);
	float3 R = 2.0 * dot(E, vVNormal) * vVNormal - E;

	vector		vTexCube = texCUBE(CubeSampler, R);

	vTexCube.xyz = max(6.10352e-5, vTexCube.xyz);
	vTexCube.xyz = vTexCube.xyz > 0.04045 ? pow(vTexCube.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTexCube.xyz * (1.0 / 12.92);
	vTexCube.a = 1;

	vector vMtlColor = g_vLightDiffuse * g_vMtrlDiffuse;

	if (vRMAOInfo.g < 0.000001f)
	{
		vTexCube = g_vLightAmbient;
	}

	Out.vShade = OrenNayarLighting(N, L, V, vRMAOInfo.r) * vMtlColor;
	if (Out.vShade.r < g_vLightAmbient.r && Out.vShade.g < g_vLightAmbient.g && Out.vShade.b < g_vLightAmbient.b)
		Out.vShade = g_vLightAmbient * 0.75f;
	Out.vRef = SpecularBRDF(N, L, V, H, vRMAOInfo.r, vRMAOInfo.g) * vTexCube;
	Out.vShade.w = 1.f;

	Out.vEtc.xyz = vPosition.xyz;
	Out.vEtc = 1.f;

	return Out;
}

PS_OUT PS_MAIN_POINT(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector		vNormalInfo = tex2D(NormalSampler, In.vTexUV);
	vector		vDepthInfo = tex2D(DepthSampler, In.vTexUV);
	vector		vRMAOInfo = tex2D(RMAOSampler, In.vTexUV);

	vector		vNormal = vector(vNormalInfo.xyz * 2.f - 1.f, 0.f);

	vector		vLook;

	vector		vPosition;
	vector		vVPosition;
	vector		vVNormal;
	if (vDepthInfo.g > 4.f)
		return Out;

	if (vDepthInfo.w > 1.f)
	{
		Out.vShade = vector(0.3f, 0.3f, 0.3f, 1);
		Out.vRef = vector(0, 0, 0, 0);
		return Out;
	}

	// 투영영역안에 있는 픽셀의 위치. ( /z)
	vPosition.x = In.vTexUV.x * 2.f - 1.f;
	vPosition.y = In.vTexUV.y * -2.f + 1.f;
	vPosition.z = vDepthInfo.r;
	vPosition.w = 1.f;


	Out.vSpecular.x = vDepthInfo.g;

	vector vCurViewPortPos = vPosition;
	// 투영행렬까지 곱해놓은 상태의 위치를 구하자.(* 투영행렬)
	float		fViewZ = vDepthInfo.g * 30000.0f;

	vPosition = vPosition * fViewZ;
	vVNormal = vNormal * fViewZ;
	// 뷰스페이스 영역상의 위치를 구한다.
	vPosition = mul(vPosition, g_matProjInv);
	vVPosition = vPosition;


	//if (vVPosition.z > 8000.f)
	//{
	//	Out.vShade = vector(0, 0, 0, 0);
	//	Out.vRef = vector(0, 0, 0, 0.0f);
	//	return Out;
	//}


	//vector vVPos = vPosition;
	vVNormal = normalize(mul(vVNormal, g_matProjInv));
	// 월드 스페이스 영역상의 위치를 구한다.
	vPosition = mul(vPosition, g_matViewInv);

	vector		vLightDir = vPosition - g_vLightPos;

	float		fAtt = max((g_fRange - length(vLightDir)), 0.f) / g_fRange;
	fAtt *= fAtt;

	if (fAtt < 0.00001f)
	{
		Out.vShade = vector(0, 0, 0, 0);
		Out.vRef = vector(0, 0, 0, 0);
		return Out;
	}

	vLook = vPosition - g_vCamPosition;

	float3 L = -normalize(vPosition - g_vLightPos);				// 광원벡터
	float3 N = normalize(vNormal);				// 법선벡터
	float3 V = -normalize(vLook);			// 시선벡터
	float3 H = normalize(L + V);			// 하프벡터

	float3 E = -normalize(vVPosition);
	float3 R = 2.0 * dot(E, vVNormal) * vVNormal - E;

	vector		vTexCube = texCUBE(CubeSampler, R);

	vTexCube.xyz = max(6.10352e-5, vTexCube.xyz);
	vTexCube.xyz = vTexCube.xyz > 0.04045 ? pow(vTexCube.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTexCube.xyz * (1.0 / 12.92);
	vTexCube.a = 1;

	vector vMtlColor = g_vLightDiffuse * g_vMtrlDiffuse;

	if (vRMAOInfo.g < 0.000001)
	{
		vTexCube = g_vLightAmbient;
	}

	//if(length(vPosition.xyz - g_vLightPos.xyz) > g_fRange)
	//{
	//	Out.vShade = vector(0, 0, 0, 1);
	//	Out.vRef = vector(0, 0, 0, 0);
	//	return Out;
	//}



	if (g_UseShadow)
	{
		bool bShadow = IsShadowing(vPosition.xyz);

		if (!bShadow)
		{
			//Out.vShade = (max(dot(normalize(vLightDir) * -1.f, vNormal), 0.f) + (g_vLightAmbient * g_vMtrlAmbient)) * (g_vLightDiffuse * g_vMtrlDiffuse) * fAtt;	
			///Out.vShade.a = 1.f;

			Out.vShade = OrenNayarLighting(N, L, V, vRMAOInfo.r) * vMtlColor * fAtt;
			Out.vRef = SpecularBRDF(N, L, V, H, vRMAOInfo.r, vRMAOInfo.g) * vTexCube * fAtt;
			//if (fAtt > 0.0f && Out.vShade.r < g_vLightAmbient.r && Out.vShade.g < g_vLightAmbient.g && Out.vShade.b < g_vLightAmbient.b)
			//	Out.vShade = g_vLightAmbient;
		}
		else
		{
			Out.vShade = vector(0, 0, 0, 0);
			Out.vShade.a = 1.f;
		}
	}
	else
	{
		Out.vShade = OrenNayarLighting(N, L, V, vRMAOInfo.r) * vMtlColor * fAtt;
		Out.vRef = SpecularBRDF(N, L, V, H, vRMAOInfo.r, vRMAOInfo.g) * vTexCube * fAtt;

	}

	Out.vEtc = vector(0, 0, 0, 0);

	return Out;
}

PS_OUT PS_MAIN_SPOT(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector		vNormalInfo = tex2D(NormalSampler, In.vTexUV);
	vector		vDepthInfo = tex2D(DepthSampler, In.vTexUV);
	vector		vRMAOInfo = tex2D(RMAOSampler, In.vTexUV);

	vector		vNormal = vector(vNormalInfo.xyz * 2.f - 1.f, 0.f);

	vector		vLook;

	vector		vPosition;
	vector		vVPosition;
	vector		vVNormal;

	if (vDepthInfo.g > 4.f)
		return Out;

	if (vDepthInfo.w > 1.f)
	{
		Out.vShade = vector(0.3f, 0.3f, 0.3f, 1);
		Out.vRef = vector(0, 0, 0, 0);
		return Out;
	}

	vPosition.x = In.vTexUV.x * 2.f - 1.f;
	vPosition.y = In.vTexUV.y * -2.f + 1.f;
	vPosition.z = vDepthInfo.r;
	vPosition.w = 1.f;

	Out.vSpecular.x = vDepthInfo.g;	Out.vSpecular.x = vDepthInfo.g;

	vector vCurViewPortPos = vPosition;
	float		fViewZ = vDepthInfo.g * 30000.0f;

	vPosition = vPosition * fViewZ;

	vPosition = mul(vPosition, g_matProjInv);

	vPosition = mul(vPosition, g_matViewInv);

	//if (length(vPosition.xyz - g_vLightPos.xyz) > g_fRange)
	//{
	//	Out.vShade = vector(0, 0, 0, 0);
	//	Out.vRef = vector(0, 0, 0, 0);
	//	return Out;
	//}
	vector		vLightDir = vPosition - g_vLightPos;
	vLook = vPosition - g_vCamPosition;

	float theta = dot(normalize(-vLightDir), normalize(-g_vLightDir));
	float epsilon = g_fLightCutOff - g_fLightOutCutOff;
	float intensity = 1.f - clamp((theta - g_fLightOutCutOff) / epsilon, 0.0, 1.0);

	if (theta < g_fLightCutOff)
	{
		Out.vShade = vector(0, 0, 0, 0);
		Out.vRef = vector(0, 0, 0, 0);
		return Out;
	}

	float3 L = -normalize(vPosition - g_vLightPos);				// 광원벡터
	float3 N = normalize(vNormal);				// 법선벡터
	float3 V = -normalize(vLook);			// 시선벡터
	float3 H = normalize(L + V);			// 하프벡터

	float3 E = -normalize(vVPosition);
	float3 R = 2.0 * dot(E, vVNormal) * vVNormal - E;

	vector		vTexCube = texCUBE(CubeSampler, R);

	vTexCube.xyz = max(6.10352e-5, vTexCube.xyz);
	vTexCube.xyz = vTexCube.xyz > 0.04045 ? pow(vTexCube.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vTexCube.xyz * (1.0 / 12.92);
	vTexCube.a = 1;

	vector vMtlColor = g_vLightDiffuse * g_vMtrlDiffuse;

	if (vRMAOInfo.g < 0.000001)
	{
		vTexCube = g_vLightAmbient;
	}



	vector		vReflect;
	//vector		vLook;

	vReflect = reflect(normalize(vLightDir), normalize(vNormal));

	if (theta > g_fLightCutOff)
	{
		if (g_UseShadow)
		{
			bool bShadow = IsShadowing(vPosition.xyz);

			if (!bShadow)
			{
				float		fAtt = max((g_fRange - length(vLightDir)), 0.f) / g_fRange;
				fAtt *= fAtt;

				Out.vShade = OrenNayarLighting(N, L, V, vRMAOInfo.r) * vMtlColor * fAtt * intensity;
				Out.vRef = SpecularBRDF(N, L, V, H, vRMAOInfo.r, vRMAOInfo.g)  * fAtt * intensity;
				//if (Out.vShade.r < g_vLightAmbient.r && Out.vShade.g < g_vLightAmbient.g && Out.vShade.b < g_vLightAmbient.b)
				//	Out.vShade = g_vLightAmbient;
			}
			else
			{
				Out.vShade = vector(0, 0, 0, 0);
				Out.vRef = vector(0, 0, 0, 0);
			}
		}
		//vector vStem = tex2D(StemSampler, In.vTexUV);
		//if ((vDepthInfo.r - vStem.r) > 0.005f)
		//{
		//	Out.vShade = g_vLightDiffuse * 0.2f;
		//	Out.vShade.a *= vStem.b * 0.1f;
		//}
	}
	else
	{
		Out.vShade = vector(0, 0, 0, 0);
		Out.vRef = vector(0, 0, 0, 0);
	}

	Out.vEtc = vector(0, 0, 0, 0);

	//Out.vLightShaft = vector(0, 0, 0, 0);


	return Out;
}

technique	DefaultDevice
{
	pass Render_Directional
	{
		AlphaBlendEnable = true;
		SrcBlend = one;
		DestBlend = one;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN_DIRECTIONAL();
	}

	pass Render_Point
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = one;
		DestBlend = one;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN_POINT();
	}

	pass Render_Spot
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = one;
		DestBlend = one;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN_SPOT();
	}
}